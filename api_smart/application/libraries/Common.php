<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Common {

		function delete_file($file_path) {
			if (is_file($file_path)) {
				return unlink($file_path);
			} else {
				return false;
			}
		}

	}
