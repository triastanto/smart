<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	include (APPPATH . 'libraries/httpful.phar');
	class RestCli {
		public function call($options = array()) {
			$response = null;
			
			if($options['method'] == 'get') {
				$response = \Httpful\Request::get($options['url'])
					->addHeader('X-API-KEY', apikey)
					->expectsJson()
					->send();
			}
			else if ($options['method'] == 'post') {
				$response = \Httpful\Request::post($options['url'])
					->addHeader('X-API-KEY', apikey)
					->expectsJson()
					->sendsJson()
					->body(json_encode($options['body']))
					->send();
			}
			else if ($options['method'] == 'put' ) {
				$response = \Httpful\Request::put($options['url'])
					->addHeader('X-API-KEY', apikey)
					->expectsJson()
					->sendsJson()
					->body(json_encode($options['body']))
					->send();
			}
			else if($options['method'] == 'delete') {
				$response = \Httpful\Request::delete($options['url'])
					->addHeader('X-API-KEY', apikey)
					->expectsJson()
					->sendsJson()
					->body(json_encode($options['body']))
					->send();
			}

			return $response;
		}
		
		public function call_x($options = array()) {
			$response = null;
			
			if($options['method'] == 'get') {
				$response = \Httpful\Request::get($options['url'])
					->expectsJson()
					->send();
			}
			else if ($options['method'] == 'post') {
				$response = \Httpful\Request::post($options['url'])
					->expectsJson()
					->sendsJson()
					->body(json_encode($options['body']))
					->send();
			}
			else if ($options['method'] == 'put' ) {
				$response = \Httpful\Request::put($options['url'])
					->expectsJson()
					->sendsJson()
					->body(json_encode($options['body']))
					->send();
			}
			else if($options['method'] == 'delete') {
				$response = \Httpful\Request::delete($options['url'])
					->expectsJson()
					->sendsJson()
					->body(json_encode($options['body']))
					->send();
			}

			return $response;
		}

	}
