<?php 
//die();
defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
class Mobile extends CI_Controller {
    public $models = array('mobile');
	public $link = LINK;
    function __construct()
    {
        parent::__construct();
        $this->load->model('model_mobile');
        //$this->load->library('auth_ldap');
    }
 
  public function index(){
    $this->load->view('welcome_message');
  }
  public function get_versi(){
    $hsl="1.0.1";
    $this->output->set_content_type('application/json')->set_output(json_encode($hsl));
  }
  public function cek_koneksi(){
    $hsl="1";
    $this->output->set_content_type('application/json')->set_output(json_encode($hsl));
  }
  public function cari_karyawan(){
    $db2=$this->load->database('db2',true);
    $sql="select * from structdisp where no ='1'";
    $query = $db2->query($sql);
    $hsl='';
    $hsl.='<table data-role="table" class="ui-body-d ui-shadow table-stripe ui-responsive ui-table ui-table-columntoggle">';
    $hsl.='<thead><tr class="active"><th>No</th><th>Nik</th><th>Nama</th><th>Abreviasi</th><th>Kode CC</th><th>Cost Center</th></tr></thead>';
        
    if($query->num_rows()>0){
        foreach($query->result() as $row){
            $hsl.=$row->empname;
        }
        
    }
    $hsl.='</table>';
    $hsl.='<br><a href="javascript:;" class="btn btn-danger btn-block" onclick="close_cari_karyawan();"><i class="fa fa-times pull-right"></i> Close</a>';
    
    $this->output->set_content_type('application/json')->set_output(json_encode($hsl));
  }
 public function get_pedoman_kearsipan($grup){
    $sql="SELECT * FROM m_home WHERE grup='$grup' order by tgl_input desc";
    $query = $this->db->query($sql);
    $no=1;
    $hsl='';
    //$hsl.='Pedoman Kearsipan';
    if($query->num_rows()>0){
        $hsl.='<table data-role="table" class="ui-body-d ui-shadow table-stripe ui-responsive ui-table ui-table-columntoggle">';
        $hsl.='<thead><tr class="active"><th>No</th><th>Tanggal Input</th><th>Keterangan</th></tr></thead>';
        foreach($query->result() as $row){
            $hsl.='<tbody><tr><td>'.$no.'</td><td>'.date('d/m/Y',strtotime($row->tgl_input)).'</td><td><a href="javascript:;" onclick="view_pedoman(`'.$row->filename.'`);">'.$row->keterangan.'</a></td></tr></tbody>';
            $no++;
            
        }
        $hsl.='</table>';
    }
    $hsl.='<br><a href="javascript:;" class="btn btn-danger btn-block" onclick="close_pedoman();"><i class="fa fa-times pull-right"></i> Close</a>';
    
    $this->output->set_content_type('application/json')->set_output(json_encode($hsl));
 }
 public function get_disposisi($kode){
	$url =$this->link;
    $dtm = $this->model_mobile->get_dtm($kode)->row_array();
    $kodesuratutama =$dtm['kodesuratutama'];
    
    $rsql ="SELECT * FROM t_surat_deployment where kodesuratutama='".$kodesuratutama."'";
    $query = $this->db->query($rsql);
    $hsl='';
    if($query->num_rows()>0){
        $no =0;
        $hsl.='<h2>View Disposisi</h2><table data-role="table" class="ui-body-d ui-shadow table-stripe ui-responsive ui-table ui-table-columntoggle">';
        $hsl.='<thead><tr class="active"><th>No</th><th>Penerima Surat / Disposisi Oleh</th><th>Tanggal Disposisi</th><th>Catatan</th><th>Progress</th><th>Pesan</th><th>Download</th></tr></thead>';
        foreach($query->result() as $row){
            $no++;
            $ms =$this->model_mobile->get_ms($row->kodesuratbawah)->row_array();
            //$hsl.=$ms['kodeprogressurat'];
            $ps = $this->model_mobile->get_ps($ms['kodeprogressurat'])->row_array();
            $psn=$this->model_mobile->get_psn($ms['kodepesan'])->row_array();
            $pr =$this->model_mobile->get_pr($row->kodesuratbawah)->row_array();
            if($pr['size']=='' or $pr['size']==0){
				$dw='-';
			}else{
				$dw='<a href="'.$url.'/modules/surat/suratmasuk/download.php?idp='.base64_encode($row->kodesuratbawah).'" target="_Blank"><img src="http://sso.krakatausteel.com/smart/images/download.png" border="0" width="20" height="20" title="Download File Lampiran"></a>';
			}
            if($ms['kodesifatsurat']!=2){
                $hsl.='<tbody><tr><td>'.$no.'</td><td>'.$ms['empniktujuan'].', '.$ms['empnametujuan'].'</td><td>'.date('d/m/Y H:i:s',strtotime($row->tanggaldeployment)).'</td><td>'.$ms['catatan'].'</td><td>'.$ps['progressurat'].'</td><td>'.$pr['keterangan'].'</td><td>'.$dw.'</td></tr></tbody>';
                
                }
        }
    }
    
    
    
    $hsl.='</table>';
    $hsl.='<br><a href="javascript:;" class="btn btn-danger btn-block" onclick="close_disposisi();"><i class="fa fa-times pull-right"></i> Close</a>';
    
    $this->output->set_content_type('application/json')->set_output(json_encode($hsl));
 }
 public function get_disposisi_by($kode,$user){
    $ms = $this->model_mobile->get_ms($kode)->row_array();
    $tanggalpenyelesaian =$ms['tanggalpenyelesaian'];
    $catatan =$ms['catatan'];
    $kodepesan =$ms['catatanatasan'];
    $pesan =$ms['pesan'];
    
    $rsql ="SELECT
	a.*,b.catatanatasan,c.pesan
    FROM
    	t_suratmasuk_d a
    left JOIN t_suratmasuk_m b ON b.kodesuratmasuk=a.kodesuratmasuk
    left JOIN m_pesan c on c.kodepesan=b.catatanatasan
    WHERE
    	kodesuratmasukatas ='".$kode."'";
    $query = $this->db->query($rsql);
    $hsl='';
    $no =1;
        $hsl.='<form id="form_disposisi" class="form-horizontal" data-parsley-validate="true" cellspacing="0" width="100%" enctype="multipart/form-data">';
        $hsl.='<label for="tanggalpenyelesaian">Target Penyelesaian :</label><input value='.$tanggalpenyelesaian.' style="width:100%;" type="date" name="tanggalpenyelesaian" id="tanggalpenyelesaian" placeholder="Tanggal Penyelesaian">';
        $hsl.='<label for="catatan">Catatan :</label><textarea style="width:100%;" name="catatan" id="catatan">'.$catatan.'</textarea>';
        /*$sqlpesan = "select * from m_pesan";
        $querypesan=$this->db->query($sqlpesan);
        $hsl.='<label for="pesan">Pesan :</label><select class="form-control selectpicker" data-size="10" data-live-search="true" data-style="btn-white" name="pesan" id="pesan" data-type="pesan">';
        $hsl.='<option value='.$kodepesan.'>'.$pesan.'</option>';
        foreach($querypesan->result() as $rowpesan){$hsl.='<option value='.$rowpesan->kodepesan.'>'.$rowpesan->pesan.'</option>';}
        $hsl.='</select>';*/
        //$hsl.='<label for="file">Lampiran :</label><input type="file" class="form-control" name="file" id="file" />';
        $hsl.='</form>';
        $hsl.='<a href="javascript:;" class="btn btn-primary btn-block" onclick="cari_karyawan();"><i class="fa fa-search pull-right"></i> Cari</a>';
        $hsl.='<form id="form_dis_pesan"><table id="tbldispos"  class="table table-striped">';
        $hsl.='<thead><tr class="active"><th>Nik</th><th>Nama</th><th>Pesan</th><th>Hapus</th></tr></thead><tbody>';
    if($query->num_rows()>0){
        
        foreach($query->result() as $row){
            
            if($row->kodesifatsurat==3){
                $sifatsurat='Struktural (Bisa Disposisi)';
            }else{
                $sifatsurat='Functional (Tidak Bisa Disposisi)';
            }
            $hsl.='<tr><td>'.$row->empnikdisposisi.'<input type="hidden" name="kodesurat[]" value="'.$row->kodesuratmasuk.'" id ="'.$row->kodesuratmasuk.'" class="form-control" readonly ><input type="hidden" name="nikdispos[]" value="'.$row->empnikdisposisi.'" id ="'.$row->empnikdisposisi.'" class="form-control" readonly ></td><td>'.$row->empnamedisposisi.'<input type="hidden" value="'.$row->empnamedisposisi.'" class="form-control" readonly ></td><td>'.$this->pesan_combo($row->catatanatasan,$row->pesan).'</td><td><a href="javascript:;" class="btn btn-warning btn-block" onclick="hapus_disposisi(\'' .$row->kodesuratmasuk. '\',\'' .$row->empnikdisposisi. '\',\'' .$kode. '\');"><i class="fa fa-trash"></i></a></td></tr>';
            
            $no++;    
        }
    }
    
    
    
    $hsl.='</tbody></table></form>';
    $hsl.='<form id="form_list_dispos" class="form-horizontal" data-parsley-validate="true" cellspacing="0" width="100%"><table id="tempdisposisi"  class="table table-striped"><tbody></tbody></table></form>';
    $hsl.='<br><a href="javascript:;" class="btn btn-success btn-block" onclick="simpan_disposisi(\'' .$kode. '\');"><i class="fa fa-check pull-right"></i> Simpan</a>';
    $hsl.='<br><a href="javascript:;" class="btn btn-warning btn-block" onclick="close_disposisi();"><i class="fa fa-minus pull-right"></i> Batal</a>';
    
    $this->output->set_content_type('application/json')->set_output(json_encode($hsl));
 }
 public function pesan_combo($kodepesan,$pesan){
        $hsl='';
        $sqlpesan = "select * from m_pesan";
        $querypesan=$this->db->query($sqlpesan);
        $hsl.='<select class="form-control selectpicker" data-size="10" data-live-search="true" data-style="btn-white" name="pesan[]" id="pesan[]" data-type="pesan">';
        $hsl.='<option value='.$kodepesan.'>'.$pesan.'</option>';
        foreach($querypesan->result() as $rowpesan){$hsl.='<option value='.$rowpesan->kodepesan.'>'.$rowpesan->pesan.'</option>';}
        $hsl.='</select>';
        
        return $hsl;
 }
 public function pesan_combo2(){
        $hsl='';
        $sqlpesan = "select * from m_pesan";
        $querypesan=$this->db->query($sqlpesan);
        $hsl.='<select class="form-control selectpicker" data-size="10" data-live-search="true" data-style="btn-white" name="pesantemp[]"  data-type="pesantemp">';
        //$hsl.='<option value=></option>';
        foreach($querypesan->result() as $rowpesan){$hsl.='<option value='.$rowpesan->kodepesan.'>'.$rowpesan->pesan.'</option>';}
        $hsl.='</select>';
        
        $this->output->set_content_type('application/json')->set_output(json_encode($hsl));
 }
 public function get_listpemaraf($kode){
    $sql="SELECT * FROM t_surat_paraf WHERE kodesuratutama='".$kode."'";
    $query = $this->db->query($sql);
    $hsl='';
       
    if($query->num_rows()>0){
        
        $no = 1;
        $hsl.='<h2>List pemaraf</h2><table data-role="table" class="ui-body-d ui-shadow table-stripe ui-responsive ui-table ui-table-columntoggle">';
        $hsl.='<thead><tr class="active"><th>No</th><th>Nik</th><th>Nama</th><th>Paraf</th></tr></thead>';
     
        foreach($query->result() as $row){
		if($row->validasiparaf=='1'){
			$paraf = date('d/m/Y H:i:s',strtotime($row->tanggalparaf));
		}else{
			$paraf = '';
		}
        $hsl.='<tbody><tr><td>'.$no.'</td><td>'.$row->empnikparaf.'</td><td>'.$row->empnameparaf.'</td><td>'.$paraf.'</td></tr></tbody>';
        $no++;    
        }
        $hsl.='</table>';
        
        $hsl.='<br><a href="javascript:;" class="btn btn-danger btn-block" onclick="close_riwayat();"><i class="fa fa-times pull-right"></i> Close</a>';
    
        }else{
        $hsl.='<h1>Tidak ada list pemaraf</h1><br><a href="javascript:;" class="btn btn-danger btn-block" onclick="close_riwayat();"><i class="fa fa-times pull-right"></i> Close</a>';
    }
    //$hsl.='</table><br><a href="javascript:;" class="btn btn-danger btn-block" onclick="close_riwayat();"><i class="fa fa-times pull-right"></i> Close</a>';
    
    $this->output->set_content_type('application/json')->set_output(json_encode($hsl));
 }
 public function get_riwayat_perubahan($kode){
    $sql="SELECT * FROM t_suratmasuk_memo_history WHERE kodesurat='".$kode."' order by waktu desc";
    $query = $this->db->query($sql);
    $hsl='';
       
    if($query->num_rows()>0){
        
        $no = 1;
        $hsl.='<h2>Riwayat Perubahan</h2><table data-role="table" class="ui-body-d ui-shadow table-stripe ui-responsive ui-table ui-table-columntoggle">';
        $hsl.='<thead><tr class="active"><th>No</th><th>Waktu</th><th>Nik</th><th>Nama</th><th>Kolom</th><th>Status</th></tr></thead>';
     
        foreach($query->result() as $row){
        $hsl.='<tbody><tr><td>'.$no.'</td><td>'.date('d/m/Y H:i:s',strtotime($row->waktu)).'</td><td>'.$row->empnikparaf.'</td><td>'.$row->empnameparaf.'</td><td>'.$row->perubahan.'</td><td>'.$row->persetujuan.'</td></tr></tbody>';
        $no++;    
        }
        $hsl.='</table>';
        
        $hsl.='<br><a href="javascript:;" class="btn btn-danger btn-block" onclick="close_riwayat();"><i class="fa fa-times pull-right"></i> Close</a>';
    
        }else{
        $hsl.='<h1>Tidak Ada Riwayat Perubahan</h1><br><a href="javascript:;" class="btn btn-danger btn-block" onclick="close_riwayat();"><i class="fa fa-times pull-right"></i> Close</a>';
    }
    //$hsl.='</table><br><a href="javascript:;" class="btn btn-danger btn-block" onclick="close_riwayat();"><i class="fa fa-times pull-right"></i> Close</a>';
    
    $this->output->set_content_type('application/json')->set_output(json_encode($hsl));
 }
 public function get_base_encode($kode,$user){
    
    $data=array('statusview'=>1);
    $where=array('kodesuratmasuk'=>$kode);
    $update = $this->model_mobile->valid_masuk($where,$data);
    $hsl = base64_encode($kode);
    $this->output->set_content_type('application/json')->set_output(json_encode($hsl));
 }
  public function get_base_encode_2($kode){
    
    $hsl = base64_encode($kode);
    $this->output->set_content_type('application/json')->set_output(json_encode($hsl));
 }
 public function get_pedoman(){
    // jumlah pedoman kearsipan
    $pedoman = $this->model_mobile->get_pedoman_kearsipan(1)->row_array();
    $jmlpedoman =$pedoman['jmlpedoman'];
    
    //jumlah petunjuk 
    $petunjuk = $this->model_mobile->get_petunjuk(2)->row_array();
    $jmlpetunjuk = $petunjuk['jmlpetunjuk'];
    //jumlah informasi
    $informasi = $this->model_mobile->get_informasi(3)->row_array();
    $jmlinformasi=$informasi['jmlinformasi'];
    
    $hsl='';
    $hsl.='<div style="padding: 0px;">';
    $hsl.=  '<div class="ui-grid-a ui-responsive">';
    $hsl.=      '<div> 
                        <div class="col-md-12" onclick="pedoman_kearsipan();">
        					<div class="widget widget-stats bg-blue">
        						<div class="stats-icon"><i class="fa fa-archive"></i></div>
        						<div class="stats-info">
        							<h4>PEDOMAN KEARSIPAN</h4>
        							<p class="notif"><h4>'.$jmlpedoman.' Pedoman</h4></p>	
        						</div>
        					</div>
        				</div>
                    </div>';
    $hsl.=      '<div> 
                        <div class="col-md-12" onclick="petunjuk_penggunaan();">
        					<div class="widget widget-stats bg-orange">
        						<div class="stats-icon"><i class="fa fa-book"></i></div>
        						<div class="stats-info">
        							<h4>PETUNJUK PENGGUNAAN</h4>
        							<p class="notif"><h4>'.$jmlpetunjuk.' Petunjuk</h4></p>	
        						</div>
        						
        					</div>
        				</div>
                    </div>';
    $hsl.=      '<div> 
                        <div class="col-md-12" onclick="informasi_kearsipan();">
        					<div class="widget widget-stats bg-green">
        						<div class="stats-icon"><i class="fa fa-info-circle"></i></div>
        						<div class="stats-info">
        							<h4>INFORMASI SEPUTAR KEARSIPAN</h4>
        							<p class="notif"><h4>'.$jmlinformasi.' Informasi</h4></p>	
        						</div>
        						
        					</div>
        				</div>
                    </div>';
    $hsl.=  '</div>';
    $hsl.='</div>';
    $this->output->set_content_type('application/json')->set_output(json_encode($hsl));
 }
 public function notifikasi_progres(){
    echo 'xxxxxx';
 }
 public function get_page_index($user){
    // jumlah edaran
    $edaran = $this->model_mobile->get_edaran($user)->row_array();
    $jmledaran =$edaran['jmledaran'];
    // jumlah surat yang masuk
    $smasuk = $this->model_mobile->get_smasuk($user)->row_array();
    $jmlsmasuk =$smasuk['jmlsmasuk'];
    // jumlah yang harus divalidasi
    $validasi = $this->model_mobile->get_validasi($user)->row_array();
    $jmlvalidasi=$validasi['jmlvalidasi'];
    // jumlah yang harus diparaf
    $paraf = $this->model_mobile->get_paraf($user)->row_array();
    $jmlparaf = $paraf['jmlparaf'];
    // jumlah surat yang belum divalidasi
    $skeluar = $this->model_mobile->get_skeluar($user)->row_array();
    $jmlskeluar = $skeluar['jmlkeluar'];
    $progress = $this->model_mobile->get_progress($user)->row_array();
    $jmlprogress = $progress['jmlprogress'];
    
    $hsl='';
    $hsl.='<div style="padding: 0px;">';
    $hsl.=  '<div class="ui-grid-a ui-responsive">';
    $hsl.=  '<a href="edaran.html" data-transition="slide">
                <div class="col-md-12">
					<div class="widget widget-stats bg-green">
						<div class="stats-icon"><i class="fa fa-newspaper-o"></i></div>
						<div class="stats-info">
							<h4>EDARAN</h4>
							<p class="notif" id="edaran"><h4>'.$jmledaran.' Surat Edaran</h4></p>	
						</div>
					</div>
	           </div></a>';
    $hsl.=  '<a href="suratmasuk.html" data-transition="slide">
                <div class="col-md-12">
					<div class="widget widget-stats bg-blue">
						<div class="stats-icon"><i class="fa fa-envelope"></i></div>
						<div class="stats-info">
							<h4>SURAT MASUK</h4>
							<p class="notif" id="suratmasuk"><h4>'.$jmlsmasuk.' Belum Dibaca</h4></p>	
						</div>
					</div>
	           </div></a>';
    $hsl.=  '<a href="notifikasiprogress.html" data-transition="slide">
                <div class="col-md-12">
					<div class="widget widget-stats bg-red">
						<div class="stats-icon"><i class="fa fa-bar-chart-o"></i></div>
						<div class="stats-info">
							<h4>NOTIFIKASI PROGRES</h4>
							<p class="notif" id="progress"><h4>'.$jmlprogress.' Progress</h4></p>	
						</div>
					</div>
	           </div></a>';
    $hsl.='
            <a href="suratkeluar.html">
            <div class="col-md-12">
				<div class="widget widget-stats bg-orange">
					<div class="stats-icon"><i class="fa fa-envelope-o"></i></div>
					<div class="stats-info">
						<h4>SURAT KELUAR</h4>
						<p class="notif" id="suratkeluar"><h4>'.$jmlskeluar.' Belum Tervalidasi</h4></p>	
					</div>
					
				</div>
			</div></a>';
    $hsl.= '<a href="validasiparaf.html" data-transition="slide">
            <div class="col-md-12">
				<div class="widget widget-stats bg-green">
					<div class="stats-icon"><i class="fa fa-pencil"></i></div>
					<div class="stats-info">
						<h4>PARAF</h4>
						<p class="notif" id="paraf"><h4>'.$jmlparaf.' Belum Diparaf</h4></p>	
					</div>
					
				</div>
			</div></a>';
    $hsl.= '<a href="validasi.html" data-transition="slide">
            <div class="col-md-12">
				<div class="widget widget-stats bg-purple">
					<div class="stats-icon"><i class="fa fa-check-square-o"></i></div>
					<div class="stats-info">
						<h4>VALIDASI</h4>
						<p class="notif" id="validasi"><h4>'.$jmlvalidasi.' Belum Divalidasi</h4></p>	
					</div>
					
				</div>
			</div></a>';
    $hsl.= '<a href="pedoman.html" data-transition="slide">
            <div class="col-md-12">
				<div class="widget widget-stats bg-aqua">
					<div class="stats-icon"><i class="fa fa-info"></i></div>
					<div class="stats-info">
						<h4>PEDOMAN</h4>
						<p>-</p>	
					</div>
					
				</div>
			</div></a>';
    $hsl.= '<a href="#" data-transition="slide">
            <div class="col-md-12">
				<div class="widget widget-stats bg-black">
					<div class="stats-icon"><i class="fa fa-phone-square"></i></div>
					<div class="stats-info">
						<h4>CONTACT US</h4>
			            <h4>Dinas Document Management <br>(0254) 372016 / (0254) 372285</h4>	
					</div>
					
				</div>
			</div></a>';
    $hsl.=  '</div>';
    $hsl.='</div>';
    $this->output->set_content_type('application/json')->set_output(json_encode($hsl));
 }
 
 public function simpan_paraf($kode,$user){
    
    //get data dari tabel surat keluar
    $suratkeluar=$this->model_mobile->get_datasurat($kode)->row_array();
    $nosuratold=$suratkeluar['nosuratkeluar'];
    $tglsuratold=$suratkeluar['tanggalsurat'];
    $isiringkasold=$suratkeluar['isiringkasan'];
    // get data dari tabel surat memo
    $datamemo = $this->model_mobile->get_memo($kode)->row_array();
    $isimemo = $datamemo['memo'];
    //inputan dari client
    $nosuratkeluar =$this->input->post('nosuratkeluar');
    $tanggalsurat =$this->input->post('tanggalsurat');
    $isiringkasan =$this->input->post('isiringkasan');
    $ketparaf =$this->input->post('ketparaf');
    $memo =$this->input->post('memo');
    
    /*if($ketparaf==''){
        $hsl="0";//"Keterangan paraf harus diisi !";
        $this->output->set_content_type('application/json')->set_output(json_encode($hsl));
    }else{*/
        $data = array(
              'nosuratkeluar' => $nosuratkeluar,
              'tanggalsurat' => $tanggalsurat,
              'isiringkasan' => $isiringkasan
            );
        $where = array(
                'kodesuratkeluar' => $kode
            );
        $res=$this->model_mobile->simpan_paraf($data,$where);
        if($res){
            //upate memo
            $data2=array('kodesuratkeluar'=>$kode,'memo'=>$memo,'pembuat'=>$user);
            $savememo=$this->model_mobile->simpan_memo($data2,$where);
            //update paraf all
            $dataparafall=array('validasiparaf'=>'0','tanggalparaf'=>'','keterangan_paraf'=>'','persetujuan'=>'');
            $whereparafall=array('kodesuratutama'=>$kode);
            $updateparafall=$this->model_mobile->updateparaf($dataparafall,$whereparafall);
            //update paraf
            $dataparaf=array('validasiparaf'=>'1','tanggalparaf'=>date("Y-m-d H:i:s"),'keterangan_paraf'=>$ketparaf,'persetujuan'=>'');
            $whereparaf=array('kodesuratutama'=>$kode,'empnikparaf'=>$user);
            $updateparaf=$this->model_mobile->updateparaf($dataparaf,$whereparaf);
            // echo $this->db->last_query($savememo);
            if($updateparaf){
                // cek perubahan
                $cekriwayat = $this->cek_perubahan($kode,$nosuratkeluar,$nosuratold,$tanggalsurat,$tglsuratold,$ketparaf,$memo,$isimemo,$isiringkasan,$isiringkasold,$user);
                // cek kalo ada yang belum paraf
                $cekparaf = $this->cek_belum_paraf($kode);
				
                // kalau ada yang belum paraf
                if($cekparaf > 0){
                    $penerima = $this->model_mobile->get_receive_paraf($kode,$user);
				
                    $pesan ="Ada Surat Yang Harus Anda Paraf";
                    $heading="Paraf";
                }else{
                    $penerima = $this->model_mobile->get_receive_validasi($kode);
                    $pesan ="Ada Surat Yang Harus Anda Validasi";
                    $heading="Validasi";
                }
                    $datapenerima=array();
                    //data penerima notif di array_push
                    foreach($penerima->result_array() as $value){
                        array_push($datapenerima,$value['playerid']);
                    }
                //hapus session paraf
                $hapus_session = $this->delete_session_paraf($kode,$user);
                // fungsi untuk mengirim notifikasi ke mobile
                $kirimnotif=$this->send_notification($datapenerima,$pesan,$heading);
                $hsl="Data berhasil disimpan !";
                $this->output->set_content_type('application/json')->set_output(json_encode($hsl));
            }
			

        }else{
            $hsl="Data gagal disimpan !";
            $this->output->set_content_type('application/json')->set_output(json_encode($hsl));
        }
    //}
	$datalog = array('log_time'=>date('Y-m-d H:i:s'),'log_user'=>$user,'log_aksi'=>'update','log_url'=>site_url(uri_string()),'log_item'=>'Paraf','kode'=>$kode,'log_ip'=>$this->get_client_ip(),'log_device'=>$_SERVER['HTTP_USER_AGENT']);
				$this->model_mobile->log_activity($datalog);
    
    
    
 }
 public function cek_session_paraf($kode){
    $hsl = $this->db
            ->where('kodesuratutama',$kode)
			->where('empnikparaf','')
            ->count_all_results('session_paraf',false);
	if($hsl){
		$hsl = $hsl;
	}else{
		$hsl=0;
	}
	//$hsl=0;
    $this->output->set_content_type('application/json')->set_output(json_encode($hsl));
 }
 public function cek_belum_paraf($kode){
    
    $hsl = $this->db
       ->where('validasiparaf',0)
       ->where('kodesuratutama',$kode)
       ->count_all_results('t_surat_paraf');
    return $hsl;
 }
 public function cek_perubahan($kode,$nosuratkeluar,$nosuratold,$tanggalsurat,$tglsuratold,$ketparaf,$memo,$isimemo,$isiringkasan,$isiringkasold,$user){
    
    //get data dari tabel paraf
    $dataparaf = $this->model_mobile->get_dataparaf($user,$kode)->row_array();
    $namapemaraf =$dataparaf['empnameparaf'];
   
    if($nosuratold!=$nosuratkeluar){
      $data = array('kodesurat'=>$kode,'waktu'=>date("Y-m-d H:i:s"),'empnikparaf'=>$user,'empnameparaf'=>$namapemaraf,'memo'=>$nosuratold,'perubahan'=>'Nomor Surat','isiperubahan'=>$nosuratkeluar,'persetujuan'=>'','keterangan_paraf'=>'');
      $insert = $this->model_mobile->insert_to_riwayat($data);
    }
    if($tglsuratold!=$tanggalsurat){
       $data = array('kodesurat'=>$kode,'waktu'=>date("Y-m-d H:i:s"),'empnikparaf'=>$user,'empnameparaf'=>$namapemaraf,'memo'=>$tglsuratold,'perubahan'=>'Tanggal Surat','isiperubahan'=>$tanggalsurat,'persetujuan'=>'','keterangan_paraf'=>'');
       $insert = $this->model_mobile->insert_to_riwayat($data);
    }
    if($isiringkasold!=$isiringkasan){
       $data = array('kodesurat'=>$kode,'waktu'=>date("Y-m-d H:i:s"),'empnikparaf'=>$user,'empnameparaf'=>$namapemaraf,'memo'=>$isiringkasold,'perubahan'=>'Perihal','isiperubahan'=>$isiringkasan,'persetujuan'=>'','keterangan_paraf'=>'');
       $insert = $this->model_mobile->insert_to_riwayat($data);
    }
    if($isimemo!=$memo){
       $data = array('kodesurat'=>$kode,'waktu'=>date("Y-m-d H:i:s"),'empnikparaf'=>$user,'empnameparaf'=>$namapemaraf,'memo'=>$memo,'perubahan'=>'Memo','isiperubahan'=>$isimemo,'persetujuan'=>'','keterangan_paraf'=>'');
       $insert = $this->model_mobile->insert_to_riwayat($data);
       $wherememo = array('kodesuratkeluar'=>$kode);
       $datamemo = array('memo'=>$memo,'pembuat'=>$user);
       $updatememo = $this->model_mobile->update_memo($wherememo,$datamemo);
    }
    
    
 }
 public function contactus(){
    $judul =$this->input->post('judul');
    $deskripsi =$this->input->post('desikripsi');
    if($judul=='' || $deskripsi==''){
        $hsl="Judul dan deskripsi harus diisi !";
        $this->output->set_content_type('application/json')->set_output(json_encode($hsl));
    }else{
        $data = array(
              'judul' => $judul,
              'deskripsi' => $deskripsi,
              'creator' => "",
              'tglcreate' => date("Y-m-d H:i:s"),
              'email' =>""
            );
        $res=$this->{$this->models[0]}->contactus($data);
        //$this->output->set_content_type('application/json')->set_output(json_encode($res));
        
        if($res){
            $hsl="Data berhasil disimpan !";
            $this->output->set_content_type('application/json')->set_output(json_encode($hsl));
        }else{
            $hsl="Data gagal disimpan !";
            $this->output->set_content_type('application/json')->set_output(json_encode($hsl));
        }
    }
    
    
    
 }
 public function read_kar_pelimpahan($user){
	$db2=$this->load->database('db2',true);
	$data =$db2->get_where('structdisp',array('empnik'=>$user))->row_array();
	$gol = $data['emppersk'];
    $data = $this->model_mobile->read_kar_pelimpahan($user,$gol);
    echo $data;
 }
 public function readkaryawan($user){
	$db2=$this->load->database('db2',true);
	$data =$db2->get_where('structdisp',array('empnik'=>$user))->row_array();
	$gol = $data['emppersk'];
    $data = $this->model_mobile->readkaryawan($user,$gol);
    echo $data;
 }
 public function readdireksi(){
    $data = $this->model_mobile->readdireksi();
    echo $data;
 }
 public function readedaran($user) {
        $datauser =$this->model_mobile->get_user($user)->row_array();
        $groupid=$datauser['group_id'];
        $nik    =$datauser['nik'];
        $data = $this->model_mobile->readedaran($groupid,$user,$nik);
        echo $data;
 }
 public function readparaf($user) {
        $datauser =$this->model_mobile->get_user($user)->row_array();
        $groupid=$datauser['group_id'];
        $nik    =$datauser['nik'];
        $data = $this->model_mobile->readparaf($groupid,$user,$nik);
        echo $data;
 }
 public function readvalidasi($user) {
        $datauser =$this->model_mobile->get_user($user)->row_array();
        $groupid=$datauser['group_id'];
        $nik    =$datauser['nik'];
        $data = $this->model_mobile->readvalidasi($groupid,$user,$nik);
        echo $data;
 }
 public function readsuratmasuk($user) {
        $datauser =$this->model_mobile->get_user($user)->row_array();
        $groupid=$datauser['group_id'];
        $nik    =$datauser['nik'];
        $data = $this->model_mobile->readsuratmasuk($groupid,$user,$nik);
        echo $data;
		//echo $user."-".$groupid."-".$nik;
 }
 public function readsuratkeluar($user) {
        $datauser =$this->model_mobile->get_user($user)->row_array();
        $groupid=$datauser['group_id'];
        $nik    =$datauser['nik'];
        $data = $this->model_mobile->readsuratkeluar($groupid,$user,$nik);
        echo $data;
 }
 public function readnotifprogress($user) {
        $datauser =$this->model_mobile->get_user($user)->row_array();
        $groupid=$datauser['group_id'];
        $nik    =$datauser['nik'];
        $data = $this->model_mobile->readnotifprogress($groupid,$user,$nik);
        // echo $data;
 }
 public function setuju_paraf($kode,$user){
    $data=array('validasiparaf'=>"1",'tanggalparaf'=>date("Y-m-d H:i:s"),'persetujuan'=>'Setuju');
    $where=array('kodesuratutama'=>$kode,'empnikparaf'=>$user);
    $setuju=$this->model_mobile->updateparaf($data,$where);
    if($setuju){
        //get data dari tabel paraf
        $dataparaf = $this->model_mobile->get_dataparaf($user,$kode)->row_array();
        $namapemaraf =$dataparaf['empnameparaf'];
        //insert to riwayat perubahan
        $datariwayat=array('kodesurat'=>$kode,'waktu'=>date("Y-m-d H:i:s"),'empnikparaf'=>$user,'empnameparaf'=>$namapemaraf,'persetujuan'=>'Setuju');
        $insert = $this->model_mobile->insert_to_riwayat($datariwayat);
        //get data surat
        $suratkeluar=$this->model_mobile->get_datasurat($kode)->row_array();
        $isiringkasan=$suratkeluar['isiringkasan'];
        $cekparaf = $this->cek_belum_paraf($kode);
        if($cekparaf > 0){
            $penerima = $this->model_mobile->get_receive_paraf($kode,$user);
            $pesan ="Ada Surat Yang Harus Anda Paraf";
            $heading="Paraf";
        }else{
            $penerima = $this->model_mobile->get_receive_validasi($kode);
            $pesan ="Ada Surat Yang Harus Anda Validasi";
            $heading="validasi";
        }
        $datapenerima=array();
        foreach($penerima->result_array() as $value){
            array_push($datapenerima,$value['playerid']);
        }
        
        $kirimnotif=$this->send_notification($datapenerima,$pesan,$heading);
        $response="1";
    }else{
        $response="0";
    }
	$datalog = array('log_time'=>date('Y-m-d H:i:s'),'log_user'=>$user,'log_aksi'=>'paraf','log_url'=>site_url(uri_string()),'log_item'=>'Paraf','kode'=>$kode,'log_ip'=>$this->get_client_ip(),'log_device'=>$_SERVER['HTTP_USER_AGENT']);
				$this->model_mobile->log_activity($datalog);
    $this->output->set_content_type('application/json')->set_output(json_encode($response));
 }
 public function tolak_paraf($kode,$user,$keterangan){
    $replace =str_replace("%20"," ",$keterangan);
    $data=array('validasiparaf'=>"2",'tanggalparaf'=>date("Y-m-d H:i:s"),'persetujuan'=>'Tidak Setuju');
    $where=array('kodesuratutama'=>$kode,'empnikparaf'=>$user);
    $tolak=$this->model_mobile->updateparaf($data,$where);
    if($tolak){
        //get data dari tabel paraf
        $dataparaf = $this->model_mobile->get_dataparaf($user,$kode)->row_array();
        $namapemaraf =$dataparaf['empnameparaf'];
        //insert to riwayat perubahan
        $datariwayat=array('kodesurat'=>$kode,'waktu'=>date("Y-m-d H:i:s"),'empnikparaf'=>$user,'empnameparaf'=>$namapemaraf,'perubahan'=>'Tidak setuju','persetujuan'=>'','keterangan_paraf'=>$replace);
        $insert = $this->model_mobile->insert_to_riwayat($datariwayat);
        //get data surat
        $suratkeluar=$this->model_mobile->get_datasurat($kode)->row_array();
        $isiringkasan=$suratkeluar['isiringkasan'];
        $cekparaf = $this->cek_belum_paraf($kode);
        if($cekparaf > 0){
            $penerima = $this->model_mobile->get_receive_paraf($kode,$user);
            $pesan ="Ada Surat Yang Harus Anda Paraf";
            $heading="Paraf";
        }else{
            $penerima = $this->model_mobile->get_receive_validasi($kode);
            $pesan ="Ada Surat Yang Harus Anda Validasi";
            $heading="Validasi";
        }
        $datapenerima=array();
        foreach($penerima->result_array() as $value){
            array_push($datapenerima,$value['playerid']);
        }
        
        $kirimnotif=$this->send_notification($datapenerima,$pesan,$heading);
        $response="1";
    }else{
        $response="0";
    }
	$datalog = array('log_time'=>date('Y-m-d H:i:s'),'log_user'=>$user,'log_aksi'=>'tolak paraf','log_url'=>site_url(uri_string()),'log_item'=>'Paraf','kode'=>$kode,'log_ip'=>$this->get_client_ip(),'log_device'=>$_SERVER['HTTP_USER_AGENT']);
	$this->model_mobile->log_activity($datalog);
    $this->output->set_content_type('application/json')->set_output(json_encode($response));
 }
 public function validasi($kode,$user){
    $suratkeluar=$this->model_mobile->get_datasurat($kode)->row_array();
	
    $playerid=array($suratkeluar['playerid']);
    $nomorsurat=$suratkeluar['nosuratkeluar'];
    $isiringkasan=$suratkeluar['isiringkasan'];
    $pembuat=$suratkeluar['pembuat'];
    $empkostl_asal=$suratkeluar['empkostl_asal'];
	$statusedaran = $suratkeluar['statusedaran'];
	$kodejudul = $suratkeluar['kodejudul'];
	$abbr_unit_asal = $suratkeluar['abbr_unit_asal'];
	
	
    $datasingkatan = $this->model_mobile->get_datasingkatan($abbr_unit_asal)->row_array();
    $singkatan=$datasingkatan['singkatan'];
	
    $query = $this->db->query("select * from t_suratkeluar_m where abbr_unit_asal ='".$abbr_unit_asal."' and validasiparaf='1'");
	
    $jum = $query->num_rows();//$this->db->count_all_results("select * from t_suratkeluar_m where kodesuratkeluar=$kode");
    
	$jumsur = $jum + 1;
	
	$nosurat = sprintf("%05s", $jumsur)."/". $singkatan."-KS/". $this->romaw(date('m'))."/". date('Y');
	
    $whereskeluar=array('kodesuratkeluar'=>$kode);
    $data=array('validasiparaf'=>"1",'tanggalparaf'=>date("Y-m-d H:i:s"),'nosuratkeluardivisi'=>$nosurat,'tanggalsurat'=>date('Y-m-d'));
    $valid = $this->model_mobile->validasi($whereskeluar,$data);
    if($valid){
        $kodesuratbawah = $this->model_mobile->get_kodesuratutama($kode);
		
        $datakodesuratmasuk=array();
        foreach($kodesuratbawah->result_array() as $value){
            array_push($datakodesuratmasuk,$value['kodesuratbawah']);
        }
        
        //$wheresmasuk=array('kodesuratmasuk'=>$datakodesuratmasuk);
        $datasmasuk=array('validasi'=>"1",'waktu'=>date("Y-m-d H:i:s"),'tanggalsurat'=>date('Y-m-d'),'nosuratmasukdivisi'=>$nosurat);
        //$validsmasuk=$this->model_mobile->valid_masuk($wheresmasuk,$datasmasuk);
        $this->db->where_in('kodesuratmasuk',$datakodesuratmasuk);
        $validsmasuk=$this->db->update('t_suratmasuk_m',$datasmasuk);
        if($validsmasuk){
			if($kodejudul=='2'){
				
				if($statusedaran=='1'){
				$edaran   = $this->model_mobile->get_receive_token();
				
				$dataedaran = array();
				foreach($edaran->result_array() as $valueedaran){
					array_push($dataedaran,$valueedaran['playerid']);
				}
				
				$pesanedaran ="Ada Edaran baru";
				$headingedaran="Edaran";
				$kirimnotifedaran=$this->send_notification($dataedaran,$pesanedaran,$headingedaran);
				}else{
					
					$penerima = $this->model_mobile->get_receive($datakodesuratmasuk);
					$datapenerima=array();
					foreach($penerima->result_array() as $value){
					array_push($datapenerima,$value['playerid']);
					}
					$pesan ="Ada Edaran baru";
					$heading="Edaran";
					$kirimnotif=$this->send_notification($datapenerima,$pesan,$heading);
				}
			}else{
					$penerima = $this->model_mobile->get_receive($datakodesuratmasuk);
					$datapenerima=array();
					foreach($penerima->result_array() as $value){
					array_push($datapenerima,$value['playerid']);
					}
					$pesan ="Ada Surat Masuk yang belum dibaca";
					$heading="Surat Masuk";
					$kirimnotif=$this->send_notification($datapenerima,$pesan,$heading);
			}
			
				$pesan2="Surat Keluar dengan nomor ".$nomorsurat." sudah dikirim";
				$heading2="Validasi";
				$kirimkepembuat=$this->send_notification($playerid,$pesan2,$heading2);
				$output="1";
        }else{
            $output="0";
        }
    }else{
            $output="0";
    }
	$datalog = array('log_time'=>date('Y-m-d H:i:s'),'log_user'=>$user,'log_aksi'=>'validasi','log_url'=>site_url(uri_string()),'log_item'=>'Validasi','kode'=>$kode,'log_ip'=>$this->get_client_ip(),'log_device'=>$_SERVER['HTTP_USER_AGENT']);
	$this->model_mobile->log_activity($datalog);
    $this->output->set_content_type('application/json')->set_output(json_encode($output));
 }
 public function romaw($index = 1){
	$romaw = array('01' => 'I', '02' => 'II', '03' => 'III', '04' => 'IV', '05' => 'V', '06' => 'VI', '07' => 'VII', '08' => 'VIII', '09' => 'IX', '10' => 'X', '11' => 'XI', '12' => 'XII');
	return $romaw[$index];
}
 
 public function get_token($playerid,$user){
    //$user       =$this->input->post('user');
    //$playerid   =$this->input->post('token');
    $data=array('userid'=>$user,'playerid'=>$playerid,'create_at'=>date("Y-m-d H:i:s"));
    $data2=array('playerid'=>$playerid,'create_at'=>date("Y-m-d H:i:s"));
    //$where=array('userid'=>$user);
    $where=array('playerid'=>$playerid);
	$where2=array('userid'=>$user);
    //$ada =$this->db->where('userid',$user)->get('token');
    $ada =$this->db->where('playerid',$playerid)->get('token');
	$ada2=$this->db->where('userid',$user)->get('token');
    if($ada->num_rows()==0 && $ada2->num_rows()==0){
		$this->db->insert('token',$data);
    }else{
		$this->db->where($where2);
        $this->db->delete('token');
		
		$this->db->where($where);
        $this->db->delete('token');
		
		$this->db->insert('token',$data);
	}
		
	/*if($ada2->num_rows()>0){
		$this->db->where($where2);
        $this->db->delete('token');
        //$this->db->insert('token',$data);
	}/*else{
        $this->db->insert('token',$data);
    }*/
	//$this->db->insert('token',$data);*/
    $this->output->set_content_type('application/json')->set_output(json_encode($ada));
 }
 public function get_paraf($kode,$user)
  {
    //get data dari tabel paraf
    $dataparaf = $this->model_mobile->get_dataparaf($user,$kode)->row_array();
    $namapemaraf =$dataparaf['empnameparaf'];
    $datasession = array('kodesuratutama'=>$kode,'empnikparaf'=>$user,'empnameparaf'=>$namapemaraf,'tanggalparaf'=>date("Y-m-d H:i:s"));
	$this->delete_session_paraf($kode,$user);
    $inserttosession =$this->model_mobile->insert_to_session($datasession);
    /*$data = $this->db
                  ->where('kodesuratkeluar',$kode)
                  ->get('t_suratkeluar_vd')
                  ->result_array();*/
    $data = $this->model_mobile->get_edit_format($kode)->result_array();
    $this->output
           ->set_content_type('application/json')
           ->set_output(json_encode( $data ));
  }
  public function delete_session_paraf($kode,$user){
    $where =array('kodesuratutama'=>$kode,'empnikparaf'=>$user);
    $delete =$this->model_mobile->delete_session($where);
  }
  function send_notification($penerima,$pesan,$heading){
        	$content = array(
			"en" => $pesan
			);
		
		$fields = array(
			'app_id' => "1935ab00-42e8-41b4-b364-5b86c47c60c2",
			'include_player_ids' =>$penerima,
            'headings' => array("en" =>$heading),
			'data' => array("foo" => "bar"),
            'android_group'  => "Smart",
            'android_group_message' => array("en" => "Smart"),
			'contents' => $content
		);
		
		$fields = json_encode($fields);
    	//print("\nJSON sent:\n");
    	//print($fields);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
												   'Authorization: Basic NGEwMGZmMjItY2NkNy0xMWUzLTk5ZDUtMDAwYzI5NDBlNjJj'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);
		
		return $response;
	
	
    	$response = sendMessage();
    	$return["allresponses"] = $response;
    	$return = json_encode( $return);
	
	//print("\n\nJSON received:\n");
	//print($return);
	//print("\n");
        
    }
    function get_data_hse(){
        $db2=$this->load->database('db2',true);
        $data =$db2->get_where('user_login',array('regno'=>'111110'))->row_array();
        echo $data['fullname'];
    }
    function cek_ldap(){
        $this->load->library('auth_ldap');
    }
    function simpan_disposisi($kode,$user){
        
        $suratmasuk =$this->model_mobile->get_ms($kode)->row_array();
        $nosuratmasuk=$suratmasuk['nosuratmasuk'];
        $tanggalsurat=$suratmasuk['tanggalsurat'];
        $tanggalselesai=$this->input->post('tanggalpenyelesaian');
		
		if($tanggalselesai==''){
			$tanggalpenyelesaian = date('Y-m-d', strtotime('+7 days'));
		}else{
			$tanggalpenyelesaian = $tanggalselesai;
		}
		
        $asalsurat=$suratmasuk['asalsurat'];
        $tujuansurat=$suratmasuk['tujuansurat'];
        $kodeklasifikasi=$suratmasuk['kodeklasifikasi'];
        $indeks=$suratmasuk['indeks'];
        $isiringkasan=$suratmasuk['isiringkasan'];
        $kodeasli=$suratmasuk['kodeasli'];
        $lokasi=$suratmasuk['lokasi'];
        $masasimpanaktif=$suratmasuk['masasimpanaktif'];
        $masasimpaninaktif=$suratmasuk['masasimpaninaktif'];
        $aktifjatuhtempo=$suratmasuk['aktifjatuhtempo'];
        $inaktifjatuhtempo=$suratmasuk['inaktifjatuhtempo'];
        $fileid=$suratmasuk['fileid'];
        $filename=$suratmasuk['filename'];
        $type=$suratmasuk['type'];
        $size=$suratmasuk['size'];
        $path=$suratmasuk['path'];
        $tanggalterimasurat=$suratmasuk['tanggalterimasurat'];
        $catatan=$this->input->post('catatan');
        $kodepesan=$this->input->post('pesan');
        $dari=$suratmasuk['dari'];
        $jenissurat=$suratmasuk['jenissurat'];
        $empniksurat=$suratmasuk['empniktujuan'];
        $empnamesurat=$suratmasuk['empnametujuan'];
        $empkostl_surat=$suratmasuk['empkostl_tujuan'];
        $emp_cskt_ltext_surat=$suratmasuk['emp_cskt_ltext_tujuan'];
        $empshortsurat=$suratmasuk['empshort_tujuan'];
        $empposidsurat=$suratmasuk['empposid_tujuan'];
        $empniktujuan=$this->input->post('empnik');
        $empnametujuan=$this->input->post('empname');
        $empkostl_tujuan=$this->input->post('empkostl');
        $emp_cskt_ltext_tujuan=$this->input->post('emp_cskt_ltext');
        $empshort_tujuan=$this->input->post('emp_hrp1000_s_short');
        $empposid_tujuan=$this->input->post('empposid');
        $emppersk=$this->input->post('emppersk');
        $pesantemp=$this->input->post('pesantemp');
        
        $kodesuratdisposisi =$this->input->post('kodesurat');
        $nikdisposisi =$this->input->post('nikdispos');
        $pesandisposisi =$this->input->post('pesan');
        
        $pembuat=$suratmasuk['empniktujuan'];
        $status='0';
        $nosuratmasukdivisi=$suratmasuk['nosuratmasukdivisi'];
        $kodesifatsurat=$suratmasuk['kodesifatsurat'];
        $kodeprogressurat='0';
        $suratdari='0';
        $dinasterkait='0';
        $statusview='0';
        $tembusantambahan='';
        $validasi=$suratmasuk['validasi'];
        $sifatsurat=$suratmasuk['sifatsurat'];
        $waktu=$suratmasuk['waktu'];
		$unitkerja_asal=$suratmasuk['unitkerja_asal'];
		$abbr_unit_asal=$suratmasuk['abbr_unit_asal'];
        $jabatan_asal=$suratmasuk['jabatan_asal'];
		
        $dataupdatedeploy = array('catatan'=>$catatan);
        $whereupdatedeploy = array('kodesuratatas'=>$kode);
        
        $dataupdate_masuk_m = array('catatan'=>$catatan,'tanggalpenyelesaian'=>$tanggalpenyelesaian,'status'=>'1');
        $whereupdate_masuk_m = array('kodesuratmasuk'=>$kode);
        
        $kodesuratbawah = $this->model_mobile->get_kodesuratbawah($kode);
        $datakodesuratmasuk=array();
        foreach($kodesuratbawah->result_array() as $value){
            array_push($datakodesuratmasuk,$value['kodesuratbawah']);
        }
        $dataupdate_masuk_m_tglselesai= array('tanggalpenyelesaian'=>$tanggalpenyelesaian);
        
        $dataupdate_masuk_d = array('kodepesan'=>$kodepesan);
        $whereupdate_masuk_d= array('kodesuratmasukatas'=>$kode);
        
        
                            
        $dispos = array();
        $deploy = array();
        $detail = array();
        $nikdispos = array();
        $pesandispos = array();
            $sql="SELECT max(cast(substring(kodesuratmasuk,11,14) as SIGNED)) as maxKode FROM t_suratmasuk_m";
            $query = $this->db->query($sql);
            if($query->num_rows()>0){
                foreach($query->result() as $row){
                    
                    (int) $row->maxKode >=0 ? $noUrut = $row->maxKode : $noUrut=1;
                    if($empniktujuan==''){
                        if($nikdisposisi!=''){
                            foreach($nikdisposisi as $key => $value){
                            $datapesan = array('catatanatasan'=>$pesandisposisi[$key]);
                            $whereupdate_pesan = array('kodesuratmasuk'=>$kodesuratdisposisi[$key]);
                            array_push($pesandispos,$datapesan);
                            $this->db->update('t_suratmasuk_m',$datapesan,$whereupdate_pesan);
                            }
                        }
                        
                        $this->db->update('t_surat_deployment',$dataupdatedeploy,$whereupdatedeploy);
                        $this->db->update('t_suratmasuk_m',$dataupdate_masuk_m,$whereupdate_masuk_m);
                        
                        $this->db->where_in('kodesuratmasuk',$datakodesuratmasuk);
                        $this->db->update('t_suratmasuk_m',$dataupdate_masuk_m_tglselesai);
                        
                        //$this->db->update('t_suratmasuk_d',$dataupdate_masuk_d,$whereupdate_masuk_d);
                        
                        $output=1;
                        $this->output->set_content_type('application/json')->set_output(json_encode($output));
                    }else{
                        if($nikdisposisi!=''){
                            foreach($nikdisposisi as $key => $value){
                            $datapesan = array('catatanatasan'=>$pesandisposisi[$key]);
                            $whereupdate_pesan = array('kodesuratmasuk'=>$kodesuratdisposisi[$key]);
                            array_push($pesandispos,$datapesan);
                            $this->db->update('t_suratmasuk_m',$datapesan,$whereupdate_pesan);
                            }
                        }
                        
                        $db2=$this->load->database('db2',true);
                        foreach ($empniktujuan as $key => $value) {
						$structdisp =$db2->get_where('structdisp',array('empnik'=>$value))->row_array();
						$gol = $structdisp['emppersk'];
						$zh12 = $db2->group_by('Objectabbr')->get_where('org_text',array('Objectabbr'=>$structdisp['emp_hrp1000_o_short']),'EndDate','9999-12-31')->row_array();
						
						$structjab = $db2->get_where('structjab',array('no'=>$structdisp['emp_hrp1000_s_short'],'gol'=>$structdisp['emppersk']))->row_array();
						
							
                    $noUrut = $noUrut + 1;
                    $kodesuratmasuk = "SM" . date('Ymd') . str_repeat("0",14-strlen($noUrut)).$noUrut;
                    $substrsifat[$key] = substr($emppersk[$key],1,1);
                    if($substrsifat[$key]=='F'){
                        $kodesifat[$key] =4;
                    }else if($substrsifat[$key]=='S'){
                        $kodesifat[$key] =3;
                    }else{
                        $kodesifat[$key] =0;
                    }
					if($aktifjatuhtempo=='0000-00-00'||$inaktifjatuhtempo=='0000-00-00'||$tanggalterimasurat=='0000-00-00'){
						$data_m =array(
                    'kodesuratmasuk'=>$kodesuratmasuk,
                    'nosuratmasuk'=>$nosuratmasuk,
                    'tanggalsurat'=>$tanggalsurat,
                    'tanggalpenyelesaian'=>$tanggalpenyelesaian,
                    'asalsurat'=>$asalsurat,
                    'tujuansurat'=>$value,
                    'kodeklasifikasi'=>$kodeklasifikasi,
                    'indeks'=>$indeks,
                    'isiringkasan'=>$isiringkasan,
                    'kodeasli'=>$kodeasli,
                    'lokasi'=>$lokasi,
                    'masasimpanaktif'=>$masasimpanaktif,
                    'masasimpaninaktif'=>$masasimpaninaktif,
                    //'aktifjatuhtempo'=>$aktifjatuhtempo,
                    //'inaktifjatuhtempo'=>$inaktifjatuhtempo,
                    'fileid'=>$fileid,
                    'filename'=>$filename,
                    'type'=>$type,
                    'size'=>$size,
                    'path'=>$path,
                    //'tanggalterimasurat'=>'',
                    'catatan'=>'',
                    'kodepesan'=>'0',
                    'dari'=>$dari,
                    'jenissurat'=>$jenissurat,
                    'empniksurat'=>$empniksurat,
                    'empnamesurat'=>$empnamesurat,
                    'empkostl_surat'=>$empkostl_surat,
                    'emp_cskt_ltext_surat'=>$emp_cskt_ltext_surat,
                    'empshortsurat'=>$empshortsurat,
                    'empposidsurat'=>$empposidsurat,
                    'empniktujuan'=>$value,
                    'empnametujuan'=>$empnametujuan[$key],
                    'empkostl_tujuan'=>$empkostl_tujuan[$key],
                    'emp_cskt_ltext_tujuan'=>$emp_cskt_ltext_tujuan[$key],
                    'empshort_tujuan'=>$empshort_tujuan[$key],
                    'empposid_tujuan'=>$empposid_tujuan[$key],
                    'pembuat'=>$pembuat,
                    'status'=>$status,
                    'nosuratmasukdivisi'=>$nosuratmasukdivisi,
                    'kodesifatsurat'=>$kodesifat[$key],
                    'kodeprogressurat'=>$kodeprogressurat,
                    'suratdari'=>$suratdari,
                    'dinasterkait'=>$dinasterkait,
                    'statusview'=>$statusview,
                    'tembusantambahan'=>$tembusantambahan,
                    'validasi'=>$validasi,
                    'sifatsurat'=>$sifatsurat,
                    'waktu'=>$waktu,
                    'catatanatasan'=>$pesantemp[$key],
                    'unitkerjasurat'=>$unitkerja_asal,
					'abbr_unitsurat'=>$abbr_unit_asal,
					'jabatansurat'=>$jabatan_asal,
					'unitkerja_tujuan'=>$zh12['Objectname'],
					'abbr_unit_tujuan'=>$structdisp['emp_hrp1000_o_short'],
					'jabatan_tujuan'=>$structjab['jabatan']
                    );
					}else{
					$data_m =array(
                    'kodesuratmasuk'=>$kodesuratmasuk,
                    'nosuratmasuk'=>$nosuratmasuk,
                    'tanggalsurat'=>$tanggalsurat,
                    'tanggalpenyelesaian'=>$tanggalpenyelesaian,
                    'asalsurat'=>$asalsurat,
                    'tujuansurat'=>$value,
                    'kodeklasifikasi'=>$kodeklasifikasi,
                    'indeks'=>$indeks,
                    'isiringkasan'=>$isiringkasan,
                    'kodeasli'=>$kodeasli,
                    'lokasi'=>$lokasi,
                    'masasimpanaktif'=>$masasimpanaktif,
                    'masasimpaninaktif'=>$masasimpaninaktif,
                    'aktifjatuhtempo'=>$aktifjatuhtempo,
                    'inaktifjatuhtempo'=>$inaktifjatuhtempo,
                    'fileid'=>$fileid,
                    'filename'=>$filename,
                    'type'=>$type,
                    'size'=>$size,
                    'path'=>$path,
                    'tanggalterimasurat'=>$tanggalterimasurat,
                    'catatan'=>'',
                    'kodepesan'=>'0',
                    'dari'=>$dari,
                    'jenissurat'=>$jenissurat,
                    'empniksurat'=>$empniksurat,
                    'empnamesurat'=>$empnamesurat,
                    'empkostl_surat'=>$empkostl_surat,
                    'emp_cskt_ltext_surat'=>$emp_cskt_ltext_surat,
                    'empshortsurat'=>$empshortsurat,
                    'empposidsurat'=>$empposidsurat,
                    'empniktujuan'=>$value,
                    'empnametujuan'=>$empnametujuan[$key],
                    'empkostl_tujuan'=>$empkostl_tujuan[$key],
                    'emp_cskt_ltext_tujuan'=>$emp_cskt_ltext_tujuan[$key],
                    'empshort_tujuan'=>$empshort_tujuan[$key],
                    'empposid_tujuan'=>$empposid_tujuan[$key],
                    'pembuat'=>$pembuat,
                    'status'=>$status,
                    'nosuratmasukdivisi'=>$nosuratmasukdivisi,
                    'kodesifatsurat'=>$kodesifat[$key],
                    'kodeprogressurat'=>$kodeprogressurat,
                    'suratdari'=>$suratdari,
                    'dinasterkait'=>$dinasterkait,
                    'statusview'=>$statusview,
                    'tembusantambahan'=>$tembusantambahan,
                    'validasi'=>$validasi,
                    'sifatsurat'=>$sifatsurat,
                    'waktu'=>$waktu,
                    'catatanatasan'=>$pesantemp[$key],
                    'unitkerjasurat'=>$unitkerja_asal,
					'abbr_unitsurat'=>$abbr_unit_asal,
					'jabatansurat'=>$jabatan_asal,
					'unitkerja_tujuan'=>$zh12['Objectname'],
					'abbr_unit_tujuan'=>$structdisp['emp_hrp1000_o_short'],
					'jabatan_tujuan'=>$structjab['jabatan']
                    );	
					}
                    
                    array_push($dispos,$data_m);
                    $dtm = $this->model_mobile->get_dtm($kode)->row_array();
                    $kodesuratutama =$dtm['kodesuratutama'];
                    $data_deploy = array('kodesuratatas'=>$kode,'kodesuratbawah'=>$kodesuratmasuk,'tanggaldeployment'=>date("Y-m-d H:i:s"),'userdeployment'=>$pembuat,'kodesuratutama'=>$kodesuratutama,'catatan'=>$catatan);
                    array_push($deploy,$data_deploy);
                    $data_detail = array('kodesuratmasuk'=>$kodesuratmasuk,'tanggalterimasurat'=>date('Y-m-d'),'empnikdisposisi'=>$value,'empnamedisposisi'=>$empnametujuan[$key],'empkostldisposisi'=>$empkostl_tujuan[$key],'emp_cskt_ltextdisposisi'=>$emp_cskt_ltext_tujuan[$key],'empshortdisposisi'=>$empshort_tujuan[$key],'empposiddisposisi'=>$empposid_tujuan[$key],'status'=>'0','kodesifatsurat'=>$kodesifat[$key],'kodesuratmasukatas'=>$kode);
                    array_push($detail,$data_detail);
                    
                    array_push($nikdispos,$value);

                    }
                    
                    
                    if($dispos){
                        $res = $this->db->insert_batch('t_suratmasuk_m', $dispos);
                        $res2= $this->db->insert_batch('t_surat_deployment',$deploy);
                        $res3= $this->db->insert_batch('t_suratmasuk_d',$detail);
                        
                        $this->db->update('t_surat_deployment',$dataupdatedeploy,$whereupdatedeploy);
                        $this->db->update('t_suratmasuk_m',$dataupdate_masuk_m,$whereupdate_masuk_m);
                        
                        //$kd = array ('SM2018071700000000000004','SM2018071700000000000005');
                        $kodesuratbawah = $this->model_mobile->get_kodesuratbawah($kode);
                        $datakodesuratmasuk=array();
                        foreach($kodesuratbawah->result_array() as $val){
                            array_push($datakodesuratmasuk,$val['kodesuratbawah']);
                        }
                       
                        $this->db->where_in('kodesuratmasuk',$datakodesuratmasuk);
                        $this->db->update('t_suratmasuk_m',$dataupdate_masuk_m_tglselesai);
                        
                        
                        //$this->db->update('t_suratmasuk_d',$dataupdate_masuk_d,$whereupdate_masuk_d);
                        
                        if(!$res){
                        $output=0;
                        $this->output->set_content_type('application/json')->set_output(json_encode($output));
                        }else{
                            
                        $penerima = $this->model_mobile->get_receive_disposisi($nikdispos);
                        $datapenerima=array();
                        foreach($penerima->result_array() as $value){
                            array_push($datapenerima,$value['playerid']);
                        }
                        //print_r($datapenerima);
                        //die();
                        $pesan ="Ada Surat Masuk yang belum dibaca";
                        $heading="Surat Masuk";
                        $kirimnotif=$this->send_notification($datapenerima,$pesan,$heading);    
                        
                        $output=1;
                        $this->output->set_content_type('application/json')->set_output(json_encode($output));
                        }
                    }
                    }
                    
                }
                        
                
            }
        $datalog = array('log_time'=>date('Y-m-d H:i:s'),'log_user'=>$user,'log_aksi'=>'disposisi','log_url'=>site_url(uri_string()),'log_item'=>'Surat Masuk','kode'=>$kode,'log_ip'=>$this->get_client_ip(),'log_device'=>$_SERVER['HTTP_USER_AGENT']);
	$this->model_mobile->log_activity($datalog);
        
        
    }
    public function hapus_disposisi($kode,$nik){
        $where_d =array('kodesuratmasuk'=>$kode);
        $where_deploy =array('kodesuratbawah'=>$kode);
        $where_m =array('kodesuratmasuk'=>$kode);
        $delete =$this->model_mobile->hapus_disposisi($where_d,$where_deploy,$where_m);
        if($delete){
            $output=1;
            $this->output->set_content_type('application/json')->set_output(json_encode($output));
        }else{
            $output=0;
            $this->output->set_content_type('application/json')->set_output(json_encode($output));
        }
        
        
    }
    public function validate_logincek($user,$pwd){
    $ada =$this->db->where('userid',$user)->where('userpass', md5($pwd))->get('sys_user');
           if( $ada->num_rows()>0)  {
             $data=$ada->result_array();
             $this->output->set_content_type('application/json')->set_output(json_encode($data));
           }else{
              $res ="0";
             $this->output->set_content_type('application/json')->set_output(json_encode($res));
           }
        
    }
    public function validate_login($username,$pwd){
		//$username ='document.centre';
		//$pwd ='2019maret';
    if (!($connect=@ldap_connect("ldap://corp.krakatausteel.com",433))){
             $res ="0";
             $this->output->set_content_type('application/json')->set_output(json_encode($res));
        }else{
            //ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($connect, LDAP_OPT_REFERRALS, 0);
            ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3);
            if (!($bind=@ldap_bind($connect, "krakatausteel\\".$username, $pwd))){
			 $res ="0";
             $this->output->set_content_type('application/json')->set_output(json_encode($res));
            }else{
                 $hsl = @ldap_bind($connect, "KRAKATAUSTEEL\\".$username, $pwd);
                 $nmm = $username.'@corp.krakatausteel.com';
			     $filter="(|(userprincipalname=$nmm))";
			     $dn		= "dc=corp,DC=krakatausteel,DC=com";
			     $ldapSearch = ldap_search($connect,$dn, $filter);
			     $info = ldap_get_entries($connect, $ldapSearch);
                 
                for($i = 0; $i < $info["count"]; $i++){
					
    				if(!isset($info[$i]["employeenumber"][0])||($info[$i]["employeenumber"][0]=='93001484')){
						$regno = $info[$i]["mailnickname"][0];
					}else{
						if($info[$i]["employeenumber"][0] < 15000 ||($info[$i]["employeenumber"][0]>=900000 && $info[$i]["employeenumber"][0]<=990017)){
						$regno = (int)$info[$i]["employeenumber"][0];
						}
						if($info[$i]["employeenumber"][0]>111000 && $info[$i]["employeenumber"][0]<113447){
							$regno = $info[$i]["mailnickname"][0];
						}
					}
                    
                }
				//print_r($info);
				//echo $regno;
				//die();
                //$this->output->set_content_type('application/json')->set_output(json_encode(substr($regno,1,5)));
                 $ada =$this->db->where('userid',$regno)->get('sys_user');
                 if( $ada->num_rows()>0)  {
                     $data=$ada->result_array();
                     //$fullname = $datauser['fullname'];
                     //$res ="1";
                     $this->output->set_content_type('application/json')->set_output(json_encode($data));
                   }else{
                      //$res ="0";
                      $ada =$this->db->where('userid',$username)->get('sys_user');
                      $data=$ada->result_array();
                      $this->output->set_content_type('application/json')->set_output(json_encode($res));
                   }
                 
            }
        }
        
    }
    public function login_ldap(){
		//$username ='test.web1';
        //$pwd ='test.web1';
        //$username ='triastanto';
        //$pwd ='satzinger2005';
		//$username ='document.centre';
		//$pwd ='2019maret';
		//$username ='subdit.corsec';
		//$pwd ='corsec1218';
		//$username ='nuraeni.ismi';
		//$pwd ='sayangaku';
		//$username ='divisi.fpd';
        //$pwd ='tiatiatia';
        //$username ='subdit.akuntansi';
		//$pwd ='gaktautuh';
		$username ='eka.rohmayanti';
		$pwd ='priambudi';
		
		
        if (!($connect=@ldap_connect("ldap://corp.krakatausteel.com",433))){
             $res ="0";
             $this->output->set_content_type('application/json')->set_output(json_encode($res));
        }else{
            //ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($connect, LDAP_OPT_REFERRALS, 0);
            ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3);
            if (!($bind=@ldap_bind($connect, "krakatausteel\\".$username, $pwd))){
			 $res ="0";
             $this->output->set_content_type('application/json')->set_output(json_encode($res));
            }else{
                 $hsl = @ldap_bind($connect, "KRAKATAUSTEEL\\".$username, $pwd);
                 $nmm = $username.'@corp.krakatausteel.com';
			     $filter="(|(userprincipalname=$nmm))";
			     $dn		= "dc=corp,DC=krakatausteel,DC=com";
			     $ldapSearch = ldap_search($connect,$dn, $filter);
			     $info = ldap_get_entries($connect, $ldapSearch);
                 print_r($info);
				 die();
                for($i = 0; $i < $info["count"]; $i++){
    				
    				$regno = $username;//$info[$i]["employeenumber"][0];
					if(!isset($info[$i]["employeenumber"][0])){
						$regno = $info[$i]["mailnickname"][0];
					}else{
						if($info[$i]["employeenumber"][0] < 15000){
						
						$regno = (int)$info[$i]["employeenumber"][0];
						
						}
						if($info[$i]["employeenumber"][0]>111000 && $info[$i]["employeenumber"][0]<113447){
							
							$regno = $info[$i]["mailnickname"][0];
							//echo $regno;
							//die();
						}
					}
					
                    
                }
				//print_r($info);
				//echo $regno;
				//die();
                //$this->output->set_content_type('application/json')->set_output(json_encode(substr($regno,1,5)));
                $ada =$this->db->where('userid',$regno)->get('sys_user');
                 if( $ada->num_rows()>0)  {
                     $data=$ada->result_array();
                     //$fullname = $datauser['fullname'];
                     //$res ="1";
                     $this->output->set_content_type('application/json')->set_output(json_encode($data));
                   }else{
                      $res ="0";
                     $this->output->set_content_type('application/json')->set_output(json_encode($res));
                   }
                 
            }
        }
        
    }
    public function get_view_suratkeluar($kode,$user){
        $url =$this->link;
        //$url ='http://sso.krakatausteel.com/smart';
        //cari data user
        $datauser =$this->model_mobile->get_user($user)->row_array();
        $groupid=$datauser['group_id'];
        $nik    =$datauser['nik'];
        //cari data surat
        $suratkeluar=$this->model_mobile->get_datasurat($kode)->row_array();
        $jenissurat=$suratkeluar['jenissurat'];
        $sifatsurat=$suratkeluar['sifatsurat'];
        $isiringkasan=$suratkeluar['isiringkasan'];
        $validasiparaf =$suratkeluar['validasiparaf'];
        $hsl='';
        $kodeencode = base64_encode($kode);
        if($groupid=='1'||$groupid=='2'||$groupid=='4'||$groupid=='5'||$groupid=='7'){
            if($jenissurat=='1'){
                $hsl.='<object type="text/html" style="float:left;width:100%;height:600px;background-color:white;" data="'.$url.'/modules/report/label/memodinaskeluar_view.php?idp='.$kodeencode.'"></object>';
        
            }else if($jenissurat=='2'){
                $hsl.='<object type="text/html" style="float:left;width:100%;height:600px;background-color:white;" data="'.$url.'/modules/report/label/memosuratkeluar_view.php?idp='.$kodeencode.'"></object>';
        
            }else{
                $hsl.='<object type="text/html" style="float:left;width:100%;height:600px;background-color:white;" data="'.$url.'/modules/report/label/memosuratkeluaratas_view.php?idp='.$kodeencode.'"></object>';
        
            }
            
        }else{
            if($suratkeluar=='3'){
                $hsl.=$isiringkasan;
            }else{
                if($jenissurat=='1'){
                    $hsl.='<object type="text/html" style="float:left;width:100%;height:600px;background-color:white;" data="'.$url.'/modules/report/label/memodinaskeluar_view.php?idp='.$kodeencode.'"></object>';
        
                }else if($jenissurat=='2'){
                    $hsl.='<object type="text/html" style="float:left;width:100%;height:600px;background-color:white;" data="'.$url.'/modules/report/label/memosuratkeluar_view.php?idp='.$kodeencode.'"></object>';
        
                }else{
                    $hsl.='<object type="text/html" style="float:left;width:100%;height:600px;background-color:white;" data="'.$url.'/modules/report/label/memosuratkeluaratas_view.php?idp='.$kodeencode.'"></object>';
            
                }
            }
        }
            $hsl.='<a href="javascript:;" class="btn btn-danger btn-block" onclick="close_view();"><i class="fa fa-times pull-right"></i> Close</a>';
            $hsl.='<a href="javascript:;" class="btn btn-primary btn-block" onclick="riwayat(\'' .$kode. '\');"><i class="fa fa-search pull-right"></i> Riwayat Perubahan</a>';
            if($validasiparaf=='0'){
                $hsl.='<a href="javascript:;" class="btn btn-default btn-block" ><i class="fa fa-check-square-o pull-right"></i> Menunggu Divalidasi</a>';
            }else{
                $hsl.='<a href="javascript:;" class="btn btn-success btn-block" ><i class="fa fa-check-square-o pull-right"></i> Sudah Divalidasi</a>';
            }
            
        $this->output->set_content_type('application/json')->set_output(json_encode($hsl));
    }
    public function get_view_paraf($kode,$user){
        //$url ='http://sso.krakatausteel.com/smart';
		$url =$this->link;
        //cari data user
        $datauser =$this->model_mobile->get_user($user)->row_array();
        $groupid=$datauser['group_id'];
        $nik    =$datauser['nik'];
        //cari data surat
        $suratkeluar=$this->model_mobile->get_datasurat($kode)->row_array();
        $jenissurat=$suratkeluar['jenissurat'];
        $sifatsurat=$suratkeluar['sifatsurat'];
        $isiringkasan=$suratkeluar['isiringkasan'];
        $validasiparaf =$suratkeluar['validasiparaf'];
        //cek data paraf
        $dataparaf =$this->model_mobile->get_dataparaf($kode,$user)->row_array();
        $paraf=$dataparaf['validasiparaf'];
        //cek session paraf
        $sessionparaf =$this->model_mobile->cek_session_paraf($kode)->row_array();//$this->db->where('kodesuratutama',$kode)->count_all_results('session_paraf');
        $empnikparaf=$sessionparaf['empnikparaf'];
        $empnameparaf=$sessionparaf['empnameparaf'];
        $hsl='';
        $kodeencode = base64_encode($kode);
		//edit memo surat keluar view
        $hsl.='<object type="text/html" style="float:left;width:100%;height:600px;background-color:white;" data="'.$url.'/modules/report/label/memodinaskeluar_view.php?idp='.$kodeencode.'"></object>';
        $hsl.='<a href="javascript:;" class="btn btn-danger btn-block" onclick="close_view();"><i class="fa fa-times pull-right"></i> Close</a>';
        $hsl.='<a href="javascript:;" class="btn btn-primary btn-block" onclick="riwayat(\'' .$kode. '\');"><i class="fa fa-search pull-right"></i> Riwayat Perubahan</a>';
        if($empnikparaf!='' && $empnikparaf!=$user){
            $hsl.='<a href="#" class="btn btn-warning btn-block" ><i class="fa fa-pencil-square pull-right"></i>Diedit oleh '.$empnameparaf.' ( '.$empnikparaf.' )</a>';
        }else{
            if($validasiparaf=='0'){
                $hsl.='<a href="javascript:paraf(\'' .$kode. '\');" class="btn btn-warning btn-block" ><i class="fa fa-pencil-square pull-right"></i> Edit Format</a>';
            }
            if($validasiparaf=='1'){
                $hsl.='<a href="javascript:;" class="btn btn-success btn-block" ><i class="fa fa-check-square-o pull-right"></i> Sudah divalidasi</a>';
            }
            if($validasiparaf=='2'){
                $hsl.='<a href="javascript:;" class="btn btn-warning btn-block" ><i class="fa fa-check-square-o pull-right"></i> Menolak paraf</a>';
            }
            
        }
        if($paraf=='0'||$paraf==null||$paraf==''){
            $hsl.='<a href="javascript:setuju_paraf(\'' .$kode. '\');" class="btn btn-success btn-block" ><i class="fa fa-check-square-o pull-right"></i> Setuju Paraf</a>';
            $hsl.='<a href="javascript:tolak_paraf(\'' .$kode. '\');" class="btn btn-warning btn-block" ><i class="fa fa-minus pull-right"></i> Tolak Paraf</a>';
        }
        if($paraf=='1'){
            $hsl.='<a href="javascript:;" class="btn btn-success btn-block" ><i class="fa fa-check-square-o pull-right"></i> Paraf sudah disetujui</a>';
        }
        if($paraf=='2'){
            $hsl.='<a href="javascript:;" class="btn btn-warning btn-block" ><i class="fa fa-minus pull-right"></i> Paraf tidak disetujui</a>';
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($hsl));
    }
    public function get_view_validasi($kode,$user){
        $url =$this->link;
        //$url ='http://sso.krakatausteel.com/smart';
        //cari data user
        $datauser =$this->model_mobile->get_user($user)->row_array();
        $groupid=$datauser['group_id'];
        $nik    =$datauser['nik'];
        //cari data surat
        $suratkeluar=$this->model_mobile->get_datasurat($kode)->row_array();
        $jenissurat=$suratkeluar['jenissurat'];
        $sifatsurat=$suratkeluar['sifatsurat'];
        $isiringkasan=$suratkeluar['isiringkasan'];
        $validasiparaf =$suratkeluar['validasiparaf'];
        $jmlparaf =$this->db->where('kodesuratutama',$kode)->count_all_results('t_surat_paraf');
        $jmlsdhparaf =$this->db->where('kodesuratutama',$kode)->where('validasiparaf',1)->count_all_results('t_surat_paraf');
        $hsl='';
        $kodeencode = base64_encode($kode);
        if($jenissurat=='1'){
            $hsl.='<object type="text/html" style="float:left;width:100%;height:600px;background-color:white;" data="'.$url.'/modules/report/label/memodinaskeluar_view.php?idp='.$kodeencode.'"></object>';
        
        }
        if($jenissurat=='2'){
            $hsl.='<object type="text/html" style="float:left;width:100%;height:600px;background-color:white;" data="'.$url.'/modules/report/label/memosuratkeluar_view.php?idp='.$kodeencode.'"></object>';
        
        }
        if($jenissurat=='3'){
            $hsl.='<object type="text/html" style="float:left;width:100%;height:600px;background-color:white;" data="'.$url.'/modules/report/label/memosuratkeluaratas_view.php?idp='.$kodeencode.'"></object>';
        
        }
        $hsl.='<a href="javascript:;" class="btn btn-danger btn-block" onclick="close_view();"><i class="fa fa-times pull-right"></i> Close</a>';
        $hsl.='<a href="javascript:;" class="btn btn-primary btn-block" onclick="riwayat(\'' .$kode. '\');"><i class="fa fa-search pull-right"></i> Riwayat Perubahan</a>';
		$hsl.='<a href="javascript:;" class="btn btn-success btn-block" onclick="listpemaraf(\'' .$kode. '\');"><i class="fa fa-list pull-right"></i> List pemaraf</a>';
        if($validasiparaf=='0'){
            if($jmlparaf==$jmlsdhparaf){
                $hsl.='<a href="javascript:paraf(\'' .$kode. '\');" class="btn btn-warning btn-block" ><i class="fa fa-pencil-square pull-right"></i> Edit Format</a>';
                $hsl.='<a href="javascript:validasi(\'' .$kode. '\');" class="btn btn-warning btn-block" ><i class="fa fa-gears (alias) pull-right"></i> Validasi</a>';
            }else{
                $hsl.='<a href="#" class="btn btn-warning btn-block" ><i class="fa fa-gears (alias) pull-right"></i> Menunggu pemaraf</a>';
            }
        }else{
            $hsl.='<a href="javascript:;" class="btn btn-success btn-block" ><i class="fa fa-check-square-o pull-right"></i> Sudah Divalidasi</a>';
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($hsl));
    }
	public function get_view_masuk($kode,$user){
		// update status menjadi satu artinya surat sudah dilihat
		$data=array('statusview'=>1);
        $where=array('kodesuratmasuk'=>$kode);
        $update = $this->model_mobile->valid_masuk($where,$data);
		
		$url =$this->link;
		$hsl='';
		//mencari data user
        $datauser =$this->model_mobile->get_user($user)->row_array();
        $groupid=$datauser['group_id'];
        $nik    =$datauser['nik'];
		//cari data surat masuk
        $suratmasuk=$this->model_mobile->get_datasuratmasuk($kode)->row_array();
		
		$jenissurat=$suratmasuk['jenissurat'];
        $sifatsurat=$suratmasuk['sifatsurat'];
        $isiringkasan=$suratmasuk['isiringkasan'];
        $size =$suratmasuk['size'];
        $validasi =$suratmasuk['validasi'];
        $status =$suratmasuk['status'];
        $empniktujuan =$suratmasuk['empniktujuan'];
        $kodeprogressurat =$suratmasuk['kodeprogressurat'];
        $kodesifatsurat =$suratmasuk['kodesifatsurat'];
		$kodesuratmasuk =$suratmasuk['kodesuratmasuk'];
		$kodeencode = base64_encode($kode);
		
		$db2=$this->load->database('db2',true);
		$sql ="SELECT * from structdisp where empnik='$user'";
		$query = $db2->query($sql);
		//jika pegawai
		if($query->num_rows()>0){
			$data =$db2->get_where('structdisp',array('empnik'=>$user))->row_array();
			$jf = substr_count($data['emppersk'],"F");
			if($jf>0){
			//bisa tidak bisa disposisi
			$posisidispo = 0;
			}else{			
				//bisa disposisi
				$posisidispo = 1;
			}
		}else{
			$posisidispo = 1;
		}
		
		//kalau sifat surat rahasia
		
		if($sifatsurat=='3'){
            if($groupid!='3'){
                if($jenissurat=='1'){
                    $hsl.='<object type="text/html" style="float:left;width:100%;height:500px;background-color:white;" data="'.$url.'/modules/report/label/memodinasmasuk_view.php?idp='.$kodeencode.'"></object>';
                }
                if($jenissurat=='2'){
                    //$hsl.='<object type="text/html" style="float:left;width:100%;height:500px;background-color:white;" data="'.$url.'/arsip/modules/surat/suratmasuk/download.php?idp='.$kodeencode.'"></object>';
                    $hsl.='Untuk membaca surat eksternal silahkan download lampiran !';
                }
                if($jenissurat=='3'){
                    $hsl.='<object type="text/html" style="float:left;width:100%;height:500px;background-color:white;" data="'.$url.'/modules/report/label/memodinasmasukatas`_view.php?idp='.$kodeencode.'"></object>';
                }
            }else{
                //$hsl.=$isiringkasan;
				$hsl.='<object type="text/html" style="float:left;width:100%;height:500px;background-color:white;" data="'.$url.'/modules/report/label/memodinasmasukatas_view.php?idp='.$kodeencode.'"></object>';
            }
        }
		//sifat surat biasa atau sifat surat tidak ada karena surat di create disurat masuk
		if($sifatsurat=='2'||$sifatsurat=='1'||$sifatsurat==''){
			//$hsl.=$jenissurat;
			
			if($jenissurat=='1'){
				$hsl.='<object type="text/html" style="float:left;width:100%;height:500px;background-color:white;" data="'.$url.'/modules/report/label/memodinasmasuk_view.php?idp='.$kodeencode.'"></object>';
			}
			if($jenissurat=='2'){
				if($size!='0'){
					$hsl.='<p align="center"><strong>Untuk membaca surat eksternal silahkan download lampiran !</strong></p>';
				}else{
					//$hsl.=$isiringkasan;
					$hsl.='<object type="text/html" style="float:left;width:100%;height:500px;background-color:white;" data="'.$url.'/modules/report/label/memodinasmasukatas_view.php?idp='.$kodeencode.'"></object>';
				}
			}
			if($jenissurat=='3'){
                    $hsl.='<object type="text/html" style="float:left;width:100%;height:500px;background-color:white;" data="'.$url.'/modules/report/label/memodinasmasukatas_view.php?idp='.$kodeencode.'"></object>';
            }
		}
		//jika bukan sekertaris
		if($groupid=='1'||$groupid=='2'||$groupid=='4'||$groupid=='5'||$groupid=='7'){
			//====================================jika belum divalidasi==========================//
			if($validasi=='0'){
				$hsl.='<a href="javascript:;" class="btn btn-default btn-block" ><i class="fa fa-check-square-o pull-right"></i> Menunggu Divalidasi</a>';
			}else{
				if($posisidispo==1){
					//jika kodesifat 1 asli 2 struktural
					if($kodesifatsurat=='0'||$kodesifatsurat=='1'||$kodesifatsurat=='3'||$kodesifatsurat=='4'){
						if(($groupid=='6'&&$status!='3')||($groupid=='7'&&$status!='3')){
							$hsl.='<a href="javascript:;" class="btn btn-success btn-block" onclick="tambah_disposisi(\'' .$kode. '\');"><i class="fa fa-plus pull-right"></i> Tambah Disposisi</a>';
						}else{
							//=========================functional group 4 bisa disposisi===============//
							if($groupid=='4'&&$status!='3'){
								$hsl.='<a href="javascript:;" class="btn btn-success btn-block" onclick="tambah_disposisi(\'' .$kode. '\');"><i class="fa fa-plus pull-right"></i> Tambah Disposisi</a>';
							}else{
								//====================jika mempunyai bawahan akan bisa disposisi================//
								$db2=$this->load->database('db2',true);
								$sql ="SELECT * from structdisp where dirnik='$empniktujuan' and empnik!='$empniktujuan'";
								$query = $db2->query($sql);
								if($query->num_rows()>0 && $kodeprogressurat==''){
									$hsl.='<a href="javascript:;" class="btn btn-success btn-block" onclick="tambah_disposisi(\'' .$kode. '\');"><i class="fa fa-plus pull-right"></i> Tambah Disposisi</a>';
								}
								
							}
							
						}
					}
					if($kodeprogressurat=='3'){
						$hsl.='<a href="javascript:;" class="btn btn-default btn-block" ><i class="fa fa-power-off pull-right"></i> Sudah Diclose</a>';
					}
					if($kodesifatsurat=='2'){
						$hsl.='<a href="javascript:;" class="btn btn-success btn-block" onclick="tambah_disposisi(\'' .$kode. '\');"><i class="fa fa-plus pull-right"></i> Tambah Disposisi</a>';
					}
				}else{
					// tidak bisa disposisi
				}
			
				
			}
		}else{
			//jika sekertaris
			//====================================jika belum divalidasi==========================//
			if($validasi=='0'){
				$hsl.='<a href="javascript:;" class="btn btn-default btn-block" ><i class="fa fa-check-square-o pull-right"></i> Menunggu Divalidasi</a>';
			}else{
				//jika sifat surat biasa sekertaris bisa disposisi
				if($sifatsurat=='1'||$sifatsurat=='2'){
					if($kodesifatsurat=='0'||$kodesifatsurat=='1'||$kodesifatsurat=='2'||$kodesifatsurat=='3'||$kodesifatsurat=='4'){
						//====================jika mempunyai bawahan akan bisa disposisi================//
						$db2=$this->load->database('db2',true);
                        $sql ="SELECT * from structdisp where dirnik='$empniktujuan' and empnik!='$empniktujuan'";
                        $query = $db2->query($sql);
						if($query->num_rows()>0 && $kodeprogressurat==''){
                            $hsl.='<a href="javascript:;" class="btn btn-success btn-block" onclick="tambah_disposisi(\'' .$kode. '\');"><i class="fa fa-plus pull-right"></i> Tambah Disposisi</a>';
                        }
						
					}
					if($status=='3'){
                        $hsl.='<a href="javascript:;" class="btn btn-default btn-block" ><i class="fa fa-power-off pull-right"></i> Sudah Diclose</a>';
                    }
					
				}else{
                     if($status=='3'){
                        $hsl.='<a href="javascript:;" class="btn btn-default btn-block" ><i class="fa fa-power-off pull-right"></i> Sudah Diclose</a>';
                    }
                }
			}
		}//akhir else atau kondisi jika sekretaris
		//===================================progres================================//
		//jika bukan sekertaris
		if($groupid=='1'||$groupid=='2'||$groupid=='4'||$groupid=='5'||$groupid=='7'){
			//================Kondisi Logika Internal============================//
			if($jenissurat=='1' || $jenissurat=='3'){
				if($validasi=='0'){
                $hsl.='<a href="javascript:;" class="btn btn-default btn-block" ><i class="fa fa-check-square-o pull-right"></i> Menunggu Divalidasi</a>';
				}else{
					
					//jikas sifatnya sli atau struktural
					if($kodesifatsurat=='1'||$kodesifatsurat=='2'||$kodesifatsurat=='3'||$kodesifatsurat=='4'){
						$sql= "select * from t_surat_deployment where kodesuratatas='$kodesuratmasuk'";
						$query = $this->db->query($sql);
						$cek = $query->num_rows();
						//jika sudah disposisi namun masih kosong disposisinya
						if($query->num_rows()>=1){
							//jika sudah disposisi
							if($status=='1'){
								//=====================Sudah Input Progress===================//
								if($kodeprogressurat==''|| $kodeprogressurat==0){
									$hsl.='<a href="javascript:;" class="btn btn-primary btn-block" onclick="list_progress(\'' .$kode. '\');"><i class="fa fa-check pull-right"></i> Sudah Di disposisi</a>';
								}
								//==================jika ada revisi progres====================//
								if($kodeprogressurat=='1'||$kodeprogressurat=='2'){
									if($user==$empniktujuan){
										$hsl.='<a href="javascript:;" class="btn btn-success btn-block" onclick="input_progress(\'' .$kode. '\');"><i class="fa fa-plus pull-right"></i> Input Progress</a>';
									}else{
										$hsl.='';
									}
								}
							}
							//jika sudah memberikan progres
							if($status=='2'){
								//=====================Sudah Input Progress===================//
								if($kodeprogressurat!=''){
									$hsl.='<a href="javascript:;" class="btn btn-primary btn-block" onclick="input_progress(\'' .$kode. '\');"><i class="fa fa-check pull-right"></i>Sudah Input Progress</a>';
								}
								
							}
							//jika sudah close progres
							if($status=='3'){
								//=====================Sudah Input Progress===================//
								if($kodeprogressurat!=''){
									$hsl.='<a href="javascript:;" class="btn btn-default btn-block" ><i class="fa fa-power-off pull-right"></i> Sudah Diclose</a>';
								}
							}
						}else{
							//jika status 0 belum disposisi
							if($status==0){
								//===================Belum Input Progress======================//
								if($kodeprogressurat==0){
									if($user==$empniktujuan){
										$hsl.='<a href="javascript:;" class="btn btn-success btn-block" onclick="input_progress(\'' .$kode. '\');"><i class="fa fa-plus pull-right"></i> Input Progress</a>';
									}else{
										$hsl.='';
									}
									
								}
								//===================Sudah close Progress=======================//
								if($kodeprogressurat=='3'){
									$hsl.='<a href="javascript:;" class="btn btn-default btn-block" ><i class="fa fa-power-off pull-right"></i> Sudah Diclose</a>';
								}
							}
							if($status=='1'){
								if($user==$empniktujuan){
									if($user==$empniktujuan){
										$hsl.='<a href="javascript:;" class="btn btn-success btn-block" onclick="input_progress(\'' .$kode. '\');"><i class="fa fa-plus pull-right"></i> Input Progress</a>';
									}else{
										$hsl.='';
									}
								}else{
									$hsl.='';
								}
							}
							//jika sudah memberikan progres
							if($status=='2'){
								//=====================Sudah Input Progress===================//
								if($kodeprogressurat!=''){
									$hsl.='<a href="javascript:;" class="btn btn-primary btn-block" onclick="input_progress(\'' .$kode. '\');"><i class="fa fa-check pull-right"></i>Sudah Input Progress</a>';
								}
							}
							//jika sudah close progres
							if($status=='3'){
								//=====================Sudah Input Progress===================//
								if($kodeprogressurat!=''){
									$hsl.='<a href="javascript:;" class="btn btn-default btn-block" ><i class="fa fa-power-off pull-right"></i> Sudah Diclose</a>';
								}
							}
							
						}
					}else{
						$hsl.='';
					}
					
				}
			}
			//==================================End Kondisi Logika Internal==========================//
			//====================================Kondisi Logika Eksternal===========================//
			if($jenissurat=='2'){
				//status untuk status surat
				//kodsifatsurat 1 asli, 2 tembusan, 3 struktural, 4 fungsional
				//kodeprogress untuk mengetahui progres surat
				//jika belum ada progres
				//jika status 0 belum disposisi
				if($status=='0'){
					//===================Belum Input Progress======================//
					if($kodeprogressurat==0){
						if($user==$empniktujuan){
							$hsl.='<a href="javascript:;" class="btn btn-success btn-block" onclick="input_progress(\'' .$kode. '\');"><i class="fa fa-plus pull-right"></i> Input Progress</a>';
						}else{
							$hsl.='';
						}
					}
					//===================Sudah close Progress=======================//
					if($kodeprogressurat==3){
						$hsl.='<a href="javascript:;" class="btn btn-default btn-block" ><i class="fa fa-power-off pull-right"></i> Sudah Diclose</a>';
					}
				}
				//jika sudah disposisi
				if($status=='1'){
					//=====================Sudah Input Progress===================//
					if($kodeprogressurat==''){
						$sql= "select * from t_surat_deployment where kodesuratatas='$kodesuratmasuk'";
						$query = $this->db->query($sql);
						if($query->num_rows()>0){
							$hsl.='<a href="javascript:;" class="btn btn-primary btn-block" onclick="list_progress(\'' .$kode. '\');"><i class="fa fa-check pull-right"></i> Sudah Di disposisi</a>';
						}else{
							$hsl.='<a href="javascript:;" class="btn btn-default btn-block" ><i class="fa fa-plus-square-o pull-right"></i> Menunggu Disposisi</a>';
						}
					}
					//==================jika ada revisi progres====================//
					if($kodeprogressurat=='1'||$kodeprogressurat=='2'){
						if($user==$empniktujuan){
							$hsl.='<a href="javascript:;" class="btn btn-success btn-block" onclick="input_progress(\'' .$kode. '\');"><i class="fa fa-plus pull-right"></i> Input Progress</a>';
						}else{
							$hsl.='';
						}
					}
				}
				//jika sudah memberikan progres
				if($status=='2'){
					//=====================Sudah Input Progress===================//
					if($kodeprogressurat!=''){
						$hsl.='<a href="javascript:;" class="btn btn-primary btn-block" onclick="input_progress(\'' .$kode. '\');"><i class="fa fa-check pull-right"></i>Sudah Input Progress</a>';
					}
				}
				//jika sudah close progres
				if($status=='3'){
					//=====================Sudah Input Progress===================//
					if($kodeprogressurat!=''){
						$hsl.='<a href="javascript:;" class="btn btn-default btn-block" ><i class="fa fa-power-off pull-right"></i> Sudah Diclose</a>';
					}
				}
			}
			//==================================End Kondisi Logika Eksternal=========================================//
		}else{
			//jika sekertaris
			//====================================jika belum divalidasi==========================//
			if($validasi==0){
				$hsl.='';
			}else{
				if($status=='0'){
					if($sifatsurat=='1'||$sifatsurat=='2'){
						if($user==$empniktujuan){
							$hsl.='<a href="javascript:;" class="btn btn-success btn-block" onclick="input_progress(\'' .$kode. '\');"><i class="fa fa-plus pull-right"></i> Input Progress</a>';
						}else{
							$hsl.='';
						}
					}else{
						// jika sifat surat penting rahasia tidak bisa
						$hsl.='';
					}
				}
				if($status=='1'){
					if($sifatsurat=='1'||$sifatsurat=='2'){
						$hsl.='<a href="javascript:;" class="btn btn-primary btn-block" onclick="list_progress(\'' .$kode. '\');"><i class="fa fa-check pull-right"></i> Sudah Di disposisi</a>';
					}else{
					$hsl.='';
					}
				}
				if($status=='3'){
					//=====================Sudah Input Progress===================//
					if($kodeprogressurat!=''){
						$hsl.='<a href="javascript:;" class="btn btn-default btn-block" ><i class="fa fa-power-off pull-right"></i> Sudah Diclose</a>';
					}
					
				}
				
			}
		}
		
		$db2=$this->load->database('db2',true);
		$sql ="SELECT * from structdisp where dirnik='$empniktujuan' and empnik!='$empniktujuan'";
		$query = $db2->query($sql);
		// jika mempunyai bawahan dan bisa disposisi maka sembunyikan disposisi
		if($query->num_rows()>0){
			$hsl.='';
		}else{
			//jika tidak mempunyai bawahan
			$hsl.='<a href="javascript:;" class="btn btn-warning btn-block" onclick="cetak_disposisi(\'' .$kode. '\');"><i class="fa fa-search pull-right"></i> View Disposisi</a>';
		}
		
		$hsl.='<a href="javascript:;" class="btn btn-danger btn-block" onclick="close_viewmasuk();"><i class="fa fa-times pull-right"></i> Close</a>';
		
		
		
		$this->output->set_content_type('application/json')->set_output(json_encode($hsl));
	}
	public function cetak_disposisi($kode){
		//cari di deployment
		$dtm = $this->db->get_where('t_surat_deployment', ["kodesuratbawah" => $kode])->row();
		//echo $dtm->kodesuratutama;
		//die();
		if($dtm->kodesuratutama !=""){
		$depan = substr($dtm->kodesuratutama,0,2);		
		}else{
			$dtm = $this->db->get_where('t_surat_deployment', ["kodesuratatas" => $kode])->row();
			$depan = substr($dtm->kodesuratutama,0,2);
		}
		if($depan=='SK'){
			$surat = $this->db->get_where('t_suratkeluar_m', ["kodesuratkeluar" =>$dtm->kodesuratutama])->row();
			$suratnya = $surat->kodesuratkeluar;
			$sql="SELECT COUNT(kodesuratatas) as a, kodesuratatas FROM `t_surat_deployment` where kodesuratutama='".$dtm->kodesuratutama."' and kodesuratatas!='".$dtm->kodesuratutama."' group by kodesuratatas order by kodesuratatas asc";
			$asal = "[".$surat->empnik_asal."] ".$surat->empname_asal."";
		}else{
			$surat = $this->db->get_where('t_suratmasuk_m', ["kodesuratmasuk" =>$dtm->kodesuratutama])->row();
			$suratnya = $surat->kodesuratmasuk;
			$sql="SELECT COUNT(kodesuratatas) as a, kodesuratatas FROM `t_surat_deployment` where kodesuratutama='".$dtm->kodesuratutama."' group by kodesuratatas order by kodesuratatas asc";
			$asal = "[".$surat->empnamesurat."] ".$surat->emp_cskt_ltext_surat."";
		}
		$hsl='';
		$hsl.='<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
		<tr align="center">
			<td class="judul">
				<!--img src="../../../../images/hitam.jpg" width="150" height="80" /-->
				<br/>
				<b>LEMBAR DISPOSISI</b>
			</td>
		</tr>
		</table>';
		$hsl.='<p> &nbsp; </p>
			<table data-role="table" class="ui-body-d ui-shadow table-stripe ui-responsive ui-table ui-table-columntoggle">
				<tr align="center">
					<td width="20%" colspan="2"> INDEX </td>
					<td width="30%"> KODE KLASIFIKASI </td>
					<td width="20%"> TANGGAl/ NO SURAT </td>
					<td width="20%"> SIFAT SURAT </td>
				</tr>
				<tr align="center">
					<td colspan="2">'.$suratnya.'</td>
					<td>'.$surat->kodeklasifikasi.'</td>
					<td>'.$surat->tanggalsurat.'/'.$surat->nosuratkeluar.'</td>
					<td>'.$this->ss($surat->sifatsurat).'</td>
				</tr>
				<tr>
					<td width="18%"> ASAL SURAT </td>
					<td width="2%" align="center"> : </td>
					<td colspan="3">'.$asal.' </td>
				</tr>
				<tr>
					<td> PERIHAL </td>
					<td align="center"> : </td>
					<td colspan="3">'.$surat->isiringkasan.'</td>
				</tr>';
				$no=1;
				$rs = $this->db->query($sql);
				foreach($rs->result() as $row){
					
					$surm = $this->db->get_where('t_suratmasuk_m', ["kodesuratmasuk" =>$row->kodesuratatas])->row();
					
					$hsl.='<tr>
							<td> DISPOSISI KE '.$no.' </td>
							<td align="center"> : </td>
							<td colspan="3"> ['.$surm->empniktujuan.'] '.$surm->empnametujuan.'</td>
						</tr>
						<tr>
							<td> TARGET PENYELESAIAN </td>
							<td align="center"> : </td>
							<td colspan="3"> '.$surm->tanggalpenyelesaian.' </td>
						</tr>
						<tr>
							<td> CATATAN </td>
							<td align="center"> : </td>
							<td colspan="3"> '.$surm->catatan.' </td>
						</tr>
						<tr>
							<td> KEPADA </td>
							<td align="center"> : </td>
							<td> [ NIK ] NAMA</td>
							<td></td>
							<td></td>
					</tr>';
					$no++;
					$nom=1;
					$sql2="SELECT * FROM t_surat_deployment where kodesuratatas='".$row->kodesuratatas."'";
					$query2 = $this->db->query($sql2);
					foreach($query2->result() as $row2){
						$surm2 = $this->db->get_where('t_suratmasuk_m', ["kodesuratmasuk" => $row2->kodesuratbawah])->row();
						$hsl.='<tr>
								<td></td>
								<td align="center">'.$nom.'</td>
								<td align="center">['.$surm2->empniktujuan.'] '.$surm2->empnametujuan.'</td>
								<td align="center"></td>
								<td align="center"></td>
							   </tr>';
						$nom++;
							
						}
				}
			$hsl.='</table>';
			$hsl.='<br><a href="javascript:;" class="btn btn-danger btn-block" onclick="close_disposisi();"><i class="fa fa-times pull-right"></i> Close</a>';
		$this->output->set_content_type('application/json')->set_output(json_encode($hsl));
	}
	public function ss($index = 1){
    $ss = array(1 => 'Biasa', 2 => 'Penting', 3 => 'Rahasia');
    return $ss[$index];
	}
    /*public function cetak_disposisi($kode){
		//cari di deployment
		$deploy = $this->db->get_where('t_surat_deployment', ["kodesuratbawah" => $kode])->row();
		//print_r($deploy);
		//die();
		//cari kodesuratatas di suratmasuk detpail
		$smasukdetail = $this->db->get_where('t_suratmasuk_d', ["kodesuratmasuk" => $kode])->row();
		
		$kodesuratmasukatas = $smasukdetail->kodesuratmasukatas;
		//cari atas yang mendisposisi
		$this->db->select('t_surat_deployment.*,t_suratmasuk_m.empnamesurat,t_suratmasuk_m.tanggalpenyelesaian,m_pesan.pesan');
		$this->db->join('t_suratmasuk_m','t_suratmasuk_m.kodesuratmasuk=t_surat_deployment.kodesuratbawah');
		$this->db->join('m_pesan','m_pesan.kodepesan=t_suratmasuk_m.catatanatasan','left');
		$atasanpendisposisi = $this->db->get_where('t_surat_deployment', ["kodesuratatas" => $kodesuratmasukatas])->row();
		//echo $atasanpendisposisi;
		//die();
		// cari kodesuratbawah di t_deployement join dengan surat_masuk_m
		$this->db->join('t_suratmasuk_m','t_suratmasuk_m.kodesuratmasuk=t_surat_deployment.kodesuratbawah');
		$this->db->join('m_pesan','m_pesan.kodepesan=t_suratmasuk_m.catatanatasan','left');
		$disposisi = $this->db->get_where('t_surat_deployment', ["kodesuratatas" => $kodesuratmasukatas])->result();
		
        $url =$this->link;
		//$url ='https://192.168.43.2/arsip';
        $kodeencode = base64_encode($kode);
        $hsl='';
		$hsl.='<table data-role="table" class="ui-body-d ui-shadow table-stripe ui-responsive ui-table ui-table-columntoggle">
		<tr><td>Disposisi Dari :</td><td colspan="2">'.$atasanpendisposisi->empnamesurat.'</td></tr>
		<tr><td>Kepada :</td><td colspan="2"></td></tr>';
		$no=0;
		foreach ($disposisi as $row){
			$no++;
			$hsl.='<tr><td>'.$no.'</td><td>'.$row->empnametujuan.'</td><td>'.$row->pesan.'</td></tr>';
		}
		
		$hsl.='<tr><td>Target</td><td colspan="2">'.$atasanpendisposisi->tanggalpenyelesaian.'</td></tr>
		<tr><td>Catatan</td><td colspan="2">'.$atasanpendisposisi->catatan.'</td></tr>
		</table>';
        //$hsl.='<object type="text/html" style="float:left;width:100%;height:500px;background-color:white;" data="'.$url.'/modules/surat/suratmasuk/ajax/viewdisposisi.php?idp='.$kodeencode.'"></object>';
        $hsl.='<br><a href="javascript:;" class="btn btn-danger btn-block" onclick="close_disposisi();"><i class="fa fa-times pull-right"></i> Close</a>';
        $this->output->set_content_type('application/json')->set_output(json_encode($hsl));
    }*/
    public function carikaryawan(){
        $hsl='';
        //$hsl.='<ul class="nav nav-tabs"><li class="active"><a href="#default-tab-1" data-toggle="tab">Struktur Karyawan</a></li><li class=""><a href="#default-tab-2" data-toggle="tab">Struktur Direksi Dan Komisaris</a></li></ul>';
        //$hsl.='<div class="tab-content"><div class="tab-pane fade active in" id="default-tab-1"><table class="table table-email" id="daftar-karyawan"><thead><tr><th data-priority="persist">Nik</th><th data-priority="1">Nama</th><th data-priority="2">Cos Center</th><th data-priority="3">Jabatan</th></tr></thead></table></div><div class="tab-pane fade" id="default-tab-2"><table class="table table-email" id="daftar-direksi"><thead><tr><th data-priority="persist">Nik</th><th data-priority="1">Nama</th><th data-priority="2">Cos Center</th><th data-priority="3">Jabatan</th></tr></thead></table></div></div>';
        $hsl.='<a href="javascript:;" class="btn btn-primary btn-block" onclick="struk_karyawan();"><i class="fa fa-sitemap pull-right"></i> Sturktur Karyawan</a>';
        $hsl.='<a href="javascript:;" class="btn btn-success btn-block" onclick="struk_direksi();"><i class="fa fa-sitemap pull-right"></i> Sturktur Direksi Dan Komisaris</a>';
		//$hsl.='<a href="javascript:;" class="btn btn-warning btn-block" onclick="struk_kar_pelimpahan();"><i class="fa fa-sitemap pull-right"></i> Sturktur Karyawan Pelimpahan</a>';
        $hsl.='<table class="table table-email" id="daftar-karyawan"><thead><tr><th data-priority="persist">Nik</th><th data-priority="1">Nama</th><th data-priority="2">Cos Center</th><th data-priority="3">Jabatan</th></tr></thead></table>';
        //$hsl.='<div id="karyawan"><table class="table table-email" id="daftar-karyawan"><thead><tr><th data-priority="persist">Nik</th><th data-priority="1">Nama</th><th data-priority="2">Cos Center</th><th data-priority="3">Jabatan</th></tr></thead></table></div>';
        //$hsl.='<div id="direksi"><table class="table table-email" id="daftar-karyawan"><thead><tr><th data-priority="persist">Nik</th><th data-priority="1">Nama</th><th data-priority="2">Cos Center</th><th data-priority="3">Jabatan</th></tr></thead></table></div>';
        $hsl.='<a href="javascript:;" class="btn btn-danger btn-block" onclick="close_cari_karyawan();"><i class="fa fa-times pull-right"></i> Close</a>';
        $this->output->set_content_type('application/json')->set_output(json_encode($hsl));
    }
    public function get_view_progress($kode,$user){
        $suratmasuk=$this->model_mobile->get_datasuratprogress($kode)->row_array();
        $hsl='';
        $hsl.='<form id="form_progress" class="form-horizontal" data-parsley-validate="true" cellspacing="0" width="100%" enctype="multipart/form-data">';
        $hsl.='<div class="col-md-12"><div class="form-group"><label for="kodesuratmasuk">Kode Surat Masuk</label><input value='.$suratmasuk['kodesuratmasuk'].' style="width:100%;" type="text" name="kodesuratmasuk" id="kodesuratmasuk" placeholder="Kode Surat Masuk" readonly="true"></div></div>';
        $hsl.='<div class="col-md-12"><div class="form-group"><label for="nosuratmasuk">Nomor Surat Masuk</label><input value='.$suratmasuk['nosuratmasukdivisi'].' style="width:100%;" type="text" name="nosuratmasuk" id="nosuratmasuk" placeholder="Nomor Surat Masuk" readonly="true"></div></div>';
        $hsl.='<div class="col-md-12"><div class="form-group"><label for="status">Status Progres</label><input value="'.$suratmasuk['progressurat'].'" style="width:100%;" type="text" name="status" id="status" placeholder="Status Progres" readonly="true"></div></div>';
        $hsl.='<div class="col-md-12"><div class="form-group"><label for="Progres">Status Progres</label><input value="'.$suratmasuk['progress'].'" style="width:100%;" type="text" name="progress" id="progress" placeholder="Progres" readonly="true"></div></div>';
        $hsl.='</form>';
        $hsl.='<a href="javascript:;" class="btn btn-primary btn-block" onclick="nilaiprogress(\''.$suratmasuk['kodesuratmasuk'].'\');"><i class="fa fa-comments"></i></a>';
        $hsl.='<a href="javascript:;" class="btn btn-warning btn-block" onclick="tutup_progress(\'' .$suratmasuk['kodesuratmasuk']. '\',\'' .$suratmasuk['nosuratmasukdivisi']. '\');"><i class="fa fa-power-off pull-right"></i> Tutup Progres</a>';
        
        $this->output->set_content_type('application/json')->set_output(json_encode($hsl));
        
    }
    public function tutup_progress($kode,$user){
			
        $data_d=array('kodeprogressurat'=>"3",'kodesifatsurat'=>"3",'kodeprogress'=>"4");
        $data_m=array('status'=>"3",'kodeprogressurat'=>"3",'kodesifatsurat'=>"3");
        $where = array('kodesuratmasuk'=>$kode);
        $progress = $this->model_mobile->tutup_progress($where,$data_d,$data_m);
        if($progress){
			$komenprogress=$this->model_mobile->get_komenprogress($kode)->row_array();
			$playerid=array($komenprogress['playerid']);
			$pesan="Surat no ".$komenprogress['nosuratmasukdivisi']." sudah di close oleh bapak / ibu ".$komenprogress['empnamesurat']." (".$komenprogress['empniksurat'].")";
			$heading="Tutupprogress";
			$kirimkedisposisi=$this->send_notification($playerid,$pesan,$heading);
			
            $output ='1';   
        }else{
            $output ='0';
        }
		$datalog = array('log_time'=>date('Y-m-d H:i:s'),'log_user'=>$user,'log_aksi'=>'tutup progress','log_url'=>site_url(uri_string()),'log_item'=>'Notifikasi progress','kode'=>$kode,'log_ip'=>$this->get_client_ip(),'log_device'=>$_SERVER['HTTP_USER_AGENT']);
	$this->model_mobile->log_activity($datalog);
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
    }
    public function input_progress($kode,$user){
        $sm_d =$this->model_mobile->get_pr($kode)->row_array();
		$detailsm =$this->model_mobile->get_suratmasuk($kode)->row_array();
        
		if($sm_d['tanggalterimasurat']=='' || $sm_d['tanggalterimasurat']=='0000-00-00'){
			$sm_d['tanggalterimasurat'] = date("Y-m-d");
		}else{
			$sm_d['tanggalterimasurat'] = $sm_d['tanggalterimasurat'];
		}
        $hsl='';
		
        $hsl.='<form id="fminput_progress" class="form-horizontal" data-parsley-validate="true" cellspacing="0" width="100%" enctype="multipart/form-data">';
        $hsl.='<div class="col-md-12"><div class="form-group"><label for="keterangan">Tanggal terima</label><input class="form-control"  style="width:100%;" type="text" name="tanggalterimasurat" id="tanggalterimasurat" value="'.$sm_d['tanggalterimasurat'].'" readonly></div></div>';
		$hsl.='<div class="col-md-12"><div class="form-group"><label for="tanggalpenyelesaian">Target penyelesaian</label><input class="form-control"  style="width:100%;" type="text" name="tanggalpenyelesaian" id="tanggalpenyelesaian" value="'.$detailsm['tanggalpenyelesaian'].'" readonly></div></div>';
        $hsl.='<div class="col-md-12"><div class="form-group"><label for="catatan">Catatan</label><input class="form-control"  style="width:100%;" type="text" name="catatan" id="catatan" value="'.$detailsm['catatan'].'" readonly></div></div>';
        
        $hsl.='<div class="col-md-12"><div class="form-group"><label for="status">Status Progres</label>'.$this->status_progress().'</div></div>';
        $hsl.='<div class="col-md-12"><div class="form-group"><label for="status">Progress Pekerjaan</label>'.$this->progress_pekerjaan().'</div></div>';
        $hsl.='<div class="col-md-12"><div class="form-group"><label for="keterangan">Keterangan</label><input class="form-control"  style="width:100%;" type="text" name="keterangan" id="keterangan" placeholder="Keterangan"></div></div>';
        $hsl.='<div class="col-md-6"><div class="form-group"><a href="javascript:;" class="btn btn-success btn-block" onclick="simpan_progress(\'' .$kode. '\');"><i class="fa fa-check pull-right"></i> Simpan</a></div></div>';
        $hsl.='<div class="col-md-6"><div class="form-group"><a href="javascript:;" class="btn btn-warning btn-block" onclick="close_disposisi();"><i class="fa fa-minus pull-right"></i>Batal</a></div></div>';
        $hsl.='</form>';
        
        $this->output->set_content_type('application/json')->set_output(json_encode($hsl));
    }
    public function progress_pekerjaan(){
        $hsl='';
        $sqlprogres = "select * from m_progress";
        $queryprogres=$this->db->query($sqlprogres);
        $hsl.='<select class="form-control selectpicker" data-size="10" data-live-search="true" data-style="btn-white" name="progress" id="progress">';
        foreach($queryprogres->result() as $rowprogres){$hsl.='<option value='.$rowprogres->kodeprogress.'>'.$rowprogres->progress.'</option>';}
        $hsl.='</select>';
        
        return $hsl;
    }
     public function status_progress(){
        $hsl='';
        $sqlprogres = "select * from m_progressurat";
        $queryprogres=$this->db->query($sqlprogres);
        $hsl.='<select class="form-control selectpicker" data-size="10" data-live-search="true" data-style="btn-white" name="statusprogress" id="statusprogress">';
        foreach($queryprogres->result() as $rowprogres){
            $hsl.='<option value='.$rowprogres->kodeprogressurat.'>'.$rowprogres->progressurat.'</option>';
            }
        $hsl.='</select>';
        
        return $hsl;
    }
    public function status_progress2($kodeprogress){
        $hsl='';
        $sqlprogres = "select * from m_progressurat";
        $queryprogres=$this->db->query($sqlprogres);
        $hsl.='<select class="form-control selectpicker" data-size="10" data-live-search="true" data-style="btn-white" name="statusprogress" id="statusprogress">';
        foreach($queryprogres->result() as $rowprogres){
            if($kodeprogress==$rowprogres->kodeprogressurat){
                $hsl.='<option value='.$rowprogres->kodeprogressurat.' selected>'.$rowprogres->progressurat.'</option>';
            }else{
                $hsl.='<option value='.$rowprogres->kodeprogressurat.'>'.$rowprogres->progressurat.'</option>';
            }
            
            }
        $hsl.='</select>';
        
        return $hsl;
    }
    public function simpan_progress($kode,$user){
        
        $tglterima =$this->input->post('tanggalterimasurat');
        $statusprogress = $this->input->post('statusprogress');
        $progress = $this->input->post('progress');
        $keterangan = $this->input->post('keterangan');
        $catatan = $this->input->post('catatan');
        
        $cekposisi = $this->model_mobile->get_dtm($kode)->row_array();
		
        //print_r($cekposisi);
		//die();
        $sql="SELECT t_surat_deployment.*,t_suratmasuk_m.kodesifatsurat FROM t_surat_deployment INNER JOIN t_suratmasuk_m ON t_surat_deployment.kodesuratbawah = t_suratmasuk_m.kodesuratmasuk
                where kodesuratutama ='".$cekposisi['kodesuratutama']."' and kodesifatsurat='1' or kodesuratutama ='".$cekposisi['kodesuratutama']."' and kodesifatsurat='3'";
        $query = $this->db->query($sql);
        //echo ($query->num_rows());
        //die();
		$notifprogress=$this->model_mobile->get_notifprogress($kode)->row_array();
		$playerid=array($notifprogress['playerid']);
		$pesan="Ada notifikasi progress baru";
        $heading="Progress";
        
		
        if($query->num_rows()>0){
            //update surat masuk status 2
            $this->db->update('t_suratmasuk_m',array('status'=>2),array('kodesuratmasuk'=>$kode));
            //hapus history progress
            $this->db->where(array('kodesuratmasuk'=>$kode));
            $this->db->delete('t_surat_history_progress');
            $where =array('kodesuratmasuk'=>$kode);
            $datahistory = array('kodesuratmasuk'=>$kode,'tanggalterimasurat'=>$tglterima,'kodeprogress'=>$progress,'kodeprogressurat'=>$statusprogress,'keterangan'=>$keterangan);
            $datad = array('tanggalupdate'=>date('Y-m-d'),'tanggalterimasurat'=>$tglterima,'kodeprogress'=>$progress,'kodeprogressurat'=>$statusprogress,'keterangan'=>$keterangan);
            $datam = array('kodeprogressurat'=>$statusprogress);
            $this->db->insert('t_surat_history_progress',$datahistory);
            $this->db->update('t_suratmasuk_d',$datad,$where);
            $this->db->update('t_suratmasuk_m',$datam,$where);
			
			$kirimkependisposisi=$this->send_notification($playerid,$pesan,$heading);
            
        }else{
            $awal = substr($cekposisi['kodesuratutama'], 0, 2);
            if($awal=='SK'){
                $where =array('kodesuratmasuk'=>$kode);
                $datahistory = array('kodesuratmasuk'=>$kode,'tanggalterimasurat'=>$tglterima,'kodeprogress'=>$progress,'kodeprogressurat'=>$statusprogress,'keterangan'=>$keterangan);
                $datad = array('tanggalupdate'=>date('Y-m-d'),'tanggalterimasurat'=>$tglterima,'kodeprogress'=>$progress,'kodeprogressurat'=>$statusprogress,'keterangan'=>$keterangan);
                $datam = array('kodeprogressurat'=>$statusprogress);
                $this->db->insert('t_surat_history_progress',$datahistory);
                $this->db->update('t_suratmasuk_d',$datad,$where);
                $sqlcekjum ="select * from t_surat_deployment where kodesuratutama='".$cekposisi['kodesuratutama']."'";
                $querycekjum = $this->db->query($sqlcekjum);
                if($querycekjum->num_rows()>1){
                    $datam = array('kodeprogressurat'=>$progress,'status'=>2,'tanggalpenyelesaian'=>date('Y-m-d'),'catatan'=>$catatan);
                    }else{
                    $datam = array('kodeprogressurat'=>3,'status'=>3,'tanggalpenyelesaian'=>date('Y-m-d'),'catatan'=>$catatan);
                        
                    }
                    $this->db->update('t_suratmasuk_m',$datam,$where);
                }else{
                    //update surat masuk status 2
                    $this->db->update('t_suratmasuk_m',array('status'=>2),array('kodesuratmasuk'=>$kode));
                    //hapus history progress
                    $this->db->where(array('kodesuratmasuk'=>$kode));
                    $this->db->delete('t_surat_history_progress');
                    
                    $datahistory = array('kodesuratmasuk'=>$kode,'tanggalterimasurat'=>$tglterima,'kodeprogress'=>$progress,'kodeprogressurat'=>$statusprogress,'keterangan'=>$keterangan);
                    $this->db->insert('t_surat_history_progress',$datahistory);
                    
                    $datad = array('tanggalupdate'=>date('Y-m-d'),'tanggalterimasurat'=>$tglterima,'kodeprogress'=>$progress,'kodeprogressurat'=>$statusprogress,'keterangan'=>$keterangan);
                    $where =array('kodesuratmasuk'=>$kode);
                    $this->db->update('t_suratmasuk_d',$datad,$where);
                    $datam = array('kodeprogressurat'=>$statusprogress);
                    $this->db->update('t_suratmasuk_m',$datam,$where);
            
                }
				$kirimkependisposisi=$this->send_notification($playerid,$pesan,$heading);
        }
        $output = 1;
		$datalog = array('log_time'=>date('Y-m-d H:i:s'),'log_user'=>$user,'log_aksi'=>'input progress','log_url'=>site_url(uri_string()),'log_item'=>'Surat Masuk','kode'=>$kode,'log_ip'=>$this->get_client_ip(),'log_device'=>$_SERVER['HTTP_USER_AGENT']);
	$this->model_mobile->log_activity($datalog);
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
        
    }
    public function list_progress($kode,$user){
        $listprogress = $this->model_mobile->get_listprogress($kode)->result();
        //print_r($listprogress);
        //die();
        $hsl='';
        $hsl.='<h2 align=center>View Progress</h2>';
        $hsl.='<table class="table table-striped">';
        $hsl.='<thead><tr><th>No</th><th>Nama ( Nik )</th><th>Progress</th><th>Komentar</th><th>Tutup Progress</th></tr></thead>';
        $no= 1;
        foreach($listprogress as $row){
            $koment='';
            $close ='';
             if($row->statusm==3){
                $koment = '<a href="javascript:;" class="btn btn-danger btn-block"><i class="fa fa-power-off"></i> Sudah di close</a>';
                }if($row->statusm==2){
                $koment = '<a href="javascript:;" class="btn btn-primary btn-block" onclick="nilaiprogress(\''.$row->kodesuratmasuk.'\');"><i class="fa fa-comments"></i></a>';
                    
                }
                if($row->statusm==1){
                    if($row->kodeprogressurat=='2'||$row->kodeprogressurat==''){
                        $koment='<a href="javascript:;" class="btn btn-warning btn-block"><i class="fa fa-spinner"></i></a>';
                    }
                    
                }
                if($row->statusm==0){
                    if($row->kodeprogressurat=='2'){
                     $koment = '<a href="javascript:;" class="btn btn-primary btn-block" onclick="nilaiprogress(\''.$row->kodesuratmasuk.'\');"><i class="fa fa-comments"></i></a>';
                   
                    }
                    if($row->kodeprogressurat=='0'){
                     $koment = 'Belum ada progress';  
                    }
                    
                }
                if($row->statusm==2 && $row->kodeprogressurat=='2'){
                    $close='<a href="javascript:;" class="btn btn-warning btn-block" onclick="tutup_progress(\''.$row->kodesuratmasuk.'\',\''.$row->nosuratmasukdivisi.'\');"><i class="fa fa-power-off "></i>Tutup progress</a>';
                }
                if($row->statusm==3){
                    $close='<a href="javascript:;" class="btn btn-danger btn-block"><i class="fa fa-power-off "></i> Sudah di close</a>';
                }
             
              $hsl.='<tr><td>'.$no.'</td><td>'.$row->empnamedisposisi.'('.$row->empnikdisposisi.')</td><td>'.$row->progressurat.'<br>'.$row->keterangan.'</td><td>'.$koment.'</td><td>'.$close.'</td></tr>';
            $no++;
        }
        $hsl.='</table>';
		$jumdisp = $this->db
		   ->where('kodesuratmasukatas',$kode)
		   ->count_all_results('t_suratmasuk_d');
		$jumprog = $this->db
		   ->where('kodesuratmasukatas',$kode)
		   ->where('kodeprogressurat',3)
		   ->count_all_results('t_suratmasuk_d');
		   
		if($jumdisp==$jumprog){
			$hsl.='<formid="fmcloseallaprogress" class="form-horizontal" data-parsley-validate="true" cellspacing="0" width="100%" enctype="multipart/form-data">';
			$hsl.='<div class="col-md-12"><div class="form-group"><label for="keterangan">Keterangan Pekerjaan</label><textarea class="form-control" name="keterangan" id="keterangan"></textarea></div></div>';
			$hsl.='<div class="col-md-6"><div class="form-group"><a href="javascript:;" class="btn btn-success btn-block" onclick="tutupprogresssendiri(\''.$kode.'\')"><i class="fa fa-check pull-right"></i>Tutup progress</a></div></div>';
			$hsl.='<div class="col-md-6"><div class="form-group"><a href="javascript:;" class="btn btn-warning btn-block" onclick="close_disposisi();"><i class="fa fa-minus pull-right"></i>Batal</a></div></div>';
			$hsl.='</form>';
		}
        $this->output->set_content_type('application/json')->set_output(json_encode($hsl));
    }
	public function closeallprogress($kode){
		$this->output->set_content_type('application/json')->set_output(json_encode($kode));
	}
    public function nilai_progress($kode,$user){
        $dt =$this->model_mobile->get_suratmasuk($kode)->row_array();
        $assu=$this->model_mobile->get_dtm($dt['kodesuratmasuk'])->row_array();
        $assal =$this->model_mobile->get_suratkeluar($assu['kodesuratutama'])->row_array();
        //print_r($assal);
        $hsl='';
        $hsl.='<h2 align=center>Close Progress</h2>';
        $hsl.='<form id="fm_close_progress" class="form-horizontal" data-parsley-validate="true" cellspacing="0" width="100%" enctype="multipart/form-data">';
        $hsl.='<div class="col-md-12"><div class="form-group"><label for="tanggalterimasurat">Tanggal terima</label><input class="form-control"  style="width:100%;" type="text" name="tanggalterimasurat" id="tanggalterimasurat" value="'.date("Y-m-d").'" readonly></div></div>';
        $hsl.='<div class="col-md-12"><div class="form-group"><label for="tanggalpenyelesaian">Tanggal penyelesaian</label><input class="form-control"  style="width:100%;" type="text" name="tanggalpenyelesaian" id="tanggalpenyelesaian" value="'.$dt['tanggalpenyelesaian'].'" readonly></div></div>';
        $hsl.='<div class="col-md-12"><div class="form-group"><label for="status">Status Progres</label>'.$this->status_progress2($dt['kodeprogressurat']).'</div></div>';
        $hsl.='<div class="col-md-12"><div class="form-group"><label for="komentar">Komentar</label><input class="form-control"  style="width:100%;" type="text" name="komentar" id="komentar" placeholder="Komentar"></div></div>';
        $hsl.='<div class="col-md-6"><div class="form-group"><a href="javascript:;" class="btn btn-success btn-block" onclick="simpannilai_progress(\'' .$kode. '\');"><i class="fa fa-check pull-right"></i> Simpan</a></div></div>';
        $hsl.='<div class="col-md-6"><div class="form-group"><a href="javascript:;" class="btn btn-warning btn-block" onclick="close_disposisi();"><i class="fa fa-minus pull-right"></i>Batal</a></div></div>';
        
        $hsl.='</form>';
        $this->output->set_content_type('application/json')->set_output(json_encode($hsl));
    }
    public function simpannilai_progress($kode,$user){
        $kodeprogres = $this->input->post('statusprogress');
        $komentar = $this->input->post('komentar');
        if($kodeprogres==1||$kodeprogres==2){
            $status=1;
        }
        if($kodeprogres==3||$kodeprogres==4){
            $status==3;
        }
        $data_d = array('kodeprogressurat'=>$kodeprogres,'catatan'=>$komentar);
        $data_m = array('status'=>$status);
        $where = array('kodesuratmasuk'=>$kode);
        $this->db->update('t_suratmasuk_d',$data_d,$where);
        $res = $this->db->update('t_suratmasuk_m',$data_m,$where);
        if($res){
			$komenprogress=$this->model_mobile->get_komenprogress($kode)->row_array();
			$playerid=array($komenprogress['playerid']);
			$pesan="Komentar progress no ".$komenprogress['nosuratmasukdivisi']." dari ".$komenprogress['empnamesurat']." (".$komenprogress['empniksurat'].")";
			$heading="Komenprogress";
			$kirimkedisposisi=$this->send_notification($playerid,$pesan,$heading);
            $output = 1;
            $this->output->set_content_type('application/json')->set_output(json_encode($output));
        }else{
            $output = 0;
            $this->output->set_content_type('application/json')->set_output(json_encode($output));
        }
        $datalog = array('log_time'=>date('Y-m-d H:i:s'),'log_user'=>$user,'log_aksi'=>'nilai progress','log_url'=>site_url(uri_string()),'log_item'=>'Notifikasi progress','kode'=>$kode,'log_ip'=>$this->get_client_ip(),'log_device'=>$_SERVER['HTTP_USER_AGENT']);
	$this->model_mobile->log_activity($datalog);
        
    }
    public function tutupprogresssendiri($kode){
		if(isset($kode)){
			$keterangan = $this->input->post('keterangan');
			$data_m = array('status'=>'3','kodeprogressurat'=>'3');
			$data_d = array('keterangan'=>$keterangan,'kodeprogressurat'=>'2');
			$where = array('kodesuratmasuk'=>$kode);
			$m = $this->db->update('t_suratmasuk_m',$data_m,$where);
			$d = $this->db->update('t_suratmasuk_d',$data_d,$where);
			if($m && $d){
				$output = 1;
				$this->output->set_content_type('application/json')->set_output(json_encode($output));
			}else{
				$output = 0;
				$this->output->set_content_type('application/json')->set_output(json_encode($output));
			}
		}else{
			$this->output->set_content_type('application/json')->set_output(json_encode("Kode suratmasuk tidak ditemukan !"));
		}
		
		//$this->output->set_content_type('application/json')->set_output(json_encode($kode));
    }
	function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
	}
    function test2(){

        $edaran   = $this->model_mobile->get_receive_token();
        $dataedaran = array();
			foreach($edaran->result_array() as $valueedaran){
                array_push($dataedaran,$valueedaran['playerid']);
            }
        $pesanedaran ="Ada Edaran yang belum dibaca.";
        $headingedaran="Edaran";
        $kirimnotifedaran=$this->send_notification($dataedaran,$pesanedaran,$headingedaran);
		
	}
  
}
