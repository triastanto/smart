<?php
class Model_mobile extends CI_Model {
		function __construct() {
			parent::__construct();
		}
  public function get_suratmasuk($kode){
		$this->db->where('t_suratmasuk_m.kodesuratmasuk',$kode);
		$this->db->join("t_suratmasuk_d", "t_suratmasuk_d.kodesuratmasuk=t_suratmasuk_m.kodesuratmasuk", "inner");
        $data = $this->db->get('t_suratmasuk_m');
        return $data;
  }
  public function get_suratkeluar($kode){
    $this->db->where('kodesuratkeluar',$kode);
        $data = $this->db->get('t_suratkeluar_m');
        return $data;
  }
  public function get_m_pesan(){
     $data = $this->db->get('m_pesan')->result_array();
		return $data;
  }
  public function contactus($data){
        $res = $this->db->insert('t_contact_us',$data);
        return $res;
    }
  public function get_pedoman_kearsipan($grup){
    $this->db->select('count(*) as jmlpedoman');
    $this->db->where('grup',$grup);
    $data = $this->db->get('m_home');
    return $data;
  }
  public function get_petunjuk($grup){
    $this->db->select('count(*) as jmlpetunjuk');
    $this->db->where('grup',$grup);
    $data = $this->db->get('m_home');
    return $data;
  }
  public function get_informasi($grup){
    $this->db->select('count(*) as jmlinformasi');
    $this->db->where('grup',$grup);
    $data = $this->db->get('m_home');
    return $data;
  }
  public function get_progress($user){
    $this->db->select('count(*) as jmlprogress');
    $this->db->where('empniksurat',$user);
    $this->db->where('status','2');
	$this->db->where('validasi','1');
    //$this->db->join('sys_groupuser','sys_user.userid = sys_groupuser.userid','inner');
    //$this->db->join('t_suratmasuk_m','t_suratmasuk_m.empniksurat = sys_groupuser.userid','inner');
    $data = $this->db->get('t_suratmasuk_m');
    return $data;
  }
  public function get_smasuk($user){
    $this->db->select('count(*) as jmlsmasuk');
    $this->db->where('empniktujuan',$user);
    $this->db->where('validasi','1');
    $this->db->where('statusview','0');
    $data = $this->db->get('t_suratmasuk_m');
    return $data;
  }
  public function get_edaran($user){
    $this->db->select('count(*) as jmledaran');
	$this->datatables->join("t_surat_deployment", "t_surat_deployment.kodesuratatas=t_suratkeluar_m.kodesuratkeluar", "inner");
	$this->datatables->join("t_suratmasuk_m", "t_suratmasuk_m.kodesuratmasuk=t_surat_deployment.kodesuratbawah", "inner");
	$where = "kodejudul='2' and validasiparaf='1' and statusedaran='1' and t_suratkeluar_m.tanggalsurat between '".date('Y-m')."-01' and '".date('Y-m')."-31' or
					kodejudul='2' and validasiparaf='1' and statusedaran='2' and empniktujuan='".$user."' and t_suratkeluar_m.tanggalsurat between '".date('Y-m')."-01' and '".date('Y-m')."-31'";
    //$this->db->where('kodejudul',2);
    //$this->db->where('validasiparaf',1);
	$this->db->where($where);
    $data = $this->db->get('t_suratkeluar_m');
    return $data;
  }
  public function get_validasi($user){
    $this->db->select('count(*) as jmlvalidasi');
    $this->db->where('empnik_asal',$user);
    $this->db->where('validasiparaf',0);
	$this->db->where('statuscancel',0);
    $data = $this->db->get('t_suratkeluar_m');
    return $data;
  }
  public function get_paraf($user){
    $this->db->select('count(*) as jmlparaf');
	$this->db->join("t_suratkeluar_m", "t_suratkeluar_m.kodesuratkeluar=t_surat_paraf.kodesuratutama", "inner");
    $this->db->where('t_surat_paraf.empnikparaf',$user);
    $this->db->where('t_surat_paraf.validasiparaf',0);
	$this->db->where('t_suratkeluar_m.statuscancel',0);
    $data = $this->db->get('t_surat_paraf');
    return $data;
	
  }
  public function get_skeluar($user){
    $this->db->select('count(*) as jmlkeluar');
    $this->db->where('validasiparaf',0);
    $this->db->where('pembuat',$user);
	$this->db->where('statuscancel',0);
    $data = $this->db->get('t_suratkeluar_m');
    return $data;
    
  }
  public function readnotifprogress($groupid,$user,$nik){
    /*$this->datatables->select('t_suratmasuk_m.kodesuratmasuk,t_suratmasuk_m.empniktujuan,t_suratmasuk_m.empnametujuan,t_suratmasuk_m.empkostl_tujuan,t_suratmasuk_m.emp_cskt_ltext_tujuan,t_suratmasuk_m.status,t_suratmasuk_m.statusview,t_suratmasuk_m.kodeprogressurat,m_progressurat.progressurat,t_suratmasuk_d.keterangan,t_suratmasuk_m.size');
    $this->datatables->edit_column('empniktujuan', '[$1-$2] $3', 'empniktujuan, empnametujuan,emp_cskt_ltext_tujuan');
    $this->datatables->from('sys_user');
    $this->datatables->join("sys_groupuser", "sys_groupuser.userid=sys_user.userid", "inner");
    $this->datatables->join("t_suratmasuk_m", "t_suratmasuk_m.empniksurat=sys_groupuser.userid", "inner");
	$this->datatables->join("t_suratmasuk_d", "t_suratmasuk_d.kodesuratmasuk=t_suratmasuk_m.kodesuratmasuk", "inner");
    $this->datatables->join("m_progressurat", "m_progressurat.kodeprogressurat=t_suratmasuk_m.kodeprogressurat", "inner");*/
    $this->datatables->select('t_suratmasuk_m.kodesuratmasuk,t_suratmasuk_m.empniktujuan,t_suratmasuk_m.empnametujuan,t_suratmasuk_m.empkostl_tujuan,t_suratmasuk_m.emp_cskt_ltext_tujuan,t_suratmasuk_m.status,t_suratmasuk_m.statusview,t_suratmasuk_m.kodeprogressurat,m_progressurat.progressurat,t_suratmasuk_d.keterangan,t_suratmasuk_m.size');
	$this->datatables->edit_column('empniktujuan', '[$1-$2] $3', 'empniktujuan, empnametujuan,emp_cskt_ltext_tujuan');
	$this->datatables->from('t_suratmasuk_m');
	$this->datatables->join("m_progressurat", "m_progressurat.kodeprogressurat=t_suratmasuk_m.kodeprogressurat", "inner");
	$this->datatables->join("t_suratmasuk_d", "t_suratmasuk_d.kodesuratmasuk=t_suratmasuk_m.kodesuratmasuk", "inner");
    $array = array('empniksurat' => $user,'t_suratmasuk_m.status' => 2,'validasi' => 1);
    $this->datatables->where($array);
    $data = $this->datatables->generate();        
    return $data;
    
  }
  public function readvalidasi($groupid,$user,$nik){
        if($groupid=="1"){
                $this->datatables->select("kodesuratkeluar,nosuratkeluar,tanggalsurat,asalsurat,tujuan_surat,isiringkasan,validasiparaf,empname_asal,empnik_asal,emp_cskt_ltext_asal,DATE_FORMAT(tanggalsurat, '%d/%m/%Y') tanggalsuratnice,size");
                $this->datatables->edit_column('empnik_asal', '[$1-$2] $3', 'empnik_asal, empname_asal,emp_cskt_ltext_asal');
                $this->datatables->from('t_suratkeluar_m');
                //$array = array('empnik_asal' => $user,'validasiparaf' => 0);
                //$this->datatables->where($array);
                $data = $this->datatables->generate();
            }
        if($groupid=="7"){
                $this->datatables->select("kodesuratkeluar,nosuratkeluar,tanggalsurat,asalsurat,tujuan_surat,isiringkasan,validasiparaf,empname_asal,empnik_asal,emp_cskt_ltext_asal,DATE_FORMAT(tanggalsurat, '%d/%m/%Y') tanggalsuratnice,size");
                $this->datatables->edit_column('empnik_asal', '[ $1-$2 ] $3', 'empnik_asal, empname_asal,emp_cskt_ltext_asal');
                $this->datatables->from('t_suratkeluar_m');
                $array = array('empnik_asal' => $user,'validasiparaf' => 0,'t_suratkeluar_m.statuscancel'=>0);
                $this->datatables->where($array);
                $data = $this->datatables->generate();
            }
        if($groupid=="2" ||$groupid=="3"||$groupid=="4" || $groupid=="5"){
                $this->datatables->select("kodesuratkeluar,nosuratkeluar,tanggalsurat,asalsurat,tujuan_surat,isiringkasan,validasiparaf,empname_asal,empnik_asal,emp_cskt_ltext_asal,DATE_FORMAT(tanggalsurat, '%d/%m/%Y') tanggalsuratnice,size");
                $this->datatables->edit_column('empnik_asal', '[ $1-$2 ] $3', 'empnik_asal, empname_asal,emp_cskt_ltext_asal');
                $this->datatables->from('t_suratkeluar_m');
                $array = array('empnik_asal' => $user,'validasiparaf' => 0,'t_suratkeluar_m.statuscancel'=>0);
                $this->datatables->where($array);
                $data = $this->datatables->generate();
            }
             return $data;
  }
   /*public function readkaryawan($user,$gol){
                $db2=$this->load->database('db2',true);
                $this->datatables->set_database('db2');
                $this->datatables->select('empnik,empname,emp_cskt_ltext,empposid,emp_hrp1000_s_short,empkostl,emppersk,emppostx,unitkerja,jabatan,emp_hrp1000_o_short');
                $this->datatables->from('structdisp');
				$this->datatables->join("zhrom0012", "zhrom0012.noorg=structdisp.emp_hrp1000_o_short", "inner");
				$this->datatables->join("structjab", "structjab.no=structdisp.emp_hrp1000_s_short AND structjab.gol=structdisp.emppersk", "LEFT");
                $array = array('dirnik'=>$user);
                //$this->datatables->where($array);
				$this->datatables->group_by('empnik');
				//$this->datatables->or_where(['emppersk'=>$gol]);
				//$this->datatables->where(['structdisp.no'=>1]);
                $data = $this->datatables->generate();
             return $data;
  }*/
   public function readkaryawan($user,$gol){
                $db2=$this->load->database('db2',true);
                $this->datatables->set_database('db2');
                $this->datatables->select('empnik,empname,emp_cskt_ltext,empposid,emp_hrp1000_s_short,empkostl,emppersk,emppostx');
                $this->datatables->from('structdisp');
                //$array = array('empnik'=>$user);
                //$this->datatables->where($array);
				$this->datatables->group_by('empnik');
				//$this->datatables->or_where(['emppersk'=>$gol]);
				//$this->datatables->where(['no'=>1]);
                $data = $this->datatables->generate();
             return $data;
  }
  public function readdireksi(){
                $db2=$this->load->database('db2',true);
                $this->datatables->set_database('db2');
                $this->datatables->select('empnik,empname,emp_cskt_ltext,empposid,emp_hrp1000_s_short,empkostl,emppersk,emppostx');
                $this->datatables->from('structdireksi');
                $array = array('no' => '1');
                $this->datatables->where($array);
                $data = $this->datatables->generate();
             return $data;
  }
  public function readedaran($groupid,$user,$nik){
                $this->datatables->select("kodesuratkeluar,nosuratkeluar,t_suratkeluar_m.tanggalsurat,t_suratkeluar_m.asalsurat,tujuan_surat,t_suratkeluar_m.isiringkasan,validasiparaf,empname_asal,empnik_asal,emp_cskt_ltext_asal,t_suratkeluar_m.size,DATE_FORMAT(t_suratkeluar_m.tanggalsurat, '%d/%m/%Y') tanggalsuratnice");
                //$this->datatables->select("DATE_FORMAT(tanggalsurat, '%d/%m/%Y') tanggalsuratnice");
                $this->datatables->edit_column('empnik_asal', '[$1-$2] $3', 'empnik_asal, empname_asal,emp_cskt_ltext_asal');
                $this->datatables->from('t_suratkeluar_m');
				$this->datatables->join("t_surat_deployment", "t_surat_deployment.kodesuratatas=t_suratkeluar_m.kodesuratkeluar", "inner");
				$this->datatables->join("t_suratmasuk_m", "t_suratmasuk_m.kodesuratmasuk=t_surat_deployment.kodesuratbawah", "inner");
                //$array = array('kodejudul' =>2,'validasiparaf'=>1,'statusedaran'=>1);
				$where = "kodejudul='2' and validasiparaf='1' and statusedaran='1' and t_suratkeluar_m.tanggalsurat between '".date('Y-m')."-01' and '".date('Y-m')."-31' or
					kodejudul='2' and validasiparaf='1' and statusedaran='2' and empniktujuan='".$user."' and t_suratkeluar_m.tanggalsurat between '".date('Y-m')."-01' and '".date('Y-m')."-31'";
                $this->datatables->where($where);
                $data = $this->datatables->generate();
             return $data;
  }
  public function readparaf($groupid,$user,$nik){
        if($groupid=="1"){
                $this->datatables->select("kodesuratkeluar,nosuratkeluar,tanggalsurat,asalsurat,tujuan_surat,isiringkasan,empname_asal,empnik_asal,emp_cskt_ltext_asal,size,DATE_FORMAT(tanggalsurat, '%d/%m/%Y') tanggalsuratnice");
                $this->datatables->edit_column('empnik_asal', '[$1-$2] $3', 'empnik_asal, empname_asal,emp_cskt_ltext_asal');
                $this->datatables->from('t_surat_paraf');
                $this->datatables->join("t_suratkeluar_m", "t_surat_paraf.kodesuratutama=t_suratkeluar_m.kodesuratkeluar", "inner");
            
                $data = $this->datatables->generate();
            }else{
                $this->datatables->select("kodesuratkeluar,nosuratkeluar,tanggalsurat,asalsurat,tujuan_surat,isiringkasan,empname_asal,empnik_asal,emp_cskt_ltext_asal,size,DATE_FORMAT(tanggalsurat, '%d/%m/%Y') tanggalsuratnice");
                $this->datatables->edit_column('empnik_asal', '[ $1-$2 ] $3', 'empnik_asal, empname_asal,emp_cskt_ltext_asal');
                $this->datatables->from('t_surat_paraf');
                $this->datatables->join("t_suratkeluar_m", "t_surat_paraf.kodesuratutama=t_suratkeluar_m.kodesuratkeluar", "inner");
            
                $array = array('t_surat_paraf.empnikparaf' => $user,'t_surat_paraf.validasiparaf'=>0,'t_suratkeluar_m.statuscancel'=>0);
                $this->datatables->where($array);
                $data = $this->datatables->generate();
            }
             return $data;
  }
  public function readsuratmasuk($groupid,$user,$nik){
        
        if($groupid=="1"){
            //$this->datatables->select('kodesuratmasuk,nosuratmasuk,tanggalsurat,empniksurat,tujuansurat,isiringkasan,empname_asal,empnik_asal,emp_cskt_ltext_asal,empniktujuan,empnametujuan,kodeklasifikasi,nosuratmasukdivisi,statusview,tanggalsuratnice,size,kodesifatsurat,jenissurat,group_id,status');
            $this->datatables->select("t_suratmasuk_m.kodesuratmasuk,t_suratmasuk_m.nosuratmasuk,t_suratmasuk_m.tanggalsurat,t_suratmasuk_m.empniksurat,t_suratmasuk_m.tujuansurat,t_suratmasuk_m.isiringkasan,CASE WHEN (t_suratmasuk_m.jenissurat = '2' AND t_surat_deployment.kodesuratatas is null) THEN t_suratmasuk_m.emp_cskt_ltext_surat WHEN (t_suratmasuk_m.jenissurat = '2' AND t_surat_deployment.kodesuratatas IS NOT NULL) THEN(SELECT emp_cskt_ltext_surat from t_suratmasuk_m WHERE kodesuratmasuk=t_surat_deployment.kodesuratatas) ELSE t_suratkeluar_m.empname_asal END as empname_asal,CASE WHEN (t_suratmasuk_m.jenissurat = '2' AND t_surat_deployment.kodesuratatas is null) THEN t_suratmasuk_m.empnamesurat WHEN (t_suratmasuk_m.jenissurat = '2' AND t_surat_deployment.kodesuratatas IS NOT NULL) THEN (SELECT empnamesurat from t_suratmasuk_m WHERE kodesuratmasuk=t_surat_deployment.kodesuratatas)ELSE t_suratkeluar_m.empnik_asal END as empnik_asal,t_suratkeluar_m.emp_cskt_ltext_asal,t_suratmasuk_m.empniktujuan,t_suratmasuk_m.empnametujuan,t_suratmasuk_m.kodeklasifikasi,t_suratmasuk_m.nosuratmasukdivisi,t_suratmasuk_m.statusview,DATE_FORMAT(t_suratmasuk_m.tanggalsurat, '%d/%m/%Y') tanggalsuratnice,t_suratmasuk_m.size,t_suratmasuk_m.kodesifatsurat,t_suratmasuk_m.jenissurat,t_suratmasuk_m.status");
            $this->datatables->edit_column('empnik_asal', '[$1-$2] $3', 'empnik_asal, empname_asal,emp_cskt_ltext_asal');
            $this->datatables->edit_column('empniktujuan', '[$1] $2', 'empniktujuan, empnametujuan');
            $this->datatables->edit_column('nosuratmasukdivisi', '$1/$2', 'kodeklasifikasi, nosuratmasukdivisi');
            
            $this->datatables->from('t_suratmasuk_m');
            $this->datatables->join("t_surat_deployment", "t_surat_deployment.kodesuratbawah=t_suratmasuk_m.kodesuratmasuk", "left");
            $this->datatables->join("t_suratkeluar_m", "t_suratkeluar_m.kodesuratkeluar=t_surat_deployment.kodesuratutama", "left");
            $this->datatables->where('t_suratmasuk_m.validasi', '1');
  
            $data = $this->datatables->generate();
        }if($groupid=="2"||$groupid=="4"||$groupid=="5"||$groupid=="7"){
            $this->datatables->select("t_suratmasuk_m.kodesuratmasuk,t_suratmasuk_m.nosuratmasuk,t_suratmasuk_m.tanggalsurat,t_suratmasuk_m.empniksurat,t_suratmasuk_m.tujuansurat,t_suratmasuk_m.isiringkasan,CASE WHEN (t_suratmasuk_m.jenissurat = '2' AND t_surat_deployment.kodesuratatas is null) THEN t_suratmasuk_m.emp_cskt_ltext_surat WHEN (t_suratmasuk_m.jenissurat = '2' AND t_surat_deployment.kodesuratatas IS NOT NULL) THEN(SELECT emp_cskt_ltext_surat from t_suratmasuk_m WHERE kodesuratmasuk=t_surat_deployment.kodesuratatas) ELSE t_suratkeluar_m.empname_asal END as empname_asal,CASE WHEN (t_suratmasuk_m.jenissurat = '2' AND t_surat_deployment.kodesuratatas is null) THEN t_suratmasuk_m.empnamesurat WHEN (t_suratmasuk_m.jenissurat = '2' AND t_surat_deployment.kodesuratatas IS NOT NULL) THEN (SELECT empnamesurat from t_suratmasuk_m WHERE kodesuratmasuk=t_surat_deployment.kodesuratatas)ELSE t_suratkeluar_m.empnik_asal END as empnik_asal,t_suratkeluar_m.emp_cskt_ltext_asal,t_suratmasuk_m.empniktujuan,t_suratmasuk_m.empnametujuan,t_suratmasuk_m.kodeklasifikasi,t_suratmasuk_m.nosuratmasukdivisi,t_suratmasuk_m.statusview,DATE_FORMAT(t_suratmasuk_m.tanggalsurat, '%d/%m/%Y') tanggalsuratnice,t_suratmasuk_m.size,t_suratmasuk_m.kodesifatsurat,t_suratmasuk_m.jenissurat,t_suratmasuk_m.status");
            $this->datatables->edit_column('empnik_asal', '[$1-$2] $3', 'empnik_asal, empname_asal,emp_cskt_ltext_asal');
            $this->datatables->edit_column('empniktujuan', '[$1] $2', 'empniktujuan, empnametujuan');
            $this->datatables->edit_column('nosuratmasukdivisi', '$1/$2', 'kodeklasifikasi, nosuratmasukdivisi');
            
            $this->datatables->from('t_suratmasuk_m');
            $this->datatables->join("t_surat_deployment", "t_surat_deployment.kodesuratbawah=t_suratmasuk_m.kodesuratmasuk", "left");
            $this->datatables->join("t_suratkeluar_m", "t_suratkeluar_m.kodesuratkeluar=t_surat_deployment.kodesuratutama", "left");
			$where ="tujuansurat='".$nik."' and t_suratmasuk_m.validasi='1'  or empniktujuan='".$nik."' and t_suratmasuk_m.validasi='1'";
            //$this->datatables->where('t_suratmasuk_m.validasi', '1');
            //$this->datatables->where('t_suratmasuk_m.tujuansurat', $nik);
			//$this->datatables->or_where('t_suratmasuk_m.empniktujuan', $nik);
            $this->datatables->where($where);
            $data = $this->datatables->generate();
        }if($groupid=="3"||$groupid=="6"){
            $this->datatables->select("t_suratmasuk_m.kodesuratmasuk,t_suratmasuk_m.nosuratmasuk,t_suratmasuk_m.tanggalsurat,t_suratmasuk_m.empniksurat,t_suratmasuk_m.tujuansurat,t_suratmasuk_m.isiringkasan,CASE WHEN (t_suratmasuk_m.jenissurat = '2' AND t_surat_deployment.kodesuratatas is null) THEN t_suratmasuk_m.emp_cskt_ltext_surat WHEN (t_suratmasuk_m.jenissurat = '2' AND t_surat_deployment.kodesuratatas IS NOT NULL) THEN(SELECT emp_cskt_ltext_surat from t_suratmasuk_m WHERE kodesuratmasuk=t_surat_deployment.kodesuratatas) ELSE t_suratkeluar_m.empname_asal END as empname_asal,CASE WHEN (t_suratmasuk_m.jenissurat = '2' AND t_surat_deployment.kodesuratatas is null) THEN t_suratmasuk_m.empnamesurat WHEN (t_suratmasuk_m.jenissurat = '2' AND t_surat_deployment.kodesuratatas IS NOT NULL) THEN (SELECT empnamesurat from t_suratmasuk_m WHERE kodesuratmasuk=t_surat_deployment.kodesuratatas)ELSE t_suratkeluar_m.empnik_asal END as empnik_asal,t_suratkeluar_m.emp_cskt_ltext_asal,t_suratmasuk_m.empniktujuan,t_suratmasuk_m.empnametujuan,t_suratmasuk_m.kodeklasifikasi,t_suratmasuk_m.nosuratmasukdivisi,t_suratmasuk_m.statusview,DATE_FORMAT(t_suratmasuk_m.tanggalsurat, '%d/%m/%Y') tanggalsuratnice,t_suratmasuk_m.size,t_suratmasuk_m.kodesifatsurat,t_suratmasuk_m.jenissurat,t_suratmasuk_m.status");
            $this->datatables->edit_column('empnik_asal', '[$1-$2] $3', 'empnik_asal, empname_asal,emp_cskt_ltext_asal');
            $this->datatables->edit_column('empniktujuan', '[$1] $2', 'empniktujuan, empnametujuan');
            $this->datatables->edit_column('nosuratmasukdivisi', '$1/$2', 'kodeklasifikasi, nosuratmasukdivisi');
            
            $this->datatables->from('sys_groupuser');
            $this->datatables->join("sys_user", "sys_user.userid=sys_groupuser.userid", "inner");
            $this->datatables->join("t_suratmasuk_m", "t_suratmasuk_m.empshort_tujuan=sys_groupuser.pos_abbr and t_suratmasuk_m.empposid_tujuan = sys_groupuser.empposid", "inner");
            $this->datatables->join("t_surat_deployment", "t_surat_deployment.kodesuratbawah=t_suratmasuk_m.kodesuratmasuk", "left");
            $this->datatables->join("t_suratkeluar_m", "t_suratkeluar_m.kodesuratkeluar=t_surat_deployment.kodesuratutama", "left");
            $array = array('t_suratmasuk_m.validasi' => '1', 'sys_user.userid' => $user);
            $this->datatables->where($array);
            
            $data = $this->datatables->generate();
            
        }
        
        return $data;
    }
    public function readsuratkeluar($groupid,$user,$nik){
        if($groupid=="1"){
            /*$this->datatables->select("kodesuratkeluar,nosuratkeluar,tanggalsurat,asalsurat,tujuan_surat,isiringkasan,validasiparaf,empname_asal,empnik_asal,emp_cskt_ltext_asal,DATE_FORMAT(tanggalsurat, '%d/%m/%Y') tanggalsuratnice,size,jenissurat,sifatsurat");
            $this->datatables->edit_column('empnik_asal', '[ $1-$2 ] $3', 'empnik_asal, empname_asal,emp_cskt_ltext_asal');
            //$this->datatables->edit_column('username', '<a href="profiles/edit/$1">$2</a>', 'id, username');
            $this->datatables->from("t_suratkeluar_m");
            //$this->datatables->join("sys_groupuser", "sys_groupuser.pos_abbr=t_suratkeluar_m.empshort_asal", "inner");
            //$this->datatables->join("sys_user", "sys_user.userid=sys_groupuser.userid", "inner");
            $data = $this->datatables->generate();*/
			$this->datatables->select("kodesuratkeluar,nosuratkeluar,tanggalsurat,asalsurat,tujuan_surat,isiringkasan,validasiparaf,empname_asal,empnik_asal,emp_cskt_ltext_asal,DATE_FORMAT(tanggalsurat, '%d/%m/%Y') tanggalsuratnice,size,jenissurat,group_id,sifatsurat");
			$this->datatables->edit_column('empnik_asal', '[ $1-$2 ] $3', 'empnik_asal, empname_asal,emp_cskt_ltext_asal');
			$this->datatables->from('t_suratkeluar_m');
			$array = array('statuscancel'=>0);
            $this->datatables->where($array);
			$data = $this->datatables->generate();
			
        }else if($groupid=="4" || $groupid=="5"){
            /*$this->datatables->select("kodesuratkeluar,nosuratkeluar,tanggalsurat,asalsurat,tujuan_surat,isiringkasan,validasiparaf,empname_asal,empnik_asal,emp_cskt_ltext_asal,DATE_FORMAT(tanggalsurat, '%d/%m/%Y') tanggalsuratnice,size,jenissurat,group_id,sifatsurat");
            $this->datatables->edit_column('empnik_asal', '[ $1-$2 ] $3', 'empnik_asal, empname_asal,emp_cskt_ltext_asal');
            $this->datatables->from('sys_user');
            $this->datatables->join("sys_groupuser", "sys_groupuser.userid=sys_user.userid", "inner");
            $this->datatables->join("t_suratkeluar_m", "sys_groupuser.pos_abbr=t_suratkeluar_m.empshort_asal", "inner");
            $array = array('sys_user.userid' => $user,'t_suratkeluar_m.statuscancel'=>0);
            $this->datatables->where($array);
            $data = $this->datatables->generate();*/
			$this->datatables->select("kodesuratkeluar,nosuratkeluar,tanggalsurat,asalsurat,tujuan_surat,isiringkasan,validasiparaf,empname_asal,empnik_asal,emp_cskt_ltext_asal,DATE_FORMAT(tanggalsurat, '%d/%m/%Y') tanggalsuratnice,size,jenissurat,sifatsurat");
			$this->datatables->edit_column('empnik_asal', '[ $1-$2 ] $3', 'empnik_asal, empname_asal,emp_cskt_ltext_asal');
			$this->datatables->from('t_suratkeluar_m');
			$array = "pembuat='".$user."' and statuscancel='0' or empnik_asal='".$user."' and statuscancel='0'";
            $this->datatables->where($array);
			$data = $this->datatables->generate();
        }else{
			$this->datatables->select("kodesuratkeluar,nosuratkeluar,tanggalsurat,asalsurat,tujuan_surat,isiringkasan,validasiparaf,empname_asal,empnik_asal,emp_cskt_ltext_asal,DATE_FORMAT(tanggalsurat, '%d/%m/%Y') tanggalsuratnice,size,jenissurat,group_id,sifatsurat");
            $this->datatables->edit_column('empnik_asal', '[ $1-$2 ] $3', 'empnik_asal, empname_asal,emp_cskt_ltext_asal');
            $this->datatables->from('sys_user');
            $this->datatables->join("sys_groupuser", "sys_groupuser.userid=sys_user.userid", "inner");
            $this->datatables->join("t_suratkeluar_m", "sys_groupuser.pos_abbr=t_suratkeluar_m.empshort_asal", "inner");
            $array = array('sys_user.userid' => $user,'t_suratkeluar_m.statuscancel'=>0);
            $this->datatables->where($array);
            $data = $this->datatables->generate();
		}
        
        return $data;
    }
    public function encode($id){
        $hsl =base64_encode($id);
        return $hsl;
        
    }
    public function proses($data,$where){
        $res = $this->db->update('t_whistleblowing',$data,$where);
    }
    public function keputusan($data,$where){
        $res = $this->db->update('t_whistleblowing',$data,$where);
    }
     public function update_bap($data,$where){
        $res = $this->db->update('t_whistleblowing',$data,$where);
    }
    public function simpan_paraf($data,$where){
        $res = $this->db->update('t_suratkeluar_m',$data,$where);
        return $res;
    }
    public function simpan_memo($data,$where){
        $res = $this->db->update('t_suratkeluar_memo',$data,$where);
        return $res;
    }
    public function updateparaf($data,$where){
        $res = $this->db->update('t_surat_paraf',$data,$where);
        return $res;
    }
    public function get_user($user){
        $this->db->where('userid',$user);
        $datauser =$this->db->get('sys_user');
        return $datauser;
        
    }
    public function get_memo($kode){
        $this->db->where('kodesuratkeluar',$kode);
        $data=$this->db->get('t_suratkeluar_memo');
        return $data;
    }
    public function get_datasurat($kode){
        $this->db->join('token as b','b.userid=a.pembuat','LEFT');
        $this->db->where('a.kodesuratkeluar',$kode);
        $data=$this->db->get('t_suratkeluar_m as a');
        return $data;
    }
	public function get_komenprogress($kode){
		$this->db->join('token as b','b.userid=a.empniktujuan','LEFT');
        $this->db->where('a.kodesuratmasuk',$kode);
        $data=$this->db->get('t_suratmasuk_m as a');
        return $data;
	}
	public function get_notifprogress($kode){
		$this->db->join('token as b','b.userid=a.empniksurat','LEFT');
        $this->db->where('a.kodesuratmasuk',$kode);
        $data=$this->db->get('t_suratmasuk_m as a');
        return $data;
	}
    public function get_datasuratmasuk($kode){
        $this->db->join('token as b','b.userid=a.pembuat','LEFT');
        $this->db->where('a.kodesuratmasuk',$kode);
        $data=$this->db->get('t_suratmasuk_m as a');
        return $data;
    }
    public function cek_session_paraf($kode){
        $this->db->where('kodesuratutama',$kode);
        $data=$this->db->get('session_paraf');
        return $data;
    }
    public function get_datasingkatan($empkostl_asal){
        $this->db->where('empkostl',$empkostl_asal);
        $data=$this->db->get('m_singkatan');
        return $data;
    }
    public function get_dataparaf($user,$kode){
        $this->db->where('empnikparaf',$user);
        $this->db->where('kodesuratutama',$kode);
        $data = $this->db->get('t_surat_paraf');
        return $data;
        
    }
    public function insert_to_session($data){
        $res = $this->db->insert('session_paraf',$data);
        return $res;
    }
    public function insert_to_riwayat($data){
        $res = $this->db->insert('t_suratmasuk_memo_history',$data);
        return $res;
    }
    public function valid_masuk($where,$data){
        $res=$this->db->update('t_suratmasuk_m',$data,$where);
        return $res;
    }
    public function validasi($where,$data){
        $res=$this->db->update('t_suratkeluar_m',$data,$where);
        return $res;
    }
    public function get_receive_token(){
        $data=$this->db->get('token');
        return $data;
    }
   public function get_receive($nomor){
        $this->db->select("a.kodesuratmasuk,a.nosuratmasukdivisi,a.empniktujuan,b.playerid");
        $this->db->join("token as b","b.userid = a.empniktujuan","inner");
        $this->db->where_in('a.kodesuratmasuk',$nomor);
        $data=$this->db->get('t_suratmasuk_m as a');
        return $data;
   }
   public function get_receive_disposisi($nikdispos){
        $this->db->where_in('userid',$nikdispos);
        $data=$this->db->get('token');
        return $data;
   }
   public function get_receive_paraf($kode,$user){
        $this->db->select("a.*,b.playerid");
        $this->db->join("token as b","b.userid = a.empnikparaf","left");
        $this->db->where('a.kodesuratutama',$kode);
        $this->db->where('a.empnikparaf <>',$user);
        $data=$this->db->get('t_surat_paraf as a');
        return $data;
   }
   public function get_receive_validasi($kode){
        $this->db->select('a.kodesuratkeluar,a.empnik_asal,a.empname_asal,b.playerid');
        $this->db->From('t_suratkeluar_m as a');
        $this->db->join('token as b','b.userid=a.empnik_asal','left');
        $this->db->where('a.kodesuratkeluar',$kode);
        $data=$this->db->get();
        return $data;
   }
   public function update_memo($where,$data){
        $res=$this->db->update('t_suratkeluar_memo',$data,$where);
        return $res;
    }
    public function delete_session($where){
        $this->db->where($where);
        $res =$this->db->delete('session_paraf');
        return $res;
    }
    public function get_dtm($kode){
        $this->db->where('kodesuratbawah',$kode);
        $data = $this->db->get('t_surat_deployment');
        return $data;
        
    }
    public function get_ms($kode){
        $this->db->select('a.*,b.*');
        $this->db->join('m_pesan as b','b.kodepesan=a.catatanatasan','left');
        $this->db->where('a.kodesuratmasuk',$kode);
        $data = $this->db->get('t_suratmasuk_m a');
        return $data;
        
    }
    public function get_ps($kode){
        $this->db->where('kodeprogressurat',$kode);
        $data = $this->db->get('m_progressurat');
        return $data;
        
    }
    public function get_psn($kode){
        $this->db->where('kodepesan',$kode);
        $data = $this->db->get('m_pesan');
        return $data;
        
    }
    public function get_pr($kode){
        $this->db->where('kodesuratmasuk',$kode);
        $data = $this->db->get('t_suratmasuk_d');
        return $data;
        
    }
    
    public function get_kodesuratbawah($kode){
    
        $this->db->where('kodesuratatas',$kode);
        $data=$this->db->get('t_surat_deployment');
        return $data;
   }
   public function get_kodesuratutama($kode){
    
        $this->db->where('kodesuratutama',$kode);
        $data=$this->db->get('t_surat_deployment');
        return $data;
   }
   public function hapus_disposisi($where_d,$where_deploy,$where_m){
        $this->db->where($where_d);
        $res =$this->db->delete('t_suratmasuk_d');
        
        $this->db->where($where_deploy);
        $res2 =$this->db->delete('t_surat_deployment');
        
        $this->db->where($where_m);
        $res3 =$this->db->delete('t_suratmasuk_m');
        
        return $res3;
        
    }
   public function get_edit_format($kode){
        $this->db->select("a.*,b.memo,DATE_FORMAT(a.tanggalsurat,'%d/%m/%Y')tanggalsuratnice,(SELECT COUNT(*) FROM t_surat_paraf WHERE validasiparaf = 1 AND kodesuratutama =a.kodesuratkeluar) as jmlsudahparaf");
        $this->db->join("t_suratkeluar_memo as b","b.kodesuratkeluar=a.kodesuratkeluar","left");
        $this->db->where('a.kodesuratkeluar',$kode);
        $data =$this->db->get("t_suratkeluar_m as a");
        return $data;
   }
   public function get_datasuratprogress($kode){
        $this->db->join('token as b','b.userid=a.pembuat','LEFT');
        $this->db->join('m_progressurat as c','c.kodeprogressurat=a.status','left');
        $this->db->join('m_progress as d','d.kodeprogress=a.kodeprogressurat','left');
        $this->db->where('a.kodesuratmasuk',$kode);
        $data=$this->db->get('t_suratmasuk_m as a');
        return $data;
    }
    public function tutup_progress($where,$data_d,$data_m){
        $this->db->update('t_suratmasuk_d',$data_d,$where);
        $update = $this->db->update('t_suratmasuk_m',$data_m,$where);
        return $update;
    }
    public function get_listprogress($kode){
        $this->db->select('a.kodesuratmasuk,a.empnamedisposisi,a.empnikdisposisi,a.status as statusd,b.progressurat,a.kodeprogressurat,c.status as statusm,c.nosuratmasukdivisi,a.keterangan');
        $this->db->join('m_progressurat b','b.kodeprogressurat=a.kodeprogressurat','left');
        $this->db->join('t_suratmasuk_m c','c.kodesuratmasuk=a.kodesuratmasuk','left');
        $this->db->where('a.kodesuratmasukatas',$kode);
        $data = $this->db->get('t_suratmasuk_d a');
        return $data;
    }
    public function cekprogress($kode){
        $this->db->where('kodesuratmasuk',$kode);
        $data = $this->db->get('t_suratmasuk_m');
        return $data;
    }
	public function log_activity($data){
		$res = $this->db->insert('log_transaksi',$data);
        return $res;
	}
}