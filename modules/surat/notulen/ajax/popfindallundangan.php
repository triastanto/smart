<?
session_start();
include_once('../../../../config.php');
include_once('../../../../includes/functions.php');
include_once('../../../../includes/koneksi.php');
include_once('../../../../includes/ajaxpagination.php');

?>
<script type="text/javascript" src="../../../../javascript/jquery-1.8.3.min.js"></script> 
    <link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/reset.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/grid.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/layout.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/nav.css" media="screen" />
    <style>
    	*{font-family:Trebuchet MS,"Arial", sans-serif;color:black; font-size:11px;}

    </style> 
<title>Search Material</title>
<table width="100%" cellpadding="2" cellspacing="2" border="0" style="border: 1px solid #DEDEDE;font-size: 10pt;" class="head_sr">
	<tr height="30">
		<td width="15%">CARI :
		</td>
		<td width="40%">
            <select name="pilihcari" id="pilihcari">
                <option value="">-Pilih Pencarian-</option>
                <option value="empnik">NIK</option>
                <option value="empname">Nama</option>
                <option value="emp_hrp1000_s_short">Abbreviasi</option>
                <option value="empkostl">Kode CC</option>
               <option value="emportx">Cost Center</option>
            </select>&nbsp;
			<input type="text" name="txtval" id="txtval" size="40" value="<?=$_REQUEST['fv']?>" />			
			<input type="hidden" name="no" id="no" size="10" value="<?=$_REQUEST['no']?>" />			
		</td>	
        <td width="38%" ><input type="button" name="button" id="cari" value="Cari" class='btn btn-blue' >&nbsp; 
        </td>
	</tr>
</table>
<div id="view">
	<table width="100%" border="0">
		<tr class="head_tr">
			<th align="center" width="3%">NO</th>
			<th align="center" width="10%">NIK</th>
			<th align="center" width="20%">NAMA</th>
			<th align="center" width="10%">ABBREVIASI</th>
			<th align="center" width="10%">KODE CC</th>
			<th align="center" width="47%">COST CENTER</th>
		</tr>
    	<?	
    		// $rsql = "SELECT hic.structdisp.empkostl, hic.structdisp.emportx FROM hic.structdisp group by hic.structdisp.empkostl limit 0,20";
    		$rsql = "SELECT
						hic.structdisp.*
					FROM
						hic.structdisp
					WHERE
						hic.structdisp.`no` = '1'
					ORDER BY hic.structdisp.emp_hrp1000_s_short ASC limit 0,15";
    		// echo $rsql;
    		$rs = mysql_query($rsql);	
    		$i=1;
    		while($lev=mysql_fetch_array($rs)){
    		    $i%2 == 0 ? $cl='#caefff' : $cl='#e4f3f9';
    			echo'
    			<tr bgcolor="'.$cl.'" onClick="ada(\''.$lev['empnik'].'\' , \''.$lev['empname'].'\' , \''.$lev['empkostl'].'\' , \''.$lev['emportx'].'\', \''.$lev['emp_hrp1000_s_short'].'\')">
    				<td align="center">'.$i.'</td>
    				<td align="left">'.$lev['empnik'].'</td>
    				<td align="left">'.$lev['empname'].'</td>
    				<td align="left">'.$lev['emp_hrp1000_s_short'].'</td>
    				<td align="left">'.$lev['empkostl'].'</td>
    				<td align="left">'.$lev['emportx'].'</td> 
    			</tr>';
                $i++;	
    		}
    		@mysql_free_result($rs);	
    	?>
	</table>
</div>
<div id="naf">
	<?
		$rsql 	= "SELECT
						hic.structdisp.*
					FROM
						hic.structdisp
					WHERE
						hic.structdisp.`no` = '1'
					ORDER BY hic.structdisp.emp_hrp1000_s_short ASC";
		$hd		= "";
		$hal	= 0;
		$order 	= "";
		$pager 	= new ajaxpagination($conn, $rsql, $hd, $hal, 15, 15, $order);
		$rs = $pager->paginate();	
		include_once('../../../../includes/nav.php');
	?>
</div>
<script type="text/javascript" >
$(document).ready( function() {
	$('#naf a').click(function() {
		$("#view").load("../../../../modules/surat/notulen/ajax/popfindallundangan_detil.php", {'hal': $(this).attr("title"), 'mod' : $("#txtmod").val()});
		$('#naf').fadeOut('slow').load("../../../../modules/surat/notulen/ajax/popfindallundangan_page.php", {'hal':$(this).attr("title") }).fadeIn("slow");
		// alert('a');
	}); 
	$('#cari').click(function() {
		var a = $("#txtval").val();
		var pilih = $("#pilihcari").val();
		if (a=='' || pilih==''){
			alert("Masukkan nilai yang akan dicari!");
		}else{
			// alert("tes");
			$("#view").load("../../../../modules/surat/notulen/ajax/popfindallundangan_detil.php", {'pilih': pilih, 'nm': a });
			$('#naf').fadeOut('slow').load("../../../../modules/surat/notulen/ajax/popfindallundangan_page.php", {'hal':$(this).attr("title"), 'nm':a }).fadeIn("slow");
		}
		e.stopImmediatePropagation();
		// alert('b');
	}); 
}); 
function ada(empnik, empname, empkostl, emportx){ 
	var no = $("#no").val();
	parent.$("#empnikdisposisi"+no).val(empnik);
	parent.$("#empnamedisposisi"+no).val(empname);
	parent.$("#empkostldisposisi"+no).val(empkostl);
	parent.$("#emportxdisposisi"+no).val(emportx);
	parent.$("#empshortdisposisi"+no).val(emportx);
	parent.$.colorbox.close(); 
}
</script>