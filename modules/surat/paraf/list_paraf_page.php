<?php

	include_once('../../../config.php');
	include_once('../../../includes/functions.php');
	include_once('../../../includes/ajaxpagination.php');
	include_once('../../../includes/koneksi.php');
	
	$hal 	= mysql_real_escape_string($_REQUEST['hal']);
	$nm 	= mysql_real_escape_string($_REQUEST['nm']);
	$pilih	= mysql_real_escape_string($_REQUEST['pilih']);
    
	if ($pilih=='tanggalsurat'){
	   $nm=_convertDate($nm);
	}
	
	if($_REQUEST['group_id']==1){
		if (trim($nm)!=''){
			$whr ="where t_suratkeluar_m.$pilih like '%".$nm."%' and statuscancel='0'";
		}else{
			$whr ="and statuscancel='0'";
		}
		$rsql 	= "	SELECT
						t_surat_paraf.*,
						t_suratkeluar_m.*
					FROM
						t_surat_paraf
						INNER JOIN t_suratkeluar_m ON t_surat_paraf.kodesuratutama = t_suratkeluar_m.kodesuratkeluar ".$whr." order by tanggalsurat desc";
	}else{
		if (trim($nm)!=''){
			$whr ="and $pilih like '%".$nm."%' and statuscancel='0'";
		}else{
			$whr ="and statuscancel='0'";
		}
		$rsql 	= "SELECT
						t_surat_paraf.*,
						t_suratkeluar_m.*
					FROM
						t_surat_paraf
						INNER JOIN t_suratkeluar_m ON t_surat_paraf.kodesuratutama = t_suratkeluar_m.kodesuratkeluar
					where t_surat_paraf.empnikparaf='".$_REQUEST['userid']."' ".$whr." order by  tanggalsurat desc";
	}
	
	// echo $rsql;
	$hd		= "";
	$order 	= "";
    $pager 	= new ajaxpagination($conn, $rsql, $hd, $hal, 15, 15, $order);
	$rs = $pager->paginate();
	include_once('../../../includes/nav.php');
	
?>
<script type="text/javascript" >
	var x = 1;
	function cek(no){
		var kodesuratkeluar	= $('#kodesuratkeluar'+no).val();
		var validasiparaf 	= $('#validasiparaf'+no).val();
		var userid 			= $('#user').val();
		
		$.ajax({
			url: "modules/surat/paraf/cekstatus.php?kodesuratkeluar="+kodesuratkeluar+"&validasiparaf="+validasiparaf+"&userid="+userid,
			cache: false,
			success: function(msg){
				$("#notifikasi"+no).html(msg);
			}
		});
		var waktu = setTimeout("cek("+no+")",2000);
	}

	$(document).ready( function(){
		$('#naf a').click(function() {
			var userid 		= $('#user').val();
			var group_id 	= $('#group_id').val();
			$("#view").load("modules/surat/paraf/list_paraf_detil.php", {'hal': $(this).attr("title"), 'userid': userid, 'group_id': group_id });
			$('#naf').fadeOut('slow').load("modules/surat/paraf/list_paraf_page.php", {'hal':$(this).attr("title"), 'userid': userid, 'group_id': group_id}).fadeIn("slow");
		});
		
		var jumlah = $('#jumlah').val();
		for(a=1; a<=jumlah; a++){
			// alert(a);
			cek(a);
		}
	});
</script>