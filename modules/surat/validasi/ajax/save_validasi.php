<?		
session_start();
if (!isset($_SESSION['user'])) {
	echo '<script language="Javascript">window.location="index.php"</script>';
}
include_once('../../../../config.php');
include_once('../../../../includes/koneksi.php');
include_once('../../../../includes/functions.php');

if (trim($_POST['kodesuratkeluar']) == ''){ $error[] = '- Kode surat keluar harus diisi'; }
if (trim($_POST['nosuratkeluar']) == ''){ $error[] = '- No surat harus diisi'; }
if (trim($_POST['isiringkasan']) == ''){ $error[] = '- Isi ringkasan arsip harus diisi'; }
// if (trim($_POST['empnikparaf']) == ''){ $error[] = '- NIK Paraf harus diisi'; }
// if (trim($_POST['empnameparaf']) == ''){ $error[] = '- Nama Paraf harus diisi'; }
// if (trim($_POST['keteranganparaf']) == ''){ $error[] = '- Keterangan Paraf harus diisi'; }

if (isset($error)) {
	echo "Error: \n ".implode("\n ", $error);
} else {
	
	function send_notification($penerima,$pesan,$heading){
        	$content = array(
			"en" => $pesan
			);
		
		$fields = array(
			'app_id' => "1935ab00-42e8-41b4-b364-5b86c47c60c2",
			'include_player_ids' =>$penerima,
            'headings' => array("en" =>$heading),
			'data' => array("foo" => "bar"),
            'android_group'  => "Smart",
            'android_group_message' => array("en" => "Smart"),
			'contents' => $content
		);
		
		$fields = json_encode($fields);
    	//print("\nJSON sent:\n");
    	//print($fields);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
												   'Authorization: Basic NGEwMGZmMjItY2NkNy0xMWUzLTk5ZDUtMDAwYzI5NDBlNjJj'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);
		
		return $response;
	
	
    	$response = sendMessage();
    	$return["allresponses"] = $response;
    	$return = json_encode( $return);
	
		//print("\n\nJSON received:\n");
		//print($return);
		//print("\n");
    }
	
	
	$fileName 	= $_FILES['userfile']['name'];
	$tmpName 	= $_FILES['userfile']['tmp_name']; 
	$fileSize 	= $_FILES['userfile']['size']; 
	$fileType 	= $_FILES['userfile']['type'];
	$eks = explode("/",$fileType);
	$ekstensi = $eks[1];
	$uploadDir = "../../../../surat/";
	$uploadDir2 = "surat/";
	$namafile = $_POST['kodesuratkeluar']."_".$fileName;
	$filePath = $uploadDir.$namafile;
	$filePath2 = $uploadDir2.$namafile;
	$result = move_uploaded_file($tmpName,$filePath);
	
	if($fileName!=''){
		if ($_POST['act']=='new'){
			$sql = "update t_suratkeluar_m set 
						tembusantambahan= '".$_POST['tembusantambahan']."',
						keteranganparaf= '".$_POST['keteranganparaf']."',
						fileid='".$fileName."',
						filename='".$namafile."',
						type='".$fileType."',
						size='".$fileSize."',
						path='".$filePath2."' 
					where kodesuratkeluar = '".$_POST['kodesuratkeluar']."'";
			$sql2 = "update t_suratkeluar_memo set memo= '".$_POST['memo']."' where kodesuratkeluar = '".$_POST['kodesuratkeluar']."'";
			$qpar = mysql_query("select * from t_surat_paraf where kodesuratutama='".$_POST['kodesuratkeluar']."'");
			while($dpar = mysql_fetch_array($qpar)){
				mysql_query("update t_surat_paraf set validasiparaf='0', tanggalparaf='', persetujuan='', keterangan_paraf='' where kodesuratutama='".$_POST['kodesuratkeluar']."'");
			}
		}else{
			$sql = "update t_suratkeluar_m set 
						tembusantambahan= '".$_POST['tembusantambahan']."',
						keteranganparaf= '".$_POST['keteranganparaf']."',
						fileid='".$fileName."',
						filename='".$namafile."',
						type='".$fileType."',
						size='".$fileSize."',
						path='".$filePath2."' 
					where kodesuratkeluar = '".$_POST['kodesuratkeluar']."'";		
			$sql2 = "update t_suratkeluar_memo set memo= '".$_POST['memo']."' where kodesuratkeluar = '".$_POST['kodesuratkeluar']."'";
			$qpar = mysql_query("select * from t_surat_paraf where kodesuratutama='".$_POST['kodesuratkeluar']."'");
			while($dpar = mysql_fetch_array($qpar)){
				mysql_query("update t_surat_paraf set validasiparaf='0', tanggalparaf='', persetujuan='', keterangan_paraf='' where kodesuratutama='".$_POST['kodesuratkeluar']."'");
			}
		}
	}else{
		if ($_POST['act']=='new'){
			$sql = "update t_suratkeluar_m set tembusantambahan= '".$_POST['tembusantambahan']."', keteranganparaf= '".$_POST['keteranganparaf']."' where kodesuratkeluar = '".$_POST['kodesuratkeluar']."'";
			$sql2 = "update t_suratkeluar_memo set memo= '".$_POST['memo']."' where kodesuratkeluar = '".$_POST['kodesuratkeluar']."'";
			$qpar = mysql_query("select * from t_surat_paraf where kodesuratutama='".$_POST['kodesuratkeluar']."'");
			while($dpar = mysql_fetch_array($qpar)){
				mysql_query("update t_surat_paraf set validasiparaf='0', tanggalparaf='', persetujuan='', keterangan_paraf='' where kodesuratutama='".$_POST['kodesuratkeluar']."'");
			}
		}else{
			//=============================================MEMO================================================//
			$ck = mysql_fetch_array(mysql_query("select * from t_suratkeluar_memo where kodesuratkeluar = '".$_POST['kodesuratkeluar']."'"));
			if($ck['memo']==$_POST['memo']){
				// echo'sama';
				// $sql = "update t_suratkeluar_m set tembusantambahan= '".$_POST['tembusantambahan']."' where kodesuratkeluar = '".$_POST['kodesuratkeluar']."'";		
				// $sql2 = "update t_suratkeluar_memo set memo= '".$_POST['memo']."' where kodesuratkeluar = '".$_POST['kodesuratkeluar']."'";
			}else{
				// echo'tidak sama';
				$sql = "update t_suratkeluar_m set tembusantambahan= '".$_POST['tembusantambahan']."' where kodesuratkeluar = '".$_POST['kodesuratkeluar']."'";		
				$sql2 = "update t_suratkeluar_memo set memo= '".$_POST['memo']."' where kodesuratkeluar = '".$_POST['kodesuratkeluar']."'";
				$sav = mysql_query($sql);
				$sav2 = mysql_query($sql2);
				
				mysql_query("insert t_suratmasuk_memo_history set 
								memo= '".$_POST['memo']."', 
								empnikparaf= '".$_POST['empnik_asal']."', 
								empnameparaf= '".$_POST['empname_asal']."', 
								waktu= '".date('Y-m-d H:i:s')."', 
								kodesurat = '".$_POST['kodesuratkeluar']."',
								perubahan='Memo'");
			}
			//=============================================KET PARAF================================================//
			$ck2 =mysql_fetch_array(mysql_query("select * from t_suratkeluar_m where kodesuratkeluar = '".$_POST['kodesuratkeluar']."'"));
			if($ck2['keteranganparaf']==$_POST['keteranganparaf']){
				//tidak perlu update history
				// echo'a';
			}else{
				// echo'ab';
				mysql_query("update t_suratkeluar_m set keteranganparaf='".$_POST['keteranganparaf']."' where kodesuratkeluar='".$_POST['kodesuratkeluar']."' ");
				mysql_query("insert t_suratmasuk_memo_history set 
								memo= '".$_POST['memo']."', 
								empnikparaf= '".$_POST['empnik_asal']."', 
								empnameparaf= '".$_POST['empname_asal']."', 
								waktu= '".date('Y-m-d H:i:s')."', 
								kodesurat = '".$_POST['kodesuratkeluar']."',
								perubahan='Keterangan Paraf'");
			}
			//=============================================NO SURAT================================================//
			$ck3 =mysql_fetch_array(mysql_query("select * from t_suratkeluar_m where kodesuratkeluar='".$_POST['kodesuratkeluar']."'"));
			if($ck3['nosuratkeluar']==$_POST['nosuratkeluar']){
				//tidak perlu update history
				// echo'a';
			}else{
				// echo'ab';
				mysql_query("update t_suratkeluar_m set nosuratkeluar='".$_POST['nosuratkeluar']."' where kodesuratkeluar='".$_POST['kodesuratkeluar']."'");
				$sqlno = mysql_query("select * from t_surat_deployment where kodesuratutama='".$_POST['kodesuratkeluar']."'");
				while($qno =mysql_fetch_array($sqlno)){
					mysql_query("update t_suratmasuk_m set nosuratmasukdivisi='".$_POST['nosuratkeluar']."' where kodesuratkeluar='".$qno['kodesuratbawah']."'");
				}
				mysql_query("insert t_suratmasuk_memo_history set 
								memo= '".$_POST['memo']."', 
								empnikparaf= '".$_POST['empnik_asal']."', 
								empnameparaf= '".$_POST['empname_asal']."', 
								waktu= '".date('Y-m-d H:i:s')."', 
								kodesurat = '".$_POST['kodesuratkeluar']."',
								perubahan='No Surat'");
			}
			//=============================================TANGGAL================================================//
			$ck4 =mysql_fetch_array(mysql_query("select * from t_suratkeluar_m where kodesuratkeluar = '".$_POST['kodesuratkeluar']."'"));
			if($ck4['tanggalsurat']==$_POST['tanggalsurat']){
				//tidak perlu update history
				// echo'a';
			}else{
				$t = explode("-",$_POST['tanggalsurat']); 			$tanggalsurat = $t[2]."-".$t[1]."-".$t[0];
				mysql_query("update t_suratkeluar_m set tanggalsurat='".$tanggalsurat."' where kodesuratkeluar='".$_POST['kodesuratkeluar']."' ");
				mysql_query("insert t_suratmasuk_memo_history set 
								memo= '".$_POST['memo']."', 
								empnikparaf= '".$_POST['empnik_asal']."', 
								empnameparaf= '".$_POST['empname_asal']."', 
								waktu= '".date('Y-m-d H:i:s')."', 
								kodesurat = '".$_POST['kodesuratkeluar']."',
								perubahan='Tanggal Surat'");
			}
			$qpar = mysql_query("select * from t_surat_paraf where kodesuratutama='".$_POST['kodesuratkeluar']."'");
			while($dpar = mysql_fetch_array($qpar)){
				mysql_query("update t_surat_paraf set validasiparaf='0', tanggalparaf='', persetujuan='', keterangan_paraf='' where kodesuratutama='".$_POST['kodesuratkeluar']."'");
			}
			//kirim notif ke pemaraf
			$penerima = mysql_query("select a.*,b.playerid from t_surat_paraf a left join token b on b.userid=a.empnikparaf where kodesuratutama='".$_POST['kodesuratkeluar']."'");
			while($r = mysql_fetch_array($penerima)){
			$datapenerima = array($r['playerid']);
			$pesan ="Ada Surat Yang Harus Anda Paraf";
			$heading="Paraf";
			send_notification($datapenerima,$pesan,$heading);
			}
			//echo "\n Data berhasil diubah";
			
		}
	}
	// echo "\n ".$sql;
	// echo "\n ".$sql2;
	
	
	if (mysql_query){
		echo " OK ".base64_encode('surat/validasi')." ".base64_encode('list_validasi');
	}else{
		echo "Error : \n Data gagal tersimpan";
	}
	//log aktivity
		$log_time = date('Y-m-d H:i:s');
		$log_user = $_SESSION['user'];
		$log_aksi = "update";
		$log_url  = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?"https" : "http") . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		$log_item ="Validasi";
		$kode     =$_POST['kodesuratkeluar'];
		$log_ip = get_client_ip();
		$useragent = $_SERVER['HTTP_USER_AGENT'];
		$info = $useragent;
		$log_device = $info;
		log_activity($log_time,$log_user,$log_aksi,$log_url,$log_item,$kode,$log_ip,$log_device);
}
//die();
?>