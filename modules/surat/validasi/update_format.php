<?
    out("title", "KORESPONDENSI >> VALIDASI >> UPDATE FORMAT RINGKASAN");
    $u_agent = $_SERVER['HTTP_USER_AGENT'];
    if(preg_match('/Chrome/i',$u_agent)){
    	//$tg = "780px;";
    }else{
    	//$tg = "740px;";
    }
    //untuk memanggil fungsi group ID user atau admin
    $grup_id = $_SESSION['group_id'];
    $act	= base64_decode($_REQUEST['act']);
    if ($act=='edit'){
    	$idp=base64_decode($_REQUEST['idp']);
    	$dt	= mysql_fetch_array(mysql_query("select * from t_suratkeluar_m where kodesuratkeluar='".$idp."'"));
    	$memo	= mysql_fetch_array(mysql_query("select * from t_suratkeluar_memo where kodesuratkeluar='".$idp."'"));
		$kodesuratkeluar = $dt['kodesuratkeluar'];
		$tanggal = $dt['tanggalsurat'];
    }else{
    	$act='new';
    }
?>
<script type="text/javascript" src="javascript/jquery.form.js"></script>	
<script type="text/javascript">
	$(document).ready( function() {
		$(".popup").colorbox({ 		iframe:true		,width:"80%"		,height:"70%"	});
		
			$(".datepicker").datepicker({ dateFormat: "dd-mm-yy" }).val();

			var options = { 
				beforeSubmit:  showRequest,  // pre-submit callback 
				success:       showResponse,  // post-submit callback 
				url:'modules/surat/validasi/ajax/save_validasi.php',
				iframe:true
			};
			$('#myfm').submit(function(event) { 
				event.preventDefault();
				$(this).ajaxSubmit(options); 
				event.stopImmediatePropagation();
				return false; 
			});
		
	});

	function showRequest(formData, jqForm, options){ 
		var form = jqForm[0]; 
		return true; 
	} 
	 
	function showResponse(responseText, statusText, xhr, $form){
		var mydata = responseText.split(" ");
		if (mydata[0]=="OK"){
			alert("Data sudah disimpan!");
			window.location.href = "?mod="+mydata[1]+"&op="+mydata[2];
		}else{
			alert(responseText); 
		}
	}
</script>
<script src="ckeditor/ckeditor.js"></script>
<form method="POST" action="modules/surat/validasi/ajax/save_validasi.php" enctype="multipart/form-data" name="myfm" id="myfm" >
	<div style="padding:3px; width:99%;  border:2px #E6F0F3 solid;">
		<table width="99%" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td width="30%" valign="top">
					<table width="100%">
						<tr>
							<td width="35%"></td>
							<td width="5%"><strong></strong></td>
							<td width="60%"></td>
						</tr>
						<tr>
							<td>No Surat keluar</td>
							<td><strong>:</strong></td>
							<td>
								<input type="text" name="nosuratkeluar" id="nosuratkeluar" value="<?=$dt['nosuratkeluar']?>" size="30" />
								<input type="hidden" name="empnik_asal" id="empnik_asal" value="<?=$dt['empnik_asal']?>" size="30" />
								<input type="hidden" name="empname_asal" id="empname_asal" value="<?=$dt['empname_asal']?>" size="30" />
							</td>
						</tr>
						<tr>
							<td>Tanggal Surat keluar</td>
							<td><strong>:</strong></td>
							<td><input type="text" name="tanggalsurat" id="tanggalsurat" class="datepicker" value="<?=_convertDate($tanggal)?>" size="10" /></td>
						</tr>
					</table>
				</td>
				<td width="35%" colspan="2" valign="top">					
					<table width="100%">
						<tr>
							<td width="30%"></td>
							<td width="2%"><strong></strong></td>
							<td width="68%"></td>
						</tr>
						<tr>
							<td>Judul Surat</td>
							<td><strong>:</strong></td>
							<td>
								<select name="kodejudul" style="width:75%;">									
									<?
										$sql1 = mysql_query("select * from m_judul");
										echo'<option value="" > -- Pilih Judul Surat -- </option>';
										while ($rdt=mysql_fetch_array($sql1)){
											echo '<option value="'.$rdt[0].'"';
											if ($rdt[0]==$dt['kodejudul']){ echo ' selected '; };
											echo '>'.$rdt[0].' - '.$rdt[1].'</option>';
										}
									?>
								</select>
							</td>
						</tr>
						<tr>
							<td valign="top">Perihal</td>
							<td valign="top"><strong>:</strong></td>
							<td><input type="text" name="isiringkasan" id="isiringkasan" value="<?=$dt['isiringkasan']?>" size="40" readonly /></td>
						</tr>
					</table>
				</td>
				<td width="35%" valign="top">
					<table width="100%">
						<tr>
							<td width="30%"></td>
							<td width="2%"><strong></strong></td>
							<td width="68%"></td>
						</tr>
						<tr>
							<td>Keterangan Paraf</td>
							<td><strong>:</strong></td>
							<td><input type="text" name="keteranganparaf" id="keteranganparaf" value="<?=$dt['keteranganparaf']?>" size="40" /></td>
						</tr>
						<tr>
							<td>Lampiran</td>
							<td><strong>:</strong></td>
							<td><input type="file" name="userfile" /></td>
						</tr>
						<tr>
							<td colspan="3">
								<?
									echo'<table width="100%">';
									if($act=='new'){
										echo'
											<input type="hidden" name="jumpar" value="5" />
											<tr>
												<td>Diparaf 1</td>
												<td><strong>:</strong></td>
												<td>											
													<input type="text" name="empnikparaf1" id="empnikparaf1" value="'.$dt['empnikparaf'].'" size="5" readonly />
													<img src="images/magnifier.png" border="0" onclick="paraf(1)" />
													<input type="text" name="empnameparaf1" id="empnameparaf1" value="'.$dt['empnameparaf'].'" size="20" readonly />
												</td>
											</tr>
											<tr>
												<td>Diparaf 2</td>
												<td><strong>:</strong></td>
												<td>											
													<input type="text" name="empnikparaf2" id="empnikparaf2" value="'.$dt['empnikparaf'].'" size="5" readonly />
													<img src="images/magnifier.png" border="0" onclick="paraf(2)" />
													<input type="text" name="empnameparaf2" id="empnameparaf2" value="'.$dt['empnameparaf'].'" size="20" readonly />
												</td>
											</tr>
											<tr>
												<td>Diparaf 3</td>
												<td><strong>:</strong></td>
												<td>
													<input type="text" name="empnikparaf3" id="empnikparaf3" value="'.$dt['empnikparaf'].'" size="5" readonly />
													<img src="images/magnifier.png" border="0" onclick="paraf(3)" />
													<input type="text" name="empnameparaf3" id="empnameparaf3" value="'.$dt['empnameparaf'].'" size="20" readonly />
												</td>
											</tr>';
										
									}else{
										//kosong
									}
									echo'</table>';
								?>
							</td>
						</tr>
						<tr>
							<td></td>
							<td><strong></strong></td>
							<td><input type="hidden" name="kodesuratkeluar" id="kodesuratkeluar" value="<?=$kodesuratkeluar?>" size="30" readonly /></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" width="55%">
					<table width="99%" cellpadding="0" cellspacing="0" border="0">
						<tr><td><b>Memo Surat : </b></td></tr>
						<tr>
							<td>
								<textarea class="ckeditor" cols="50%" id="editor1" name="memo" rows="3"><?=$memo['memo']?></textarea>
							</td>
						</tr>
					</table>
				</td>
				<td colspan="2" width="45%" valign="top">
					<table width="99%" cellpadding="0" cellspacing="0" border="0">
						<tr><td><b>TEMBUSAN LAIN-LAIN : </b></td></tr>
						<tr>
							<td>
								<textarea cols="60%" rows="6" name="tembusantambahan"><?=$dt['tembusantambahan']?></textarea>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<p>&nbsp;</p>
		<table width="99%" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td colspan="2" align="center">
					<input name="act" type="hidden" value="<?=$act?>" />
					<input name="groupid" type="hidden" value="<?=$_SESSION['group_id']?>" />
					<input class="btn btn-blue" style="width:80px;" type="submit" name="submit" value="Kirim" />
					<?
						echo'
							<input class="btn btn-blue" style="width:80px;" type="button" name="Cancel" value="Cancel" onclick="window.location=\'?mod='.base64_encode('surat/validasi').'&op='.base64_encode('list_validasi').' \'" />
						';
					?>
				</td>
				<td>&nbsp;</td>
			</tr>
		</table>
	</div>
</form>
