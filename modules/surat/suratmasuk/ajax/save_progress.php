<?		
session_start();
if (!isset($_SESSION['user'])) {
	echo '<script language="Javascript">window.location="index.php"</script>';
}
include_once('../../../../config.php');
include_once('../../../../includes/koneksi.php');
include_once('../../../../includes/functions.php');

if (trim($_POST['kodesuratmasuk']) == ''){ $error[] = '- Kode surat masuk harus diisi'; }

if (isset($error)) {
	echo "Error: \n ".implode("\n ", $error);
} else {
	$fileName 	= $_FILES['userfile']['name'];
	$tmpName 	= $_FILES['userfile']['tmp_name']; 
	$fileSize 	= $_FILES['userfile']['size']; 
	$fileType 	= $_FILES['userfile']['type'];
	$eks = explode("/",$fileType);
	$ekstensi = $eks[1];
	$uploadDir 	= "/nfs/arsip/surat/";
	$uploadDir2 = "surat/";
	
	if ($_POST['act']=='edit'){
		if($fileName!=''){
			
			if($fileSize>2097152){
				echo "Error : \n Lampiran Anda Melebihi 2 Mb";
				die();
			}else{
				if($fileName!=''){
					$namafile 	= $_POST['kodesuratmasuk']."_".$fileName;
					$filePath 	= $uploadDir.$namafile;
					$filePath2 	= $uploadDir2.$namafile;
					if($fileSize=="" or $fileSize==0){
						echo "Error : \n Maaf file yang anda upload tidak terbaca oleh sistem kami";
						die();
					}else{
						$result = move_uploaded_file($tmpName,$filePath); 
					}
				}
				$cekposisi = mysql_fetch_array(mysql_query("SELECT * FROM t_surat_deployment where kodesuratbawah ='".$_POST['kodesuratmasuk']."'"));
				$cek = mysql_num_rows(mysql_query("SELECT t_surat_deployment.*, t_suratmasuk_m.kodesifatsurat
													FROM
														t_surat_deployment INNER JOIN t_suratmasuk_m ON t_surat_deployment.kodesuratbawah = t_suratmasuk_m.kodesuratmasuk
													where kodesuratutama ='".$cekposisi['kodesuratutama']."' and kodesifatsurat='1'
														or kodesuratutama ='".$cekposisi['kodesuratutama']."' and kodesifatsurat='3'"));

				if($cek>1){
					mysql_query("update t_suratmasuk_m  set status='2' where kodesuratmasuk = '".$_POST['kodesuratmasuk']."'");
					mysql_query("delete from t_surat_history_progress  where kodesuratmasuk = '".$_POST['kodesuratmasuk']."'");
					for($i=1; $i<=$_POST['jm']; $i++){
						if(trim($_POST['kodeprogress'.$i]!='')){
							mysql_query("insert t_surat_history_progress set
											kodesuratmasuk 		= '".$_POST['kodesuratmasuk']."',
											tanggalterimasurat 	= '".date('Y-m-d')."',
											kodeprogress 		= '".$_POST['kodeprogress'.$i]."',
											kodeprogressurat 	= '".$_POST['kodeprogressurat'.$i]."',
											keterangan 	= '".$_POST['keterangan'.$i]."'
							"); 
							mysql_query("update t_suratmasuk_d set
											tanggalupdate 	= '".date('Y-m-d')."',
											kodeprogress 	= '".$_POST['kodeprogress'.$i]."',
											kodeprogressurat= '".$_POST['kodeprogressurat'.$i]."',
											keterangan 	= '".$_POST['keterangan'.$i]."',
											fileid='".$fileName."',
											filename='".$namafile."',
											type='".$fileType."',
											size='".$fileSize."',
											path='".$filePath2."'
								where kodesuratmasuk = '".$_POST['kodesuratmasuk']."'");
							
							mysql_query("update t_suratmasuk_m set kodeprogressurat= '".$_POST['kodeprogressurat'.$i]."' where kodesuratmasuk = '".$_POST['kodesuratmasuk']."'");
						}
					}
				}else{
					
					$awal = substr($cekposisi['kodesuratutama'], 0, 2);
					if($awal=='SK'){
						$cekjum = mysql_num_rows(mysql_query("select * from t_surat_deployment where kodesuratutama='".$cekposisi['kodesuratutama']."'"));
						for($i=1; $i<=1; $i++){
							if(trim($_POST['kodeprogress'.$i]!='')){
								mysql_query("insert t_surat_history_progress set
												kodesuratmasuk 		= '".$_POST['kodesuratmasuk']."',
												tanggalterimasurat 	= '".date('Y-m-d')."',
												kodeprogress 		= '".$_POST['kodeprogress'.$i]."',
												kodeprogressurat 	= '".$_POST['kodeprogressurat'.$i]."',
												keterangan 	= '".$_POST['keterangan'.$i]."'
								"); 
								mysql_query("update t_suratmasuk_d set
												tanggalupdate 	= '".date('Y-m-d')."',
												kodeprogress 	= '".$_POST['kodeprogress'.$i]."',
												kodeprogressurat= '".$_POST['kodeprogressurat'.$i]."',
												keterangan 	= '".$_POST['keterangan'.$i]."',
												fileid='".$fileName."',
												filename='".$namafile."',
												type='".$fileType."',
												size='".$fileSize."',
												path='".$filePath2."'
									where kodesuratmasuk = '".$_POST['kodesuratmasuk']."'");
								if($cekjum>1){
									mysql_query("update t_suratmasuk_m  set status='2', kodeprogressurat='".$_POST['kodeprogressurat'.$i]."', tanggalpenyelesaian='".date('Y-m-d')."', catatan='".$_POST['catatan']."' where kodesuratmasuk = '".$_POST['kodesuratmasuk']."'");
								}else{
									mysql_query("update t_suratmasuk_m  set status='3', kodeprogressurat='3', tanggalpenyelesaian='".date('Y-m-d')."', catatan='".$_POST['catatan']."' where kodesuratmasuk = '".$_POST['kodesuratmasuk']."'");
								}
							}
						}
					}else{
						mysql_query("update t_suratmasuk_m  set status='2' where kodesuratmasuk = '".$_POST['kodesuratmasuk']."'");
						mysql_query("delete from t_surat_history_progress  where kodesuratmasuk = '".$_POST['kodesuratmasuk']."'");
						for($i=1; $i<=$_POST['jm']; $i++){
							if(trim($_POST['kodeprogress'.$i]!='')){
								mysql_query("insert t_surat_history_progress set
												kodesuratmasuk 		= '".$_POST['kodesuratmasuk']."',
												tanggalterimasurat 	= '".date('Y-m-d')."',
												kodeprogress 		= '".$_POST['kodeprogress'.$i]."',
												kodeprogressurat 	= '".$_POST['kodeprogressurat'.$i]."',
												keterangan 	= '".$_POST['keterangan'.$i]."'
								"); 
								mysql_query("update t_suratmasuk_d set
												tanggalupdate 	= '".date('Y-m-d')."',
												kodeprogress 	= '".$_POST['kodeprogress'.$i]."',
												kodeprogressurat= '".$_POST['kodeprogressurat'.$i]."',
												keterangan 	= '".$_POST['keterangan'.$i]."',
												fileid='".$fileName."',
												filename='".$namafile."',
												type='".$fileType."',
												size='".$fileSize."',
												path='".$filePath2."'
									where kodesuratmasuk = '".$_POST['kodesuratmasuk']."'");
								
								mysql_query("update t_suratmasuk_m set kodeprogressurat= '".$_POST['kodeprogressurat'.$i]."' where kodesuratmasuk = '".$_POST['kodesuratmasuk']."'");
							}
						}
					}
				}
			}
		}else{
		
			if($fileName!=''){
				$namafile 	= $_POST['kodesuratmasuk']."_".$fileName;
				$filePath 	= $uploadDir.$namafile;
				$filePath2 	= $uploadDir2.$namafile;
				$result = move_uploaded_file($tmpName,$filePath); 
			}
			$cekposisi = mysql_fetch_array(mysql_query("SELECT * FROM t_surat_deployment where kodesuratbawah ='".$_POST['kodesuratmasuk']."'"));
			$cek = mysql_num_rows(mysql_query("SELECT t_surat_deployment.*, t_suratmasuk_m.kodesifatsurat
												FROM
													t_surat_deployment INNER JOIN t_suratmasuk_m ON t_surat_deployment.kodesuratbawah = t_suratmasuk_m.kodesuratmasuk
												where kodesuratutama ='".$cekposisi['kodesuratutama']."' and kodesifatsurat='1'
													or kodesuratutama ='".$cekposisi['kodesuratutama']."' and kodesifatsurat='3'"));
			// echo"SELECT * FROM t_surat_deployment where kodesuratbawah ='".$_POST['kodesuratmasuk']."'";
			// echo $cek;
			// exit;
			if($cek>1){
				mysql_query("update t_suratmasuk_m  set status='2' where kodesuratmasuk = '".$_POST['kodesuratmasuk']."'");
				mysql_query("delete from t_surat_history_progress  where kodesuratmasuk = '".$_POST['kodesuratmasuk']."'");
				for($i=1; $i<=$_POST['jm']; $i++){
					if(trim($_POST['kodeprogress'.$i]!='')){
						mysql_query("insert t_surat_history_progress set
										kodesuratmasuk 		= '".$_POST['kodesuratmasuk']."',
										tanggalterimasurat 	= '".date('Y-m-d')."',
										kodeprogress 		= '".$_POST['kodeprogress'.$i]."',
										kodeprogressurat 	= '".$_POST['kodeprogressurat'.$i]."',
										keterangan 	= '".$_POST['keterangan'.$i]."'
						"); 
						mysql_query("update t_suratmasuk_d set
										tanggalupdate 	= '".date('Y-m-d')."',
										kodeprogress 	= '".$_POST['kodeprogress'.$i]."',
										kodeprogressurat= '".$_POST['kodeprogressurat'.$i]."',
										keterangan 	= '".$_POST['keterangan'.$i]."',
										fileid='".$fileName."',
										filename='".$namafile."',
										type='".$fileType."',
										size='".$fileSize."',
										path='".$filePath2."'
							where kodesuratmasuk = '".$_POST['kodesuratmasuk']."'");
						
						mysql_query("update t_suratmasuk_m set kodeprogressurat= '".$_POST['kodeprogressurat'.$i]."' where kodesuratmasuk = '".$_POST['kodesuratmasuk']."'");
					}
				}
			}else{
				
				$awal = substr($cekposisi['kodesuratutama'], 0, 2);
				// echo $cekposisi['kodesuratutama']."--".$awal;
				// exit;
				// echo $awal;
				if($awal=='SK'){
					// echo'ab -- '.$awal."-".$cek." -- ".$cekposisi['kodesuratutama'];
					$cekjum = mysql_num_rows(mysql_query("select * from t_surat_deployment where kodesuratutama='".$cekposisi['kodesuratutama']."'"));
					
					// exit;
					for($i=1; $i<=1; $i++){
						if(trim($_POST['kodeprogress'.$i]!='')){
							mysql_query("insert t_surat_history_progress set
											kodesuratmasuk 		= '".$_POST['kodesuratmasuk']."',
											tanggalterimasurat 	= '".date('Y-m-d')."',
											kodeprogress 		= '".$_POST['kodeprogress'.$i]."',
											kodeprogressurat 	= '".$_POST['kodeprogressurat'.$i]."',
											keterangan 	= '".$_POST['keterangan'.$i]."'
							"); 
							mysql_query("update t_suratmasuk_d set
											tanggalupdate 	= '".date('Y-m-d')."',
											kodeprogress 	= '".$_POST['kodeprogress'.$i]."',
											kodeprogressurat= '".$_POST['kodeprogressurat'.$i]."',
											keterangan 	= '".$_POST['keterangan'.$i]."',
											fileid='".$fileName."',
											filename='".$namafile."',
											type='".$fileType."',
											size='".$fileSize."',
											path='".$filePath2."'
								where kodesuratmasuk = '".$_POST['kodesuratmasuk']."'");
							if($cekjum>1){
								mysql_query("update t_suratmasuk_m  set status='2', kodeprogressurat='".$_POST['kodeprogressurat'.$i]."', tanggalpenyelesaian='".date('Y-m-d')."', catatan='".$_POST['catatan']."' where kodesuratmasuk = '".$_POST['kodesuratmasuk']."'");
							}else{
								mysql_query("update t_suratmasuk_m  set status='3', kodeprogressurat='3', tanggalpenyelesaian='".date('Y-m-d')."', catatan='".$_POST['catatan']."' where kodesuratmasuk = '".$_POST['kodesuratmasuk']."'");
							}
						}
					}
					// exit;
				}else{
					// echo'abc'.$awal."-".$cek."--".$cekposisi['kodesuratutama'];
					mysql_query("update t_suratmasuk_m  set status='2' where kodesuratmasuk = '".$_POST['kodesuratmasuk']."'");
					mysql_query("delete from t_surat_history_progress  where kodesuratmasuk = '".$_POST['kodesuratmasuk']."'");
					for($i=1; $i<=$_POST['jm']; $i++){
						if(trim($_POST['kodeprogress'.$i]!='')){
							mysql_query("insert t_surat_history_progress set
											kodesuratmasuk 		= '".$_POST['kodesuratmasuk']."',
											tanggalterimasurat 	= '".date('Y-m-d')."',
											kodeprogress 		= '".$_POST['kodeprogress'.$i]."',
											kodeprogressurat 	= '".$_POST['kodeprogressurat'.$i]."',
											keterangan 	= '".$_POST['keterangan'.$i]."'
							"); 
							mysql_query("update t_suratmasuk_d set
											tanggalupdate 	= '".date('Y-m-d')."',
											kodeprogress 	= '".$_POST['kodeprogress'.$i]."',
											kodeprogressurat= '".$_POST['kodeprogressurat'.$i]."',
											keterangan 	= '".$_POST['keterangan'.$i]."',
											fileid='".$fileName."',
											filename='".$namafile."',
											type='".$fileType."',
											size='".$fileSize."',
											path='".$filePath2."'
								where kodesuratmasuk = '".$_POST['kodesuratmasuk']."'");
							
							mysql_query("update t_suratmasuk_m set kodeprogressurat= '".$_POST['kodeprogressurat'.$i]."' where kodesuratmasuk = '".$_POST['kodesuratmasuk']."'");
						}
					}
				}
			}
		}
		//log aktivity
		$log_time = date('Y-m-d H:i:s');
		$log_user = $_SESSION['user'];
		$log_aksi = "update";
		$log_url  = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?"https" : "http") . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		$log_item ="Progress";
		$kode     =$_POST['kodesuratmasuk'];
		$log_ip = get_client_ip();
		$useragent = $_SERVER['HTTP_USER_AGENT'];
		$info = $useragent;
		$log_device = $info;
		log_activity($log_time,$log_user,$log_aksi,$log_url,$log_item,$kode,$log_ip,$log_device);

	}else{
		// echo'b';
	}
	
	if (mysql_query){
		echo "OK ".base64_encode('surat/suratmasuk')." ".base64_encode('list_suratmasuk');
	}else{
		echo "Error : \n Data gagal tersimpan";
	}
}
die();
?>