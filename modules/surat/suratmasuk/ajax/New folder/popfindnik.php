<?
session_start();
include_once('../../../../config.php');
include_once('../../../../includes/functions.php');
include_once('../../../../includes/koneksi.php');
include_once('../../../../includes/ajaxpagination.php');

?>
<script type="text/javascript" src="../../../../javascript/jquery-1.8.3.min.js"></script>
<style>
	*{font-family:Trebuchet MS,"Arial", sans-serif;color:black; font-size:11px;}
</style>
<title>Search Material</title>
<table width="100%" cellpadding="2" cellspacing="2" border="0" style="border: 1px solid #DEDEDE;font-size: 10pt;">
	<tr height="30">
		<td width="15%">Cari NIK / Cost Center</td>
		<td>
			<input type="text" name="txtval" id="txtval" size="40" value="<?=$_REQUEST['fv']?>" />			
			<input type="text" name="hal" id="hal" size="10" value="1" />			
		</td>
		<td width="48%" ><input type="button" name="button" id="cari" value="Cari" ></td>
	</tr>
</table>
<div id="view">
	<table width="100%" border="0">
		<tr>
			<th align="center" width="3%">NO</th>
			<th align="center" width="10%">NIK</th>
			<th align="center" width="15%">Nama</th>
			<th align="center" width="20%">Jabatan</th>
			<th align="center" width="10%">Kode CC</th>
			<th align="center" width="17%">Cost Center</th>
			<th align="center" width="20%">Direktorat</th>
		</tr>
	
	<?	
		$rsql = "SELECT hic.structdisp.empnik, hic.structdisp.empname, hic.structdisp.emppostx, hic.structdisp.empkostl, hic.structdisp.emp_cskt_ltext, hic.structdisp.emportx FROM hic.structdisp group by hic.structdisp.empkostl limit 0,10";
		$rs = mysql_query($rsql);	
		$i=0;
		while($lev=mysql_fetch_array($rs)){
			echo'
			<tr bgcolor="'.rowClass(++$i).'" onClick="ada(\''.$lev['empnik'].'\' , \''.$lev['empname'].'\' , \''.$lev['emppostx'].'\' , \''.$lev['empkostl'].'\' , \''.$lev['emp_cskt_ltext'].'\' , \''.$lev['emportx'].'\')">
				<td align="center">'.$i.'</td>
				<td align="left">'.$lev['empnik'].'</td>
				<td align="left">'.$lev['empname'].'</td>
				<td align="left">'.$lev['emppostx'].'</td>
				<td align="left">'.$lev['empkostl'].'</td>
				<td align="left">'.$lev['emp_cskt_ltext'].'</td>
				<td align="left">'.$lev['emportx'].'</td>
			</tr>';
		}
		@mysql_free_result($rs);	
	?>
	</table>
</div>
<div id="naf">
	<?
		$rsql 	= "SELECT hic.structdisp.empnik, hic.structdisp.empname, hic.structdisp.emppostx, hic.structdisp.empkostl, hic.structdisp.emp_cskt_ltext, hic.structdisp.emportx FROM hic.structdisp group by hic.structdisp.empkostl";
		$hd		= "";
		$hal	= 0;
		$order 	= "";
		$pager 	= new ajaxpagination($conn, $rsql, $hd, $hal, 10, 10, $order);
		$rs = $pager->paginate();	
		include_once('../../../../includes/nav.php');
	?>
</div>
<script type="text/javascript" >
$(document).ready( function() {
	$('#naf a').click(function() {
		alert($(this).attr("title"));
		var hal = $(this).attr("title");
		$("#hal").val(hal);
		$("#view").load("../../../../modules/surat/suratmasuk/ajax/popfindnik_detil.php", {'hal': hal, 'mod' : $("#txtmod").val(), 'op': $("#txtop").val() });
		$('#naf').fadeOut('slow').load("../../../../modules/surat/suratmasuk/ajax/popfindnik_page.php", {'hal':hal}).fadeIn("slow");
		
	});
	
	$('#cari').click(function() {
		var a = $("#txtval").val();
		if (a==''){
			alert("Masukkan nilai yang akan dicari!");
		}else{
			// alert("tes");
			$("#view").load("../../../../modules/surat/suratmasuk/ajax/popfindnik_detil.php", {'nm': a });
			$('#naf').fadeOut('slow').load("../../../../modules/surat/suratmasuk/ajax/popfindnik_page.php", {'hal':$(this).attr("title"), 'nm':a}).fadeIn("slow");
		}
		// e.stopImmediatePropagation();
		// alert('b');
	});
});

function ada(nik, nama, jabatan, empkostl, emp_cskt_ltext,emportx){ 
	parent.$("#empniksurat").val(nik);
	parent.$("#empnamesurat").val(nama);
	// parent.$("#jabatan").val(jabatan);
	parent.$("#empkostl").val(empkostl);
	parent.$("#emp_cskt_ltext").val(emp_cskt_ltext);
	// parent.$("#direktorat").val(emportx);
	parent.$.colorbox.close(); 
}
</script>