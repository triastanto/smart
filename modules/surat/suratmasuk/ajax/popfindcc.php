<?
session_start();
include_once('../../../../config.php');
include_once('../../../../includes/functions.php');
include_once('../../../../includes/koneksi.php');
include_once('../../../../includes/ajaxpagination.php');

?>
<script type="text/javascript" src="../../../../javascript/jquery-1.8.3.min.js"></script> 
    <link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/reset.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/grid.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/layout.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/nav.css" media="screen" />
    <style>
    	*{font-family:Trebuchet MS,"Arial", sans-serif;color:black; font-size:11px;}

    </style> 
<title>Search Material</title>
<table width="100%" cellpadding="2" cellspacing="2" border="0" style="border: 1px solid #DEDEDE;font-size: 10pt;" class="head_sr">
	<tr height="30">
		<td width="15%">CARI :
		</td>
		<td width="40%">
            <select name="pilihcari" id="pilihcari">
                <option value="">-Pilih Pencarian-</option>
                <option value="empnik">NIK</option>
                <option value="empname">Nama</option>
                <option value="emp_hrp1000_s_short">Abbreviasi</option>
                <option value="empkostl">Kode CC</option>
               <option value="emp_cskt_ltext">Cost Center</option>
            </select>&nbsp;
			<input type="text" name="txtval" id="txtval" size="40" value="<?=$_REQUEST['fv']?>" />			
			<input type="hidden" name="user" id="user"  value="<?=$_REQUEST['user']?>" />			
		</td>	
        <td width="38%" ><input type="button" name="button" id="cari" value="Cari" class='btn btn-blue' >&nbsp; 
        </td>
	</tr>
</table>
<div id="view">
	<table width="100%" border="0">
		<tr class="head_tr">
			<th align="center" width="3%">NO</th>
			<th align="center" width="10%">NIK</th>
			<th align="center" width="20%">NAMA</th>
			<th align="center" width="10%">ABBREVIASI</th>
			<th align="center" width="10%">KODE CC</th>
			<th align="center" width="47%">COST CENTER</th>
		</tr>
    	<?	
    		// $rsql = "SELECT hic.structdisp.empkostl, hic.structdisp.emp_cskt_ltext FROM hic.structdisp group by hic.structdisp.empkostl limit 0,20";
    		$rsql = "SELECT
						hic.structdisp.`no`,
						hic.structdisp.empnik,
						hic.structdisp.empname,
						hic.structdisp.empjobstext,
						hic.structdisp.emppostx,
						hic.structdisp.empkostl,
						hic.structdisp.emportx,
						hic.structdisp.emp_hrp1000_s_short,
						hic.structdisp.emp_cskt_ltext,
						hic.structdisp.emportx,
						arsip3.sys_groupuser.userid,
						arsip3.sys_groupuser.pos_abbr,
						arsip3.sys_user.userid,
						arsip3.sys_user.userpass
					FROM
						hic.structdisp
						INNER JOIN arsip3.sys_groupuser ON arsip3.sys_groupuser.pos_abbr = hic.structdisp.emp_hrp1000_s_short
						INNER JOIN arsip3.sys_user ON arsip3.sys_groupuser.userid = arsip3.sys_user.userid
					WHERE
						hic.structdisp.`no` = '1' AND
						hic.structdisp.emppersk = 'AS' and
						arsip3.sys_groupuser.userid='".$_REQUEST['user']."'
						OR
						hic.structdisp.`no` = '1' AND
						hic.structdisp.emppersk = 'BS' and
						arsip3.sys_groupuser.userid='".$_REQUEST['user']."'
					ORDER BY
						hic.structdisp.emp_hrp1000_s_short ASC limit 0,15";
    		// echo $rsql;
    		$rs = mysql_query($rsql);	
    		$i=1;
    		while($lev=mysql_fetch_array($rs)){
    		    $i%2 == 0 ? $cl='#caefff' : $cl='#e4f3f9';
				$nama = str_replace("'", "`" , $lev['empname']);
    			echo'
    			<tr bgcolor="'.$cl.'" onClick="ada(\''.$lev['empnik'].'\' , \''.$nama.'\' ,\''.$lev['empkostl'].'\' , \''.$lev['emportx'].'\' ,  \''.$lev['emp_hrp1000_s_short'].'\', \''.$lev['emportx'].'\')">
    				<td align="center">'.$i.'</td>
    				<td align="left">'.$lev['empnik'].'</td>
    				<td align="left">'.$lev['empname'].'</td>
    				<td align="left">'.$lev['emp_hrp1000_s_short'].'</td>
    				<td align="left">'.$lev['empkostl'].'</td>
    				<td align="left">'.$lev['emportx'].'</td> 
    			</tr>';
                $i++;	
    		}
    		@mysql_free_result($rs);	
    	?>
	</table>
</div>
<div id="naf">
	<?
		$rsql 	= "SELECT
						hic.structdisp.`no`,
						hic.structdisp.empnik,
						hic.structdisp.empjobstext,
						hic.structdisp.emppostx,
						hic.structdisp.empkostl,
						hic.structdisp.emp_hrp1000_s_short,
						hic.structdisp.emp_cskt_ltext,
						arsip3.sys_groupuser.userid,
						arsip3.sys_groupuser.pos_abbr,
						arsip3.sys_user.userid,
						arsip3.sys_user.userpass
					FROM
						hic.structdisp
						INNER JOIN arsip3.sys_groupuser ON arsip3.sys_groupuser.pos_abbr = hic.structdisp.emp_hrp1000_s_short
						INNER JOIN arsip3.sys_user ON arsip3.sys_groupuser.userid = arsip3.sys_user.userid
					WHERE
						hic.structdisp.`no` = '1' AND
						hic.structdisp.emppersk = 'AS' and
						arsip3.sys_groupuser.userid='".$_REQUEST['user']."'
						OR
						hic.structdisp.`no` = '1' AND
						hic.structdisp.emppersk = 'BS' and
						arsip3.sys_groupuser.userid='".$_REQUEST['user']."'
					ORDER BY
						hic.structdisp.emp_hrp1000_s_short";
		$hd		= "";
		$hal	= 0;
		$order 	= "";
		$pager 	= new ajaxpagination($conn, $rsql, $hd, $hal, 15, 15, $order);
		$rs = $pager->paginate();	
		include_once('../../../../includes/nav.php');
	?>
</div>
<script type="text/javascript" >
$(document).ready( function() {
	$('#naf a').click(function() {
		$("#view").load("../../../../modules/surat/suratmasuk/ajax/popfindcc_detil.php", {'hal': $(this).attr("title"), 'mod' : $("#txtmod").val(), 'user': $("#user").val() });
		$('#naf').fadeOut('slow').load("../../../../modules/surat/suratmasuk/ajax/popfindcc_page.php", {'hal':$(this).attr("title"), 'user': $("#user").val() }).fadeIn("slow");
		// alert('a');
	}); 
	$('#cari').click(function() {
		var a = $("#txtval").val();
		var pilih = $("#pilihcari").val();
		if (a=='' || pilih==''){
			alert("Masukkan nilai yang akan dicari!");
		}else{
			// alert("tes");
			$("#view").load("../../../../modules/surat/suratmasuk/ajax/popfindcc_detil.php", {'pilih': pilih, 'nm': a, 'user': $("#user").val()  });
			$('#naf').fadeOut('slow').load("../../../../modules/surat/suratmasuk/ajax/popfindcc_page.php", {'hal':$(this).attr("title"), 'nm':a, 'user': $("#user").val() }).fadeIn("slow");
		}
		e.stopImmediatePropagation();
		// alert('b');
	}); 
}); 
function ada(empniktujuan, empnametujuan,empkostl_tujuan,emp_cskt_ltext_tujuan,empshorttujuan){ 
	parent.$("#empniktujuan").val(empniktujuan);
	parent.$("#empnametujuan").val(empnametujuan);
	parent.$("#empkostl_tujuan").val(empkostl_tujuan);
	parent.$("#emp_cskt_ltext_tujuan").val(emp_cskt_ltext_tujuan);
	parent.$("#empshorttujuan").val(empshorttujuan);
	parent.$.colorbox.close(); 
}
</script>