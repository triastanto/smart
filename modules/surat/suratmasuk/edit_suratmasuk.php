<?
out("title", "MASTER DATA > SATUAN > INPUT SATUAN");
$u_agent = $_SERVER['HTTP_USER_AGENT'];
if(preg_match('/Chrome/i',$u_agent)){
	//$tg = "780px;";
}else{
	//$tg = "740px;";
}
//untuk memanggil fungsi group ID user atau admin
$grup_id = $_SESSION['group_id'];

$act	= base64_decode($_REQUEST['act']);
if ($act=='edit'){
	$idp=base64_decode($_REQUEST['idp']);
	$dt	= mysql_fetch_array(mysql_query("select * from t_suratmasuk_m where kodesuratmasuk='".$idp."'"));
	$kodesuratmasuk = $dt['kodesuratmasuk'];
}else{
	$act='new';
	$query = "SELECT max(kodesuratmasuk) as maxKode FROM t_suratmasuk_m ";
	$hasil = mysql_query($query);
	$data  = mysql_fetch_array($hasil);
	$kddata = $data['maxKode'];
	$noUrut = (int) substr($kddata, 8, 14);
	$noUrut++;
	$kodesuratmasuk = "SM" . date('Ym') . sprintf("%014s", $noUrut);
}


?>
<script type="text/javascript" src="javascript/jquery.form.js"></script>	
<script type="text/javascript">
	$(document).ready( function() {
		$(".popup").colorbox({ 		iframe:true		,width:"80%"		,height:"70%"	});
		
			var datePicker = $('.date-picker');
				datePicker.datepicker({
				showOn: "button",
				buttonImage: "templates/blue/images/calendar.gif",
				buttonImageOnly: true
			});
					

			var options = { 
				beforeSubmit:  showRequest,  // pre-submit callback 
				success:       showResponse,  // post-submit callback 
				url:'modules/surat/suratmasuk/ajax/save_suratmasuk.php',
				iframe:true
			}; 	
			
			$('#myfm').submit(function(event) { 
				event.preventDefault();
				$(this).ajaxSubmit(options); 
				event.stopImmediatePropagation();
				return false; 
			});
		
	});

	function showRequest(formData, jqForm, options){ 
		var form = jqForm[0]; 
		return true; 
	} 
	 
	function showResponse(responseText, statusText, xhr, $form){
		var mydata = responseText.split(" ");
		if (mydata[0]=="OK"){
			alert("Data sudah disimpan!");
			window.location.href = "?mod="+mydata[1]+"&op="+mydata[2];
		}else{
			alert(responseText); 
		}
	}
	
	
	//=================================================================================================================================//
	//====================================================Tabel Dinamis================================================================//
	//=================================================================================================================================//
	var idrow = 2;
	var idq=idrow-1;
	
	function add(){
		// alert('a');
		var i = $('#jm').val();
		var jml_nik = $('#optionstable tr').length - 1;
		var noakhir = $('#nourut'+i).val();
		noakhir++
		nourut = noakhir;
		
		if (noakhir <10) {
			nourut = "000" + noakhir;
		}else if (noakhir <100){
			nourut = "00" + noakhir;
		}else if (noakhir <1000){
			nourut = "0" + noakhir;
		}else if (noakhir <10000){
			nourut = noakhir;
		}
		
		i++;
		
		var appendTxt = '<tr bgcolor="#8ed2ff" align="center">'+
							'<td align="center">'+i+'<input type="hidden" name="nourut'+i+'" value="'+i+'" onclick="tanggal('+i+');" /></td>'+
							'<td><input type="text" name="empnikdisposisi'+i+'" id="empnikdisposisi'+i+'" size="7" readonly /> <img src="images/magnifier.png" border="0" onclick="cc('+i+')"></td>'+
							'<td><input type="text" name="empnamedisposisi'+i+'" id="empnamedisposisi'+i+'" size="20" readonly /></td>'+
							'<td><input type="hidden" name="empkostldisposisi'+i+'" id="empkostldisposisi'+i+'" size="10" readonly /><input type="text" name="emp_cskt_ltextdisposisi'+i+'" id="emp_cskt_ltextdisposisi'+i+'" size="35" readonly /></td>'+
						'</tr>';
				
		$("#optionstable tr:last").after(appendTxt);
		var jml_minta = $('#optionstable tr').length - 1;
		$('#jml_minta').val(jml_minta);
		var jm = jml_minta-1;
		$('#jm').val(jm);
	}
	function removeTableColumn() {
		var a = $('#jm').val();
		if(a>1){
			var totalawal = $("#totaljumlah").val();
			var nilaihapus = parseFloat($("#total"+a).val());
			var totalakhir = parseFloat(totalawal) - nilaihapus;
			$("#totaljumlah").val(totalakhir);
			var b = 1;
			$('#jm').val(a-b)
			$('#optionstable tr:last').remove(); // Deletes the last title
			$('#optionstable tr').each(function() {
				$(this).remove('td:last'); // Should delete the last td for each row, but it doesn't work
			});
		}
	}
	//=================================================================================================================================//
	//================================================End Tabel Dinamis================================================================//
	//=================================================================================================================================//
	function cc(no){
		// alert('a'+no);
		$.colorbox({
			iframe:true,
			width:"80%", 
			height:"75%",
			title: "Preview Cost Center",
			href:"modules/surat/suratmasuk/ajax/popfindnikdis.php?no="+no
		});
	}
</script>
<form method="POST" action="modules/surat/suratmasuk/ajax/save_suratmasuk.php" enctype="multipart/form-data" name="myfm" id="myfm" >
	<div style="padding:3px; width:99%;  border:2px #E6F0F3 solid;">
		<table width="99%" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td width="50%" valign="top">
					<table width="99%" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td width="30%">Kode Surat Masuk</td>
							<td width="3%"><strong>:</strong></td>
							<td width="67%"><input type="text" name="kodesuratmasuk" id="kodesuratmasuk" value="<?=$kodesuratmasuk?>" size="30" readonly /></td>
						</tr>
						<tr>
							<td>No Surat Masuk</td>
							<td><strong>:</strong></td>
							<td><input type="text" name="nosuratmasuk" id="nosuratmasuk" value="<?=$dt['nosuratmasuk']?>" size="30" /></td>
						</tr>
						<tr>
							<td>Tanggal Surat Masuk</td>
							<td><strong>:</strong></td>
							<td><input type="text" name="tanggalsurat" id="tanggalsurat" value="<?=$dt['tanggalsurat']?>" size="10" class="date-picker" /></td>
						</tr>
						<!--tr>
							<td>Tanggal Penyelesaian</td>
							<td><strong>:</strong></td>
							<td><input type="text" name="tanggalpenyelesaian" id="tanggalpenyelesaian" value="<?=$dt['tanggalpenyelesaian']?>" size="10" class="date-picker" /></td>
						</tr-->
						<tr>
							<td>Asal Surat</td>
							<td><strong>:</strong></td>
							<td><input type="text" name="asalsurat" id="asalsurat" value="<?=$dt['asalsurat']?>" size="30" /></td>
						</tr>
						<tr>
							<td>NIK Pengirim</td>
							<td><strong>:</strong></td>
							<td>
								<input type="text" name="empniksurat" id="empniksurat" value="<?=$dt['empniksurat']?>" size="10" readonly />
								<a href="modules/surat/suratmasuk/ajax/popfindnik.php" class="popup" title="Cari NIK Karyawan"><img src="images/magnifier.png" border="0"></a>
								<input type="text" name="empnamesurat" id="empnamesurat" value="<?=$dt['empnamesurat']?>" size="20" readonly />
							</td>
						</tr>
						<tr>
							<td>Unit Kerja</td>
							<td><strong>:</strong></td>
							<td>
								<input type="text" name="empkostl" id="empkostl" value="<?=$dt['empkostl']?>" size="15" readonly />
								<input type="text" name="emp_cskt_ltext" id="emp_cskt_ltext" value="<?=$dt['emp_cskt_ltext']?>" size="35" readonly />
							</td>
						</tr>
						<tr>
							<td>Tujuan Surat</td>
							<td><strong>:</strong></td>
							<td><input type="text" name="tujuansurat" id="tujuansurat" value="<?=$dt['tujuansurat']?>" size="30" /></td>
						</tr>
						<tr>
							<td>Kode Klasifikasi</td>
							<td><strong>:</strong></td>
							<td>
								<select name="kodeklasifikasi" style="width:75%;">									
									<?
										$sql1 = mysql_query("select * from m_klasifikasi");
										echo'<option value="" > -- Pilih Klasifikasi -- </option>';
										while ($rdt=mysql_fetch_array($sql1)){
											echo '<option value="'.$rdt[0].'"';
											if ($rdt[0]==$dt['kodeklasifikasi']){ echo ' selected '; };
											echo '>'.$rdt[0].' - '.$rdt[1].'</option>';
										}
									?>
								</select>
							</td>
						</tr>
					<!--
						<tr>
							<td>Index</td>
							<td><strong>:</strong></td>
							<td><input type="text" name="index" id="index" value="<?=$dt['index']?>" size="50" /></td>
						</tr>
					-->
						<tr>
							<td>Isi Ringkasan</td>
							<td><strong>:</strong></td>
							<td><input type="text" name="isiringkasan" id="isiringkasan" value="<?=$dt['isiringkasan']?>" size="50" /></td>
						</tr>
						<tr>
							<td>Keterangan</td>
							<td><strong>:</strong></td>
							<td>
								<select name="asli" style="width:45%;">									
									<?
										$sql1 = mysql_query("select * from m_keaslian");
										echo'<option value="" > -- Pilih -- </option>';
										while ($rdt=mysql_fetch_array($sql1)){
											echo '<option value="'.$rdt[0].'"';
											if ($rdt[0]==$dt['kodeasli']){ echo ' selected '; };
											echo '>'.$rdt[1].'</option>';
										}
									?>
								</select>
							</td>
						</tr>
					</table>
				</td>
				<td width="50%" valign="top">
					<table width="99%" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td width="30%">Lokasi Penyimpanan</td>
							<td width="3%"><strong>:</strong></td>
							<td width="67%"><input type="text" name="lokasi" id="lokasi" value="<?=$dt['lokasi']?>" size="30" /></td>
						</tr>
						<tr>
							<td>Aktif</td>
							<td><strong>:</strong></td>
							<td><input type="text" name="aktif" value="<?=$dt['masasimpanaktif']?>" size="20"/></td>
						</tr>
						<tr>
							<td>In Aktif</td>
							<td><strong>:</strong></td>
							<td><input type="text" name="inaktif" value="<?=$dt['masasimpaninaktif']?>" size="20"/></td>
						</tr>	
					<!--						
						<tr>
							<td>Jatuh Tempo Aktif</td>
							<td><strong>:</strong></td>
							<td><input type="text" name="aktifjatuhtempo" id="aktifjatuhtempo" value="<?=$dt['aktifjatuhtempo']?>" size="10" class="date-picker" /></td>
						</tr>
						<tr>
							<td>Jatuh Tempo In Aktif</td>
							<td><strong>:</strong></td>
							<td><input type="text" name="inaktifjatuhtempo" id="inaktifjatuhtempo" value="<?=$dt['inaktifjatuhtempo']?>" size="10" class="date-picker" /></td>
						</tr>
					-->
						<tr><td colspan="3"><b>DISPOSISI : </b></td></tr>
						<tr>
							<td colspan="3">
								<table id="optionstable" border="0" cellpadding="1" cellspacing="1" width="100%">		
									<tr align="left">
										<td colspan="12">
											<input type="hidden" name="jm" id="jm" class="jum" size="1" value="1" />
											<input type="button" class="add btn btn-blue" value="+ TAMBAH DISPOSISI" onclick="add()"/>
											<input type="button" class="hapus btn btn-blue" value="- HAPUS DISPOSISI" onclick="removeTableColumn()"/>
										</td>
									</tr>
									<tr align="center">
										<th width="2%"><b>NO </b></th>
										<th width="25%"><b>NIK</b></th>
										<th width="33%"><b>NAMA</b></th>
										<th width="40%"><b>COST CENTER</b></th>
									</tr>
									<tr bgcolor="#8ed2ff" align="center">
										<td align="center">1</td>
										<td>
											<input type="text" name="empnikdisposisi1" id="empnikdisposisi1" size="7" readonly />
											<img src="images/magnifier.png" border="0" onclick="cc(1)">
										</td>
										<td><input type="text" name="empnamedisposisi1" id="empnamedisposisi1" size="20" readonly /></td>
										<td><input type="hidden" name="empkostldisposisi1" id="empkostldisposisi1" size="10" readonly /><input type="text" name="emp_cskt_ltextdisposisi1" id="emp_cskt_ltextdisposisi1" size="35" readonly /></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>					
			<tr>
				<td colspan="2" align="center">
					<input name="act" type="hidden" value="<?=$act?>" />
					<input class="btn btn-blue" style="width:80px;" type="submit" name="submit" value="Simpan" />
					<?
						echo'
							<input class="btn btn-blue" style="width:80px;" type="button" name="Cancel" value="Cancel" onclick="window.location=\'?mod='.base64_encode('surat/suratmasuk').'&op='.base64_encode('list_suratmasuk').' \'" />
						';
					?>
				</td>
				<td>&nbsp;</td>
			</tr>
		</table>
	</div>
</form>
