<?php
	session_start();
	$_SESSION['userid'] = "budi";
?>

<html>
	<head>
		<title>Notifikasi</title>
		<link rel="stylesheet" href="gaya.css" type="text/css">
		<script type="text/javascript" src="jquery-1.4.3.min.js"></script>
		<script type="text/javascript" src="notifikasi.js"></script>
	</head>

	<body>
		<span id="pesan">
			MESSAGE NOTIFIKASI <span id="notifikasi"></span>
		</span>
		<div id="info">
			<div id="loading"><br>Loading...<img src="loading.gif"></div>
			<div id="konten-info"></div>
		</div>
	</body>
</html>
