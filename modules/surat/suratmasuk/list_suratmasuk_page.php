<?php

	include_once('../../../config.php');
	include_once('../../../includes/functions.php');
	include_once('../../../includes/ajaxpagination.php');
	include_once('../../../includes/koneksi.php');
	
	$hal 		= mysql_real_escape_string($_REQUEST['hal']);
	$pilih 		= mysql_real_escape_string($_REQUEST['pilih']);
	$nm 		= mysql_real_escape_string($_REQUEST['nm']);
	$userid		= mysql_real_escape_string($_REQUEST['userid']);
	$group_id	= mysql_real_escape_string($_REQUEST['group_id']);
	$empposid	= mysql_real_escape_string($_REQUEST['empposid']);
	
	
	if($group_id==1){
		if (trim($nm)!=''){
			$whr ="where validasi='1' and $pilih like '%".$nm."%'";
		}else{
			$whr ="where validasi='1'";
		}
		$rsql = "SELECT * from t_suratmasuk_m ".$whr." order by tanggalsurat desc";
	}
	if($group_id==2 or $group_id==4 or $group_id==5 or $group_id==7){
		if (trim($nm)!=''){
			// $whr ="where tujuansurat='".$userid."' and validasi='1' and $pilih like '%".$nm."%' or empniktujuan='".$userid."' and validasi='1' and $pilih like '%".$nm."%'";
			$whr ="where empposid_tujuan='".$empposid."' and validasi='1' and $pilih like '%".$nm."%'";
		}else{
			// $whr ="where tujuansurat='".$userid."' and validasi='1' or empniktujuan='".$userid."' and validasi='1'";
			$whr ="where empposid_tujuan='".$empposid."' and validasi='1'";
		}
		$rsql = "SELECT * from t_suratmasuk_m  ".$whr." order by tanggalsurat desc";
		
	}
	if($group_id==3){
		if (trim($nm)!=''){
			$whr ="where sys_user.userid='".$userid."' and validasi='1' and $pilih like '%".$nm."%'";
		}else{
			// $whr ="where sys_user.userid='".$userid."' and validasi='1' or t_suratmasuk_m.empniktujuan='".$userid."' and validasi='1'";
			$whr ="where sys_user.userid='".$userid."' and validasi='1'";
		}
		$rsql = "SELECT
					sys_user.*,
					sys_groupuser.*,
					t_suratmasuk_m.*
				FROM
					sys_groupuser
					INNER JOIN sys_user ON sys_user.userid = sys_groupuser.userid
					INNER JOIN t_suratmasuk_m ON sys_groupuser.pos_abbr = t_suratmasuk_m.empshort_tujuan AND t_suratmasuk_m.empposid_tujuan = sys_groupuser.empposid
				".$whr."
				order by t_suratmasuk_m.statusview, t_suratmasuk_m.tanggalsurat desc";
	}
	if($group_id==6){
		if (trim($nm)!=''){
			$whr ="where sys_user.userid='".$userid."' and validasi='1' and $pilih like '%".$nm."%'";
		}else{
			$whr ="where sys_user.userid='".$userid."' and validasi='1'";
		}
		$rsql = "SELECT
					sys_user.*,
					sys_groupuser.*,
					t_suratmasuk_m.*
				FROM
					sys_groupuser
					INNER JOIN sys_user ON sys_user.userid = sys_groupuser.userid
					INNER JOIN t_suratmasuk_m ON sys_groupuser.pos_abbr = t_suratmasuk_m.empshort_tujuan AND t_suratmasuk_m.empposid_tujuan = sys_groupuser.empposid
				".$whr."
				order by t_suratmasuk_m.statusview, t_suratmasuk_m.tanggalsurat desc";
	}
	// echo $rsql;
	
	$hd		= "";
	$order 	= "";
	$pager 	= new ajaxpagination($conn, $rsql, $hd, $hal, 10, 10, $order);
	$rs = $pager->paginate();	
	
	include_once('../../../includes/nav.php');

?>
<script type="text/javascript" >
$(document).ready( function() {
	$('#naf a').click(function() {
		var a 			= $("#txtval").val();
		var pilih 		= $("#pilih").val();
		var userid 		= $('#user').val();
		var group_id 	= $('#group_id').val(); 
		var empposid 	= $('#empposid').val(); 
		// alert(userid +"-"+ group_id +"-"+ pilih);
		$("#view").load("modules/surat/suratmasuk/list_suratmasuk_detil.php", {'hal': $(this).attr("title"), 'nm': a, 'userid': userid, 'group_id': group_id, 'pilih':pilih, 'empposid':empposid});
		$('#naf').fadeOut('slow').load("modules/surat/suratmasuk/list_suratmasuk_page.php", {'hal':$(this).attr("title"), 'nm': a, 'userid': userid, 'group_id': group_id, 'pilih':pilih, 'empposid':empposid}).fadeIn("slow");
	});
});
</script>