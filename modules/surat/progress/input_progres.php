<?
	out("title", "SURAT > SURAT MASUK");
	$u_agent = $_SERVER['HTTP_USER_AGENT'];
	if(preg_match('/Chrome/i',$u_agent)){
		//$tg = "780px;";
	}else{
		//$tg = "740px;";
	}
	//untuk memanggil fungsi group ID user atau admin
	$grup_id = $_SESSION['group_id'];

	$act	= base64_decode($_REQUEST['act']);
	// echo $act;
	if ($act=='edit'){
		$idp=base64_decode($_REQUEST['idp']);
		$dt	= mysql_fetch_array(mysql_query("select * from t_suratmasuk_m where kodesuratmasuk='".$idp."'"));
		$ct	= mysql_fetch_array(mysql_query("select * from t_suratmasuk_d where kodesuratmasuk='".$idp."'"));
		$kodesuratmasuk = $dt['kodesuratmasuk'];
		
		$assu = mysql_fetch_array(mysql_query("select * from t_surat_deployment where kodesuratbawah='".$kodesuratmasuk."'"));
		$asal = mysql_fetch_array(mysql_query("select * from t_suratkeluar_m where kodesuratkeluar='".$assu['kodesuratutama']."'"));
		
		if($_SESSION['group_id']==4){
			$jum = mysql_num_rows(mysql_query("select * from t_suratmasuk_d where empnikdisposisi='".$_SESSION['nik']."' and kodesuratmasuk='".$kodesuratmasuk."'"));
		}else{
			$jum = mysql_num_rows(mysql_query("SELECT
						sys_groupuser.*,
						t_suratmasuk_d.*
					FROM
						sys_groupuser
						INNER JOIN t_suratmasuk_d ON t_suratmasuk_d.empkostldisposisi = sys_groupuser.kostl
					where sys_groupuser.userid= '".$_SESSION['user']."' and kodesuratmasuk='".$kodesuratmasuk."'"));
		}
		 // $jum1 = mysql_num_rows(mysql_query("select * from t_suratmasuk_d_tembusan where kodesuratmasuk='".$kodesuratmasuk."'"));
		// echo"select * from t_suratmasuk_m where kodesuratmasuk='".$idp."'";
		$t = explode("-",$dt['tanggalsurat']); 			$tanggalsurat = $t[1]."/".$t[2]."/".$t[0];
		$t1 = explode("-",$dt['tanggalpenyelesaian']); 	$tanggalpenyelesaian = $t1[1]."/".$t1[2]."/".$t1[0];
		$t2 = explode("-",$dt['tanggalterimasurat']); 	$tanggalterimasurat = $t2[1]."/".$t2[2]."/".$t2[0];
	}else{
		$act='new';
		
		// $query = "SELECT max(kodesuratmasuk) as maxKode FROM t_suratmasuk_m ";
		// $hasil = mysql_query($query);
		// $data  = mysql_fetch_array($hasil);
		// $kddata = $data['maxKode'];
		// $noUrut = (int) substr($kddata, 8, 14);
		// $noUrut++;
		// $kodesuratmasuk = "SM" . date('Ym') . sprintf("%014s", $noUrut);
		$jum = 1;
	}
?>
<script type="text/javascript" src="javascript/jquery.form.js"></script>	
<script type="text/javascript">
	$(document).ready( function() {
		$(".datepicker").datepicker({ dateFormat: "dd-mm-yy" }).val();
		
		var options = {
			beforeSubmit:  showRequest,  // pre-submit callback 
			success:       showResponse,  // post-submit callback 
			url:'modules/surat/suratmasuk/ajax/save_progress.php',
			iframe:true
		}; 	
		
		$('#myfm').submit(function(event) { 
			event.preventDefault();
			$(this).ajaxSubmit(options); 
			event.stopImmediatePropagation();
			return false; 
		});
		
	});

	function showRequest(formData, jqForm, options){ 
		var form = jqForm[0]; 
		return true; 
	} 
	 
	function showResponse(responseText, statusText, xhr, $form){
		var mydata = responseText.split(" ");
		if (mydata[0]=="OK"){
			alert("Data sudah disimpan!");
			window.location.href = "?mod="+mydata[1]+"&op="+mydata[2];
		}else{
			alert(responseText); 
		}
	}
	
	function progres(str, no){
		// alert(str);
		if (str==""){
		  document.getElementById("keluarnilai"+no).innerHTML="";
		  return;
		}
		if (window.XMLHttpRequest){
			xmlhttp=new XMLHttpRequest();
		}else{
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function(){
			if (xmlhttp.readyState==4 && xmlhttp.status==200){
				document.getElementById("keluarnilai"+no).innerHTML=xmlhttp.responseText;
				// alert("keluarnilai"+no);
			}
			
		}
		xmlhttp.open("GET","modules/surat/suratmasuk/ajax/nilai.php?kodeprogress="+str+"&no="+no,true);
		xmlhttp.send();
	}
</script>
<form method="POST" action="modules/surat/suratmasuk/ajax/save_progress.php" enctype="multipart/form-data" name="myfm" id="myfm" >
	<div style="padding:3px; width:99%;  border:2px #E6F0F3 solid;">
		<table width="99%" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td width="50%" valign="top">
					<table width="99%" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td width="30%">Kode Surat Masuk</td>
							<td width="3%"><strong>:</strong></td>
							<td width="67%"><input type="text" name="kodesuratmasuk" id="kodesuratmasuk" value="<?=$kodesuratmasuk?>" size="30" readonly /></td>
						</tr>
						<tr>
							<td>No Surat Masuk</td>
							<td><strong>:</strong></td>
							<td><input type="text" name="nosuratmasuk" id="nosuratmasuk" value="<?=$dt['nosuratmasuk']?>" size="30" /></td>
						</tr>
						<tr>
							<td>Tanggal Surat Masuk</td>
							<td><strong>:</strong></td>
							<td><input type="text" name="tanggalsurat" id="tanggalsurat" value="<?=_convertDate($dt['tanggalsurat'])?>" size="10" class="datepicker" /></td>
						</tr>
						<tr>
							<td>Tanggal Terima Surat</td>
							<td><strong>:</strong></td>
							<td><input type="text" name="tanggalterimasurat" id="tanggalterimasurat" value="<?=_convertDate(date('Y-m-d'))?>" size="10" class="datepicker" onclick="cek()" /></td>
						</tr>
						
						<tr>
							<td>Asal Surat</td>
							<td><strong>:</strong></td>
							<td>
								<select name="jenissurat" onchange="showUser(this.value)">
									<?
										
										if($act=='new'){
											echo'
												<option value=""> -- Pilih -- </option>
												<option value="1"> INTERNAL </option>
												<option value="2"> EKSTERNAL </option>
											';
										}else{
											echo'<option value="" > -- Pilih -- </option>';
											for($a=1; $a<=2; $a++){
												if($a==1){ $isi ='INTERNAL'; } else{ $isi ='EKSTERNAL'; }
												echo '<option value="'.$a.'"';
												if ($a==$dt['jenissurat']){ echo ' selected '; };
												echo '>'.$isi.'</option>';
											}
										}
									?>
								</select>
							</td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td>
								<div id="cihuy">
									<?
										// echo $act."--".$dt['jenissurat'];
										if($act=='edit'){
											if($dt['jenissurat']==1){
												echo'
												<table width="100%">
													<tr>
														<td width="20%">Disposisi Oleh </td>
														<td width="80%">
															<input type="text" name="empniksurat" id="empniksurat" value="'.$asal['empnik_asal'].'" size="5" readonly />
															<img src="images/magnifier.png" border="0" onclick="popup()" />
															<input type="text" name="empnamesurat" id="empnamesurat" value="'.$asal['empname_asal'].'" size="20" readonly />
														</td>
													</tr>
													<tr>
														<td>Cost Center </td>
														<td>
															<input type="text" name="empkostl_surat" id="empkostl_surat" value="'.$asal['empkostl_asal'].'" size="5" readonly />&nbsp;&nbsp;&nbsp;&nbsp;
															<input type="text" name="emp_cskt_ltext_surat" id="emp_cskt_ltext_surat" value="'.$asal['emp_cskt_ltext_asal'].'" size="20" readonly />
														</td>
													</tr>
												</table>';
											}else{
												echo'
												<table width="100%">
													<tr>
														<td width="20%">Penanggung Jawab </td>
														<td width="80%">
															<input type="text" name="empnamesurat" id="empnamesurat" value="'.$dt['empnamesurat'].'" size="30" />
														</td>
													</tr>
													<tr>
														<td>Instansi </td>
														<td>
															<input type="text" name="emp_cskt_ltext_surat" id="emp_cskt_ltext_surat" value="'.$dt['emp_cskt_ltext_surat'].'" size="30" />
														</td>
													</tr>
												</table>';
											}
										}
									?>								
								</div>
							</td>
						</tr>
						<tr>
							<td>Tujuan Surat</td>
							<td><strong>:</strong></td>
							<td>
								<!--<input type="text" name="tujuansurat" id="tujuansurat" value="" size="30" />-->
								<input type="text" name="empniktujuan" id="empniktujuan" value="<?=$dt['empniktujuan']?>" size="5" readonly />
								<img src="images/magnifier.png" border="0" onclick="tujuan()" />
								<input type="text" name="empnametujuan" id="empnametujuan" value="<?=$dt['empnametujuan']?>" size="20" readonly />
							</td>
						</tr>
						<tr>
							<td>Cost Center Tujuan</td>
							<td><strong>:</strong></td>
							<td>
								<input type="text" name="empkostl_tujuan" id="empkostl_tujuan" value="<?=$dt['empkostl_tujuan']?>" size="5" readonly />&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="text" name="emp_cskt_ltext_tujuan" id="emp_cskt_ltext_tujuan" value="<?=$dt['emp_cskt_ltext_tujuan']?>" size="20" readonly />
							</td>
						</tr>
						<tr>
							<td>Kode Klasifikasi</td>
							<td><strong>:</strong></td>
							<td>
								<select name="kodeklasifikasi" style="width:75%;" onclick="cek()">									
									<?
										$sql1 = mysql_query("select * from m_klasifikasi");
										echo'<option value="" > -- Pilih Klasifikasi -- </option>';
										while ($rdt=mysql_fetch_array($sql1)){
											echo '<option value="'.$rdt[0].'"';
											if ($rdt[0]==$dt['kodeklasifikasi']){ echo ' selected '; };
											echo '>'.$rdt[0].' - '.$rdt[1].'</option>';
										}
									?>
								</select>
							</td>
						</tr>
					</table>
				</td>
				<td width="50%" valign="top">
					<table width="99%" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td>Isi Ringkasan</td>
							<td><strong>:</strong></td>
							<td><input type="text" name="isiringkasan" id="isiringkasan" value="<?=$dt['isiringkasan']?>" size="50" onkeyup="cek()" /></td>
						</tr>
						<!--
						<tr>
							<td>Keterangan</td>
							<td><strong>:</strong></td>
							<td>
								<select name="asli" style="width:45%;">									
									<?
										$sql1 = mysql_query("select * from m_keaslian");
										echo'<option value="" > -- Pilih -- </option>';
										while ($rdt=mysql_fetch_array($sql1)){
											echo '<option value="'.$rdt[0].'"';
											if ($rdt[0]==$dt['kodeasli']){ echo ' selected '; };
											echo '>'.$rdt[1].'</option>';
										}
									?>
								</select>
							</td>
						</tr>
                        <tr>
							<td>Lokasi Penyimpanan</td>
							<td><strong>:</strong></td>
							<td><input type="text" name="lokasi" id="lokasi" value="<?=$dt['lokasi']?>" size="30" /></td>
						</tr>
						<tr>
							<td>Aktif</td>
							<td><strong>:</strong></td>
							<td><input type="text" name="aktif" id="aktif" value="<?=$dt['masasimpanaktif']?>" size="20" readonly /></td>
						</tr>
						<tr>
							<td>In Aktif</td>
							<td><strong>:</strong></td>
							<td><input type="text" name="inaktif" id="inaktif" value="<?=$dt['masasimpaninaktif']?>" size="20" readonly /></td>
						</tr>
						<tr>
							<td width="30%">Lampiran</td>
							<td width="3%"><strong>:</strong></td>
							<td width="67%"><input type="file" name="userfile" /></td>
						</tr>
					-->
						<tr>
							<td>Tanggal Penyelesaian</td>
							<td><strong>:</strong></td>
							<td><input type="text" name="tanggalpenyelesaian" id="tanggalpenyelesaian" value="<?=_convertDate($dt['tanggalpenyelesaian'])?>" size="10" class="datepicker" /></td>
						</tr>
						<tr>
							<td valign="top">Catatan</td>
							<td valign="top"><strong>:</strong></td>
							<td><textarea name="catatan" cols="40" rows="4"><?=$assu['catatan']?></textarea></td>
						</tr>
						<tr>
							<td>Upload Progress</td>
							<td><strong>:</strong></td>
							<td><input type="file" name="userfile" /></td>
						</tr>
						<?
							if($ct['catatan']!=''){
								echo'									
									<tr>
										<td>Komentar Atasan</td>
										<td><strong>:</strong></td>
										<td><b>'.$ct['catatan'].'</b></td>
									</tr>
								';
							}
						?>
						<tr>
							<td></td>
							<td><strong></strong></td>
							<td><input type="hidden" name="pembuat" id="user" value="<?=$_SESSION['user']?>" size="47" /></td>
						</tr>
					</table>
				</td>
			</tr>
		
			<tr><td colspan="2"><b>DISPOSISI : </b></td></tr>
			<tr>
				<td colspan="2">
					<table border="0" cellpadding="1" cellspacing="1" width="100%">		
						<tr align="left">
							<td colspan="12">
								<input type="hidden" name="jm" id="jm" class="jum" size="1" value="<?=$jum?>" />
							</td>
						</tr>
						<tr align="center">
							<th width="2%"><b>NO </b></th>
							<th width="10%"><b>NIK</b></th>
							<th width="13%"><b>NAMA</b></th>
							<th width="20%"><b>COST CENTER</b></th>
							<th width="10%"><b>STATUS</b></th>
							<th width="20%"><b>PROGRESS PEKERJAAN</b></th>
							<th width="30%"><b>KETERANGAN PEKERJAAN</b></th>
						</tr>
						<? 
							$sqlcek = mysql_num_rows(mysql_query("select ".$dbname2.".structdisp.* from ".$dbname2.".structdisp where empnik='".$_SESSION['nik']."' and no='1'"));
							if($_SESSION['group_id']==2 or $_SESSION['group_id']==4 or $_SESSION['group_id']==5){
								if($sqlcek<3){
									$sql = mysql_query("SELECT * FROM t_suratmasuk_m where empniktujuan='".$_SESSION['nik']."' and kodesuratmasuk='".$kodesuratmasuk."'");
								}else{
									$sql = mysql_query("SELECT * FROM t_suratmasuk_d where t_suratmasuk_d.empnikdisposisi='".$_SESSION['nik']."' and t_suratmasuk_d.kodesuratmasuk='".$kodesuratmasuk."'");
								}
								// $pos='a';
							}else{
								$sql = mysql_query("SELECT
											sys_groupuser.*,
											t_suratmasuk_d.*
										FROM
											sys_groupuser
											INNER JOIN t_suratmasuk_d ON t_suratmasuk_d.empkostldisposisi = sys_groupuser.kostl
										where sys_groupuser.userid= '".$_SESSION['user']."' and t_suratmasuk_d.kodesuratmasuk='".$kodesuratmasuk."'");
								// $pos='b';
							}
							// echo $sql;
							// echo $sqlcek." - select ".$dbname2.".structdisp.* from ".$dbname2.".structdisp where empnik='".$_SESSION['nik']."'";
							// echo $_SESSION['group_id'];
							// echo $pos;
							$no=1;
							while($det = mysql_fetch_array($sql)){
								if($sqlcek<3){
									$empnikdisposisi = $det['empniktujuan'];
									$empnamedisposisi = $det['empnametujuan'];
									$empkostldisposisi = $det['empkostl_tujuan'];
									$emp_cskt_ltextdisposisi = $det['emp_cskt_ltext_tujuan'];
								}else{
									$empnikdisposisi = $det['empnikdisposisi'];
									$empnamedisposisi = $det['empnamedisposisi'];
									$empkostldisposisi = $det['empkostldisposisi'];
									$emp_cskt_ltextdisposisi = $det['emp_cskt_ltextdisposisi'];
								}
								
								echo'
									<tr bgcolor="#8ed2ff" align="center">
										<td align="center">'.$no.'</td>
										<td>
											<input type="hidden" name="no'.$no.'" id="no'.$no.'" size="10" value="'.$det['no'].'" readonly />
											<input type="text" name="empnikdisposisi'.$no.'" id="empnikdisposisi'.$no.'" size="10" value="'.$empnikdisposisi.'" readonly />
										</td>
										<td><input type="text" name="empnamedisposisi'.$no.'" id="empnamedisposisi'.$no.'" size="20" value="'.$empnamedisposisi.'" readonly /></td>
										<td>
											<input type="hidden" name="empkostldisposisi'.$no.'" id="empkostldisposisi'.$no.'" size="10" value="'.$empkostldisposisi.'" readonly />
											<input type="text" name="emp_cskt_ltextdisposisi'.$no.'" id="emp_cskt_ltextdisposisi'.$no.'" size="35" value="'.$emp_cskt_ltextdisposisi.'" readonly />
										</td>
										<td>
											<select name="kodeprogressurat'.$no.'" style="width:95%;">
									';
												
												$sql1 = mysql_query("select * from m_progressurat order by kodeprogressurat limit 0,2");
												echo'<option value="" > -- Pilih -- </option>';
												while ($rdt=mysql_fetch_array($sql1)){
													echo '<option value="'.$rdt[0].'"';
													if ($rdt[0]==$det['kodeprogressurat']){ echo ' selected '; };
													echo '>'.$rdt[1].'</option>';
												}
											
									echo'		
											</select>
										</td>
										<td>
											<select name="kodeprogress'.$no.'" style="width:95%;"  onchange="progres(this.value, '.$no.')">
									';
												
												$sql1 = mysql_query("select * from m_progress");
												echo'<option value="" > -- Pilih -- </option>';
												while ($rdt=mysql_fetch_array($sql1)){
													echo '<option value="'.$rdt[0].'"';
													if ($rdt[0]==$det['kodeprogress']){ echo ' selected '; };
													echo '>'.$rdt[1].'</option>';
												}
											
									echo'
											</select>
										</td>
										<td><input type="text" name="keterangan'.$no.'" id="keterangan'.$no.'" size="50" value="'.$det['keterangan'].'" /></td>
									</tr>
								';
								$no++;
							}
						?>
					</table>
				</td>
			</tr>			
			<tr>
				<td colspan="2" align="center">
					<input name="act" type="hidden" value="<?=$act?>" />
					<input class="btn btn-blue" style="width:80px;" type="submit" name="submit" value="Simpan" />
					<?
						echo'
							<input class="btn btn-blue" style="width:80px;" type="button" name="Cancel" value="Cancel" onclick="window.location=\'?mod='.base64_encode('surat/suratmasuk').'&op='.base64_encode('list_suratmasuk').' \'" />
						';
					?>
				</td>
				<td>&nbsp;</td>
			</tr>
		</table>
	</div>
</form>
