<?
out("title", "KORESPONDENSI > PROSES PENERIMAAN SURAT KELUAR");
$u_agent = $_SERVER['HTTP_USER_AGENT'];
if(preg_match('/Chrome/i',$u_agent)){
	//$tg = "780px;";
}else{
	//$tg = "740px;";
}
//untuk memanggil fungsi group ID user atau admin
$grup_id = $_SESSION['group_id'];

$act	= base64_decode($_REQUEST['act']);
if ($act=='edit'){
	$idp=base64_decode($_REQUEST['idp']);
	$dt	= mysql_fetch_array(mysql_query("select * from t_suratkeluar_m where kodesuratkeluar='".$idp."'"));
	$kodesuratkeluar = $dt['kodesuratkeluar'];
	$jum = mysql_num_rows(mysql_query("select * from t_suratkeluar_d where kodesuratkeluar='".$kodesuratkeluar."'"));
}else{
	$act='new';
	$query = "SELECT max(kodesuratkeluar) as maxKode FROM t_suratkeluar_m ";
	$hasil = mysql_query($query);
	$data  = mysql_fetch_array($hasil);
	$kddata = $data['maxKode'];
	$noUrut = (int) substr($kddata, 8, 14);
	$noUrut++;
	$kodesuratkeluar = "SK" . date('Ym') . sprintf("%014s", $noUrut);
	$jum = 1;
}


?>
<script type="text/javascript" src="javascript/jquery.form.js"></script>	
<script type="text/javascript">
	$(document).ready( function() {
		$(".popup").colorbox({ 		iframe:true		,width:"80%"		,height:"70%"	});
		
			$(".datepicker").datepicker({ dateFormat: "dd-mm-yy" }).val();

			var options = { 
				beforeSubmit:  showRequest,  // pre-submit callback 
				success:       showResponse,  // post-submit callback 
				url:'modules/surat/suratkeluar/ajax/save_prosestembusan.php',
				iframe:true
			}; 	
			
			$('#myfm').submit(function(event) { 
				event.preventDefault();
				$(this).ajaxSubmit(options); 
				event.stopImmediatePropagation();
				return false; 
			});
		
	});

	function showRequest(formData, jqForm, options){ 
		var form = jqForm[0]; 
		return true; 
	} 
	 
	function showResponse(responseText, statusText, xhr, $form){
		var mydata = responseText.split(" ");
		if (mydata[0]=="OK"){
			alert("Data sudah disimpan!");
			window.location.href = "?mod="+mydata[1]+"&op="+mydata[2];
		}else{
			alert(responseText); 
		}
	}
</script>

<form method="POST" action="modules/surat/suratkeluar/ajax/save_prosestembusan.php" enctype="multipart/form-data" name="myfm" id="myfm" >
	<div style="padding:3px; width:99%;  border:2px #E6F0F3 solid;">
		<table width="99%" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td width="50%" valign="top">
					<table width="99%" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td width="30%">Kode Surat keluar</td>
							<td width="3%"><strong>:</strong></td>
							<td width="67%"><input type="text" name="kodesuratkeluar" id="kodesuratkeluar" value="<?=$kodesuratkeluar?>" size="30" readonly /></td>
						</tr>
						<tr>
							<td>No Surat keluar</td>
							<td><strong>:</strong></td>
							<td><input type="text" name="nosuratkeluar" id="nosuratkeluar" value="<?=$dt['nosuratkeluar']?>" size="30" /></td>
						</tr>
						<tr>
							<td>Tanggal Surat keluar</td>
							<td><strong>:</strong></td>
							<td><input type="text" name="tanggalsurat" class="datepicker" value="<?=_convertDate($dt['tanggalsurat'])?>" size="10" /></td>
						</tr>
						<tr>
							<td>Asal Surat</td>
							<td><strong>:</strong></td>
							<td>
								<!--<input type="text" name="asalsurat" id="asalsurat" value="<?=$dt['asalsurat']?>" size="30" />-->
								
								<input type="text" name="empnik_asal" id="empnik_asal" value="<?=$dt['empnik_asal']?>" size="5" readonly />
								 
								<input type="text" name="empname_asal" id="empname_asal" value="<?=$dt['empname_asal']?>" size="20" readonly />
							
							</td>
						</tr>						
						<tr>
							<td>Cost Center Surat</td>
							<td><strong>:</strong></td>
							<td>
								<input type="text" name="empkostl_asal" id="empkostl_asal" value="<?=$dt['empkostl_asal']?>" size="5" readonly /> 
								<input type="text" name="emp_cskt_ltext_asal" id="emp_cskt_ltext_asal" value="<?=$dt['emp_cskt_ltext_asal']?>" size="30" readonly />
								<input type="hidden" name="empshort_asal" id="empshort_asal" size="35" value="<?=$dt['empshort_asal']?>" readonly />
							</td>
						</tr>
						<tr>
							<td>Jenis Surat</td>
							<td><strong>:</strong></td>
							<td><input type="hidden" name="pembuat" id="user" value="<?=$_SESSION['user']?>" size="47" />
								<select name="jenissurat">
									<?
										
										if($act=='new'){
											echo'
												<option value=""> -- Pilih -- </option>
												<option value="1"> INTERNAL </option>
												<option value="2"> EKSTERNAL </option>
											';
										}else{
											echo'<option value="" > -- Pilih -- </option>';
											for($a=1; $a<=2; $a++){
												if($a==1){ $isi ='INTERNAL'; } else{ $isi ='EKSTERNAL'; }
												echo '<option value="'.$a.'"';
												if ($a==$dt['jenissurat']){ echo ' selected '; };
												echo '>'.$isi.'</option>';
											}
										}
										
									?>
								</select>
							</td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td>
								<div id="cihuy">
									<?
										if($act=='edit'){
											if($dt['jenissurat']==1){
												echo'
												<table width="100%">
													<tr>
														<td width="20%">NIK </td>
														<td width="80%">
															<input type="text" name="empnik_surat" id="empnik_surat" value="'.$dt['empnik_surat'].'" size="7" readonly /> 
															<input type="text" name="empname_surat" id="empname_surat" value="'.$dt['empname_surat'].'" size="25" readonly />
														</td>
													</tr>
													<tr>
														<td>Cost Center </td>
														<td>
															<input type="text" name="empkostl_surat" id="empkostl_surat" value="'.$dt['empkostl_surat'].'" size="7" readonly />
															<input type="text" name="emp_cskt_ltext_surat" id="emp_cskt_ltext_surat" value="'.$dt['emp_cskt_ltext_surat'].'" size="30" readonly />
															<input type="hidden" name="empshort_surat" id="empshort_surat" value="'.$dt['empshort_surat'].'" size="20" readonly />
														</td>
													</tr>
												</table>';
											}else{
												echo'
												<table width="100%">
													<tr>
														<td width="20%">Penanggung Jawab </td>
														<td width="80%">
															<input type="text" name="empnamesurat" id="empnamesurat" value="'.$dt['empnamesurat'].'" size="30" />
														</td>
													</tr>
													<tr>
														<td>Instansi </td>
														<td>
															<input type="text" name="emp_cskt_ltext" id="emp_cskt_ltext" value="'.$dt['emp_cskt_ltextsurat'].'" size="35" />
														</td>
													</tr>
												</table>';
											}
										}
									?>
								</div>
							</td>
						</tr>
						<tr>
							<td>Kode Klasifikasi</td>
							<td><strong>:</strong></td>
							<td>
								<select name="kodeklasifikasi" style="width:75%;">									
									<?
										$sql1 = mysql_query("select * from m_klasifikasi");
										echo'<option value="" > -- Pilih Klasifikasi -- </option>';
										while ($rdt=mysql_fetch_array($sql1)){
											echo '<option value="'.$rdt[0].'"';
											if ($rdt[0]==$dt['kodeklasifikasi']){ echo ' selected '; };
											echo '>'.$rdt[0].' - '.$rdt[1].'</option>';
										}
									?>
								</select>
							</td>
						</tr>
						<tr>
							<td>Isi Ringkasan</td>
							<td><strong>:</strong></td>
							<td><input type="text" name="isiringkasan" id="isiringkasan" value="<?=$dt['isiringkasan']?>" size="50" /></td>
						</tr>
						<tr>
							<td width="30%">Lokasi Penyimpanan</td>
							<td width="3%"><strong>:</strong></td>
							<td width="67%"><input type="text" name="lokasi" id="lokasi" value="<?=$dt['lokasi']?>" size="30" /></td>
						</tr>
						
						<tr>
							<td>Keterangan</td>
							<td><strong>:</strong></td>
							<td>
								<select name="asli" style="width:45%;">									
									<?
										$sql1 = mysql_query("select * from m_keaslian");
										echo'<option value="" > -- Pilih -- </option>';
										while ($rdt=mysql_fetch_array($sql1)){
											echo '<option value="'.$rdt[0].'"';
											if ($rdt[0]==$dt['kodeasli']){ echo ' selected '; };
											echo '>'.$rdt[1].'</option>';
										}
									?>
								</select>
							</td>
						</tr>
						
					</table>
				</td>
				<td width="50%" valign="top">
					<table width="99%" cellpadding="0" cellspacing="0" border="0">
						<tr><td colspan="3"><b>DISPOSISI : </b></td></tr>
						<tr>
							<td colspan="3">
								<table id="optionstable" border="0" cellpadding="1" cellspacing="1" width="100%">		
									<tr align="center">
										<th width="2%"><b>NO </b></th>
										<th width="25%"><b>NIK</b></th>
										<th width="33%"><b>NAMA</b></th>
										<th width="40%"><b>COST CENTER</b></th>
									</tr>
									<? 
										if($act=='new'){ 
											echo'
												<tr bgcolor="#8ed2ff" align="center">
													<td align="center">1</td>
													<td>
														<input type="text" name="empniktembusan1" id="empniktembusan1" size="10" readonly />
														<img src="images/magnifier.png" border="0" onclick="cc(1)">
													</td>
													<td><input type="text" name="empnametembusan1" id="empnametembusan1" size="20" readonly /></td>
													<td><input type="hidden" name="empkostltembusan1" id="empkostltembusan1" size="10" readonly /><input type="text" name="emp_cskt_ltexttembusan1" id="emp_cskt_ltexttembusan1" size="35" readonly /></td>
												</tr>
											';
										}else{
											$sql = mysql_query("select * from t_suratkeluar_d where kodesuratkeluar='".$kodesuratkeluar."'");
											$no=1;
											while($det = mysql_fetch_array($sql)){
												echo'
													<tr bgcolor="#8ed2ff" align="center">
														<td align="center">'.$no.'</td>
														<td>
															<input type="text" name="empniktembusan'.$no.'" id="empniktembusan'.$no.'" size="10" value="'.$det['empniktembusan'].'" readonly />
															<img src="images/magnifier.png" border="0" onclick="cc('.$no.')">
														</td>
														<td><input type="text" name="empnametembusan'.$no.'" id="empnametembusan'.$no.'" size="20" value="'.$det['empnametembusan'].'" readonly /></td>
														<td><input type="hidden" name="empkostltembusan'.$no.'" id="empkostltembusan'.$no.'" size="10" value="'.$det['empkostltembusan'].'" readonly /><input type="text" name="emp_cskt_ltexttembusan'.$no.'" id="emp_cskt_ltexttembusan'.$no.'" size="35" value="'.$det['emp_cskt_ltexttembusan'].'" readonly /></td>
													</tr>
												';
												$no++;
											}
										}
									?>
								</table>
							</td>
						</tr>
						<tr>
							<td width="30%">Penerima Surat</td>
							<td width="3%"><strong>:</strong></td>
							<td width="67%"><input type="text" name="penerimasurat" size="40" value="<?=$dt['penerimasurat']?>" /></td>
						</tr>
						<tr>
							<td width="30%">Tanggal Terima Surat</td>
							<td width="3%"><strong>:</strong></td>
							<td width="67%"><input type="text" name="tanggalterimasurat" class="datepicker" value="<?=_convertDate($dt['tanggalterimasurat'])?>" size="10" /></td>
						</tr>
						<tr>
							<td>Catatan Surat</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td colspan="3"><textarea cols="90" name="catatan" rows="10"><?=$dt['catatan']?></textarea></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<input name="act" type="hidden" value="<?=$act?>" />
					<input class="btn btn-blue" style="width:80px;" type="submit" name="submit" value="Simpan" />
					<?
						echo'
							<input class="btn btn-blue" style="width:80px;" type="button" name="Cancel" value="Cancel" onclick="window.location=\'?mod='.base64_encode('surat/suratkeluar').'&op='.base64_encode('list_suratkeluar').' \'" />
						';
					?>
				</td>
				<td>&nbsp;</td>
			</tr>
		</table>
	</div>
</form>
