<?
    session_start();
    include_once('../../../../config.php');
    include_once('../../../../includes/functions.php');
    include_once('../../../../includes/koneksi.php');
    include_once('../../../../includes/ajaxpagination.php');
?>

<script type="text/javascript" src="../../../../javascript/jquery-1.8.3.min.js"></script> 
<link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/reset.css" media="screen" />
<link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/grid.css" media="screen" />
<link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/layout.css" media="screen" />
<link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/nav.css" media="screen" />

<style>
	*{font-family:Trebuchet MS,"Arial", sans-serif;color:black; font-size:11px;}
</style> 
<div id="view">
	<table width="100%" border="0">
		<tr class="head_tr">
			<th align="center" width="3%">NO</th>
			<th align="center" width="5%">NIK</th>
			<th align="center" width="20%">NAMA</th>
			<th align="center" width="10%">ABBREVIASI</th>
			<th align="center" width="10%">KODE CC</th>
			<th align="center" width="27%">COST CENTER</th>
			<th align="center" width="25%">JABATAN</th>
		</tr>
    	<?	
            $rsql ="SELECT ".$dbname2.".structdireksi.* FROM ".$dbname2.".structdireksi ORDER BY ".$dbname2.".structdireksi.emp_hrp1000_s_short ASC limit 0,10  ";
            // echo $rsql;
    		$rs = mysql_query($rsql);	
    		$i=1;
    		while($lev=mysql_fetch_array($rs)){
    		    $i%2 == 0 ? $cl='#caefff' : $cl='#e4f3f9';
				$nama = str_replace("'", "`" , $lev['empname']);
				
				echo'
    			<tr bgcolor="'.$cl.'" onClick="ada(\''.$lev['empnik'].'\' , \''.$nama.'\' , \''.$lev['empkostl'].'\' , \''.$lev['emportx'].'\', \''.$lev['emp_hrp1000_s_short'].'\', \''.$lev['empposid'].'\' , \''.$lev['emportx'].'\' , \''.$lev['emp_hrp1000_o_short'].'\' , \''.$lev['emppostx'].'\')">
    				<td align="center">'.$i.'</td>
    				<td>'.$lev['empnik'].'</td>
					<td>'.$nama.'</td>
					<td>'.$lev['emp_hrp1000_s_short'].'</td>
					<td>'.$lev['empkostl'].'</td>
					<td>'.$lev['emportx'].'</td> 
					<td>'.$lev['emppostx'].'</td> 
    			</tr>';
                $i++;	
    		}
    		@mysql_free_result($rs);	
    	?>
	</table>
</div>
<div id="naf">
	<?
		$rsql   = "	SELECT ".$dbname2.".structdireksi.* FROM ".$dbname2.".structdireksi ORDER BY ".$dbname2.".structdireksi.emp_hrp1000_s_short ASC";
       // echo $rsql;
        $hd		= "";
		$hal	= 0;
		$order 	= "";
		$pager 	= new ajaxpagination($conn, $rsql, $hd, $hal, 10, 10, $order);
		$rs = $pager->paginate();	
		include_once('../../../../includes/nav.php');
	?>
</div>
<script type="text/javascript" >
$(document).ready( function() {	
	var empkostl = $("#empkostl").val();
	var nik = $("#nik").val();
	$('#naf a').click(function() {
		$("#view").load("../../../../modules/surat/suratkeluar/ajax/popfindnikdisdir_detil.php", {'hal': $(this).attr("title"), 'mod' : $("#txtmod").val(), 'empkostl': empkostl, 'nik': nik});
		$('#naf').fadeOut('slow').load("../../../../modules/surat/suratkeluar/ajax/popfindnikdisdir_page.php", {'hal':$(this).attr("title"), 'empkostl': empkostl, 'nik': nik }).fadeIn("slow");
		// alert('a');
	}); 
}); 
function ada(nik, nama, empkostl, emp_cskt_ltext, empshort, empposid, unitkerja, abbrunit, jabatan){ 
	var no = <?=$_REQUEST['no']?>;
	parent.$("#empniktembusan"+no).val(nik);
	parent.$("#empnametembusan"+no).val(nama);
	parent.$("#empkostltembusan"+no).val(empkostl);
	parent.$("#emp_cskt_ltexttembusan"+no).val(emp_cskt_ltext);
	// parent.$("#empshorttembusan"+no).val(empshort);
	parent.$("#empshort_tembusan"+no).val(empshort);
	parent.$("#empposid_tembusan"+no).val(empposid);
	// parent.$("#direktorat").val(emportx);
	
	parent.$("#unitkerja_tembusan"+no).val(unitkerja);
	parent.$("#abbr_unit_tembusan"+no).val(abbrunit);
	parent.$("#jabatan_tembusan"+no).val(jabatan);
	parent.$.colorbox.close(); 
}
</script>
