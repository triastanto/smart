<?
    session_start();
    include_once('../../../../config.php');
    include_once('../../../../includes/functions.php');
    include_once('../../../../includes/koneksi.php');
    include_once('../../../../includes/ajaxpagination.php');
?>
    <script type="text/javascript" src="../../../../javascript/jquery-1.8.3.min.js"></script> 
    <link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/reset.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/grid.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/layout.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/nav.css" media="screen" />
    <style>
    	*{font-family:Trebuchet MS,"Arial", sans-serif;color:black; font-size:11px;}
    </style> 
<title>Search Material</title>
<table width="100%" cellpadding="2" cellspacing="2" border="0" style="border: 1px solid #DEDEDE;font-size: 10pt;" class="head_sr">
	<tr height="30">
		<td width="15%">CARI :</td>
		<td width="40%">
            <select name="pilihcari" id="pilihcari">
                <option value="empname">Nama</option>
                <option value="empnik">NIK</option>
            </select>&nbsp;
			<input type="text" name="txtval" id="txtval" size="40" value="<?=$_REQUEST['fv']?>" />			
			<input type="hidden" name="empnik_asal" id="empnik_asal"  value="<?=$_REQUEST['empnik_asal']?>" />
			<input type="hidden" name="no" id="no" size="5" value="<?=$_REQUEST['no']?>" />			
		</td>	
        <td width="38%" ><input type="button" name="button" id="cari" value="Cari" class='btn btn-blue' >&nbsp; 
        </td>
	</tr>
</table>
<div id="view">
	<table width="100%" border="0">
		<tr class="head_tr">
			<th align="center" width="3%">NO</th>
			<th align="center" width="10%">NIK</th>
			<th align="center" width="20%">NAMA</th>
			<th align="center" width="10%">ABBREVIASI</th>
			<th align="center" width="10%">KODE CC</th>
			<th align="center" width="27%">COST CENTER</th>
			<th align="center" width="20%">JABATAN</th>
		</tr>
    	<?	
    		// $rsql = "SELECT hic.structdisp.empkostl, hic.structdisp.emp_cskt_ltext FROM hic.structdisp group by hic.structdisp.empkostl limit 0,20";

			$rsql = "
					SELECT ".$dbname2.".structdisp.* FROM ".$dbname2.".structdisp where 
						".$dbname2.".structdisp.no='1'
					ORDER BY
						".$dbname2.".structdisp.emp_hrp1000_s_short ASC limit 0,15";
    		// echo $rsql;
			// exit;
    		$rs = mysql_query($rsql);	
    		$i=1;
    		while($lev=mysql_fetch_array($rs)){
    		    $i%2 == 0 ? $cl='#caefff' : $cl='#e4f3f9';
    			$nama = str_replace("'", "`" , $lev['empname']);
				echo'
    			<tr bgcolor="'.$cl.'" onClick="ada(\''.$lev['empnik'].'\' , \''.$namaa.'\' , \''.$lev['empkostl'].'\' , \''.$lev['emportx'].'\' , \''.$lev['emp_hrp1000_s_short'].'\' , \''.$lev['empposid'].'\')">
    				<td align="center">'.$i.'</td>
    				<td align="left">'.$lev['empnik'].'</td>
    				<td align="left">'.$nama.'</td>
    				<td align="left">'.$lev['emp_hrp1000_s_short'].'</td>
    				<td align="left">'.$lev['empkostl'].'</td>
    				<td align="left">'.$lev['emportx'].'</td> 
    				<td align="left">'.$lev['emppostx'].'</td> 
    			</tr>';
                $i++;
    		}
    		@mysql_free_result($rs);	
    	?>
	</table>
</div>
<div id="naf">
	<?
		$rsql 	= "SELECT
						".$dbname2.".structdisp.*
					FROM
						".$dbname2.".structdisp
					WHERE
						".$dbname2.".structdisp.no = '1'
					ORDER BY
						".$dbname2.".structdisp.emp_hrp1000_s_short ASC";
		// echo $rsql;
		$hd		= "";
		$hal	= 0;
		$order 	= "";
		$pager 	= new ajaxpagination($conn, $rsql, $hd, $hal, 15, 15, $order);
		$rs = $pager->paginate();	
		include_once('../../../../includes/nav.php');
	?>
</div>
<script type="text/javascript" >
    $(document).ready( function() {
    	$('#naf a').click(function() {
			var no = $("#no").val();
    		var empnik_asal = $("#empnik_asal").val();
    		$("#view").load("../../../../modules/surat/suratkeluar/ajax/popfindccprdir_detil.php", {'hal': $(this).attr("title"), 'mod' : $("#txtmod").val(), 'no': no });
    		$('#naf').fadeOut('slow').load("../../../../modules/surat/suratkeluar/ajax/popfindccprdir_page.php", {'hal':$(this).attr("title"), 'no': no}).fadeIn("slow");
    		// alert('a');
    	}); 
    	$('#cari').click(function() {
			var no = $("#no").val();
    		var a = $("#txtval").val();
    		var pilih = $("#pilihcari").val();
    		var empnik_asal = $("#empnik_asal").val();
    		if (a=='' || pilih==''){
    			alert("Masukkan nilai yang akan dicari!");
    		}else{
    			// alert("tes");
    			$("#view").load("../../../../modules/surat/suratkeluar/ajax/popfindccprdir_detil.php", {'pilih': pilih, 'nm': a, 'no': no});
    			$('#naf').fadeOut('slow').load("../../../../modules/surat/suratkeluar/ajax/popfindccprdir_page.php", {'hal':$(this).attr("title"), 'nm':a, 'no': no}).fadeIn("slow");
    		}
    		e.stopImmediatePropagation();
    		// alert('b');
    	}); 
    }); 
    function ada(empnikparaf, empnameparaf, empkostl_asal, emp_cskt_ltext_asal, empshort_asal, empposidparaf){ 
		var no = $("#no").val();
    	parent.$("#empnikparaf"+no).val(empnikparaf);
    	parent.$("#empnameparaf"+no).val(empnameparaf);
    	parent.$("#empposidparaf"+no).val(empposidparaf);
    	parent.$.colorbox.close(); 
    }
</script>