<?
    session_start();
    include_once('../../../../config.php');
    include_once('../../../../includes/functions.php');
    include_once('../../../../includes/koneksi.php');
    include_once('../../../../includes/ajaxpagination.php');
?>
    <script type="text/javascript" src="../../../../javascript/jquery-1.8.3.min.js"></script> 
    <link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/reset.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/grid.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/layout.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/nav.css" media="screen" />
    <style>
    	*{font-family:Trebuchet MS,"Arial", sans-serif;color:black; font-size:11px;}
    </style> 
<title>Search Material</title>
<table width="100%" cellpadding="2" cellspacing="2" border="0" style="border: 1px solid #DEDEDE;font-size: 10pt;" class="head_sr">
	<tr height="30">
		<td width="15%">CARI :</td>
		<td width="40%">
            <select name="pilihcaridir" id="pilihcaridir">
                <option value="empname">Nama</option>
                <option value="empnik">NIK</option>
            </select>&nbsp;
			<input type="text" name="txtvaldir" id="txtvaldir" size="40" value="<?=$_REQUEST['fv']?>" />			
			<input type="hidden" name="empnik_asaldir" id="empnik_asaldir"  value="<?=$_REQUEST['empnik_asal']?>" />			
			<input type="hidden" name="no" id="no" size="5" value="<?=$_REQUEST['no']?>" />
		</td>	
        <td width="38%" ><input type="button" name="button" id="caridir" value="Cari" class='btn btn-blue' >&nbsp; 
        </td>
	</tr>
</table>
<div id="viewdir">
	<table width="100%" border="0">
		<tr class="head_tr">
			<th align="center" width="3%">NO</th>
			<th align="center" width="10%">NIK</th>
			<th align="center" width="20%">NAMA</th>
			<th align="center" width="10%">ABBREVIASI</th>
			<th align="center" width="10%">KODE CC</th>
			<th align="center" width="27%">COST CENTER</th>
			<th align="center" width="20%">JABATAN</th>
		</tr>
    	<?	
    		// $rsql = "SELECT hic.structdireksi.empkostl, hic.structdireksi.emp_cskt_ltext FROM hic.structdireksi group by hic.structdireksi.empkostl limit 0,20";
		 	$rsql = "SELECT
						".$dbname2.".structdireksi.`no`,
						".$dbname2.".structdireksi.empnik,
						".$dbname2.".structdireksi.empname,
						".$dbname2.".structdireksi.empjobstext,
						".$dbname2.".structdireksi.emppostx,
						".$dbname2.".structdireksi.empkostl,
						".$dbname2.".structdireksi.emp_hrp1000_s_short,
						".$dbname2.".structdireksi.empposid,
						".$dbname2.".structdireksi.emp_cskt_ltext,
						".$dbname2.".structdireksi.emportx
					FROM
						".$dbname2.".structdireksi
					WHERE
						".$dbname2.".structdireksi.no='1' and empnik<='990017'
					ORDER BY
						".$dbname2.".structdireksi.emp_hrp1000_s_short ASC limit 0,15";
    		// echo $rsql;
			// exit;
    		$rs = mysql_query($rsql);	
    		$i=1;
    		while($lev=mysql_fetch_array($rs)){
    		    $i%2 == 0 ? $cl='#caefff' : $cl='#e4f3f9';
				$nama = str_replace("'", "`" , $lev['empname']);
    			echo'
    			<tr bgcolor="'.$cl.'" onClick="ada(\''.$lev['empnik'].'\' , \''.$nama.'\' , \''.$lev['empkostl'].'\' , \''.$lev['emportx'].'\' , \''.$lev['emp_hrp1000_s_short'].'\' , \''.$lev['empposid'].'\')">
    				<td align="center">'.$i.'</td>
    				<td align="left">'.$lev['empnik'].'</td>
    				<td align="left">'.$lev['empname'].'</td>
    				<td align="left">'.$lev['emp_hrp1000_s_short'].'</td>
    				<td align="left">'.$lev['empkostl'].'</td>
    				<td align="left">'.$lev['emportx'].'</td> 
    				<td align="left">'.$lev['emppostx'].'</td> 
    			</tr>';
                $i++;
    		}
    		@mysql_free_result($rs);	
    	?>
	</table>
</div>
<div id="nafdir">
	<?
		$rsql 	= "SELECT
						".$dbname2.".structdireksi.`no`,
						".$dbname2.".structdireksi.empnik,
						".$dbname2.".structdireksi.empname,
						".$dbname2.".structdireksi.empjobstext,
						".$dbname2.".structdireksi.emppostx,
						".$dbname2.".structdireksi.empkostl,
						".$dbname2.".structdireksi.emp_hrp1000_s_short,
						".$dbname2.".structdireksi.empposid,
						".$dbname2.".structdireksi.emp_cskt_ltext
					FROM
						".$dbname2.".structdireksi
					WHERE
						".$dbname2.".structdireksi.no='1' and empnik<='990017'
					ORDER BY
						".$dbname2.".structdireksi.emp_hrp1000_s_short ASC";
		$hd		= "";
		$hal	= 0;
		$order 	= "";
		$pager 	= new ajaxpagination($conn, $rsql, $hd, $hal, 15, 15, $order);
		$rs = $pager->paginate();	
		include_once('../../../../includes/nav2.php');
	?>
</div>
<script type="text/javascript" >
    $(document).ready( function() {
    	$('#nafdir a').click(function() {
    		var empnik_asal = $("#empnik_asaldir").val();
    		var no = $("#no").val();
    		$("#viewdir").load("../../../../modules/surat/suratkeluar/ajax/popfindparafdir_detil.php", {'hal': $(this).attr("title"), 'mod' : $("#txtmod").val(), 'empnik_asal': empnik_asal, 'no': no });
    		$('#nafdir').fadeOut('slow').load("../../../../modules/surat/suratkeluar/ajax/popfindparafdir_page.php", {'hal':$(this).attr("title"), 'empnik_asal': empnik_asal, 'no': no }).fadeIn("slow");
    		alert('a');
    	}); 
    	$('#caridir').click(function() {
    		var a = $("#txtvaldir").val();
    		var pilih = $("#pilihcaridir").val();
    		var empnik_asal = $("#empnik_asaldir").val();
			var no = $("#no").val();
    		if (a=='' || pilih==''){
    			alert("Masukkan nilai yang akan dicari!");
    		}else{
    			// alert("tes");
    			$("#viewdir").load("../../../../modules/surat/suratkeluar/ajax/popfindparafdir_detil.php", {'pilih': pilih, 'nm': a, 'empnik_asal': empnik_asal, 'no': no });
    			$('#nafdir').fadeOut('slow').load("../../../../modules/surat/suratkeluar/ajax/popfindparafdir_page.php", {'hal':$(this).attr("title"), 'nm':a, 'empnik_asal': empnik_asal, 'no': no }).fadeIn("slow");
    		}
    		e.stopImmediatePropagation();
    		alert('b');
    	}); 
    }); 
    function ada(empnikparaf, empnameparaf, empkostl_asal, emp_cskt_ltext_asal, empshort_asal, empposidparaf){ 
    	var no = $("#no").val();
		// alert(no);
		parent.$("#empnikparaf"+no).val(empnikparaf);
    	parent.$("#empnameparaf"+no).val(empnameparaf);
    	parent.$("#empposidparaf"+no).val(empposidparaf);
    	parent.$.colorbox.close(); 
    }
</script>