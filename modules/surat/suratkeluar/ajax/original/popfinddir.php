<?
    session_start();
    include_once('../../../../config.php');
    include_once('../../../../includes/functions.php');
    include_once('../../../../includes/koneksi.php');
    include_once('../../../../includes/ajaxpagination.php');
?>
    <script type="text/javascript" src="../../../../javascript/jquery-1.8.3.min.js"></script> 
    <link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/reset.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/grid.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/layout.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/nav.css" media="screen" />
    <style>
    	*{font-family:Trebuchet MS,"Arial", sans-serif;color:black; font-size:11px;}
    </style> 
<title>Search Material</title>
<table width="100%" cellpadding="2" cellspacing="2" border="0" style="border: 1px solid #DEDEDE;font-size: 10pt;" class="head_sr">
	<tr height="30">
		<td width="15%">CARI :</td>
		<td width="40%">
            <select name="pilihcaridir" id="pilihcaridir">
                <option value="">-Pilih Pencarian-</option>
                <option value="empnik">NIK</option>
                <option value="empname">Nama</option>
                <option value="emp_hrp1000_s_short">Abbreviasi</option>
                <option value="empkostl">Kode CC</option>
               <option value="emp_cskt_ltext">Cost Center</option>
            </select>&nbsp;
			<input type="text" name="txtvaldir" id="txtvaldir" size="40" value="<?=$_REQUEST['fv']?>" />			
			<input type="hidden" name="userdir" id="userdir"  value="<?=$_REQUEST['user']?>" />			
		</td>	
        <td width="38%" ><input type="button" name="button" id="caridir" value="Caridir" class='btn btn-blue' >&nbsp; 
        </td>
	</tr>
</table>
<div id="viewdir">
	<table width="100%" border="0">
		<tr class="head_tr">
			<th align="center" width="3%">NO</th>
			<th align="center" width="10%">NIK</th>
			<th align="center" width="20%">NAMA</th>
			<th align="center" width="10%">ABBREVIASI</th>
			<th align="center" width="10%">KODE CC</th>
			<th align="center" width="27%">COST CENTER</th>
			<th align="center" width="20%">JABATAN</th>
		</tr>
    	<?	
    		$qceking = mysql_num_rows(mysql_query("SELECT
					".$dbname2.".structdireksi.*,
					".$dbname.".sys_groupuser.*,
					".$dbname.".sys_user.*,
					".$dbname.".m_singkatan.*
				FROM
					".$dbname2.".structdireksi
					INNER JOIN ".$dbname.".sys_groupuser ON ".$dbname.".sys_groupuser.pos_abbr = ".$dbname2.".structdireksi.emp_hrp1000_s_short
					INNER JOIN ".$dbname.".sys_user ON ".$dbname.".sys_groupuser.userid = ".$dbname.".sys_user.userid
					INNER JOIN ".$dbname.".m_singkatan ON ".$dbname.".m_singkatan.empkostl = ".$dbname2.".structdireksi.empkostl
				WHERE 
					".$dbname.".sys_groupuser.userid='".$_REQUEST['user']."'
				ORDER BY ".$dbname2.".structdireksi.emp_hrp1000_s_short ASC"));
			
			if($qceking > 0){
				$rsql = "SELECT
						".$dbname2.".structdireksi.*,
						".$dbname.".sys_groupuser.userid,
						".$dbname.".sys_user.userid
					FROM
						".$dbname2.".structdireksi
						INNER JOIN ".$dbname.".sys_groupuser ON ".$dbname.".sys_groupuser.pos_abbr = ".$dbname2.".structdireksi.emp_hrp1000_s_short
						INNER JOIN ".$dbname.".sys_user ON ".$dbname.".sys_groupuser.userid = ".$dbname.".sys_user.userid
					WHERE
						".$dbname.".sys_groupuser.userid='".$_REQUEST['user']."'
					ORDER BY
						".$dbname2.".structdireksi.emp_hrp1000_s_short ASC limit 0,15";
				// echo $rsql;
				$rs = mysql_query($rsql);	
				$i=1;
				while($lev=mysql_fetch_array($rs)){
					$i%2 == 0 ? $cl='#caefff' : $cl='#e4f3f9';
					$nama = str_replace("'", "`" , $lev['empname']);
					echo'
					<tr bgcolor="'.$cl.'" onClick="adadireksi(\''.$lev['empnik'].'\' , \''.$nama.'\' , \''.$lev['empkostl'].'\' , \''.$lev['emportx'].'\' , \''.$lev['emp_hrp1000_s_short'].'\' , \''.$lev['empposid'].'\' , \''.$lev['emportx'].'\' , \''.$lev['emp_hrp1000_o_short'].'\' , \''.$lev['emppostx'].'\')">
						<td align="center">'.$i.'</td>
						<td align="left">'.$lev['empnik'].'</td>
						<td align="left">'.$nama.'</td>
						<td align="left">'.$lev['emp_hrp1000_s_short'].'</td>
						<td align="left">'.$lev['empkostl'].'</td>
						<td align="left">'.$lev['emportx'].'</td> 
						<td align="left">'.$lev['emppostx'].'</td> 
					</tr>';
					$i++;
				}
				@mysql_free_result($rs);
			}else{
				echo'<tr>
						<td colspan="7"> <b>Maaf Anda Belum Menginput Singkatan Untuk Divisi Anda, Sehingga Atasan Anda Tidak Keluar. Silahkan Anda Masuk Ke Menu Administrasi -> Set Singkatan Divisi</b> </td>
					</tr>';
			}
			
		?>
	</table>
</div>
<div id="nafdir">
	<?
		$rsql 	= "SELECT
						".$dbname2.".structdireksi.*,
						".$dbname.".sys_groupuser.userid,
						".$dbname.".sys_groupuser.pos_abbr,
						".$dbname.".sys_user.userid,
						".$dbname.".sys_user.userpass
					FROM
						".$dbname2.".structdireksi
						INNER JOIN ".$dbname.".sys_groupuser ON ".$dbname.".sys_groupuser.pos_abbr = ".$dbname2.".structdireksi.emp_hrp1000_s_short
						INNER JOIN ".$dbname.".sys_user ON ".$dbname.".sys_groupuser.userid = ".$dbname.".sys_user.userid
					WHERE
						".$dbname.".sys_groupuser.userid='".$_REQUEST['user']."'
					ORDER BY
						".$dbname2.".structdisp.emp_hrp1000_s_short ASC";
		$hd		= "";
		$hal	= 0;
		$order 	= "";
		$pager 	= new ajaxpagination($conn, $rsql, $hd, $hal, 15, 15, $order);
		$rs = $pager->paginate();	
		include_once('../../../../includes/nav3.php');
	?>
</div>
<script type="text/javascript" >
    $(document).ready( function() {
    	$('#nafdir a').click(function() {
    		var a = $("#txtvaldir").val();
    		var pilih = $("#pilihcaridir").val();
    		var user = $("#userdir").val();
    		$("#view").load("../../../../modules/surat/suratkeluar/ajax/popfinddir_detil.php", {'hal': $(this).attr("title"), 'pilih': pilih, 'nm': a, 'user': user });
    		$('#nafdir').fadeOut('slow').load("../../../../modules/surat/suratkeluar/ajax/popfinddir_page.php", {'hal':$(this).attr("title"), 'pilih': pilih, 'nm': a, 'user': user }).fadeIn("slow");
    		// alert('a');
    	}); 
    	$('#caridir').click(function() {
    		var a = $("#txtvaldir").val();
    		var pilih = $("#pilihcaridir").val();
    		var user = $("#userdir").val();
    		if (a=='' || pilih==''){
    			alert("Masukkan nilai yang akan dicari!");
    		}else{
    			// alert("tes");
    			$("#viewdir").load("../../../../modules/surat/suratkeluar/ajax/popfinddir_detil.php", {'pilih': pilih, 'nm': a, 'user': user });
    			$('#nafdir').fadeOut('slow').load("../../../../modules/surat/suratkeluar/ajax/popfinddir_page.php", {'hal':$(this).attr("title"), 'pilih': pilih, 'nm': a, 'user': user}).fadeIn("slow");
    		}
    		e.stopImmediatePropagation();
    		// alert('b');
    	}); 
    }); 
    function adadireksi(empnik_asal, empname_asal, empkostl_asal, emp_cskt_ltext_asal, empshort_asal, empposid, unitkerja, abbrunit, jabatan){ 
    	parent.$("#empnik_asal").val(empnik_asal);
    	parent.$("#empname_asal").val(empname_asal);
    	parent.$("#empkostl_asal").val(empkostl_asal);
    	parent.$("#emp_cskt_ltext_asal").val(emp_cskt_ltext_asal);
    	parent.$("#empshort_asal").val(empshort_asal);
    	parent.$("#empposid_asal").val(empposid);
    	parent.$("#unitkerja_asal").val(unitkerja);
    	parent.$("#abbr_unit_asal").val(abbrunit);
    	parent.$("#jabatan_asal").val(jabatan);
		// alert(unitkerja +"-"+ abbrunit +"-"+ jabatan);
    	parent.$.colorbox.close(); 
    }
</script>