<?
session_start();
include_once('../../../../config.php');
include_once('../../../../includes/functions.php');
include_once('../../../../includes/koneksi.php');
include_once('../../../../includes/ajaxpagination.php');

?>
<script type="text/javascript" src="../../../../javascript/jquery-1.8.3.min.js"></script> 
    <link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/reset.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/grid.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/layout.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/nav.css" media="screen" />
    <style>
    	*{font-family:Trebuchet MS,"Arial", sans-serif;color:black; font-size:11px;}

    </style> 
<title>Search Material</title>
<table width="100%" cellpadding="2" cellspacing="2" border="0" style="border: 1px solid #DEDEDE;font-size: 10pt;" class="head_sr">
	<tr height="30">
		<td width="15%">CARI :
		</td>
		<td width="40%">
            <select name="pilihcari" id="pilihcari">
                <option value="">-Pilih Pencarian-</option>
                <option value="empnik">NIK</option>
                <option value="empname">Nama</option>
                <option value="emp_hrp1000_s_short">Abbreviasi</option>
                <option value="empkostl">Kode CC</option>
               <option value="emp_cskt_ltext">Cost Center</option>
            </select>&nbsp;
			<input type="text" name="txtval" id="txtval" size="40" value="<?=$_REQUEST['fv']?>" />			
		</td>	
        <td width="38%" ><input type="button" name="button" id="cari" value="Cari" class='btn btn-blue' >&nbsp; 
        </td>
	</tr>
</table>
<div id="view">
	<table width="100%" border="0">
		<tr class="head_tr">
			<th align="center" width="3%">NO</th>
			<th align="center" width="10%">NIK</th>
			<th align="center" width="20%">NAMA</th>
			<th align="center" width="10%">ABBREVIASI</th>
			<th align="center" width="10%">KODE CC</th>
			<th align="center" width="47%">COST CENTER</th>
		</tr>
    	<?	
    		// $rsql = "SELECT ".$dbname2.".structdisp.empkostl, ".$dbname2.".structdisp.emp_cskt_ltext FROM ".$dbname2.".structdisp group by ".$dbname2.".structdisp.empkostl limit 0,20";
    		$rsql = "SELECT
						".$dbname2.".structdisp.*
					FROM
						".$dbname2.".structdisp
					WHERE
						".$dbname2.".structdisp.`no` = '1' AND
						".$dbname2.".structdisp.emppersk = 'AS' or
						".$dbname2.".structdisp.`no` = '1' AND
						".$dbname2.".structdisp.emppersk = 'BS'
					ORDER BY ".$dbname2.".structdisp.emp_hrp1000_s_short ASC limit 0,15";
    		// echo $rsql;
    		$rs = mysql_query($rsql);	
    		$i=1;
    		while($lev=mysql_fetch_array($rs)){
				$nama = str_replace("'", "`" , $lev['empname']);
    		    $i%2 == 0 ? $cl='#caefff' : $cl='#e4f3f9';
    			echo'
    			<tr bgcolor="'.$cl.'" onClick="ada(\''.$lev['empnik'].'\' , \''.$nama.'\' , \''.$lev['empkostl'].'\' , \''.$lev['emportx'].'\' , \''.$lev['emp_hrp1000_s_short'].'\')">
    				<td align="center">'.$i.'</td>
    				<td align="left">'.$lev['empnik'].'</td>
    				<td align="left">'.$lev['empname'].'</td>
    				<td align="left">'.$lev['emp_hrp1000_s_short'].'</td>
    				<td align="left">'.$lev['empkostl'].'</td>
    				<td align="left">'.$lev['emportx'].'</td> 
    			</tr>';
                $i++;	
    		}
    		@mysql_free_result($rs);	
    	?>
	</table>
</div>
<div id="naf">
	<?
		$rsql 	= "SELECT
						".$dbname2.".structdisp.*
					FROM
						".$dbname2.".structdisp
					WHERE
						".$dbname2.".structdisp.`no` = '1' AND
						".$dbname2.".structdisp.emppersk = 'AS' or
						".$dbname2.".structdisp.`no` = '1' AND
						".$dbname2.".structdisp.emppersk = 'BS'
					ORDER BY ".$dbname2.".structdisp.emp_hrp1000_s_short ASC";
		$hd		= "";
		$hal	= 0;
		$order 	= "";
		$pager 	= new ajaxpagination($conn, $rsql, $hd, $hal, 15, 15, $order);
		$rs = $pager->paginate();	
		include_once('../../../../includes/nav.php');
	?>
</div>
<script type="text/javascript" >
$(document).ready( function() {
	$('#naf a').click(function() {
		$("#view").load("../../../../modules/surat/suratkeluar/ajax/popfindall_detil.php", {'hal': $(this).attr("title"), 'mod' : $("#txtmod").val()});
		$('#naf').fadeOut('slow').load("../../../../modules/surat/suratkeluar/ajax/popfindall_page.php", {'hal':$(this).attr("title") }).fadeIn("slow");
		// alert('a');
	}); 
	$('#cari').click(function() {
		var a = $("#txtval").val();
		var pilih = $("#pilihcari").val();
		if (a=='' || pilih==''){
			alert("Masukkan nilai yang akan dicari!");
		}else{
			// alert("tes");
			$("#view").load("../../../../modules/surat/suratkeluar/ajax/popfindall_detil.php", {'pilih': pilih, 'nm': a });
			$('#naf').fadeOut('slow').load("../../../../modules/surat/suratkeluar/ajax/popfindall_page.php", {'hal':$(this).attr("title"), 'nm':a }).fadeIn("slow");
		}
		e.stopImmediatePropagation();
		// alert('b');
	}); 
}); 
function ada(empnik_surat, empname_surat, empkostl_surat, emp_cskt_ltext_surat,empshort_surat){ 
	parent.$("#empnik_surat").val(empnik_surat);
	parent.$("#empname_surat").val(empname_surat);
	parent.$("#empkostl_surat").val(empkostl_surat);
	parent.$("#emp_cskt_ltext_surat").val(emp_cskt_ltext_surat);
	parent.$("#empshort_surat").val(empshort_surat);
	// parent.$("#direktorat").val(emportx);
	parent.$.colorbox.close(); 
}
</script>