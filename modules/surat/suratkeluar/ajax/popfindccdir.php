<?
    session_start();
    include_once('../../../../config.php');
    include_once('../../../../includes/functions.php');
    include_once('../../../../includes/koneksi.php');
    include_once('../../../../includes/ajaxpagination.php');
?>
    <script type="text/javascript" src="../../../../javascript/jquery-1.8.3.min.js"></script> 
    <link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/reset.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/grid.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/layout.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/nav.css" media="screen" />
    <style>
    	*{font-family:Trebuchet MS,"Arial", sans-serif;color:black; font-size:11px;}
    </style> 
<title>Search Material</title>
<table width="100%" cellpadding="2" cellspacing="2" border="0" style="border: 1px solid #DEDEDE;font-size: 10pt;" class="head_sr">
	<tr height="30">
		<td width="15%">CARI :</td>
		<td width="40%">
            <select name="pilihcari" id="pilihcari">
                <option value="">-Pilih Pencarian-</option>
                <option value="empnik">NIK</option>
                <option value="empname">Nama</option>
                <option value="emp_hrp1000_s_short">Abbreviasi</option>
                <option value="empkostl">Kode CC</option>
               <option value="emp_cskt_ltext">Cost Center</option>
            </select>&nbsp;
			<input type="text" name="txtval" id="txtval" size="40" value="<?=$_REQUEST['fv']?>" />			
			<input type="hidden" name="user" id="user"  value="<?=$_REQUEST['user']?>" />			
		</td>	
        <td width="38%" ><input type="button" name="button" id="cari" value="Cari" class='btn btn-blue' >&nbsp; 
        </td>
	</tr>
</table>
<div id="view">
	<table width="100%" border="0">
		<tr class="head_tr">
			<th align="center" width="3%">NO</th>
			<th align="center" width="10%">NIK</th>
			<th align="center" width="20%">NAMA</th>
			<th align="center" width="10%">ABBREVIASI</th>
			<th align="center" width="10%">KODE CC</th>
			<th align="center" width="27%">COST CENTER</th>
			<th align="center" width="20%">JABATAN</th>
		</tr>
    	<?	
    		// $rsql = "SELECT hic.structdisp.empkostl, hic.structdisp.emp_cskt_ltext FROM hic.structdisp group by hic.structdisp.empkostl limit 0,20";
		 	$rsql = "SELECT
						".$dbname2.".structdireksi.`no`,
						".$dbname2.".structdireksi.empnik,
						".$dbname2.".structdireksi.empname,
						".$dbname2.".structdireksi.empjobstext,
						".$dbname2.".structdireksi.emppostx,
						".$dbname2.".structdireksi.empkostl,
						".$dbname2.".structdireksi.emp_hrp1000_s_short,
						".$dbname2.".structdireksi.emp_cskt_ltext,
						".$dbname2.".structdireksi.emportx,
						".$dbname.".sys_groupuser.userid,
						".$dbname.".sys_groupuser.pos_abbr,
						".$dbname.".sys_user.userid,
						".$dbname.".sys_user.userpass
					FROM
						".$dbname2.".structdireksi
						INNER JOIN ".$dbname.".sys_groupuser ON ".$dbname.".sys_groupuser.pos_abbr = ".$dbname2.".structdireksi.emp_hrp1000_s_short
						INNER JOIN ".$dbname.".sys_user ON ".$dbname.".sys_groupuser.userid = ".$dbname.".sys_user.userid
					WHERE
						
						".$dbname.".sys_groupuser.userid='".$_REQUEST['user']."'
					ORDER BY
						".$dbname2.".structdireksi.emp_hrp1000_s_short ASC limit 0,15";
    		// echo $rsql;
    		$rs = mysql_query($rsql);	
    		$i=1;
    		while($lev=mysql_fetch_array($rs)){
    		    $i%2 == 0 ? $cl='#caefff' : $cl='#e4f3f9';
				$nama = str_replace("'", "`" , $lev['empname']);
    			echo'
    			<tr bgcolor="'.$cl.'" onClick="ada(\''.$lev['empnik'].'\' , \''.$nama.'\' , \''.$lev['empkostl'].'\' , \''.$lev['emportx'].'\' , \''.$lev['emp_hrp1000_s_short'].'\')">
    				<td align="center">'.$i.'</td>
    				<td align="left">'.$lev['empnik'].'</td>
    				<td align="left">'.$nama.'</td>
    				<td align="left">'.$lev['emp_hrp1000_s_short'].'</td>
    				<td align="left">'.$lev['empkostl'].'</td>
    				<td align="left">'.$lev['emportx'].'</td> 
    				<td align="left">'.$lev['emppostx'].'</td> 
    			</tr>';
                $i++;
    		}
    		@mysql_free_result($rs);	
    	?>
	</table>
</div>
<div id="naf">
	<?
		$rsql 	= "SELECT
						".$dbname2.".structdireksi.`no`,
						".$dbname2.".structdireksi.empnik,
						".$dbname2.".structdireksi.empname,
						".$dbname2.".structdireksi.empjobstext,
						".$dbname2.".structdireksi.emppostx,
						".$dbname2.".structdireksi.empkostl,
						".$dbname2.".structdireksi.emp_hrp1000_s_short,
						".$dbname2.".structdireksi.emportx,
						".$dbname2.".structdireksi.emp_cskt_ltext,
						".$dbname.".sys_groupuser.userid,
						".$dbname.".sys_groupuser.pos_abbr,
						".$dbname.".sys_user.userid,
						".$dbname.".sys_user.userpass
					FROM
						".$dbname2.".structdireksi
						INNER JOIN ".$dbname.".sys_groupuser ON ".$dbname.".sys_groupuser.pos_abbr = ".$dbname2.".structdireksi.emp_hrp1000_s_short
						INNER JOIN ".$dbname.".sys_user ON ".$dbname.".sys_groupuser.userid = ".$dbname.".sys_user.userid
					WHERE
						".$dbname.".sys_groupuser.userid='".$_REQUEST['user']."'
					ORDER BY
						".$dbname2.".structdireksi.emp_hrp1000_s_short ASC";
		$hd		= "";
		$hal	= 0;
		$order 	= "";
		$pager 	= new ajaxpagination($conn, $rsql, $hd, $hal, 15, 15, $order);
		$rs = $pager->paginate();	
		include_once('../../../../includes/nav.php');
	?>
</div>
<script type="text/javascript" >
    $(document).ready( function() {
    	$('#naf a').click(function() {
    		$("#view").load("../../../../modules/surat/suratkeluar/ajax/popfindccdir_detil.php", {'hal': $(this).attr("title"), 'mod' : $("#txtmod").val(), 'user': $("#user").val() });
    		$('#naf').fadeOut('slow').load("../../../../modules/surat/suratkeluar/ajax/popfindccdir_page.php", {'hal':$(this).attr("title"), 'user': $("#user").val() }).fadeIn("slow");
    		// alert('a');
    	}); 
    	$('#cari').click(function() {
    		var a = $("#txtval").val();
    		var pilih = $("#pilihcari").val();
    		if (a=='' || pilih==''){
    			alert("Masukkan nilai yang akan dicari!");
    		}else{
    			// alert("tes");
    			$("#view").load("../../../../modules/surat/suratkeluar/ajax/popfindccdir_detil.php", {'pilih': pilih, 'nm': a, 'user': $("#user").val()  });
    			$('#naf').fadeOut('slow').load("../../../../modules/surat/suratkeluar/ajax/popfindccdir_page.php", {'hal':$(this).attr("title"), 'nm':a, 'user': $("#user").val() }).fadeIn("slow");
    		}
    		e.stopImmediatePropagation();
    		// alert('b');
    	}); 
    }); 
    function ada(empnik_asal, empname_asal, empkostl_asal, emp_cskt_ltext_asal, empshort_asal){ 
    	parent.$("#empnik_asal").val(empnik_asal);
    	parent.$("#empname_asal").val(empname_asal);
    	parent.$("#empkostl_asal").val(empkostl_asal);
    	parent.$("#emp_cskt_ltext_asal").val(emp_cskt_ltext_asal);
    	parent.$("#empshort_asal").val(empshort_asal);
    	parent.$.colorbox.close(); 
    }
</script>