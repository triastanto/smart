<?
    session_start();
    include_once('../../../../config.php');
    include_once('../../../../includes/functions.php');
    include_once('../../../../includes/koneksi.php');
    include_once('../../../../includes/ajaxpagination.php');
?>
<script type="text/javascript" src="../../../../javascript/jquery-1.8.3.min.js"></script> 
<script type="text/javascript">
	function cekposisi(str){
		// alert(str);
		if (str==""){
		  document.getElementById("tampilkanaja").innerHTML="";
		  return;
		}
		if (window.XMLHttpRequest){
			xmlhttp=new XMLHttpRequest();
		}else{
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function(){
			if (xmlhttp.readyState==4 && xmlhttp.status==200){
				document.getElementById("tampilkanaja").innerHTML=xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET","../../../../modules/surat/suratkeluar/ajax/arsipkoh.php?q="+str,true);
		xmlhttp.send();
	} 
	

</script> 



<link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/reset.css" media="screen" />
<link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/grid.css" media="screen" />
<link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/layout.css" media="screen" />
<link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/nav.css" media="screen" />
<style>
	*{font-family:Trebuchet MS,"Arial", sans-serif;color:black; font-size:11px;}
</style> 
<title>Search Material</title>
<table width="100%" cellpadding="2" cellspacing="2" border="0" style="border: 1px solid #DEDEDE;font-size: 10pt;" class="head_sr">
	<tr height="30">
		<td width="15%"> CARI : </td>
		<td width="15%">
            <select name="pilihcari" id="pilihcari" onchange="cekposisi(this.value)">
                <option value="empname">Nama</option>
                <option value="emppersk">Jabatan</option>
                <option value="empnik">NIK</option>
                <option value="emp_hrp1000_s_short">Abbreviasi</option>
                <option value="empkostl">Kode CC</option>
               <option value="emp_cskt_ltext">Cost Center</option>
            </select>&nbsp;
			<input type="hidden" id="groupid" size="10" value="<?=$_REQUEST['groupid']?>" />
			<input type="hidden" id="nik" size="10" value="<?=$_REQUEST['user']?>" />
			<input type="hidden" id="empkostl" size="10" value="<?=$_REQUEST['empkostl']?>" />
			<input type="hidden" id="no" size="10" value="<?=$_REQUEST['no']?>" />
		</td>	
        <td width="25%" >
			<div id="tampilkanaja"> <input type="text" name="txtval" id="txtval2" size="40" value="" /> </div>	
		</td>
        <td width="38%" ><input type="button" name="button" id="cari2" value="Cari" class='btn btn-blue'/>&nbsp;</td>
	</tr>
</table>
<div id="view2">
	<table width="100%" border="0">
		<tr class="head_tr">
			<th align="center" width="3%">NO</th>
			<th align="center" width="5%">NIK</th>
			<th align="center" width="20%">NAMA</th>
			<th align="center" width="10%">ABBREVIASI</th>
			<th align="center" width="10%">KODE CC</th>
			<th align="center" width="27%">COST CENTER</th>
			<th align="center" width="25%">JABATAN</th>
		</tr>
    	<?	
           $rsql ="SELECT ".$dbname2.".m_plt.* FROM ".$dbname2.".m_plt ORDER BY ".$dbname2.".m_plt.emp_hrp1000_s_short asc";
            // echo $rsql;
    		$rs = mysql_query($rsql);	
    		$i=1;
    		while($lev=mysql_fetch_array($rs)){
    		    $i%2 == 0 ? $cl='#caefff' : $cl='#e4f3f9';
    			$nama = str_replace("'", "`" , $lev['empname']);
				
				$stjab = mysql_fetch_array(mysql_query("SELECT ".$dbname2.".structjab.* from ".$dbname2.".structjab where no='".$lev['emp_hrp1000_s_short']."' and gol='".$lev['emppersk']."'"));
				$unit	= mysql_fetch_array(mysql_query("select * from ".$dbname2.".org_text where ObjectID='".$lev['emporid']."' and Objectabbr='".$lev['emp_hrp1000_o_short']."' ORDER BY EndDate desc limit 1"));
				
				echo'
    			<tr bgcolor="'.$cl.'" onClick="adakepada(\''.$lev['empnik'].'\' , \''.$nama.'\' , \''.$lev['empkostl'].'\' , \''.$lev['emportx'].'\', \''.$lev['emp_hrp1000_s_short'].'\', \''.$lev['empposid'].'\' , \''.$lev['emp_cskt_ltext'].'\' , \''.$lev['emp_hrp1000_o_short'].'\' , \''.$lev['emppostx'].'\')">
    				<td align="center">'.$i.'</td>
    				<td align="left">'.$lev['empnik'].'</td>
    				<td align="left">'.$lev['empname'].'</td>
    				<td align="left">'.$lev['emp_hrp1000_s_short'].'</td>
    				<td align="left">'.$lev['empkostl'].'</td>    				
					<td>'.$lev['emp_cskt_ltext'].'</td> 
					<!--td>'.$stjab['jabatan'].'</td--> 
					<td>'.$lev['emppostx'].'</td> 
    			</tr>';
                $i++;	
    		}
    		@mysql_free_result($rs);	
    	?>
	</table>
</div>
<script type="text/javascript" >
	function adakepada(nik, nama, empkostl, emp_cskt_ltext,empshort, empposid, unitkerja, abbrunit, jabatan){ 
		var no = <?=$_REQUEST['no']?>;
		// alert(no+"-"+unitkerja+"-"+abbrunit+"-"+jabatan);
		parent.$("#empniktembusan"+no).val(nik);
		parent.$("#empnametembusan"+no).val(nama);
		parent.$("#empkostltembusan"+no).val(empkostl);
		parent.$("#emp_cskt_ltexttembusan"+no).val(emp_cskt_ltext);
		parent.$("#empshort_tembusan"+no).val(empshort);
		parent.$("#empposid_tembusan"+no).val(empposid);
    	parent.$("#unitkerja_tembusan"+no).val(unitkerja);
    	parent.$("#abbr_unit_tembusan"+no).val(abbrunit);
    	parent.$("#jabatan_tembusan"+no).val(jabatan);
		parent.$.colorbox.close(); 
	}
</script>