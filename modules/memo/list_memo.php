<?
    out("title", "NOTIFIKASI PEMINDAHAN BERKAS");
    include_once 'config.php';
	$groupid 	= $_SESSION['group_id'];
	$user 		= $_SESSION['user'];
?>
<script type="text/javascript" src="javascript/jquery.form.js"></script>	
<script type="text/javascript">
	$(document).ready( function(){
	    var options = {
			beforeSubmit:  showRequest,  // pre-submit callback 
			success:       showResponse,  // post-submit callback 
			url:'modules/pdf/cetaklabel.php',
			iframe:true
		};
		
		$('#myfm').submit(function(event) { 
			event.preventDefault();
			$(this).ajaxSubmit(options); 
			event.stopImmediatePropagation();
			return false; 
		});
	});
    function showRequest(formData, jqForm, options)
    { 
		var form = jqForm[0]; 
		return true; 
	} 
	function showResponse(responseText, statusText, xhr, $form){
		var mydata = responseText.split(" ");
		if (mydata[0]=="OK"){
			alert("Data sudah disimpan!");
			window.location.href = "?mod="+mydata[1]+"&op="+mydata[2];
		}else{
			alert(responseText); 
		}
	}
</script>

<div id="view">
	<table width="100%" border="0">
		<tr>
			<th align="center" width="3%">NO</th>
			<th align="center" width="12%">Dari</th>
			<th align="center" width="15%">Waktu</th>
			<th align="center" width="50%">Kode CC / Cost Center</th>
			<th align="center" width="10%">View Memo</th>
			<th align="center" width="10%">Status Memo</th>
		</tr>
	 <?
		$rsql = "SELECT sys_groupuser.kostl, histori_memo.*, sys_user.userid FROM sys_groupuser INNER JOIN histori_memo ON sys_groupuser.kostl = histori_memo.kodecc INNER JOIN sys_user ON sys_user.userid = sys_groupuser.userid where sys_user.userid='".$user."' GROUP BY sys_groupuser.kostl order by status ASC";
		// echo $rsql;
		$rs = mysql_query($rsql);	
		$i=0;
		while($lev=mysql_fetch_array($rs)){
			$hic = mysql_fetch_array(mysql_query("SELECT ".$dbname2.".structdisp.emp_cskt_ltext, ".$dbname2.".structdisp.empkostl FROM ".$dbname2.".structdisp where ".$dbname2.".structdisp.empkostl='".$lev['kostl']."' GROUP BY ".$dbname2.".structdisp.empkostl"));
			if($lev['status']==0){
				$im = '<img src="images/loading.gif" border="0" width="15" height="15" title="Memo Belum Dilihat">';
			}else{
				$im = '<img src="images/chek.png" border="0" width="15" height="15" title="Memo Sudah Dilihat">';
			}
			echo'<tr bgcolor="'.rowClass(++$i).'" class="bariswarna">
					<td align="center">'.$i.'</td>
					<td align="center">'.$lev['dari'].'</td>
					<td align="left">'.$lev['tanggal'].'</td>
					<td align="left">'.$lev['kostl'].' / '.$hic['emp_cskt_ltext'].'</td>
					<td align="center">
						<a href="modules/memo/viewmemo.php?id='.$lev['id'].'" class="popup"><img src="images/magnifier.png" border="0" width="15" height="15" title="Click Here for Detail Memo"></a>
					</td>
					<td align="center">'.$im.'</td>
			   </tr>';
		}
		@mysql_free_result($rs);	
	?>
	</table>
</div>
<div id="naf">
	<?
		$rsql 	= "SELECT * from t_m_workorder ";
		// echo $rsql;
		$hd		= "";
		$hal	= 0;
		$order 	= " order by kodeworkorder";
		$pager 	= new ajaxpagination($conn, $rsql, $hd, $hal, 15, 15, $order);
		$rs = $pager->paginate();	
		include_once('includes/nav.php');
	?>
</div>
<script type="text/javascript" >
    $(document).ready( function(){
    	$('#naf a').click(function(){
    		$("#view").load("modules/pemindahan/list_pemindahan_detil.php", {'hal': $(this).attr("title")});
    		$('#naf').fadeOut('slow').load("modules/pemindahan/list_pemindahan_page.php", {'hal':$(this).attr("title")}).fadeIn("slow");
    	});
    	$('#cari').click(function()
        {
    		var a = $("#txtval").val();
    		if (a==''){
    			alert("Masukkan nilai yang akan dicari!");
    		}else{
    			$("#view").load("modules/pemindahan/list_pemindahan_detil.php", {'nm': a });
    			$('#naf').fadeOut('slow').load("modules/pemindahan/list_pemindahan_page.php", {'hal':$(this).attr("title"), 'nm':a}).fadeIn("slow");
    		}
    	});
    });
	$(document).ready( function() {
	    $(".popup").colorbox({ 	
	       iframe:true,
           width:"90%",
           height:"85%"
       	});	    
        $(".pop_berita").colorbox({ 	
	       iframe:true,
           width:"98%",
           height:"95%"
       	});
    });
</script>