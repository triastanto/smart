<?
    session_start();
    include_once('../../../config.php');
    include_once('../../../includes/functions.php');
    include_once('../../../includes/koneksi.php');
    include_once('../../../includes/ajaxpagination.php');
?>
    <script type="text/javascript" src="../../../javascript/jquery-1.8.3.min.js"></script> 
    <link rel="stylesheet" type="text/css" href="../../../templates/blue/css/reset.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../../../templates/blue/css/grid.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../../../templates/blue/css/layout.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="../../../templates/blue/css/nav.css" media="screen" />
    <style>
    	*{font-family:Trebuchet MS,"Arial", sans-serif;color:black; font-size:11px;}
    </style> 
<title>Search Material</title>
<table width="100%" cellpadding="2" cellspacing="2" border="0" style="border: 1px solid #DEDEDE;font-size: 10pt;" class="head_sr">
	<tr height="30">
		<td width="15%">CARI :</td>
		<td width="40%">
			<select name="pilihcari" id="pilihcari">
				<option value="empnik">NIK</option>
				<option value="empname">Nama</option>
				<option value="emp_hrp1000_s_short">Abbreviasi</option>
				<option value="empkostl">Kode CC</option>
			   <option value="emp_cskt_ltext">Cost Center</option>
			</select>&nbsp;
			<input type="hidden" name="no" id="no" size="40" value="<?=$_REQUEST['no']?>" />	
			<input type="text" name="txtval" id="txtval" size="40" value="<?=$_REQUEST['fv']?>" />	
		</td>	
        <td width="38%" ><input type="button" name="button" id="cari" value="Cari" class='btn btn-blue' >&nbsp; 
        </td>
	</tr>
</table>
<div id="view">
	<table width="100%" border="0">
		<tr class="head_tr">
			<th align="center" width="3%">NO</th>
			<th align="center" width="10%">NIK</th>
			<th align="center" width="20%">NAMA</th>
			<th align="center" width="10%">ABBREVIASI</th>
			<th align="center" width="10%">KODE CC</th>
			<th align="center" width="27%">COST CENTER</th>
			<th align="center" width="20%">JABATAN</th>
		</tr>
    	<?	
    		// $rsql = "SELECT hic.structdisp.empkostl, hic.structdisp.emp_cskt_ltext FROM hic.structdisp group by hic.structdisp.empkostl limit 0,20";
		 	$rsql = "SELECT
						".$dbname2.".structdisp.`no`,
						".$dbname2.".structdisp.empnik,
						".$dbname2.".structdisp.empname,
						".$dbname2.".structdisp.empjobstext,
						".$dbname2.".structdisp.emppostx,
						".$dbname2.".structdisp.empkostl,
						".$dbname2.".structdisp.emp_hrp1000_s_short,
						".$dbname2.".structdisp.emp_cskt_ltext,
						".$dbname2.".structdisp.empposid
					FROM
						".$dbname2.".structdisp
					WHERE
						".$dbname2.".structdisp.`no` = '1'
					ORDER BY
						".$dbname2.".structdisp.emp_hrp1000_s_short ASC limit 0,15";
    		// echo $rsql;
			// exit;
    		$rs = mysql_query($rsql);	
    		$i=1;
    		while($lev=mysql_fetch_array($rs)){
    		    $i%2 == 0 ? $cl='#caefff' : $cl='#e4f3f9';
				$nama = str_replace("'", "`" , $lev['empname']);
    			echo'
    			<tr bgcolor="'.$cl.'" onClick="ada(\''.$lev['empnik'].'\' , \''.$nama.'\' , \''.$lev['empkostl'].'\' , \''.$lev['emp_cskt_ltext'].'\' , \''.$lev['emp_hrp1000_s_short'].'\', \''.$lev['empposid'].'\')">
    				<td align="center">'.$i.'</td>
    				<td align="left">'.$lev['empnik'].'</td>
    				<td align="left">'.$lev['empname'].'</td>
    				<td align="left">'.$lev['emp_hrp1000_s_short'].'</td>
    				<td align="left">'.$lev['empkostl'].'</td>
    				<td align="left">'.$lev['emp_cskt_ltext'].'</td> 
    				<td align="left">'.$lev['emppostx'].'</td> 
    			</tr>';
                $i++;
    		}
    		@mysql_free_result($rs);	
    	?>
	</table>
</div>
<div id="naf">
	<?
		$rsql 	= "SELECT
						".$dbname2.".structdisp.`no`,
						".$dbname2.".structdisp.empnik,
						".$dbname2.".structdisp.empname,
						".$dbname2.".structdisp.empjobstext,
						".$dbname2.".structdisp.emppostx,
						".$dbname2.".structdisp.empkostl,
						".$dbname2.".structdisp.emp_hrp1000_s_short,
						".$dbname2.".structdisp.emp_cskt_ltext,
						".$dbname2.".structdisp.empposid
					FROM
						".$dbname2.".structdisp
					WHERE
						".$dbname2.".structdisp.`no` = '1'
					ORDER BY
						".$dbname2.".structdisp.emp_hrp1000_s_short ASC";
		$hd		= "";
		$hal	= 0;
		$order 	= "";
		$pager 	= new ajaxpagination($conn, $rsql, $hd, $hal, 15, 15, $order);
		$rs = $pager->paginate();	
		include_once('../../../includes/nav.php');
	?>
</div>
<script type="text/javascript" >
    $(document).ready( function() {
    	$('#naf a').click(function() {
    		var a = $("#txtval").val();
    		var no = $("#no").val();
    		var pilih = $("#pilihcari").val();
    		$("#view").load("../../../modules/admin/ajax/popfindcc_detil.php", {'hal': $(this).attr("title"), 'mod' : $("#txtmod").val(), 'nm': a, 'no': no, 'pilih': pilih });
    		$('#naf').fadeOut('slow').load("../../../modules/admin/ajax/popfindcc_page.php", {'hal':$(this).attr("title"), 'nm': a, 'no': no, 'pilih': pilih }).fadeIn("slow");
    		// alert('a');
    	}); 
    	$('#cari').click(function() {
    		var a = $("#txtval").val();
    		var no = $("#no").val();
    		var pilih = $("#pilihcari").val();
    		if (a=='' || pilih==''){
    			alert("Masukkan nilai yang akan dicari!");
    		}else{
    			// alert("tes");
    			$("#view").load("../../../modules/admin/ajax/popfindcc_detil.php", {'pilih': pilih, 'nm': a, 'no': no, 'pilih': pilih  });
    			$('#naf').fadeOut('slow').load("../../../modules/admin/ajax/popfindcc_page.php", {'hal':$(this).attr("title"), 'nm':a, 'no': no, 'pilih': pilih }).fadeIn("slow");
    		}
    		e.stopImmediatePropagation();
    		// alert('b');
    	}); 
    }); 
	no = $("#no").val();
    function ada(empnik_asal, empname_asal, empkostl_asal, emp_cskt_ltext_asal, empshort_asal, empposid){ 
    	parent.$("#empnik"+no).val(empnik_asal);
    	parent.$("#empname"+no).val(empname_asal);
    	parent.$("#empkostl"+no).val(empkostl_asal);
    	parent.$("#emp_cskt_ltext"+no).val(emp_cskt_ltext_asal);
    	parent.$("#empshort"+no).val(empshort_asal);
    	parent.$("#empposid"+no).val(empposid);
    	parent.$.colorbox.close(); 
    }
</script>