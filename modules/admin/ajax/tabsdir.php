<?

    session_start();
    include_once('../../../config.php');
    include_once('../../../includes/functions.php');
    include_once('../../../includes/koneksi.php');
    include_once('../../../includes/ajaxpagination.php');
?>
<link rel="stylesheet" type="text/css" href="../../../templates/blue/css/reset.css" media="screen" />
<link rel="stylesheet" type="text/css" href="../../../templates/blue/css/grid.css" media="screen" />
<link rel="stylesheet" type="text/css" href="../../../templates/blue/css/layout.css" media="screen" />
<link rel="stylesheet" type="text/css" href="../../../templates/blue/css/nav.css" media="screen" /> 

<!----------------Jeasyui CSS------------------------------------------------->
<link rel="stylesheet" type="text/css" href="../../../templates/blue/css/metro/easyui.css">
<link rel="stylesheet" type="text/css" href="../../../templates/blue/css/icon.css">
<!-- =================== CSS For JQuery UI ======================================== --> 

<!-- =================== End Of CSS For JQuery UI ======================================== -->
<!-- BEGIN: load jquery -->
<script type="text/javascript" src="../../../javascript/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="../../../javascript/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="../../../javascript/jquery.ui.widget.min.js" ></script>
<script type="text/javascript" src="../../../javascript/jquery.ui.accordion.min.js" ></script>
<script type="text/javascript" src="../../../javascript/easyui/jquery.min.js"></script>
<script type="text/javascript" src="../../../javascript/easyui/jquery.easyui.min.js"></script>

<title>Search</title>
<div class="easyui-tabs" style="width:100%;height:400px;"> 
	<div title="STRUKTUR DIREKSI" style="padding:10px"> 
		<? include 'popfindnikdisdir.php'; ?>      
	</div> 
	<div title="STRUKTUR KARYAWAN" style="padding:10px"  > 
		<? include 'popfindnikdiskar.php'; ?>        
	</div>
	<div title="STRUKTUR KARYAWAN PELIMPAHAN" style="padding:10px"  > 
		<? include 'popfindplt.php'; ?>        
	</div>
</div> 
