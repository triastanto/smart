<?php
	include_once('../../config.php');
	include_once('../../includes/functions.php');
	include_once('../../includes/koneksi.php');
	include_once('../../includes/ajaxpagination.php');
	
	$hal 	= $_REQUEST['hal'];
	//echo 'Halaman : '.$hal.'<br>';
	$rsql 	= "select * from sys_menu  ";
	$hd		= "";
	$order 	= " Order by parentnode";
	$pager 	= new ajaxpagination($conn, $rsql, $hd, $hal, 20, 20, $order);
	$rs = $pager->paginate();	
	include_once('../../includes/nav.php');
?>
<script type="text/javascript" >
$(document).ready( function() {
	$('#naf a').click(function() {
		//alert($(this).attr("title")+"aaaa");
		//$("#view").load("modules/admin/detil_modul.php", {'hal': $(this).attr("title")});
		$("#view").load("modules/admin/modul_detil.php", {'hal': $(this).attr("title"), 'mod' : $("#txtmod").val(), 'op': $("#txtop").val() });
		$('#naf').fadeOut('slow').load("modules/admin/modul_page.php", {'hal':$(this).attr("title")}).fadeIn("slow");
	});
});
</script>