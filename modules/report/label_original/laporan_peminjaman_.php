<?php
	include_once("../../../config.php");
	$conn = mysql_connect($hostname,$username,$password);
	@mysql_select_db($dbname,$conn);
	include_once('../../../includes/functions.php');
	include_once('../../../includes/koneksi.php');

    // Include the main TCPDF library (search for installation path).
    require_once('../../../includes/tcpdf/tcpdf_include.php');
    require_once('../../../includes/tcpdf/config/lang/eng.php');
    require_once('../../../includes/tcpdf/tcpdf.php');
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    // set document information
    $pdf->SetCreator(PDF_CREATOR); 
    $pdf->SetTitle('Laporan Peminjaman Arsip'); 
    $pdf->SetKeywords('TCPDF, PDF, Report, Arsip');
    // set margins 
    //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
    // set some language-dependent strings (optional)  
    if (@file_exists(dirname(__FILE__).'/lang/eng.php'))
    {
    	require_once(dirname(__FILE__).'/lang/eng.php');
    	$pdf->setLanguageArray($l);
    }
    // set font
    $pdf->SetFont('tahoma', '', 9);  
     
 //------------------------------------------------------------
    $pdf->AddPage('P', 'A4');
	//tahun
    $tahun=date('Y');  
    //Array Hari
    $array_hari = array(1=>"Senin","Selasa","Rabu","Kamis","Jumat", "Sabtu","Minggu");
    $hari = $array_hari[date("N")];
    //Format Tanggal
    $tanggal = date ("j");
    //Array Bulan
    $array_bulan = array(1=>"Januari","Februari","Maret", "April", "Mei", "Juni","Juli","Agustus","September","Oktober", "November","Desember");
    $bulan = $array_bulan[date("n")]; 
    $kodepeminjaman=$_REQUEST['kodepeminjaman'];
    //query tabel transaksi peminjaman
    $isi=mysql_fetch_array(mysql_query("SELECT * FROM t_peminjaman WHERE kodepeminjaman='".$kodepeminjaman."'"));
    //query tabel master satuan
    $satuan=mysql_fetch_array(mysql_query("SELECT * FROM m_satuan WHERE kodesatuan='".$isi['kodesatuan']."'"));
     
    $tbl2 ='<style>
                .isi{
                    font-weight:bold;
                    font-size:0.8em; 
					text-align:center;
                }
                .ket{
                    font-style: italic;
                }
                .tes{
                    font-style:italic;
                } 
            </style>
            <table border="0" style="width:100%;">
                <tr>
                    <td align="center" style="width:20%;"><br/><img src="../../../images/logo2.png" style="width:120px">
                     </td>
                    <td align="center" style="width:75%;"><h3>LAPORAN PEMINJAMAN ARSIP<br/>2014</h3></td>
                 </tr><hr height="3px;"/>
				 
                 <tr>
                    <td><b>cost centre : '.$isi['costl'].'</b></td> 
                    <td><b>'.$isi['emppostx'].'</b></td>
                 </tr>
				 <tr>
					<td>&nbsp;</td>
				 </tr>
                 <tr>
                    <td colspan="2" align="left">
                        <table>
                            <tr>
                                <td style="width:15%;">nama</td>
                                <td style="width:5%;">:</td>
                                <td colspan="2">'.$isi['empname'].'</td>
                                <td>jabatan</td>
                                <td style="width:5%;">:</td>
                                <td style="width:auto;">'.$isi['jabatan'].'</td>
                            </tr>
                            <tr>
                                <td>nik</td>
                                <td style="width:5%;">:</td>
                                <td colspan="2">'.$isi['empnik'].'</td>
                                <td>telp (ext)</td>
                                <td style="width:5%;">:</td>
                                <td style="width:auto;">'.$isi['telepon'].'</td>
                            </tr>
                        </table>
                    </td>
                 </tr>
				 <tr>
					<td>&nbsp;</td>
				 </tr>
                 <tr>
                    <td colspan="2">
                        <table border="1" style="width:117%;">
                            <tr class="isi">
                                <td align="center"  style="width:15%;">Alasan Peminjaman</td>
                                <td align="center"  style="width:10%;">Jenis Arsip</td>
                                <td style="width:20%;">Uraian Masalah</td>
                                <td align="center" style="width:6%;">Tahun</td>
                                <td align="center"  style="width:6%;">Jumlah</td>
                                <td align="center"  style="width:10%;">Satuan</td>
                                <td align="center"  style="width:12%;">Tgl Peminjaman</td>
                                <td  align="center" style="width:12%;">Vld Tgl Kembali</td> 
                            </tr>'; 
                            $qr_detail=mysql_query("SELECT * FROM t_d_peminjaman WHERE kodepeminjaman='".$kodepeminjaman."'");
                            while($dtl=mysql_fetch_array($qr_detail)){
                               $satuan=mysql_fetch_array(mysql_query("SELECT * FROM m_satuan WHERE kodesatuan='".$dtl['kodesatuan']."'"));
                               $satuan=$satuan['satuan'];
                               $tbl2=$tbl2.'          
                                <tr>
                                    <td>'.$isi['alasanpeminjaman'].'</td>
                                    <td>'.$dtl['jenisarsip'].'</td>
                                    <td>'.$dtl['uraianmasalah'].'</td>
                                    <td align="center">'.date("Y",strtotime($isi['tanggalpinjam'])).'</td>
                                    <td align="center">'.$dtl['jumlah'].'</td>
                                    <td align="center">'.$satuan.'</td> 
                                    <td align="center">'.date("d-m-Y",strtotime($isi['tanggalpinjam'])).'</td>
                                    <td align="center">'.date("d-m-Y",strtotime($isi['tanggalkembali'])).'</td> 
                                </tr>';  
                            }
                     $tbl2=$tbl2.'
                        </table><br/>
                    </td>
                 </tr>
				 <tr> 
					<td align="center" colspan="3">
						<table border="0">
							<tr>
								<td>Mengetahui</td>
								<td>&nbsp;</td>
								<td>Cilegon, <b>'.$tanggal.' '.$bulan.' '.$tahun.'</b></td>
							</tr>
							<tr> 
								<td colspan="2">&nbsp;</td>
								<td>Dilaporkan Oleh</td>
							</tr>
							<tr rowspan="3">
								<td></td>
							</tr>
							<tr rowspan="3">
								<td></td>
							</tr>
							<tr rowspan="3">
								<td></td>
							</tr>
							<tr>
								<td><u><b>IRA HURAIRAH</b></u></td>
								<td>&nbsp;</td>
								<td><u><b>YUNI ARDIANTI</b></u></td>
							</tr>
							<tr>
								<td>Spesialist Document In-Active</td>
								<td>&nbsp;</td>
								<td>Officer Penilaian Document Active</td>
							</tr>
						</table>
					</td>
				 </tr> 
            </table>';           
        $pdf->writeHTML($tbl2, true, false, true, false, '');
        // reset pointer to the last page
        $pdf->lastPage();
        //Close and output PDF document
        $pdf->Output('lembar_peminjaman_arsip.pdf', 'I');
//=================================================================+
// END OF FILE
//=================================================================+