<?php
	include_once("../../../config.php");
	$conn = mysql_connect($hostname,$username,$password);
	@mysql_select_db($dbname,$conn);
    include_once('../../../includes/functions.php');
    // Include the main TCPDF library (search for installation path).
    require_once('../../../includes/tcpdf/tcpdf_include.php');
    require_once('../../../includes/tcpdf/config/lang/eng.php');
    require_once('../../../includes/tcpdf/tcpdf.php');
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    // set document information
    $pdf->SetCreator(PDF_CREATOR); 
    $pdf->SetTitle('Daftar Pertelaan Arsip Usul Musnah'); 
    $pdf->SetKeywords('TCPDF, PDF, Report, Arsip');
    // set margins 
    //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
    // set some language-dependent strings (optional)  
    if (@file_exists(dirname(__FILE__).'/lang/eng.php'))
    {
    	require_once(dirname(__FILE__).'/lang/eng.php');
    	$pdf->setLanguageArray($l);
    }
    // set font
    $pdf->SetFont('tahoma', '', 9);  
    $cc=$_REQUEST['cc'];
    $rsql = "SELECT * FROM t_m_pertelaan WHERE empkostl='$cc'";
    $isi = mysql_fetch_array(mysql_query($rsql));
	 
 //------------------------------------------------------------
    $pdf->AddPage('P', 'A4');
    $tbl2 ='<style> 
            	.isi
            	{
            		font-weight:bold;
            		font-size:32px; 
            		padding: 0em 2em 1em 0em;	
            	}  
            	.isinya
            	{  
            		line-height:2em; 
            	}
                th
                {
                    background-color:#ececec;   
                }  
                .border-head 
                {
                    border-bottom: #fffff #fffff rgb(250,0,255);
                }
            </style> 
    			<table border="1" style="align:center; text-align:left; width:100%;" >
    			   <tr>
    					<td colspan="4">
    				        <table border="0" text-align:center; class="isinya">
                                <tr>
                                    <td rowspan="2" align="center">
                                  		&nbsp;<br/>
                						<img src="../../../images/logo2.png" style="width:100px">
                                    </td>
                                    <td align="center"><br/><br/>
                                        <b>DAFTAR PERTELAAN ARSIP</b> 
                                    </td> 
                                </tr>
                                <tr> 
                                    <td align="center">
                                        <b>USUL MUSNAH</b>   
                                    </td>
                                </tr>
                            </table>
    					</td>
    					<td colspan="5">
                            <table border="0" style="text-align:left; align:left;" class="isinya">
                                <tr>
                                    <td style="width:28%;">
                                        Cost Centre
                                    </td>
                                    <td style="width:5%;">:</td>
                                    <td align="left" style="width:65%;">'.$cc.'</td> 
                                </tr>
                                <tr>
                                    <td>
                                        Unit Pengolah
                                    </td>
                                    <td style="width:5%;">:</td>
                                    <td align="left">'.$isi['emp_cskt_ltext'].'</td> 
                                </tr> 
                                <tr>
                                    <td>
                                        Jenis Arsip
                                    </td>
                                    <td style="width:5%;">:</td>
                                    <td align="left">'.$isi['jenisarsip'].'</td> 
                                </tr>
                            </table>
    					</td>
    			   </tr>
                   <tr>
                        <th style="width:13%;">Nomor Box</th> 
                        <th style="width:13%;">Kode Box</th> 
                        <th style="width:14%;">No Dokumen</th>
                        <th style="width:23.5%;">Isi Ringkas</th>
                        <th style="width:8%;">Tahun</th>
                        <th style="width:10%;">Umur Arsip</th> 
                        <th style="width:7%;">Jumlah</th>
                        <th style="width:7%;">Satuan</th>
                        <th style="width:4.5%;" align="center">Asli</th>
                   </tr>';
                       $d_pertelaan = "SELECT * FROM t_d_pertelaan WHERE kodepertelaan='".$isi['kodepertelaan']."'";
                       $qd_pertelaan=mysql_query($d_pertelaan);
                        while($isi_d = mysql_fetch_array($qd_pertelaan)){ 
                            $satuan=mysql_query("SELECT * FROM m_satuan WHERE kodesatuan='".$isi_d['kodesatuan']."'");
                            $stn=mysql_fetch_array($satuan);
                             $tbl2.='<tr>
                                        <td>'.$isi['nobox'].'</td>
                                        <td>'.$isi['kodebox'].'</td>
                                        <td>'.$isi_d['nodokumen'].'</td> 
                                        <td>'.$isi['isiringkasan'].'</td>
                                        <td align="center">'.$isi_d['aktif'].'</td>
                                        <td align="center">'.$isi_d['inaktif'].'</td> 
                                        <td align="center">'.$isi_d['jumlah'].'</td>  
                                        <td>'.$stn['satuan'].'</td>
                                        <td align="center">';
                                            if ($isi_d['asli']==''){
                                                $tbl2 .='';
                                            }else{
                                                $tbl2 .='
                                                    <img src="../../../images/ceklis.png" border="0" title="Asli">';
                                            }
                                        $tbl2 .='
                                        </td>
                                     </tr>';
                       } @mysql_free_result($qd_pertelaan); 
                       $jml_dokumen=mysql_fetch_array(mysql_query("select sum(jumlah) as jumlah from t_d_pertelaan where kodepertelaan='".$isi['kodepertelaan']."'"));	 
                       $jml_dokumen=$jml_dokumen['jumlah']; 
                   $tbl2 .='
				   <tr>
                        <td> </td>
                        <td> </td> 
                        <td> </td> 
                        <td> </td> 
                        <td> </td> 
                        <td> </td> 
                        <td> </td> 
                        <td> </td>  
                        <td align="center"><img src="../../../images/ceklis.png" border="0" title="Asli"></td>
                   </tr>
				   <tr>
						<td colspan="6">Jumlah Dokumen</td>
						<td align="center">'.$jml_dokumen.'</td>
						<td></td>
						<td></td>
				   </tr>
               </table><br/>
			   <table>
					<tr>
						<td align="center">Unit Pengolah</td>
						<td align="center">Unit Pengelola</td>
					</tr>
					<tr>
						<td align="center"><b>&nbsp;</b></td>
						<td align="center"><b>Dinas Document Management</b></td>
					</tr>
			   </table><br/><br/><br/>
			   <table> 
					<tr>
						<td align="center"><b>--------------------------------</b></td>
						<td align="center"><b>Ahmad Andi Suyudi</b></td>
					</tr>
					<tr>
						<td align="center">Manager</td>
						<td align="center">Superintendent</td>
					</tr>
			   </table>
			   ';           
        $pdf->writeHTML($tbl2, true, false, true, false, '');
        // reset pointer to the last page
        $pdf->lastPage();
        //Close and output PDF document
        $pdf->Output('Report_Pertelaan_Arsip.pdf', 'I');
//=================================================================+
// END OF FILE
//=================================================================+