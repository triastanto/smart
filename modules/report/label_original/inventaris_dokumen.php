<?php
	include_once("../../../config.php");
	$conn = mysql_connect($hostname,$username,$password);
	@mysql_select_db($dbname,$conn);
    include_once('../../../includes/functions.php');
    // Include the main TCPDF library (search for installation path).
    require_once('../../../includes/tcpdf/tcpdf_include.php');
    require_once('../../../includes/tcpdf/config/lang/eng.php');
    require_once('../../../includes/tcpdf/tcpdf.php');
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    // set document information
    $pdf->SetCreator(PDF_CREATOR); 
    $pdf->SetTitle('Daftar Inventaris Pusat Dokumen'); 
    $pdf->SetKeywords('TCPDF, PDF, Report, Arsip');
    // set margins 
    //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
    // set some language-dependent strings (optional)  
    if (@file_exists(dirname(__FILE__).'/lang/eng.php'))
    {
    	require_once(dirname(__FILE__).'/lang/eng.php');
    	$pdf->setLanguageArray($l);
    }
    // set font
    $pdf->SetFont('tahoma', '', 9);  
     
 //------------------------------------------------------------
    $pdf->AddPage('L', 'A4');
		//tahun
        $tahun=date('Y');  
        //Array Hari
        $array_hari = array(1=>"Senin","Selasa","Rabu","Kamis","Jumat", "Sabtu","Minggu");
        $hari = $array_hari[date("N")];
        //Format Tanggal
        $tanggal = date ("j");
        //Array Bulan
        $array_bulan = array(1=>"Januari","Februari","Maret", "April", "Mei", "Juni","Juli","Agustus","September","Oktober", "November","Desember");
        $bulan = $array_bulan[date("n")]; 
    $tbl2 ='<style>
                .isi{
                    font-weight:bold;
                    font-size:0.7em; 
					text-align:center;
                }
                .ket{
                    font-style: italic;
                }
                .tes{
                    font-style:italic;
                } 
				hr{
                    height:2.5px;					
				}
				th{ 
                    font-weight:bold;
					text-align:center;
				}
				.head
				{
                    font-weight:bold;
				}
            </style>
            <table border="0" style="width:100%;">
				<tr>
					<td colspan="4"><hr></td>
				</tr>
                <tr  valign="middle">
                    <td align="center" style="width:30%;" colspan="3">
						<img src="../../../images/logo2.png" style="width:80px" align="center"><br/>
                     </td> 
                    <td align="center" style="width:70%;"><h3>DAFTAR INVENTARIS PUSAT DOKUMEN</h3></td>
                </tr>
				<tr>
					<td colspan="4"><hr></td>
				</tr>
				<tr class="head">
					<td>Kodelt :</td>
					<td></td>
					<td>Rak :</td>
					<td></td>
				</tr>
            </table><br/>
            <table border="1" style="width:100%;">
				<tr>
					<th style="width:3%;">No.</th>
					<th style="width:6%;">Shelf</th>
					<th style="width:7%;">Kode Box</th>
					<th style="width:10%;">Cost Centre</th>
					<th style="width:15%;">Jenis Arsip</th>
					<th style="width:25%;">Detil</th>
					<th style="width:12%;">Klasifikasi</th>
					<th style="width:7%;">Tahun</th>
					<th style="width:4%;">JRA</th>
					<th style="width:5%;">Jumlah</th>
					<th style="width:5%;">Satuan</th>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				
            </table>';           
        $pdf->writeHTML($tbl2, true, false, true, false, '');
        // reset pointer to the last page
        $pdf->lastPage();
        //Close and output PDF document
        $pdf->Output('lembar_peminjaman_arsip.pdf', 'I');
//=================================================================+
// END OF FILE
//=================================================================+