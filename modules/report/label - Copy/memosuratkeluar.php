<?php
	include_once("../../../config.php");
	$conn = mysql_connect($hostname,$username,$password);
	@mysql_select_db($dbname,$conn);
    include_once('../../../includes/functions.php');    
    include_once('../../../includes/phpqrcode/qrlib.php');
	$dt = mysql_fetch_array(mysql_query("SELECT * from t_suratkeluar_m where kodesuratkeluar='".base64_decode($_REQUEST['idp'])."'"));
	// $x = str_replace("-", "<br/> - ", ucwords($dt['tembusantambahan']));
	// $x = strtolower($dt['tembusantambahan']);
	$x = ($dt['tembusantambahan']);
	$z = ucwords($x);
	// $z = ucwords($x);
	if($dt['nosuratkeluardivisi']==""){
		$nosuratnya =''.$dt['kodeklasifikasi'].'/'.$dt['nosuratkeluar'].'';
	}else{
		$nosuratnya =''.$dt['kodeklasifikasi'].'/'.$dt['nosuratkeluardivisi'].'';
	}
	
	//==============================barcode=================================================//
	$tempdir = "/nfs/arsip/qrcode/";
	$isi_teks = ''.$dt['kodesuratkeluar'].'-'.$dt['tanggalsurat'].'-'.$dt['empnik_asal'].'-'.$dt['empname_asal'].'';
	$namafile = "".$dt['kodesuratkeluar'].".png";
	$quality = 'H'; //ada 4 pilihan, L (Low), M(Medium), Q(Good), H(High)
	$ukuran = 5; //batasan 1 paling kecil, 10 paling besar
	$padding = 0;
	QRCode::png($isi_teks,$tempdir.$namafile,$quality,$ukuran,$padding);
	//==============================barcode=================================================//
	
	// Include the main TCPDF library (search for installation path).
    require_once('../../../includes/tcpdf/tcpdf_include.php');
    require_once('../../../includes/tcpdf/config/lang/eng.php');
    require_once('../../../includes/tcpdf/tcpdf.php');
	
	class MYPDF extends TCPDF {
		//Page header
		public function Header() {
			
		}
		// Page footer
		public function Footer() {
			
		}
	}
	
    // create new PDF document
    // $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	$pdf = new MYPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false);
    // set document information
    $pdf->SetCreator(PDF_CREATOR); 
    $pdf->SetTitle('Memo Dinas'); 
    $pdf->SetKeywords('TCPDF, PDF, Report, Arsip');
    // set margins  
    // set auto page breaks 
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    	require_once(dirname(__FILE__).'/lang/eng.php');
    	$pdf->setLanguageArray($l);
    }
    // set font
    $pdf->SetFont('tahoma', '', 10);
	$pdf->SetMargins('20', '10', '20');
	$dt = mysql_fetch_array(mysql_query("SELECT * from t_suratkeluar_m where kodesuratkeluar='".base64_decode($_REQUEST['idp'])."'"));
	$memo = mysql_fetch_array(mysql_query("SELECT * FROM t_suratkeluar_memo WHERE kodesuratkeluar = '".$dt['kodesuratkeluar']."'"));
	if($dt['size']!=''){
		$lam = '&nbsp;&nbsp;&nbsp;&nbsp; Berkas';
	}else{
		$lam = '-';
	}
	
	$ttd = mysql_fetch_array(mysql_query("SELECT $dbname2.structdisp.* FROM $dbname2.structdisp WHERE $dbname2.structdisp.emp_hrp1000_s_short = '".$dt['empshort_asal']."'"));
	$cetak = '<table width="100%">';
			for($a=1; $a<=6; $a++){
				$cetak .= '
					<tr>
						<td align="center"><p>&nbsp;</p></td>
					</tr>
				';
			}
	$cetak .= '
				<tr>
					<td>
						<table width="100%" border="0">
							<tr>
								<td width="15%"></td>
								<td width="5%"></td>
								<td width="55%"></td>
								<td width="25%" align="right"> &nbsp;'.tanggalok($dt['tanggalsurat']).' </td>
							</tr>
							<tr>
								<td> No. </td>
								<td> : </td>
								<td> '.$nosuratnya.' </td>
								<td>  </td>
							</tr>
							<tr>
								<td> Lampiran </td>
								<td> : </td>
								<td> '.$lam.' </td>
								<td> &nbsp; </td>
							</tr>
							<tr>
								<td> Sifat </td>
								<td> : </td>
								<td> '.ss($dt['sifatsurat']).' </td>
								<td> &nbsp; </td>
							</tr>
							<tr>
								<td> Perihal </td>
								<td> : </td>
								<td> '.$dt['isiringkasan'].' </td>
								<td> &nbsp; </td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td align="center"><p>&nbsp;</p></td></tr>
				<tr>
					<td>
						Kepada Yth.<br/>
						<b>'.$dt['empname_surat'].'</b><br/>
						<b>'.$dt['emp_cskt_ltext_surat'].'</b><br/>
						<b>'.$dt['alamat_surat'].'</b><br/>
						<br/>
						<br/>
						Dengan hormat,
						<br/>
						'.($memo['memo']).'
					</td>
				</tr>
				<tr>
					<td><p>&nbsp;</p></td>
				</tr>
				<tr>
					<td>
						<table width="100%">
							<tr>
								<td width="50%">';
									if($dt['validasiparaf']==1){
										$cetak .='<img src="/nfs/arsip/qrcode/'.$dt['kodesuratkeluar'].'.png" width="80" height="80" />';
									}
						$cetak .='</td>
								<td width="50%" align="center">
									<b>
										PT. KRAKATAU STEEL (PERSERO) Tbk 
									';
											if($dt['validasiparaf']==1){
												$cetak .='<p> &nbsp; <br/> </p>';
											}else{
												$cetak .='<br/><p>&nbsp;</p>';
											}
										// '.$ttd['emp_t503t_ptext'].'
									$cetak .='
										<br/>
										<u>'.ubah_huruf_awal(". ",$dt['empname_asal']).'</u><br/>
									</b>	
										'.$ttd['emppostx'].'
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><p>&nbsp;</p></td>
				</tr>
				<tr>
					<td>Tembusan Yth. :</td>
				</tr>
				<tr>
					<td>
						<table width="100%">
								<tr>
									<td width="80%">';
										$sqldp = mysql_query("select * from t_surat_deployment where kodesuratutama = '".base64_decode($_REQUEST['idp'])."'");
										// echo"select * from t_surat_deployment where kodesuratutama = '".base64_decode($_REQUEST['idp'])."'";
										$no=1;
										while($dtdp=mysql_fetch_array($sqldp)){
											$sf = mysql_fetch_array(mysql_query("select * from t_suratmasuk_m where kodesuratmasuk='".$dtdp['kodesuratbawah']."'"));
											// echo $sf['empniktujuan'];
											if($sf['empniktujuan']>=990010 and $sf['empniktujuan']<=990018){
												$kpd = mysql_fetch_array(mysql_query("SELECT $dbname2.structdireksi.* FROM $dbname2.structdireksi WHERE $dbname2.structdireksi.empnik = '".$sf['empniktujuan']."'"));
												$tm = str_replace(" (PKWT)", "", ($kpd['empjobstext']));
												$tem = ucfirst($tm);
											}else{
												$kpd = mysql_fetch_array(mysql_query("SELECT $dbname2.structdisp.* FROM $dbname2.structdisp WHERE $dbname2.structdisp.empnik = '".$sf['empniktujuan']."'"));
												$tm = str_replace(" (PKWT)", "", ($kpd['emppostx']));
												$tem = ucfirst($tm);
											}
											if($sf['kodesifatsurat']==2){
												$cetak .='<br/>- '.$tem.' PT. Krakatau Steel (Persero) Tbk';
												$no++;
											}
										}
										// $cetak .= "<br/> - ", $dt['tembusantambahan'];
										// $tembus = mysql_fetch_array(mysql_query("SELECT * from t_suratkeluar_m where kodesuratkeluar='".base64_decode($_REQUEST['idp'])."'"));
										$x1 = str_replace("-", "<br/> - ", ($z));
										$cetak .=''.$x1.'';
						$cetak .='									
									</td>
									<!--td width="20%" align="right"><img src="/nfs/arsip/qrcode/'.$dt['kodesuratkeluar'].'.png" width="80" height="80" /></td-->
								</tr>
						</table>
					</td>
				</tr>
			</table>
		'; 
		
		$pdf->AddPage('P', 'A4');
		$pdf->writeHTML($cetak, true, false, true, false, '');
		$nour++;

	// $pdf->AddPage('P', 'A4');
	// $pdf->writeHTML($tbl_1, true, false, true, false, '');
	// reset pointer to the last page
	$pdf->lastPage();
	//Close and output PDF document
	$pdf->Output('surateksternal.pdf', 'I');
	//============================================================+
	// END OF FILE
	//============================================================+