<?php
	include_once("../../../config.php");
	$conn = mysql_connect($hostname,$username,$password);
	@mysql_select_db($dbname,$conn);
	include_once('../../../includes/functions.php');
	include_once('../../../includes/koneksi.php');

    // Include the main TCPDF library (search for installation path).
    require_once('../../../includes/tcpdf/tcpdf_include.php');
    require_once('../../../includes/tcpdf/config/lang/eng.php');
    require_once('../../../includes/tcpdf/tcpdf.php');
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    // set document information
    $pdf->SetCreator(PDF_CREATOR); 
    $pdf->SetTitle('Laporan Peminjaman Arsip'); 
    $pdf->SetKeywords('TCPDF, PDF, Report, Arsip');
    // set margins 
    //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
    // set some language-dependent strings (optional)  
    if (@file_exists(dirname(__FILE__).'/lang/eng.php'))
    {
    	require_once(dirname(__FILE__).'/lang/eng.php');
    	$pdf->setLanguageArray($l);
    }
    // set font
    $pdf->SetFont('tahoma', '', 9);  
     
 //------------------------------------------------------------
    $pdf->AddPage('L', 'A4');
	//tahun
    $tahun=date('Y');  
    //Array Hari
    $array_hari = array(1=>"Senin","Selasa","Rabu","Kamis","Jumat", "Sabtu","Minggu");
    $hari = $array_hari[date("N")];
    //Format Tanggal
    $tanggal = date ("j");
    //Array Bulan
    $array_bulan = array(1=>"Januari","Februari","Maret", "April", "Mei", "Juni","Juli","Agustus","September","Oktober", "November","Desember");
    $bulan = $array_bulan[date("n")]; 
    $kodepeminjaman=$_REQUEST['kodepeminjaman'];
    //query tabel transaksi peminjaman
    $isi=mysql_fetch_array(mysql_query("SELECT * FROM t_m_peminjaman WHERE kodepeminjaman='".$kodepeminjaman."'"));
    //query tabel master satuan
  //  $satuan=mysql_fetch_array(mysql_query("SELECT * FROM m_satuan WHERE kodesatuan='".$isi['kodesatuan']."'"));
     
    $tbl2 ='<style>  
                .isi{
                    font-weight:bold;
                    font-size:0.8em; 
					text-align:center;
                }
                .ket{
                    font-style: italic;
                }
                .tes{
                    font-style:italic;
                }
                .bold{
                    font-weight:bold;
                }
                .bu{
                    font-weight:bold;
                    text-decoration: underline;
                } 
                .kwo{ 
            	    line-height: 8px;	
                }
                p.cap{ 
                    text-transform: capitalize;
                }
				.kosong{
					letter-spacing:20px;
				}
            </style>
            <table border="0" style="width:100%;">
                <tr>
                    <td style="width:70%;"></td>
                    <td style="width:30%;">
                        <table border="1" >
                            <tr class="kwo"> 
                                <td > Kode Work Order '.$kodepeminjaman.' </td>
                            </tr>
                        </table> 
                    </td>
                </tr>
                
                <tr>
                    <td align="center" style="width:auto;"><br/><img src="../../../images/logo_hitam.png" style="width:140px;"><br/></td>
                </tr>
                <tr> 
                    <td align="center" style="width:auto;"><h3>FORM PENGAJUAN PEMINJAMAN ARSIP</h3><br/></td>
                </tr>  
                <tr>
                    <td> 
                        <table   border="1">
                          <tr>
                            <td><table  border="1">
                                <tr>
                                    <td> NIK</td> 
                                    <td> : '.$isi['empnik'].'</td>
                                    <td> Telp</td> 
                                    <td> : '.$isi['telepon'].'</td>
                                </tr>
                                <tr>
                                    <td> User</td> 
                                    <td> : '.$isi['empname'].'</td>
                                    <td> Tanggal Pengajuan</td> 
                                    <td> : '._convertDate($isi['tanggalpengajuan']).'</td>
                                </tr>
                                <tr>
                                    <td> Kode CC/CostCentre</td> 
                                    <td> : '.$isi['costl'].' - '.$isi['emppostx'].'</td>
                                    <td Rowspan="2"> Alasan Peminjaman</td> 
                                    <td Rowspan="2"> : '.$isi['alasanpeminjaman'].'</td>
                                </tr>
                                <tr>
                                    <td> Jabatan</td> 
                                    <td> : '.$isi['jabatan'].'<br/></td> 
                                </tr>
                            </table></td>
                          </tr> ';
                          if ($isi['nikpemilik']==''){
                            $tbl2 .='<tr>
                                        <td><b><u> Dokumen yang dipinjam:</u></b><br/> 
                                             <table border="1" style="width:100%; align:center;"> 
                                                <tr class="bold">
                                                   <td style="width:3%;" align="center">No</td> 
                                                   <td style="width:15%;" align="center">Kode Pertelaan</td>
                                                   <td style="width:18%;" align="center">Jenis Arsip</td>
                                                   <td style="width:25%;" align="center">Nomor Dokumen</td>
                                                   <td style="width:28%;" align="center">Uraian Masalah</td>
                                                   <td style="width:8%;" align="center">Tanggal</td>
                                                </tr>';
                                                $ii=0; 
                                                $detail=mysql_query('SELECT * FROM t_d_peminjaman where kodepeminjaman="'.$isi['kodepeminjaman'].'"');
                                                   while($dett=mysql_fetch_array($detail)){ 
                                                   $ii++;
                                                   $tbl2 .='<tr>
                                                               <td align="center">'.$ii.'</td> 
                                                               <td> '.$dett['kodepertelaan'].'</td>
                                                               <td> '.$dett['jenisarsip'].'</td>
                                                               <td> '.$dett['nodokumen'].'</td>
                                                               <td> '.$dett['uraianmasalah'].'</td>
                                                               <td align="center">'._convertDate($dett['tgldokumen']).'</td> 
                                                            </tr>';
                                                   }
                                                   $tbl2 .='
                                             </table><br/>
                                        </td>
                                      </tr> ';
                          }else{
                            $tbl2 .='<tr>
                                        <td ><b><u> Dokumen yang dipinjam:</u></b>  
                                        </td>
                                    </tr>
                                    <tr> 
                                        <td>'.$isi['dokumenpinjam'].'</td>
                                    </tr>';
                          }
                          $tbl2 .=' 
                          <tr>
                            <td><br/><table border="1"> 
                                   <tr>
                                      <td colspan="2" align="center"> Unit Peminjam Arsip</td>
                                      <td colspan="2" align="center"> Unit Pemilik Arsip</td>
                                      <td colspan="2" align="center"> Unit Pengolah Arsip</td>
                                   </tr>
                                   
                                   <tr> 
                                      <td>
                                          <table>
                                            <tr>
                                                <td> Tanggal :</td>
                                            </tr>
                                            <tr>   
                                                <td align="center">User</td>
                                            </tr>
                                            <tr>
                                                <td align="center"><br/><br/><br/><br/>'.$isi['empname'].'</td>
                                            </tr>
                                            <tr> 
                                                <td align="center">('.$isi['empnik'].')</td> 
                                            </tr>
                                          </table>
                                      </td> 
                                      <td>
                                          <table>
                                            <tr>
                                                <td> Tanggal :</td>
                                            </tr>
                                            <tr>   
                                                <td align="center">'.$isi['emppostx'].'</td>
                                            </tr>
                                            <tr>
                                                <td align="center"><br/><br/><br/><br/>                                                </td>
                                            </tr>
                                            <tr> 
                                                <td align="center"> </td> 
                                            </tr>
                                          </table>
                                      </td>
                                      <td>
                                          <table>
                                            <tr>
                                                <td> Tanggal :</td>
                                            </tr>
                                            <tr>   
                                                <td align="center">User</td>
                                            </tr>
                                            <tr>
                                                <td align="center"><br/><br/><br/><br/><span class="kosong"></span></td>
                                            </tr>
                                            <tr> 
                                                <td align="center"></td> 
                                            </tr>
                                          </table>
                                      </td> 
                                      <td>
                                          <table>
                                            <tr>
                                                <td> Tanggal :</td>
                                            </tr>
                                            <tr>   
                                                <td align="center">'.$isi['emppostxpemilik'].'</td>
                                            </tr>
                                            <tr>
                                                <td align="center"><br/><br/><br/><br/>                              </td>
                                            </tr>
                                            <tr> 
                                                <td align="center"></td> 
                                            </tr>
                                          </table>
                                      </td>
                                      <td>
                                          <table>
                                            <tr>
                                                <td> Tanggal :</td>
                                            </tr>
                                            <tr>   
                                                <td align="center">Dinas Document Mgt<br/><br/><br/></td>
                                            </tr>
                                            <tr>
                                                <td align="center">                                          </td>
                                            </tr>
                                            <tr> 
                                                <td align="center">Superintendent</td> 
                                            </tr>
                                          </table>
                                      </td>
                                      <td>
                                          <table>
                                            <tr>
                                                <td> Tanggal :</td>
                                            </tr>
                                            <tr>   
                                                <td align="center">Divisi Corcom<br/><br/><br/></td>
                                            </tr>
                                            <tr>
                                                <td align="center">                                           </td>
                                            </tr>
                                            <tr> 
                                                <td align="center">Manager</td> 
                                            </tr>
                                          </table>
                                      </td>    
                                   </tr>      
                                </table>
                            </td>
                          </tr>
                        </table>
                    </td><br/>
                </tr> 
                 <tr><td>&nbsp;</td></tr>
                <tr>
                    <td>*) Unit Pemilik Arsip wajib ditandatangani apabila arsip yang dipinjam berasal dari arsip milik Divisi yang berbeda</td>
                </tr>
            </table> 
            ';           
        $pdf->writeHTML($tbl2, true, false, true, false, '');
        // reset pointer to the last page
        $pdf->lastPage();
        //Close and output PDF document
        $pdf->Output('pengajuan_peminjaman.pdf', 'I');
//=================================================================+
// END OF FILE
//=================================================================+