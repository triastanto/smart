<?php
	include_once("../../../config.php");
	$conn = mysql_connect($hostname,$username,$password);
	@mysql_select_db($dbname,$conn);
    include_once('../../../includes/functions.php');
    // Include the main TCPDF library (search for installation path).
    require_once('../../../includes/tcpdf/tcpdf_include.php');
    require_once('../../../includes/tcpdf/config/lang/eng.php');
    require_once('../../../includes/tcpdf/tcpdf.php');
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    // set document information
    $pdf->SetCreator(PDF_CREATOR); 
    $pdf->SetTitle('Report Label Arsip'); 
    $pdf->SetKeywords('TCPDF, PDF, Report, Arsip');
    // set margins 
    //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
    // set some language-dependent strings (optional)  
    if (@file_exists(dirname(__FILE__).'/lang/eng.php'))
    {
    	require_once(dirname(__FILE__).'/lang/eng.php');
    	$pdf->setLanguageArray($l);
    }
    // set font
    $pdf->SetFont('tahoma', '', 9);  

	
    $rsql = "SELECT * FROM t_m_pertelaan $where order by kodepertelaan";
    $isi = mysql_fetch_array(mysql_query($rsql)); 
	//----------------------------------------------------------------------------------------------------------------//
    $pdf->AddPage('P', 'A4');
    $tbl2 ='
			<table border="1" style="align:center; text-align:left; width:100%;">
				<tr>
					<td>
						<table border="0" text-align:center; class="isinya">
							<tr>
								<td rowspan="2" align="center">
									&nbsp;<br/>
									<img src="../../../images/logo2.png" style="width:100px"> 
								</td>
								<td align="center"><br/><br/>
									<b>DAFTAR PERTELAAN</b> 
								</td> 
							</tr>
							<tr> 
								<td align="center">
									<b>ARSIP '.$inaktif.'</b>   
								</td>
							</tr>
						</table>
					</td>
					<td>
						<br/><br/>
						<table border="0" style="text-align:left; align:left;" class="isinya">
							<tr>
								<td style="width:28%;">
									Cost Centre
								</td>
								<td style="width:5%;">:</td>
								<td align="left" style="width:65%;">'.$isi['empkostl'].'</td> 
							</tr>
							<tr>
								<td>Unit Pengolah</td>
								<td style="width:5%;">:</td>
								<td align="left">'.$isi['emp_cskt_ltext'].'</td> 
							</tr> 
						</table>
					</td>
				</tr>
			</table>
				<table border="1" width="100%">
					<tr>
						<th style="width:5%;"><b>No</b></th> 
						<th style="width:45%;"><b>Kode Pertelaan</b></th> 
						<th style="width:10%;"><b>No.Box</b></th>  
						<th style="width:40%;"><b>Jenis Arsip</b></th>
					</tr>'; 
						$d_pertelaan = "SELECT * FROM t_m_pertelaan $where order by kodepertelaan";
						$qd_pertelaan=mysql_query($d_pertelaan);
						$kd=0;                       
						while($isi_d = mysql_fetch_array($qd_pertelaan)){ 
							$kd++;                            
							$tbl2.='<tr>
										<td rowspan="2" align="center"><b>'.$kd.'</b></td> 
										<td> '.$isi_d['kodepertelaan'].'</td> 
										<td> '.$isi_d['nobox'].'</td> 
										<td> '.$isi_d['jenisarsip'].'</td> 
									</tr>
									<tr>
										<td colspan="3">
											<table>
												<tr class="isinya2">
													 <td style="width:5%;" align="center">No</td>
													 <td style="width:11%;" >No.Dokumen</td>
													 <td style="width:36%;" >Uraian Masalah</td>
													 <td style="width:7%;" >Jumlah</td>
													 <td style="width:8%;" >Satuan</td>
													 <td style="width:11%;" >Tgl Dokumen</td>
													 <td style="width:8%;" >Aktif s/d</td>
													 <td style="width:10%;" >In-Aktif s/d</td>
													 <td style="width:4%;">Asli</td>
												</tr> '; 
												$det2=mysql_query("SELECT * FROM t_d_pertelaan WHERE kodepertelaan='".$isi_d['kodepertelaan']."' ORDER BY kodepertelaan");
												$a=1;
												while($det_isi=mysql_fetch_array($det2)){
													$satuan=mysql_fetch_array(mysql_query("SELECT * FROM m_satuan WHERE kodesatuan='".$det_isi['kodesatuan']."'"));
													$satuan=$satuan['satuan'];
													$tbl2.='<tr class="tesborder">
																<td align="center">'.$a.'</td>
																<td>'.$det_isi['nodokumen'].'</td>
																<td>'.$det_isi['uraianmasalah'].'</td>
																<td align="center">'.$det_isi['jumlah'].'</td>
																<td>'.$satuan.'</td>
																<td align="center">'.date('d-m-Y',strtotime($det_isi['tanggaldokumen'])).'</td>
																<td align="center">'.date('Y',strtotime($det_isi['aktifsampaidengan'])).'</td>
																<td align="center">'.date('Y',strtotime($det_isi['inaktifsampaidengan'])).'</td>                
																<td align="center"><img src="../../../images/ceklis2.png" border="0" title="Asli"></td>
														   </tr>';
														$a++;
												   }  
							$tbl2.='
										</table>
										</td>
									</tr>
							';
						}
						@mysql_free_result($qd_pertelaan);
			$tbl2.='
				</table>';
        $pdf->writeHTML($tbl2, true, false, true, false, '');
       
//=================================================================+
// END OF FILE
//=================================================================+
 // reset pointer to the last page
$pdf->lastPage();
//Close and output PDF document
$pdf->Output('Report_pertelaan_arsip.pdf', 'I');
?>