<?php
	include_once("../../../config.php");
	$conn = mysql_connect($hostname,$username,$password);
	@mysql_select_db($dbname,$conn);
    include_once('../../../includes/functions.php');
    // Include the main TCPDF library (search for installation path).
    require_once('../../../includes/tcpdf/tcpdf_include.php');
    require_once('../../../includes/tcpdf/config/lang/eng.php');
    require_once('../../../includes/tcpdf/tcpdf.php');
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    // set document information
    $pdf->SetCreator(PDF_CREATOR); 
    $pdf->SetTitle('Report Label Arsip'); 
    $pdf->SetKeywords('TCPDF, PDF, Report, Arsip');
    
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    	require_once(dirname(__FILE__).'/lang/eng.php');
    	$pdf->setLanguageArray($l);
    }
    //------SET FONT---------------------------------------------------------/
    $pdf->SetFont('dejavusans', '', 9);
	$date=date(Y);
    $nomor='0899887987878';
    $urut='0'; 
//------------------------------------------------------------
    $pdf->AddPage('P', 'A4');
    $tbl = '
    <style>
    	.foot_label
    	{
    		font-weight:bold;
    		font-size:40px;
    		line-height:4em;
    	}
    	.isi
    	{
    		font-weight:bold;
    		font-size:32px; 
    		padding: 0em 2em 1em 0em;	
    	}
    	.isitahun
    	{
    		line-height:6em; 
    		font-weight:bold;
    		font-size:32px; 
    		padding: 0em 2em 1em 0em;
    	} 
    	.isinya
    	{  
    	   line-height:2em; 
    	}
    	.isino
    	{
    	   line-height:5em; 
  		  
    	}
    	.reportnya
    	{
    	   align:center;
    	}		
        .border-head 
        {
           border-bottom: #fffff #fffff rgb(250,0,255);
        }
    </style>
    <table border="0" style="width:250%; align:center;" align="center" class="reportnya"> 
    	<tr>
    		<td>'; 
               	  $rsql = mysql_query("SELECT * FROM t_m_pertelaan");
                  while($isi =mysql_fetch_array($rsql))
                  {
                    $urut++;
                    $rsqlh = "SELECT * FROM hic.structdisp WHERE empnik='".$isi['regno']."'";
                    $isih = mysql_fetch_array(mysql_query($rsqlh));
                    $tbl=$tbl.'
                               <table border="1" style="width:80%;">
                                 <tr >
                                     <td align="center">&nbsp;<br/>
                                        <img src="../../../images/logo2.png" style="width:100px">
                                     </td>
                                     <td style="width:5%;">&nbsp;</td>
                                     <td align="center">&nbsp;<br/>
                                        <img src="../../../images/logo2.png" style="width:100px">
                                     </td>
                                  </tr>';
                         $tbl=$tbl.'
                                  <tr class="isinya">
                                      <td align="center">
                                          <span class="isi">'.$isih['emportx'].'</span><br/>
                                               '.$isih['emportx'].'<br/>
                                          <span class="isi">'.$isi['emp_cskt_ltext'].'</span><br/>
                                          <span class="isi">'.$nomor.'</span><br class="isino"/>
                                          <span class="isi">TAHUN</span><br/>
                                          <span class="isitahun">'.$isi['daritahun'].'</span><br/>
                                      </td>
                                      <td>&nbsp;</td>
                                      <td align="center">
                                          <span class="isi">'.$isih['emportx'].'</span><br/>
                                               '.$isih['emportx'].'<br/>
                                          <span class="isi">'.$isi['emp_cskt_ltext'].'</span><br/>
                                          <span class="isi">'.$nomor.'</span><br class="isino"/>
                                          <span class="isi">TAHUN</span><br/>
                                          <span class="isitahun">'.$isi['daritahun'].'</span><br/>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td class="foot_label" align="center">'.$urut.'</td>
                                      <td>&nbsp;</td> 
                                      <td class="foot_label" align="center">'.$urut.'</td>
                                  </tr>
                              </table><br/>';
                   }
     $tbl=$tbl.'
    		  </td>
    		<td style="width:5%;">&nbsp;</td>
    	</tr>
    </table>
    <br/>';  
$pdf->writeHTML($tbl, true, false, true, false, '');
// reset pointer to the last page

//Close and output PDF document
$pdf->Output('Report_Label.pdf', 'I');
//============================================================+
// END OF FILE
//============================================================+