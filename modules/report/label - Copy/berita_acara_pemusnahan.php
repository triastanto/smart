<?php
	include_once("../../../config.php");
	$conn = mysql_connect($hostname,$username,$password);
	@mysql_select_db($dbname,$conn);
    include_once('../../../includes/functions.php');
    // Include the main TCPDF library (search for installation path).
    require_once('../../../includes/tcpdf/tcpdf_include.php');
    require_once('../../../includes/tcpdf/config/lang/eng.php');
    require_once('../../../includes/tcpdf/tcpdf.php');
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    // set document information
    $pdf->SetCreator(PDF_CREATOR); 
    $pdf->SetTitle('Report Label Arsip'); 
    $pdf->SetKeywords('TCPDF, PDF, Report, Arsip');
    // set margins  
    // set auto page breaks 
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    	require_once(dirname(__FILE__).'/lang/eng.php');
    	$pdf->setLanguageArray($l);
    }
    // set font
    $pdf->SetFont('dejavusans', '', 9);
//------------------------------------------------------------
$pdf->AddPage('P', 'A4');
        //tahun
        $tahun=date('Y');  
        //Array Hari
        $array_hari = array(1=>"Senin","Selasa","Rabu","Kamis","Jumat", "Sabtu","Minggu");
        $hari = $array_hari[date("N")];
        //Format Tanggal
        $tanggal = date ("j");
        //Array Bulan
        $array_bulan = array(1=>"Januari","Februari","Maret", "April", "Mei", "Juni","Juli","Agustus","September","Oktober", "November","Desember");
        $bulan = $array_bulan[date("n")]; 
        
$tbl2 = '
<style>
	.nodoc
	{  
		line-height:2em; 
	}  
    th
    {
        font-weight:bold;
        text-align:center;
    }
    .ket
    {
        text-align: justify; 
    }
    table
    {
		line-height:1.5em; 
            
    } 
</style>  

<table border="0" class="nodoc" style="width:100%;">
  <tr>
     <td style="width:69%;"></td>
     <td style="width:31%;">
        <table border="1" class="nodoc" style="width:100%;">
           <tr>
               <td>  No. Doc.: MS ............................ </td>
           </tr>
        </table>
     </td>
  </tr>
</table>
<table style="width: 100%;" border="0">
 <tr>
    <td style="width: 7.5%;">&nbsp;</td>
    <td style="width: 85%;"> 
        <table border="0" style="width:100%; align:right;">
            <tr>
                <td align="center">&nbsp;<br/><img src="../../../images/logo2.png" style="width:150px align:center"></td>
            </tr><br/> 
            <tr>
                <td align="center"><h3>BERITA ACARA</h3><h3><u>PEMUSNAHAN ARSIP PERUSAHAAN</u></h3>
                </td>
            </tr>
             <tr>
                <td align="center"><b>Nomor : ..............................................................</b></td>
            </tr>
             <tr>
                <td align="center">&nbsp;</td>
            </tr><br/>
             <tr>
                <td class="ket">Pada hari ini,  <b>'.$hari.'</b> tanggal <b>'.$tanggal.'</b> bulan <b>'.$bulan.'</b> tahun <b>'.$tahun.'</b>, 
                yang bertanda tangan dibawah ini menyatakan menyetujui pemusnahan arsip sesuai dengan daftar arsip di bawah ini :</td>
            </tr><br/>
            <tr>
                <td align="center">
                    <table border="1" style="width:100%;">
                        <tr>
                            <th style="width:6%;">No. </th>
                            <th style="width:30%;">Uraian</th>
                            <th>Masa Inaktif</th>
                            <th>s/d Periode</th>
                            <th style="width:10%;">Jumlah</th>
                            <th>Keterangan</th>
                        </tr> 
                        <tr>
                            <td colspan="6"></td>
                        </tr>
                    </table>
                </td>
            </tr><br/>  
            <tr>
                <td class="ket">Adapun hal ini didasarkan Surat Keputusan Direksi PT. Krakatau Steel Cilegon
                    Nomor : 100/C/DU-KS/KPTS/1993, dengan pertimbangan : 
                </td>
            </tr>
            <tr>
                <td style="width:3%;">&nbsp;</td>
                <td style="width:4%;">1.&nbsp;</td>
                <td class="ket">Telah melampaui jangka simpan sebagaimana diatur dalam Jadwal Retensi Arsip;</td>
            </tr>
            <tr>
                <td style="width:3%;">&nbsp;</td>
                <td style="width:4%;">2.&nbsp;</td>
                <td class="ket">Telah dilakukan penilaian dan dinyatakan bahwa arsip dimaksud sudah tidak bernilai guna lagi, tidak berkaitan
                dengan proses hukum dan tidak bertentangan dengan peraturan perundang-undangan. 
                </td>
            </tr>
            <tr>
                <td style="width:3%;">&nbsp;</td>
                <td style="width:4%;">3.&nbsp;</td>
                <td class="ket"><b>Telah mendapat persetujuan dari Divisi .............................</b>
                </td>
            </tr>
                <br/><br/><br/>
            <tr>
                <td style="width:45%;" align="center">CORPORATE SECRETARY</td>
                <td style="width:18%;">&nbsp;</td>
                <td style="width:40%;" align="left">Cilegon, &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$tanggal.' '.$bulan.' '.$tahun.' <br/>DIVISI ..............................................</td> 
            </tr>
                <br/><br/>
                <br/><br/><br/> 
            <tr>
                <td style="width:45%;" align="center"><b><u>IIP ARIEF BUDIMAN</u></b></td>
                <td style="width:18%;">&nbsp;</td>
                <td style="width:45%;" align="center"><b><u>............................</u></b></td>
            </tr>
            <tr>
                <td style="width:45%;" align="center">General Manager</td>
                <td style="width:18%;">&nbsp;</td>
                <td style="width:45%;" align="center"> ............................ </td>
            </tr><br/>
            <tr>
                <td style="width:90%;">
                    Berita Acara ini rangkap 2 ( dua ) masing-masing :
                </td>
            </tr>
            <tr>
                <td style="width:5%;">&nbsp;</td>
                <td colspan="2" style="width:90%;">1. Lembar kesatu, Untuk Corporate Secretary Cq. Dinas Document Mgt.</td>
            </tr>
            <tr>
                <td style="width:5%;">&nbsp;</td>
                <td colspan="2">2. Lembar kedua, Untuk Divisi ....................................................</td>
            </tr>
        </table>  
    </td>
    <td style="width:7.5%;">&nbsp;</td>
 </tr>
</table> '; 
$pdf->writeHTML($tbl2, true, false, true, false, '');
// reset pointer to the last page
$pdf->lastPage();
//Close and output PDF document
$pdf->Output('Report_Label.pdf', 'I');
//============================================================+
// END OF FILE
//============================================================+