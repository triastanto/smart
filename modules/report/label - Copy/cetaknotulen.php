<?php
	include_once("../../../config.php");
	$conn = mysql_connect($hostname,$username,$password);
	@mysql_select_db($dbname,$conn);
    include_once('../../../includes/functions.php');
	
    require_once('../../../includes/tcpdf/tcpdf_include.php');
    require_once('../../../includes/tcpdf/config/lang/eng.php');
    require_once('../../../includes/tcpdf/tcpdf.php');
	
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $pdf->SetCreator(PDF_CREATOR); 
    $pdf->SetTitle('Report Label Arsip'); 
    $pdf->SetKeywords('TCPDF, PDF, Report, Arsip');
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    	require_once(dirname(__FILE__).'/lang/eng.php');
    	$pdf->setLanguageArray($l);
    }
    // set font
    $pdf->SetFont('dejavusans', '', 10);
	$pdf->AddPage('P', 'A4');
	$idp = base64_decode($_REQUEST['idp']);
	$dt = mysql_fetch_array(mysql_query("select * from t_notulen_m where kodenotulen='".$idp."'"));
	$t = explode("-",$dt['tanggalrapat']);
	$tbl2 = '
        <style>
            .notulen{
                line-height: 100%;
            }
        </style>
		<table border="1" cellpadding="0" cellspacing="0" width="100%"> 
			<tr>
				<td width="30%" align="center">
					<img src="logo.png" width="90" height="60" />
				</td>
				<td width="40%" align="center"><p class="notulen"><h2>NOTULEN RAPAT</h2></p></td>
				<td width="30%" align="center">
					<table border="1" cellpadding="0" cellspacing="0" width="100%"> 
						<tr><td colspan="3" align="center"> Pertemuan Ke </td></tr>
						<tr><td colspan="3" align="center"> Tanggal Rapat </td></tr>
						<tr align="center">
							<td width="30%"><b>Tgl</b></td>
							<td width="40%"><b>Bln</b></td>
							<td width="30%"><b>Thn</b></td>
						</tr>
						<tr align="center">
							<td> '.$t[2].' </td>
							<td> '.$t[1].' </td>
							<td> '.$t[0].' </td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<table border="1" cellpadding="0" cellspacing="0" width="100%"> 
			<tr>
				<td>
					<table border="0" cellpadding="0" cellspacing="0" width="100%"> 
						<tr><td width="20%"> Nama Rapat</td><td width="2%"> : </td><td width="78%"> '.$dt['namarapat'].'</td></tr>
						<tr><td> Tempat Rapat</td><td> : </td><td> '.$dt['tempatrapat'].'</td></tr>
						<tr><td> Pimpinan Rapat</td><td> : </td><td> '.$dt['empnamepemimpin'].'</td></tr>
						<tr><td> Notulis</td><td> : </td><td> '.$dt['notulis'].'</td></tr>
					</table>
				</td>
			</tr>
		</table>
		
		<table border="1" cellpadding="0" cellspacing="0" width="100%"> 
			<tr>
				<td width="80%">
					<table border="1" cellpadding="0" cellspacing="0" width="100%" > 
						<tr align="center">
							<td width="5%"> <b>No</b> </td>
							<td width="75%"> <b>Nama Karyawan</b> </td>
							<td width="10%"> <b>X</b> </td>
							<td width="10%"> <b>Kos</b> </td>
						</tr>';
						$sql = mysql_query("select * from t_notulen_deploy where kodenotulen='".$dt['kodenotulen']."'");
						$no=1;
						while($dep = mysql_fetch_array($sql)){
							$tbl2 .='
								<tr>
									<td align="center"> '.$no.'</td>
									<td> '.$dep['empnamedisposisi'].'</td>
									<td></td>
									<td></td>
								</tr>
							';
							$no++;
						}
	$tbl2 .= '
					</table>
				</td>
				<td width="20%">
					<table border="1" cellpadding="0" cellspacing="0" width="100%"> 
						<tr><td> Keterangan : </td></tr>
						<tr><td> V : Hadir </td></tr>
						<tr><td> X : Tidak Hadir </td></tr>
					</table>
				</td>
			</tr>
		</table>
		<table border="1" cellpadding="0" cellspacing="0" width="100%"> 
			<tr>
				<td colspan="2">&nbsp;Catatan: BUDAYA KEGIATAN MENJELANG RAPAT KOORDINASI 
				</td>
			</tr>
			<tr>
				<td width="20%" align="center"> <b>VISI</b> </td>
				<td width="80%" align="justify"> Perusahaan Baja terpadu dengan keunggulan kompetitif untuk tumbuh dan<br/>
					&nbsp;berkembang secara berkesinambungan menjadi perusahaan terkemuka didunia.
				</td>
			</tr>
			<tr>
				<td align="center"> <b>MISI</b> </td>
				<td> Menyediakan produk baja bermutu dan jasa terkait bagi kemakmuran bangsa </td>
			</tr>
			<tr>
				<td align="center"> <b>NILAI BUDAYA PERUSAHAAN</b> </td>
				<td> Competence, Integrity, Reliable, Innovative. </td>
			</tr>
		</table>
		<table border="1" cellpadding="0" cellspacing="0" width="100%"> 
			<tr align="center" valign="center">
				<td width="5%"><b>NO </b></td>
				<td width="60%"><b>POKOK BAHASAN</b></td>
				<td width="10%"><b>TARGET</b></td>
				<td width="15%"><b>PROGRES</b></td>
				<td width="10%"><b>PIC</b></td>
			</tr>
	';
			$sqlbah = mysql_query("select * from t_notulen_pembahasan where kodenotulen='".$dt['kodenotulen']."' order by id ASC");
			$nom=1;
			while($bah = mysql_fetch_array($sqlbah)){
				$tbl2 .='
					<tr bgcolor="#8ed2ff"><td colspan="5"> <b>'.$bah['pokokbahasan'].'</b> </td></tr>
					<tr>
						<td align="center"> '.$nom.' </td>
						<td> '.$bah['isibahasan'].' </td>
						<td> '.$bah['target'].' </td>
						<td> '.$bah['progres'].' </td>
						<td> '.$bah['pic'].' </td>
					</tr>
				';
				$nom++;
			}
	$tbl2 .= '
		</table>
	';
	/*
	<table border="1" cellpadding="0" cellspacing="0" width="100%"> 
			<tr align="center">
				<td width="70%">&nbsp;</td>
				<td width="30%" align="center">
					<p>&nbsp;</p>
					Cilegon, '.date('Y-m-d').'
					<p>&nbsp;</p>
					<p>&nbsp;</p>
					'.$dt['empnamepemimpin'].'
				</td>
			</tr>
		</table>
	*/
	$pdf->writeHTML($tbl2, true, false, true, false, '');
	$pdf->lastPage();
	$pdf->Output('suratmasuk.pdf', 'I');
?>