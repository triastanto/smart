<?php
	include_once("../../../config.php");
	$conn = mysql_connect($hostname,$username,$password);
	@mysql_select_db($dbname,$conn);
    include_once('../../../includes/functions.php');
    // Include the main TCPDF library (search for installation path).
    require_once('../../../includes/tcpdf/tcpdf_include.php');
    require_once('../../../includes/tcpdf/config/lang/eng.php');
    require_once('../../../includes/tcpdf/tcpdf.php');
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    // set document information
    $pdf->SetCreator(PDF_CREATOR); 
    $pdf->SetTitle('Dokumen Sudah diverifikasi Musnah'); 
    $pdf->SetKeywords('TCPDF, PDF, Report, Arsip');
    // set margins 
    //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
    // set some language-dependent strings (optional)  
    if (@file_exists(dirname(__FILE__).'/lang/eng.php'))
    {
    	require_once(dirname(__FILE__).'/lang/eng.php');
    	$pdf->setLanguageArray($l);
    }
    // set font
    $pdf->SetFont('tahoma', '', 9);  
     
 //------------------------------------------------------------
    $pdf->AddPage('P', 'A4');
		//tahun
        $tahun=date('Y'); 
        $cc=$_REQUEST['cc']; 
        $query=mysql_query("SELECT a.*, b.*, c.satuan FROM t_m_pertelaan AS a
                            LEFT JOIN t_d_pertelaan AS b ON a.kodepertelaan=b.kodepertelaan
                            LEFT JOIN m_satuan AS c ON b.kodesatuan=c.kodesatuan
                            WHERE status='3' and a.empkostl=".$cc." ORDER BY a.kodepertelaan"); 
        //Array Hari
        $array_hari = array(1=>"Senin","Selasa","Rabu","Kamis","Jumat", "Sabtu","Minggu");
        $hari = $array_hari[date("N")];
        //Format Tanggal
        $tanggal = date ("j");
        //Array Bulan
        $array_bulan = array(1=>"Januari","Februari","Maret", "April", "Mei", "Juni","Juli","Agustus","September","Oktober", "November","Desember");
        $bulan = $array_bulan[date("n")]; 
        $tbl2 ='<style>
                .isi{
                    font-weight:bold;
                    font-size:0.7em; 
					text-align:center;
                }
                .ket{
                    font-style: italic;
                }
                .tes{
                    font-style:italic;
                } 
                </style>
            <table border="0" style="width:100%;">
                 <tr>
                    <td align="center"><br/><img src="../../../images/logo2.png" style="width:120px" align="center">
                     </td>
                 </tr>
                 <tr>
                    <td align="center"><h2>DAFTAR PERTELAAN ARSIP</h2></td>
                 </tr>
				 <tr>
					<td>&nbsp;</td>
				 </tr>
                 <tr>
                    <td><h3>Cost Centre : '.$cc.'</h3></td>
                 </tr>
				 <tr>
					<td>&nbsp;</td>
				 </tr>  
                 <tr>
                    <td>
                        <table border="1" style="width:107%;">
                            <tr class="isi">
                                <td style="width:5%;">NO</td>
                                <td width="15%">JENIS ARSIP</td>
                                <td width="25%">URAIAN</td>
                                <td width="8%">TAHUN</td> 
                                <td width="8%">S/D TAHUN</td> 
                                <td width="6%">JUMLAH</td>  
                                <td>SATUAN</td> 
                                <td>KETERANGAN</td> 
                            </tr>';
                 $no=0;
                 while($isi=mysql_fetch_array($query))
                 { 
                    $as = mysql_fetch_array(mysql_query("select * from m_keaslian where kodeasli='".$isi['asli']."'"));
                    $no++;
                    $tbl2 .=' <tr> 
                                  <td align="center">'.$no.'</td>
                                  <td>'.$isi['jenisarsip'].'</td> 
                                  <td>'.$isi['uraianmasalah'].'</td> 
                                  <td align="center">'.$isi['daritahun'].'</td> 
                                  <td align="center">'.$isi['sampaitahun'].'</td> 
                                  <td align="center">'.$isi['jumlah'].'</td> 
                                  <td>'.$isi['satuan'].'</td> 
                                  <td>'.$as['asli'].'</td> 
                             </tr>';
                 }
                    $tbl2 .='  
                        </table><br/>
                    </td>
                 </tr> 
            </table>';           
        $pdf->writeHTML($tbl2, true, false, true, false, '');
        // reset pointer to the last page
        $pdf->lastPage();
        //Close and output PDF document
        $pdf->Output('lembar_peminjaman_arsip.pdf', 'I');
//=================================================================+
// END OF FILE
//=================================================================+