<?php
	include_once("../../../config.php");
	$conn = mysql_connect($hostname,$username,$password);
	@mysql_select_db($dbname,$conn);
    include_once('../../../includes/functions.php');
    // Include the main TCPDF library (search for installation path).
    require_once('../../../includes/tcpdf/tcpdf_include.php');
    require_once('../../../includes/tcpdf/config/lang/eng.php');
    require_once('../../../includes/tcpdf/tcpdf.php');
	
	class MYPDF extends TCPDF {
		//Page header
		public function Header() {
			
		}
		// Page footer
		public function Footer() {
			
		}
	}
    // create new PDF document
    // $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	$pdf = new MYPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false);
    // set document information
    $pdf->SetCreator(PDF_CREATOR); 
    $pdf->SetTitle('Memo Dinas'); 
    $pdf->SetKeywords('TCPDF, PDF, Report, Arsip');
    // set margins  
    // set auto page breaks 
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    	require_once(dirname(__FILE__).'/lang/eng.php');
    	$pdf->setLanguageArray($l);
    }
    // set font
    $pdf->SetFont('tahoma', '', 12);


	$dt = mysql_fetch_array(mysql_query("SELECT * FROM t_suratkeluar_m WHERE kodesuratkeluar = '".base64_decode($_REQUEST['idp'])."'"));
	$jd = mysql_fetch_array(mysql_query("SELECT * FROM m_judul WHERE kodejudul = '".$dt['kodejudul']."'"));

	$sqldk = mysql_query("select * from t_surat_deployment where kodesuratutama = '".base64_decode($_REQUEST['idp'])."'");
	$memo = mysql_fetch_array(mysql_query("SELECT * FROM t_suratkeluar_memo WHERE kodesuratkeluar = '".$dt['kodesuratkeluar']."'"));
	$nour=1;
	while($dtdk = mysql_fetch_array($sqldk)){
		$jb = mysql_fetch_array(mysql_query("SELECT $dbname2.structdisp.* FROM $dbname2.structdisp WHERE $dbname2.structdisp.empnik = '".$dt['empnik_asal']."'"));
		$awal = substr($dtdk['kodesuratbawah'],0,2);
		if($awal=="SK"){
			$asalsurat = mysql_fetch_array(mysql_query("select * from t_suratkeluar_m where kodesuratkeluar='".$dtdk['kodesuratbawah']."'"));
			$kp = mysql_fetch_array(mysql_query("SELECT $dbname2.structdisp.* FROM $dbname2.structdisp WHERE $dbname2.structdisp.empnik = '".$asalsurat['empnik_asal']."'"));
		}else{
			$asalsurat = mysql_fetch_array(mysql_query("select * from t_suratmasuk_m where kodesuratmasuk='".$dtdk['kodesuratbawah']."'"));
			$kp = mysql_fetch_array(mysql_query("SELECT $dbname2.structdisp.* FROM $dbname2.structdisp WHERE $dbname2.structdisp.empnik = '".$asalsurat['empniktujuan']."'"));
		}
		// $sifatsurat = mysql_fetch_array(mysql_query("select * from t_suratkeluar_d where kodesuratkeluar='".$dtdk['kodesuratbawah']."'"));
		
		if($asalsurat['kodesifatsurat']==1){
			$cetak = $tbl."_".$nour;
			$cetak = '
				<style>
					.nodoc
					{  
						line-height:2em; 
					}
				</style>';
			$cetak .= '
				<table width="100%">
					<tr>
						<td>
							<table width="100%" border="0">
								<tr>
									<td width="65%">&nbsp;</td>
									<td width="35%" rules="none" border="1">
										&nbsp; <font face="tahoma" size="8"> Kode Surat : '.base64_decode($_REQUEST['idp']).' </font>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td align="center"><img src="../../../images/hitam.jpg" width="150" /></td>
					</tr>
					<tr>
						<td align="center"><p>&nbsp;</p></td>
					</tr>
					<tr>
						<td align="center">
							<b>
								<font face="tahoma" size="14"><u>'.$jd['judul'].'</u></font><br/>
								No. '.$dt['kodeklasifikasi'].'/'.$dt['nosuratkeluar'].'
							</b>
						</td>
					</tr>
					<tr>
						<td align="center"><p>&nbsp;</p></td>
					</tr>
					<tr>
						<td>
							<table width="100%" border="0">
								<tr>
									<td width="20%">Kepada Yth,</td>
									<td width="5%">:</td>
									<td width="75%">'.ucwords($kp['emppostx']).'</td>
								</tr>
								<tr>
									<td>Dari</td>
									<td>:</td>
									<td>'.ucwords($jb['emp_t503t_ptext']).' '.ucwords($jb['emppostx']).'</td>
								</tr>
								<tr>
									<td>Perihal</td>
									<td>:</td>
									<td>'.ucwords($dt['isiringkasan']).'</td>
								</tr>
								<tr>
									<td>Tanggal</td>
									<td>:</td>
									<td>'.tanggal($dt['tanggalsurat']).'</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td><div class="nodoc"><hr/></div></td>
					</tr>
					<tr>
						<td>
							Dengan Hormat,
							'.ucwords($memo['memo']).'
						</td>
					</tr>
					<tr>
						<td><p>&nbsp;</p></td>
					</tr>
					<tr>
						<td><p>&nbsp;</p></td>
					</tr>
					<tr>
						<td>
							<table width="100%" border="0">
								<tr>
									<td width="60%">&nbsp;</td>
									<td width="40%" align="center">
										<b>'.ucwords($jb['emppostx']).'</b>
										<p>&nbsp;</p>
										<b><u>'.ucwords($dt['empname_asal']).'</u></b><br/>
										'.ucwords($jb['emp_t503t_ptext']).'
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td><p>&nbsp;</p></td>
					</tr>
					<tr>
						<td>Tembusan :</td>
					</tr>
					<tr>
						<td>
			';
							$sqldp = mysql_query("select * from t_surat_deployment where kodesuratutama = '".base64_decode($_REQUEST['idp'])."'");						
							$no=1;
							while($dtdp=mysql_fetch_array($sqldp)){
								$sf = mysql_fetch_array(mysql_query("select * from t_suratmasuk_m where kodesuratmasuk='".$dtdp['kodesuratbawah']."'"));
								$kpd = mysql_fetch_array(mysql_query("SELECT $dbname2.structdisp.* FROM $dbname2.structdisp WHERE $dbname2.structdisp.empnik = '".$sf['empniktujuan']."'"));
								if($sf['kodesifatsurat']==2){
									$cetak .='<br/>'.$no.'. Yth. '.ucwords($kpd['emppostx']).'';
									$no++;
								}
							}
			$cetak .='
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
				
					<tr>
						<td><font face="tahoma" size="10"><u>Surat ini ditandatangani secara elektronik melalui aplikasi SMART dan dapat dianggap sebagai alat bukti yang sah.</u></font></td>
					</tr>
					<tr>
						<td>
							<font face="tahoma" size="10">
								<table width="100%" border="0">
									<tr>
										<td width="20%">CC Asal Surat</td>
										<td width="5%">:</td>
										<td width="75%">'.$dt['empkostl_asal'].'</td>
									</tr>
									<tr>
										<td>Sifat Surat</td>
										<td>:</td>
										<td>'.ss($dt['sifatsurat']).'</td>
									</tr>
									<tr>
										<td>Jenis Surat</td>
										<td>:</td>
										<td>'.js($dt['jenissurat']).'</td>
									</tr>
									<tr>
										<td>Diparaf oleh</td>
										<td>:</td>
										<td>'.$dt['empnameparaf'].'</td>
									</tr>
									<tr>
										<td>Keterangan</td>
										<td>:</td>
										<td>'.$dt['keteranganparaf'].'</td>
									</tr>
								</table>
							</font>
						</td>
					</tr>
				</table>
			'; 
			
			$pdf->AddPage('P', 'A4');
			$pdf->writeHTML($cetak, true, false, true, false, '');
			$nour++;
		}
	}

	// $pdf->AddPage('P', 'A4');
	// $pdf->writeHTML($tbl_1, true, false, true, false, '');
	// reset pointer to the last page
	$pdf->lastPage();
	//Close and output PDF document
	$pdf->Output('memodinas.pdf', 'I');
	//============================================================+
	// END OF FILE
	//============================================================+