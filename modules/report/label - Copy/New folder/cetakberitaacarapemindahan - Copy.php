<?php
    mysql_connect('localhost','root','');
    mysql_select_db('arsip');
    include_once('../../../includes/functions.php');
    // Include the main TCPDF library (search for installation path).
    require_once('../../../includes/tcpdf/tcpdf_include.php');
    require_once('../../../includes/tcpdf/config/lang/eng.php');
    require_once('../../../includes/tcpdf/tcpdf.php');
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    // set document information
    $pdf->SetCreator(PDF_CREATOR); 
    $pdf->SetTitle('Berita Acara Arsio'); 
    $pdf->SetKeywords('TCPDF, PDF, Report, Berita Acara');
    // set margins  
    // set auto page breaks 
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    	require_once(dirname(__FILE__).'/lang/eng.php');
    	$pdf->setLanguageArray($l);
    }
    // set font
    $pdf->SetFont('dejavusans', '', 9); 
    $kodewo=$_REQUEST['kodeworkorder'];
    $isinya="SELECT a.*, b.* FROM t_m_workorder as a
             LEFT JOIN t_d_workorder as b ON a.kodeworkorder=b.kodeworkorder
             WHERE a.kodeworkorder='$kodewo'
              ";
    $isi=mysql_fetch_array(mysql_query($isinya));  
    
    $hic="SELECT * FROM hic.structdisp WHERE empnik='".$isi['regnopengirim']."'";
    $hic_data=mysql_fetch_array(mysql_query($hic)); 
    
    $doccc="SELECT * FROM hic.structdisp WHERE empnik='10713'";
    $doc_centre=mysql_fetch_array(mysql_query($doccc));
    
    $isi2="SELECT * FROM t_d_workorder WHERE kodeworkorder='".$isi['kodeworkorder']."'"; 
    $no=0;
//------------------------------------------------------------
$pdf->AddPage('P', 'A4'); 
        //tahun
        $tahun=date('Y');  
        //Array Hari
        $array_hari = array(1=>"Senin","Selasa","Rabu","Kamis","Jumat", "Sabtu","Minggu");
        $hari = $array_hari[date("N")];
        //Format Tanggal
        $tanggal = date ("j");
        //Array Bulan
        $array_bulan = array(1=>"Januari","Februari","Maret", "April", "Mei", "Juni","Juli","Agustus","September","Oktober", "November","Desember");
        $bulan = $array_bulan[date("n")]; 
        
$tbl2 ='<style>
        	.nodoc
        	{  
        		line-height:2em; 
        	}  
            th
            {
                font-weight:bold;
                text-align:center;
            }
            .ket
            {
                text-align: justify; 
            } 
        </style>
    <table border="0" class="nodoc" style="width:100%;">
      <tr>
         <td style="width:67%;"></td>
         <td style="width:33%;">
            <table border="1" class="nodoc" style="width:100%;">
               <tr>
                   <td>  No. Doc.: '.$isi['kodeworkorder'].' </td>
               </tr>
            </table>
         </td>
      </tr>
    </table>
    <table style="width: 100%;" border="0">
     <tr>
        <td style="width:7.5%;">&nbsp;</td>
        <td style="width:85%;"> 
            <table border="0" style="width:100%; align:right;">
                <tr>
                    <td align="center">&nbsp;<br/><img src="../../../images/logo2.png" style="width:150px align:center"></td>
                </tr><br/> 
                <tr>
                    <td align="center"><h3><u>BERITA ACARA PEMINDAHAN ARSIP</u></h3></td>
                </tr>
                 <tr>
                    <td align="center">No. KT .00.00/ ......../ ......../<b>'.$tahun.'</b></td>
                </tr>
                 <tr>
                    <td align="center">&nbsp;</td>
                </tr>
                 <tr>
                    <td class="ket">Pada hari ini,  <b>'.$hari.'</b> tanggal <b>'.$tanggal.'</b> bulan <b>'.$bulan.'</b> tahun <b>'.$tahun.'</b>, kami yang bertanda tangan dibawah ini:</td>
                </tr><br/>
                 <tr>
                    <td align="left"> 
                        <table border="0">
                            <tr>
                                <td style="width:10%;" rowspan="3">&nbsp;</td>
                                <td align="left" style="width:13%;"> Nama</td>
                                <td align="center" style="width:2%;">:</td>
                                <td style="width:45%;">'.$isi['namapengirim'].'</td>
                            </tr>
                            <tr>
                                <td align="left"> Jabatan</td>
                                <td align="center">:</td>
                                <td>'.$hic_data['emp_t503t_ptext'].' '.$hic_data['emppostx'].'</td>
                            </tr>
                            <tr>
                                <td align="left"> No.Register</td>
                                <td align="center">:</td>
                                <td>'.$isi['regnopengirim'].'</td>
                                <td style="width:27%;">(Min. Manager/Setingkat)</td>
                            </tr>
                        </table> 
                    </td>
                </tr><br/>
                <tr>
                    <td class="ket">Dengan hal ini bertindak atas nama Unit Pengolah <b>'.$isi['emp_cskt_ltext'].'</b> yang selanjutnya disebut PIHAK KESATU (1).
                    </td>
                </tr><br/>
                <tr>
                    <td align="left"> 
                        <table border="0">
                            <tr>
                                <td style="width:10%;" rowspan="3">&nbsp;</td>
                                <td align="left" style="width:13%;"> Nama</td>
                                <td align="center" style="width:2%;">:</td>
                                <td style="width:55%;">'.$doc_centre['dirname'].'</td>
                            </tr>
                            <tr>
                                <td align="left"> Jabatan</td>
                                <td align="center">:</td>
                                <td>'.$doc_centre['emp_t503t_ptext'].' '.$doc_centre['emppostx'].'</td>
                            </tr>
                            <tr>
                                <td align="left"> No.Register</td>
                                <td align="center">:</td>
                                <td>'.$doc_centre['dirnik'].'</td> 
                            </tr>
                        </table> 
                    </td>
                </tr><br/>
                <tr>
                    <td class="ket">Dalam hal ini bertindak atas nama Corporate Secretary yang selanjutnya disebut PIHAK KEDUA (2).</td>
                </tr><br/>
                <tr>
                    <td class="ket">Menyatakan telah mengadakan serah terima arsip sebanyak <b>'.$isi['jumlahdokumen'].'</b> yang dipindahkan ke Dinas Document Management seperti tercantum dalam daftar berikut ini :
                    </td>
                </tr><br/>
                <tr>
                    <td align="center">
                        <table border="1" style="width:100%;">
                            <tr>
                                <th style="width:6%;">No. </th>
                                <th style="width:30%;">Uraian</th>
                                <th>Masa Inaktif</th>
                                <th>s/d Periode</th>
                                <th style="width:10%;">Jumlah</th>
                                <th>Keterangan</th>
                            </tr>';
							
					// echo $isi2;
                    $disi=mysql_query($isi2);
                    while($d_isi=mysql_fetch_array($disi)){
						$pt = mysql_fetch_array(mysql_query("select * from t_m_pertelaan where kodepertelaan='".$d_isi['kodepertelaan']."'"));
						$det = mysql_fetch_array(mysql_query("select * from t_d_pertelaan where kodepertelaan='".$pt['kodepertelaan']."'"));
						$jm = mysql_num_rows(mysql_query("select * from t_d_pertelaan where kodepertelaan='".$pt['kodepertelaan']."'"));
                        $no++; 
                        $tbl2=$tbl2 .'
                            <tr>
                                <td>'.$no.'</td>
                                <td>'.$pt['isiringkasan'].'</td> 
                                <td>'.$det['aktif'].'</td>  
                                <td>'.$det['inaktif'].'</td>  
                                <td>'.$jm.'</td>  
                                <td>'.$d_isi['asli'].'</td>  
                            </tr>
                        ';  
                    }
   	                    @mysql_free_result($rs);           
        $tbl2= $tbl2.'</table>
                    </td>
                </tr><br/>
                <tr>
                    <td style="width:45%;">Diterima tanggal : <b>'.$tanggal.'-'.$bulan.'-'.$tahun.'</b></td>
                    <td style="width:10%;">&nbsp;</td>
                    <td style="width:45%;" align="right"> Cilegon, '.$tanggal.'-'.$bulan.'-'.$tahun.'</td>  
                </tr> 
                <tr>
                    <td align="center">PIHAK KEDUA</td>
                    <td></td>
                    <td align="center">PIHAK KESATU</td>
                </tr>
                <tr>
                    <td align="center">CORPORATE SECRETARY</td>
                    <td></td>
                    <td align="center">DIVISI '.$isi['emp_cskt_ltext'].'</td>
                </tr><br/><br/><br/><br/><br/>
                <tr>
                    <td align="center"><b><u>IIP A.BUDIMAN</u></b></td>
                    <td></td>
                    <td align="center"><b><u>'.$isi['namapengirim'].'</u></b></td>
                </tr>
                <tr>
                    <td align="center">'.$doc_centre['emp_t503t_ptext'].'</td>
                    <td></td>
                    <td align="center">'.$hic_data['emp_t503t_ptext'].'</td>
                </tr><br/><br/>
                <tr>
                    <td colspan="2">
                        Berita Acara ini rangkap 2 ( dua ) masing-masing :
                    </td>
                </tr>
                <tr>
                    <td style="width:5%;">&nbsp;</td>
                    <td colspan="2"  style="width:90%;">1. Lembar kesatu, Untuk Corporate Secretary Cq. Dinas Document Mgt.</td>
                </tr>
                <tr>
                    <td style="width:5%;">&nbsp;</td>
                    <td colspan="2">2. Lembar kedua, Untuk Divisi <b>'.$isi['emp_cskt_ltext'].'</b></td>
                </tr>
            </table>  
        </td>
        <td style="width:7.5%;">&nbsp;</td>
     </tr>
    </table>';     
$pdf->writeHTML($tbl2, true, false, true, false, '');
// reset pointer to the last page
$pdf->lastPage();
//Close and output PDF document
$pdf->Output('Berita_Acara_'.$tahun.'.pdf', 'I');
//============================================================+
// END OF FILE
//============================================================+
