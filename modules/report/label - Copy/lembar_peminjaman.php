<?php
	include_once("../../../config.php");
	$conn = mysql_connect($hostname,$username,$password);
	@mysql_select_db($dbname,$conn);
	include_once('../../../includes/functions.php');
	include_once('../../../includes/koneksi.php');

    // Include the main TCPDF library (search for installation path).
    require_once('../../../includes/tcpdf/tcpdf_include.php');
    require_once('../../../includes/tcpdf/config/lang/eng.php');
    require_once('../../../includes/tcpdf/tcpdf.php');
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    // set document information
    $pdf->SetCreator(PDF_CREATOR); 
    $pdf->SetTitle('Laporan Peminjaman Arsip'); 
    $pdf->SetKeywords('TCPDF, PDF, Report, Arsip');
    // set margins 
    //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
    // set some language-dependent strings (optional)  
    if (@file_exists(dirname(__FILE__).'/lang/eng.php'))
    {
    	require_once(dirname(__FILE__).'/lang/eng.php');
    	$pdf->setLanguageArray($l);
    }
 //------------------------------------------------------------
    $kodepeminjaman=$_REQUEST['kodepeminjaman'];
    //query tabel transaksi peminjaman
    $isi=mysql_fetch_array(mysql_query("SELECT * FROM t_m_peminjaman WHERE kodepeminjaman='".$kodepeminjaman."'")); 
    $tbl2 ='<style>
                .isi{
                    font-weight:bold;
                    font-size:0.8em; 
					text-align:center;
                }
                .ket{
                    font-style: italic;
                }
                .tes{
                    font-style:italic;
                }
                .bold{
                    font-weight:bold;
                }
                .bu{
                    font-weight:bold;
                    text-decoration: underline;
                } 
                .valid{
                    font-weight:bold;
                    text-decoration: underline; 
                } 
                p.italic {
                    font-style: italic;
                }
            </style>
            <table border="0" style="width:100%;">
                <tr>
                    <td colspan="3"  style="width:67%;"></td>
                    <td style="width:34%;">
                        <table border="1">
                            <tr>
                                <td>&nbsp;Kode Work Order : '.$kodepeminjaman.'&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="width:auto;" colspan="4"><img src="../../../images/logo_hitam.png" style="width:130px;"></td>
                </tr>
                <tr> 
                    <td align="center" style="width:auto;" colspan="4"><h3><u>LEMBAR PEMINJAMAN ARSIP</u></h3></td>
                </tr><br/>
                <tr> 
                    <td align="center" style="width:auto;"  colspan="4">HARUS DIKEMBALIKAN DALAM KEADAAN UTUH/LENGKAP KEPADA 
                        PETUGAS PENYIMPANAN ARSIP TEPAT PADA WAKTUNYA 
                    </td>
                </tr><br/>
                <tr>
                    <td  colspan="4">
                        <table border="1"  style="width:100%;" >
                            <tr class="isi">
                                <th style="width:5%;">No.</th>
                                <th style="width:20%;">Kode Pertelaan</th>
                                <th style="width:16%;">Jenis Arsip</th>
                                <th style="width:15%;">Nomor Dokumen</th>
                                <th style="width:30%;">Uraian Masalah</th>		
                                <th style="width:11%;">Tgl Dokumen</th> 
                            </tr>';
                            
                            $det=mysql_query("SELECT kodepertelaan, jenisarsip, nodokumen, uraianmasalah, tgldokumen FROM t_d_peminjaman  
                                              WHERE kodepeminjaman='".$isi['kodepeminjaman']."'");
                            $hit=mysql_num_rows($det);
                            $no=0;
                            while($isdet=mysql_fetch_array($det)){
                                $no++;
                                $tbl2.='<tr>
                                            <td>'.$no.'</td> 
                                            <td>'.$isdet['kodepertelaan'].'</td>
                                            <td>'.$isdet['jenisarsip'].'</td>
                                            <td>'.$isdet['nodokumen'].'</td>
                                            <td>'.$isdet['uraianmasalah'].'</td>
                                            <td>'._convertDate($isdet['tgldokumen']).'</td>  
                                        </tr> ';
                            }
                            
                            $tbl2.=' 
                            <tr>
                                <td colspan="5"><b>Jumlah dokumen yang dipinjam</b></td> 
                                <td align="center"><b>'.$hit.'</b></td>
                            </tr>
                        </table>
                    </td>
                </tr> <br/>
                <tr>
                    <td colspan="4">
                        <table border="0">
                            <tr class="bu">
                                <td colspan="4"> DATA PEMINJAM ARSIP <br/></td>
                            </tr>
                            <tr>
                                <td style="width:20%;">Nama Penanggung Jawab</td>
                                <td style="width:1%;">:</td>
                                <td style="width:30%;">'.$isi['namaatasan'].'</td>
                                <td style="width:22%;">Petugas yang Menyerahkan</td>
                                <td style="width:1%;">:</td>
                                <td>'.$isi['namapenyerah'].'</td>  
                            </tr>
                            <tr>
                                <td>NIK</td>
                                <td>:</td><td>'.$isi['nikatasan'].'</td>
                                <td>Jabatan</td>
                                <td>:</td> 
                                <td>'.$isi['jabatanpenyerah'].'</td> 
                            </tr>
                            <tr>
                                <td>Jabatan</td>
                                <td>: </td>
                                <td>'.$isi['jabatan'].'</td>
                                <td>Telp</td>
                                <td>:</td> 
                                <td>'.$isi['telepon'].'</td> 
                            </tr>
                            <tr>
                                <td>Kode CC/Cost Centre</td>
                                <td>:</td>
                                <td>'.$isi['costl'].' / '.$isi['emppostx'].'</td> 
                                <td>Tanggal Peminjaman</td>
                                <td>:</td>
                                <td>'._convertDate($isi['tglpeminjaman']).'</td>  
                            </tr>
                            <tr>
                                <td>Petugas yg mengambil</td>
                                <td>:</td>
                                <td>'.$isi['petugaspengambil'].'</td>
                                <td>Tgl Maksimal Kembali</td>
                                <td>:</td>
                                <td>'._convertDate($isi['tglmaxkembali']).'<br/><br/><br/></td> 
                            </tr>
                            <tr>
                                <td colspan="3" align="center" style="width:50%;"><b>'.$isi['emppostx'].'</b></td>  
                                <td colspan="3" align="center" style="width:50%;"><b>Dinas Document Management</b>   
                                    <br/><br/><br/><br/> 
                                </td>
                                 
                            </tr> 
                            <tr class="bu">
                                <td colspan="3" align="center">( '.$isi['petugaspengambil'].' )</td> 
                                <td colspan="3" align="center">( '.$isi['namapenyerah'].' )</td> 
                            </tr> 
                            <tr>
                                <td colspan="3" align="center">( '.$isi['jabatanpengambil'].' )</td> 
                                <td colspan="3" align="center">( '.$isi['jabatanpenyerah'].' )<br/><br/></td> 
                            </tr> 
                            <tr class="valid">
                                <td colspan="3"> <b>Validasi Pengembalian Arsip</b> </td>  
                            </tr> 
                            <tr >
                                <td colspan="3">(Diisi oleh Dinas Document Mgt)</td>  
                            </tr>
                            <tr >
                                <td>Dikembalikan tanggal</td> 
                                <td>:</td> 
                                <td></td> 
                                <td>Lengkap/tidak</td> 
                                <td></td>  
                            </tr> 
                            <tr >
                                <td>Paraf Petugas</td> 
                                <td>:</td> 
                                <td></td> 
                                <td>Keterangan</td> 
                                <td>:<br/><br/></td>  
                            </tr>
                            <tr>
                                <td colspan="3">*) Lembar peminjaman dicetak 2 lembar:</td>
                            </tr> 
                            <tr>
                                <td colspan="3">1.	Lembar kesatu untuk Dinas Document Mgt</td> 
                            </tr>
                            <tr> 
                                <td colspan="3">2.	Lembar kedua untuk Divisi peminjam arsip </td>
                            </tr>      
                        </table> 
                    </td>
                </tr> 
            </table>'; 
  
 // set font
 $pdf->SetFont('tahoma', '', 9);  
               
 $pdf->AddPage('P', 'A4');           
 $pdf->writeHTML($tbl2, true, false, true, false, ''); 
  
 // reset pointer to the last page
 $pdf->lastPage();
 //Close and output PDF document
 $pdf->Output('pengajuan_peminjaman.pdf', 'I');
//=================================================================+
// END OF FILE
//=================================================================+