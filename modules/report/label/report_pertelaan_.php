<?php
	include_once("../../../config.php");
	$conn = mysql_connect($hostname,$username,$password);
	@mysql_select_db($dbname,$conn);
    include_once('../../../includes/functions.php');
    // Include the main TCPDF library (search for installation path).
    require_once('../../../includes/tcpdf/tcpdf_include.php');
    require_once('../../../includes/tcpdf/config/lang/eng.php');
    require_once('../../../includes/tcpdf/tcpdf.php');
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    // set document information
    $pdf->SetCreator(PDF_CREATOR); 
    $pdf->SetTitle('Report Label Arsip'); 
    $pdf->SetKeywords('TCPDF, PDF, Report, Arsip');
    // set margins 
    //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
    // set some language-dependent strings (optional)  
    if (@file_exists(dirname(__FILE__).'/lang/eng.php'))
    {
    	require_once(dirname(__FILE__).'/lang/eng.php');
    	$pdf->setLanguageArray($l);
    }
    // set font
    $pdf->SetFont('tahoma', '', 9);  
     
    if ($kode=$_POST['textval']!=''){
         $kode=$_POST['textval'];
         $inaktif='';
         $where="WHERE empkostl='$kode'";
    }else if($kode=$_POST['txtval_all']!=''){
         $kode=$_POST['txtval_all'];
         $inaktif='';
         $where="WHERE empkostl='$kode'";
    }else if($kode=$_POST['txtval']!=''){
        $kode=$_POST['txtval'];
        $inaktif='IN-AKTIF';
        $where="WHERE empkostl='$kode'";
    }else if($kode=$_POST['txtval_inaktif']!=''){
        $kode=$_POST['txtval_inaktif'];
        $inaktif='IN-AKTIF';
        $where="WHERE empkostl='$kode'";
    }else if($kode=$_POST['txtcc']!=''){
        $kode=$_POST['txtcc'];
        $inaktif='';
        $where="WHERE empkostl='$kode' and kodestatus='1' and tanggalnotifikasi > '".date('Y-m-d')."'";
    }else if($kode=$_POST['txtcc_in']!=''){
        $kode=$_POST['txtcc_in'];
        $inaktif='';
        $where="WHERE empkostl='$kode' and kodestatus='1' and tanggalnotifikasi <= '".date('Y-m-d')."'";
    }else if($kode=$_POST['txtcc_inaktif']!=''){
        $kode=$_POST['txtcc_inaktif'];
        $inaktif='IN-AKTIF';
        $where="WHERE empkostl='$kode'";
    }else{
        $kode=$_POST['txtval_jra'];
        $inaktif='';
        $where="WHERE empkostl='$kode'";
    }
    $rsql = "SELECT * FROM t_m_pertelaan $where order by kodepertelaan";
    $isi = mysql_fetch_array(mysql_query($rsql)); 
 //------------------------------------------------------------
    $pdf->AddPage('P', 'A4');
    $tbl2 ='<style> 
            	.isi
            	{
            	  font-weight:bold;
            	  font-size:32px; 
            	  padding: 0em 2em 1em 0em;	
            	} 
            	.isinya
            	{  
            	  line-height:2em; 
            	}
                th
                {
                  background-color:#c5c0c0;   
                } 
                .isinya2
                {
                  background-color:#dbdbdb;
                  font-weight:bold;
                  font-size:27px;   
                }  
                .border-head 
                {
                  border-bottom: #fffff #fffff rgb(250,0,255);
                } 
            </style> 
    			<table border="1" style="align:center; text-align:left; width:100%;" >
    			   <tr>
    					<td colspan="4">
    				        <table border="0" text-align:center; class="isinya">
                                <tr>
                                    <td rowspan="2" align="center">
                                  		&nbsp;<br/>
                						<img src="../../../images/logo2.png" style="width:100px"> 
                                    </td>
                                    <td align="center"><br/><br/>
                                        <b>DAFTAR PERTELAAN</b> 
                                    </td> 
                                </tr>
                                <tr> 
                                    <td align="center">
                                        <b>ARSIP '.$inaktif.'</b>   
                                    </td>
                                </tr>
                            </table>
    					</td>
    					<td colspan="5"><br/><br/>
                            <table border="0" style="text-align:left; align:left;" class="isinya">
                                <tr>
                                    <td style="width:28%;">
                                        Cost Centre
                                    </td>
                                    <td style="width:5%;">:</td>
                                    <td align="left" style="width:65%;">'.$isi['empkostl'].'</td> 
                                </tr>
                                <tr>
                                    <td>Unit Pengolah</td>
                                    <td style="width:5%;">:</td>
                                    <td align="left">'.$isi['emp_cskt_ltext'].'</td> 
                                </tr> 
                            </table>
    					</td>
    			   </tr>
                   <tr>
                      <th style="width:60%;" colspan="6"><b>Kode Pertelaan</b></th> 
                      <th style="width:10%;" colspan="3"><b>No.Box</b></th>  
                      <th style="width:30%;" colspan="3"><b>Jenis Arsip</b></th>  
                   </tr>'; 
                       $d_pertelaan = "SELECT * FROM t_m_pertelaan $where order by kodepertelaan";
                       $qd_pertelaan=mysql_query($d_pertelaan);
                        while($isi_d = mysql_fetch_array($qd_pertelaan)){ 
                              $tbl2.='<tr>
                                         <td style="width:60%;"  colspan="6">'.$isi_d['kodepertelaan'].'</td> 
                                         <td style="width:40%;"  colspan="1">'.$isi_d['nobox'].'</td> 
                                         <td style="width:40%;"  colspan="2">'.$isi_d['jenisarsip'].'</td> 
                                     </tr>
                                     <tr class="isinya2">
                                         <td style="width:5%;">No</td>
                                         <td style="width:11%;" >No.Dokumen</td>
                                         <td style="width:36%;" >Uraian Masalah</td>
                                         <td style="width:7%;" >Jumlah</td>
                                         <td style="width:8%;" >Satuan</td>
                                         <td style="width:11%;" >Tgl Dokumen</td>
                                         <td style="width:8%;" >Aktif s/d</td>
                                         <td style="width:10%;" >In-Aktif s/d</td>
                                         <td style="width:4%;">Asli</td>
                                     </tr>';
                                     $det2=mysql_query("SELECT * FROM t_d_pertelaan WHERE kodepertelaan='".$isi_d['kodepertelaan']."' ORDER BY kodepertelaan");
                                     while($det_isi=mysql_fetch_array($det2)){
                                        $i++;
                                        $satuan=mysql_fetch_array(mysql_query("SELECT * FROM m_satuan WHERE kodesatuan='".$det_isi['kodesatuan']."'"));
                                        $satuan=$satuan['satuan'];
                                        $tbl2.='<tr class="tesborder">
                                                    <td align="center">'.$i.'</td>
                                                    <td>'.$det_isi['nodokumen'].'</td>
                                                    <td>'.$det_isi['uraianmasalah'].'</td>
                                                    <td align="center">'.$det_isi['jumlah'].'</td>
                                                    <td>'.$satuan.'</td>
                                                    <td align="center">'.date('d-m-Y',strtotime($det_isi['tanggaldokumen'])).'</td>
                                                    <td align="center">'.date('Y',strtotime($det_isi['aktifsampaidengan'])).'</td>
                                                    <td align="center">'.date('Y',strtotime($det_isi['inaktifsampaidengan'])).'</td>                
                                                    <td align="center"><img src="../../../images/ceklis2.png" border="0" title="Asli"></td>
                                               </tr>';
                                       }  
                       } @mysql_free_result($qd_pertelaan); 	 
        $tbl2 .='</table>';           
        $pdf->writeHTML($tbl2, true, false, true, false, '');
        // reset pointer to the last page
        $pdf->lastPage();
        //Close and output PDF document
        $pdf->Output('Report_pertelaan_arsip.pdf', 'I');
//=================================================================+
// END OF FILE
//=================================================================+