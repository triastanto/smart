<?php
	include_once("../../../config.php");
	$conn = mysql_connect($hostname,$username,$password);
	@mysql_select_db($dbname,$conn);
    include_once('../../../includes/functions.php');
	
	$userid = base64_decode($_REQUEST['us']);
	$groupid = base64_decode($_REQUEST['gr']);
	
	// echo $userid.'-'.$groupid;
	// exit;
	
    require_once('../../../includes/tcpdf/tcpdf_include.php');
    require_once('../../../includes/tcpdf/config/lang/eng.php');
    require_once('../../../includes/tcpdf/tcpdf.php');
	
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $pdf->SetCreator(PDF_CREATOR); 
    $pdf->SetTitle('Report Label Arsip'); 
    $pdf->SetKeywords('TCPDF, PDF, Report, Arsip');
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    	require_once(dirname(__FILE__).'/lang/eng.php');
    	$pdf->setLanguageArray($l);
    }
    // set font
    $pdf->SetFont('dejavusans', '', 8);
	$pdf->AddPage('L', 'A4');
	
	$tbl2 = '
		<h2>LAPORAN SURAT KELUAR</h2>
		<table border="1" cellpadding="5" cellspacing="0" width="100%"> 
			<tr align="center" bgcolor="#b0e7f5">
				<td width="3%"> <b>No</b> </td>
				<td width="10%"> <b>Tanggal Surat</b> </td>
				<td width="15%"> <b>No Surat</b> </td>
				<td width="12%"> <b>Kode</b> </td>
				<td width="60%"> <b>Isi Ringkasan</b> </td>
			</tr>
	';
	$sql = mysql_query("select * from t_suratkeluar_m where asalsurat='".$userid."' order by kodesuratkeluar ASC");
	$no=1;
	while($dt=mysql_fetch_array($sql)){
		$tbl2 .='
			<tr>
				<td> '.$no.' </td>
				<td> '.$dt['tanggalsurat'].' </td>
				<td> '.$dt['nosuratkeluar'].' </td>
				<td> '.$dt['kodeklasifikasi'].' </td>
				<td> '.$dt['isiringkasan'].' </td>
			</tr>
		';
		$no++;
	}
	$tbl2 .= '
		</table>
	'; 
	$pdf->writeHTML($tbl2, true, false, true, false, '');
	$pdf->lastPage();
	$pdf->Output('suratmasuk.pdf', 'I');
?>