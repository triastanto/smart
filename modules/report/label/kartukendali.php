<?php
	include_once("../../../config.php");
	$conn = mysql_connect($hostname,$username,$password);
	@mysql_select_db($dbname,$conn);
    include_once('../../../includes/functions.php');
	 // Include the main TCPDF library (search for installation path).
    require_once('../../../includes/tcpdf/tcpdf_include.php');
    require_once('../../../includes/tcpdf/config/lang/eng.php');
    require_once('../../../includes/tcpdf/tcpdf.php');
	
	class MYPDF extends TCPDF {
		//Page header
		public function Header() {
			
		}
		// Page footer
		public function Footer() {
			
		}
	}
    // create new PDF document
    // $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	$pdf = new MYPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false);
    // set document information
    $pdf->SetCreator(PDF_CREATOR); 
    $pdf->SetTitle('Memo Dinas'); 
    $pdf->SetKeywords('TCPDF, PDF, Report, Arsip');
    // set margins  
    // set auto page breaks 
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    	require_once(dirname(__FILE__).'/lang/eng.php');
    	$pdf->setLanguageArray($l);
    }
    // set font
    $pdf->SetFont('tahoma', '', 10);
	$pdf->AddPage('P', 'A4');
	$kodesuratkeluar = base64_decode($_REQUEST['idp']);
	$dt = mysql_fetch_array(mysql_query("select * from t_suratkeluar_m where kodesuratkeluar='".$kodesuratkeluar."'"));
	// echo $dt['jenissurat']."-".$dt['empname_surat'];
	// if($dt['jenissurat']==1){
		// $dari = ' '.$dt['empname_asal'].'-'.$dt['emp_cskt_ltext_asal'].' ';
	// }else{
		// $dari = ' '.$dt['empname_surat'].'-'.$dt['emp_cskt_ltext_surat'].' ';
	// }
	$tbl2 = '
		<table border="1" cellpadding="5" cellspacing="0" width="100%"> 
			<tr><td align="center" colspan="4"> 
				<img src="../../../images/hitam.jpg" width="150" /> <br/>
				Kartu Kendali </td></tr>
			<tr>
				<td width="25%">Indeks <br/> '.$dt['kodeklasifikasi'].'</td>
				<td width="25%">Kode <br/> '.$dt['nosuratkeluardivisi'].'</td>
				<td width="50%" colspan="2">Tanggal '.$dt['tanggalsurat'].' </td>
			</tr>
			<tr><td colspan="4">Isi Ringkasan <br/> '.$dt['isiringkasan'].' </td></tr>
			<tr><td colspan="4">Lampiran <br/> Terlampir</td></tr>
			<tr>
				<td colspan="2">Dari : <br/> '.$dt['empname_asal'].' - '.$dt['emp_cskt_ltext_asal'].'</td>
				<td colspan="2">Kepada : <br/>
		';
				
				// echo"SELECT * FROM t_suratkeluar_d where kodesuratkeluar='".$kodesuratkeluar."'";
				if($dt['jenissurat']==2){
					$tbl2 .= ' 1. '.$dt['empname_surat'].'-'.$dt['emp_cskt_ltext_surat'].' ';
				}else{
					$sqldet = mysql_query("SELECT * FROM t_suratkeluar_d where kodesuratkeluar='".$kodesuratkeluar."'");
					$no=1;
					while($det = mysql_fetch_array($sqldet)){
						$tbl2 .=' &nbsp; '.$no.'. ['.$det['empniktembusan'].'] '.$det['empnametembusan'].'<br/>';
						$no++;
					}
				}
	$tbl2 .= '	</td>
			</tr>
			<tr>
				<td colspan="2" rowspan="2">Unit Pengelola : <br/> '.$dt['emp_cskt_ltext_asal'].'</td>
				<td colspan="2">No Surat : <br/> '.$dt['nosuratkeluardivisi'].'</td>
			</tr>
			<tr>
				<td colspan="2">
					Diterima Oleh :
					<br/>
				</td>
			</tr>
			<tr>
				<td colspan="4"> Catatan <br/> 
					<p>&nbsp;</p> 
					<p>&nbsp;</p>
				</td>
			</tr>
		</table>
	'; 
	$pdf->writeHTML($tbl2, true, false, true, false, '');
	// $pdf->AddPage('P', 'A4');
	// $pdf->writeHTML($tbl_1, true, false, true, false, '');
	// reset pointer to the last page
	$pdf->lastPage();
	//Close and output PDF document
	$pdf->Output('memodinas.pdf', 'I');
	//============================================================+
	// END OF FILE
	//============================================================+
?>