<?php
    mysql_connect('localhost','root','');
    mysql_select_db('arsip');
    include_once('../../../includes/functions.php');
    // Include the main TCPDF library (search for installation path).
    require_once('../../../includes/tcpdf/tcpdf_include.php');
    require_once('../../../includes/tcpdf/config/lang/eng.php');
    require_once('../../../includes/tcpdf/tcpdf.php');
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    // set document information
    $pdf->SetCreator(PDF_CREATOR); 
    $pdf->SetTitle('Report Label Arsip'); 
    $pdf->SetKeywords('TCPDF, PDF, Report, Arsip');
    // set margins  
    // set auto page breaks 
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    	require_once(dirname(__FILE__).'/lang/eng.php');
    	$pdf->setLanguageArray($l);
    }
    // set font
    $pdf->SetFont('Arial', '', 14);

	
//------------------------------------------------------------
$pdf->AddPage('P', 'A4');
$tbl2 = '
<style>
	.nodoc
	{  
		line-height:2em; 
	}  
    th
    {
        font-weight:bold;
        text-align:center;
    } 
</style> 
<body> 
<table style="width: 90%;" border="1" align="center">
 <tr>
    <td> 
        <table border="0" style="width:100%; align:right;">
            <tr> 
                <td>
                    <table border="0" class="nodoc" style="width:100%;">
                        <tr>
                            <td style="width:62%;"></td>
                            <td style="width:38%;">
                                <table border="1" class="nodoc" style="width:100%;">
                                    <tr>
                                        <td>  No. Doc.: PD ............................ </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>  
            </tr><br/>
            <tr>
                <td align="center">&nbsp;<br/><img src="../../../images/logo2.png" style="width:100px align:center"></td>
            </tr><br/> 
            <tr>
                <td align="center"><h3><u>BERITA ACARA PEMINDAHAN ARSIP</u></h3></td>
            </tr>
             <tr>
                <td align="center">No. KT .00.00/ ......../ ......../20....</td>
            </tr>
             <tr>
                <td align="center">&nbsp;</td>
            </tr>
             <tr>
                <td align="left">Pada hari ini, .............. tanggal ........ bulan ..................... tahun ..........., kami yang bertanda tangan dibawah ini :</td>
            </tr><br/>
             <tr>
                <td align="left"> 
                    <table border="0">
                        <tr>
                            <td style="width:15%;" rowspan="3">&nbsp;</td>
                            <td align="left" style="width:13%;"> Nama</td>
                            <td align="center" style="width:2%;">:</td>
                            <td style="width:30%;"></td>
                        </tr>
                        <tr>
                            <td align="left"> Jabatan</td>
                            <td align="center">:</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td align="left"> No.Register</td>
                            <td align="center">:</td>
                            <td></td>
                            <td style="width:23%;">(Min. Manager/Setingkat)</td>
                        </tr>
                    </table> 
                </td>
            </tr><br/>
            <tr>
                <td align="left">Dengan hal ini bertindak atas nama Unit Pengolah .................................. yang selanjutnya disebut PIHAK KESATU (1).
                </td>
            </tr><br/>
            <tr>
                <td align="left"> 
                    <table border="0">
                        <tr>
                            <td style="width:15%;" rowspan="3">&nbsp;</td>
                            <td align="left" style="width:13%;"> Nama</td>
                            <td align="center" style="width:2%;">:</td>
                            <td style="width:30%;"></td>
                        </tr>
                        <tr>
                            <td align="left"> Jabatan</td>
                            <td align="center">:</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td align="left"> No.Register</td>
                            <td align="center">:</td>
                            <td></td> 
                        </tr>
                    </table> 
                </td>
            </tr><br/>
            <tr>
                <td align="left">Dalam hal ini bertindak atas nama Corporate Secretary yang selanjutnya disebut PIHAK KEDUA (2).</td>
            </tr><br/>
            <tr>
                <td align="left">Menyatakan telah mengadakan serah terima arsip sebanyak ...... yang dipindahkan ke Dinas Document Management seperti tercantum dalam daftar berikut ini :
                </td>
            </tr><br/>
            <tr>
                <td align="center">
                    <table border="1">
                        <tr>
                            <th style="width:5%;">No. </th>
                            <th style="width:30%;">Uraian</th>
                            <th>Masa Inaktif</th>
                            <th>s/d Periode</th>
                            <th>Jumlah</th>
                            <th>Keterangan</th>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left">
            
            Diterima tanggal : 
            PIHAK KEDUA
            CORPORATE SECRETARY
            PIHAK KESATU
            DIVISI</td>
            </tr>
        </table>  
    </td>
 </tr>
</table>
</body>
'; 
$pdf->writeHTML($tbl2, true, false, true, false, '');
// reset pointer to the last page
$pdf->lastPage();
//Close and output PDF document
$pdf->Output('Report_Label.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
