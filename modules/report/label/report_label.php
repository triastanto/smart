<?php
    include_once('../../../config.php');
	include_once('../../../includes/functions.php');
	include_once('../../../includes/koneksi.php'); 
    // Include the main TCPDF library (search for installation path).
    require_once('../../../includes/tcpdf_ori/tcpdf_include.php');
    require_once('../../../includes/tcpdf_ori/config/lang/eng.php');
    require_once('../../../includes/tcpdf_ori/tcpdf.php');
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    // set document information
    $pdf->SetCreator(PDF_CREATOR); 
    $pdf->SetTitle('Report Label Arsip'); 
    $pdf->SetKeywords('TCPDF, PDF, Report, Arsip');
    // set margins 
    //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) 
    {
    	require_once(dirname(__FILE__).'/lang/eng.php');
    	$pdf->setLanguageArray($l);
    }
    // set font
    $pdf->SetFont('tahoma', '', 9);
    $pdf->AddPage('P', 'A4');
    $tbl2 = '<style>
   	                 .foot_label
                     {
                    	font-weight:bold;
                    	font-size:48px;
                     	line-height:3.4em;
                     }
   	                 .foot_label2
                     {
                    	font-weight:bold;
                    	font-size:45px;
                     	line-height:1.6em;
                     }
                     .isi
                     {
                     	font-weight:bold;
                     	font-size:32px; 
                     	padding: 0em 2em 1em 0em;	
                     } 
                     .isia
                     {
                     	font-weight:bold;
                     	font-size:36px; 
                     	padding: 0em 2em 1em 0em;	 	
                     }
                     .isi2
                     { 	
                        line-height:2.3em; 
                     }
                     .isikode
                     { 
                        line-height:2em;
                        border-left: 1px solid black;
                        background-color: #c3c3c3;
                        text-align: center;
                     	font-weight:bold;
                     	font-size:36px;  	
                     } 
                     .isitahun
                     {
                        line-height:4.8em; 
                        font-weight:bold;
                        font-size:34px; 
                        padding: 0em 2em 1em 0em;
                     } 
                     .isinya
                     {  
                        line-height:2em; 
                     }
                     .isino
                     {
                        line-height:3em; 
                     }
                     .reportnya
                     {
                        align:center;
                     }		
                     .border-head 
                     {
                        border-bottom: #fffff #fffff rgb(250,0,255);
                     }
                     .banyak
                     { 
                        line-height:115%;
                     }
                     
             </style>';
	//====================================================================================================================//
	$klikjum=$_REQUEST['totjum_all']; 	
	$jumall=$_REQUEST['jum_all']; 	
    if ($klikjum=$_REQUEST['totjum']){
        $klikjum=$_REQUEST['totjum'];
    }else if($klikjum=$_REQUEST['totjum_in']){
        $klikjum=$_REQUEST['totjum_in'];
    }else if($klikjum=$_REQUEST['totjum_ak']){
        $klikjum=$_REQUEST['totjum_ak'];
    }else{
        $klikjum=$_REQUEST['totjum_all'];
    }    
    if ($jumall=$_REQUEST['jum']){
        $jumall=$_REQUEST['jum'];
    }else if($jumall=$_REQUEST['jum_in']){
        $jumall=$_REQUEST['jum_in'];
    }else if($jumall=$_REQUEST['jum_ak']){
        $jumall=$_REQUEST['jum_ak'];
    }else{
        $jumall=$_REQUEST['jum_all'];
    }
	$jumdata = $klikjum;
	$baris = $jumdata / 2;
	$a = explode(".",$baris);
	$b = $a[0] * 2;
	$c = $jumdata - $b; 
	$query ="select * from t_m_pertelaan where kodepertelaan=''";
	for($i=0; $i<=$jumall; $i++){
		if($_POST['chek'.$i]!=''){
			$query .=" or kodepertelaan='".$_POST['chek'.$i]."' ";
		}
	}
	$query .='order by kodepertelaan ASC';
	$cekisi=mysql_num_rows(mysql_query($query));
	
	$tbl2 .='<table border="0" width="120%" align="center">';
	for($i=0; $i<$baris; $i++){
		$lim = $i * 2;
		$sql=mysql_query(''.$query.' limit '.$lim.', 2');
		$cekdt=mysql_num_rows(mysql_query(''.$query.' limit '.$lim.', 2'));
		if($cekdt>1){
			$tbl2 .='<tr>';
			while($dt=mysql_fetch_array($sql)){ 
                $rsqlh = "SELECT * FROM hic.structdisp WHERE empkostl='".$dt['empkostl']."' ";
                $isih = mysql_fetch_array(mysql_query($rsqlh)); 
				$isi = mysql_fetch_array(mysql_query("SELECT * FROM t_m_pertelaan WHERE kodepertelaan='".$dt['kodepertelaan']."'"));
				$tbl2 .='<td width="1%" align="center">&nbsp;</td>
						<td width="40%" align="center">
							<br/>
							<table border="1">
								<tr class="isikode"> 
                                    <td>
									   '.$isi['kodepertelaan'].' 
								    </td>
                                </tr>
								<tr>
									<td>&nbsp;<br/>
										<img src="../../../images/logo2.png" style="width:115px"><br/><br/>
									</td>
								</tr>
								<tr>
									<td > 
							            <span class="isia">'.$isih['emp_cskt_ltext'].'</span><br/>
                                    </td>
                                </tr>
								<tr class="isinya">
                                    <td height="200"><br/> 
                                    	<span class="isi">'.$isi['kodeklasifikasi'].'</span><br/>
                                    	<span class="isi">'.$isi['jenisarsip'].'</span><br/>
										<span class="isi">'.$isi['isiringkasan'].'</span><br class="isino"/>
										<span class="isi">TAHUN</span><br/>
										<span class="isitahun">'.$isi['daritahun'].' s/d '.$isi['sampaitahun'].'</span> 
    								</td>  
								</tr>
								<tr>
									<td class="foot_label">Box : '.$isi['nobox'].'</td>    
								</tr>
								<tr>
									<td class="foot_label2">'.$isi['ruang'].'.'.$isi['rak'].'.'.$isi['nodefinitif'].'-'.$isi['shelf'].'</td>    
								</tr>
							</table>
							<br/> 
						</td>
						<td width="1%" align="center">&nbsp;</td>';
			}
			$tbl2 .='</tr> ';
		}else{
			while($dt=mysql_fetch_array($sql)){
                $rsqlh = "SELECT * FROM hic.structdisp WHERE empkostl='".$dt['empkostl']."' and no='1'";
                $isih = mysql_fetch_array(mysql_query($rsqlh)); 
				$isi = mysql_fetch_array(mysql_query("SELECT * FROM t_m_pertelaan WHERE kodepertelaan='".$dt['kodepertelaan']."'"));
				$tbl2 .='<tr align="center">
							<td width="2%" align="center">&nbsp;</td>
							<td width="40%">
								<br/>
    							<table border="1">
    								<tr class="isikode">
                                        <td>
 										   '.$isi['kodepertelaan'].'
    								    </td>
                                    </tr>
    								<tr>
    									<td>&nbsp;<br/>
    										<img src="../../../images/logo2.png" style="width:115px"><br/><br/>
    									</td>
    								</tr>
    								<tr >
    									<td>  
   										    <span class="isia">'.$isih['emp_cskt_ltext'].'</span><br/>
                                        </td>
                                    </tr>
    								<tr class="isinya">
                                        <td  height="200"> <br/>	 
											<span class="isi">'.$isi['kodeklasifikasi'].'</span><br/>
                                        	<span class="isi">'.$isi['jenisarsip'].'</span><br/>
    										<span class="isi">'.$isi['isiringkasan'].'</span><br class="isino"/>
    										<span class="isi">TAHUN</span><br/>
    										<span class="isitahun">'.$isi['daritahun'].' s/d '.$isi['sampaitahun'].'</span>   
    									</td>  
    								</tr>
    								<tr>
    									<td class="foot_label">Box : '.$isi['nobox'].'</td>    
    								</tr>
								<tr>
									<td class="foot_label2">'.$isi['ruang'].'.'.$isi['rak'].'.'.$isi['nodefinitif'].'-'.$isi['shelf'].'</td>    
								</tr>
    							</table>
								<br/>  
							</td>
							<td width="2%" align="center">&nbsp;</td>
							<td width="2%" align="center">&nbsp;</td>
							<td width="40%">&nbsp;</td>
							<td width="2%" align="center">&nbsp;</td>
						</tr> ';
			}
		}
	}
	$tbl2 .='</table>'; 
	$pdf->writeHTML($tbl2, true, false, true, false, ''); 
	// reset pointer to the last page
	$pdf->lastPage();
	//Close and output PDF document
	$pdf->Output('Report_Label_Pertelaan.pdf', 'I');
?>