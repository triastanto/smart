<?php
	session_start();
	include_once("../../../config.php");
	$conn = mysql_connect($hostname,$username,$password);
	@mysql_select_db($dbname,$conn);
	include_once('../../../includes/functions.php');
	include_once('../../../includes/koneksi.php');

    // Include the main TCPDF library (search for installation path).
    require_once('../../../includes/tcpdf/tcpdf_include.php');
    require_once('../../../includes/tcpdf/config/lang/eng.php');
    require_once('../../../includes/tcpdf/tcpdf.php');

    $kode=base64_decode($_REQUEST['idp']);
	//==========================================Form Peminjaman Arsip====================================================//
	$dc 	= mysql_fetch_array(mysql_query("SELECT $dbname2.structdisp.* FROM $dbname2.structdisp WHERE empkostl='111101' and emppersk='CS' and no='1'"));
    $adc 	= mysql_fetch_array(mysql_query("SELECT $dbname2.structdisp.* FROM $dbname2.structdisp WHERE emp_hrp1000_s_short='1220000000' and no='1'"));
    $isinya	= mysql_fetch_array(mysql_query("SELECT * FROM t_workorder_m_pemindahan WHERE kodeworkorder='".$kode."'"));
 
    $jumbox	= mysql_num_rows(mysql_query("SELECT * FROM t_workorder_d_pemindahan WHERE kodeworkorder='".$kode."'"));
    $jumdok	= mysql_num_rows(mysql_query("SELECT t_workorder_d_pemindahan.*, t_d_pertelaan.* FROM t_workorder_d_pemindahan INNER JOIN t_d_pertelaan ON t_d_pertelaan.kodepertelaan = t_workorder_d_pemindahan.kodepertelaan where t_workorder_d_pemindahan.kodeworkorder ='".$kode."' and t_d_pertelaan.status='0'"));
    //query tabel master 
    $jpm 	= mysql_fetch_array(mysql_query("SELECT $dbname2.structdisp.* FROM $dbname2.structdisp WHERE empnik='".$isinya['regnopengirim']."' and no='1'"));
    $japm 	= mysql_fetch_array(mysql_query("SELECT $dbname2.structdisp.* FROM $dbname2.structdisp WHERE empnik='".$isinya['regnoatasan']."' and no='1'"));
	 
	$isi ='
            <table border="0" style="width:100%;">
                <tr>
                    <td style="width:70%;"></td>
                    <td style="width:30%;"> 
                        <table border="1" >
                            <tr class="kwo"> 
                                <td >Kode Work Order '.$kode.' </td>
                            </tr>
                        </table> 
                    </td>
                </tr>
                
                <tr>
                    <td align="center" style="width:auto;"><br/><img src="../../../images/logo_hitam.png" style="width:140px;"><br/></td>
                </tr>
                <tr> 
                    <td align="center" style="width:auto;"><h3>FORM PENGAJUAN PEMINDAHAN ARSIP</h3><br/></td>
                </tr>  
                <tr>
                    <td> 
                        <table border="1">
							<tr>
								<td>
									<table  border="1">
										<tr>
											<td> NIK User</td> 
											<td> : '.$isinya['regnopengirim'].'</td>
											<td> Kode CC/CostCentre </td> 
											<td> : '.$isinya['empkostl'].' - '.$isinya['emp_cskt_ltext'].'</td>
										</tr>
										<tr>
											<td> Nama User</td> 
											<td> : '.$isinya['namapengirim'].'</td>
											<td> Telepon</td> 
											<td> : '.$isinya['notelepon'].'</td>
										</tr>
										<tr>
											<td> Jabatan </td> 
											<td> : '.$jpm['emppostx'].'</td>
											<td> Tanggal Pengajuan</td> 
											<td> : '._convertDate($isinya['tanggalpengajuan']).'</td>
										</tr>
										<tr>
											<td> Nik Pimpinan </td> 
											<td> : '.$isinya['regnoatasan'].'</td>
											<td> Jumlah Dokumen</td> 
											<td> : '.$jumdok.'</td>
										</tr>
										<tr>
											<td> Nama Pimpinan </td> 
											<td> : '.$isinya['namaatasan'].'</td>
											<td> Jumlah Box</td> 
											<td> : '.$jumbox.'</td>
										</tr>
										<tr>
											<td> Jabatan </td> 
											<td> : '.$japm['emppostx'].' </td>
											<td></td> 
											<td></td>
										</tr>
									</table>
								</td>
							</tr> 
							<tr>
								<td>
									<b><u> Alasan Pemindahan Arsip :</u></b><br/> 
									'.$isinya['note'].'
									<br/>
									<br/>
									<br/>
								</td>
							</tr>
							<tr>
								<td>
									<table border="1"> 
										<tr>
										  <td colspan="2" align="center"> Unit Pengolah Arsip </td>
										  <td colspan="2" align="center"> Unit Pengelola Arsip </td>
										</tr>
										<tr> 
											<td>
												<table>
													<tr>
														<td> Tanggal : '.date('d-m-Y').'</td>
													</tr>
													<tr>   
														<td align="center">User</td>
													</tr>
													<tr>
														<td align="center">
															<br/>
															<br/>
															<br/>
															<br/>
															<br/>
															<u>...........................................</u>
														</td>
													</tr>
													<tr> 
														<td align="center">(....................)</td> 
													</tr>
												</table>
											</td>
											<td>
												<table>
													<tr>
														<td> Tanggal : '.date('d-m-Y').'</td>
													</tr>
													<tr>   
														<td align="center">'.$isinya['emppostx'].'</td>
													</tr>
													<tr>
														<td align="center">
															<br/>
															<br/>
															<br/>
															<br/>
															<br/>
															<u>...........................................</u>
														</td>
													</tr>
													<tr> 
														<td align="center">(....................)</td> 
													</tr>
												</table>
											</td>
											<td>
												<table>
													<tr>
														<td> Tanggal : '.date('d-m-Y').'</td>
													</tr>
													<tr>   
														<td align="center">'.$dc['emportx'].'</td>
													</tr>
													<tr>
														<td align="center">
															<br/>
															<br/>
															<br/>
															<br/>
															<br/>
															<u>...........................................</u>
														</td>
													</tr>
													<tr> 
														<td align="center">(....................)</td> 
													</tr>
												</table>
											</td>
											<td>
												<table>
													<tr>
														<td> Tanggal : '.date('d-m-Y').'</td>
													</tr>
													<tr>   
														<td align="center">'.$adc['emportx'].'</td>
													</tr>
													<tr>
														<td align="center">
															<br/>
															<br/>
															<br/>
															<br/>
															<br/>
															<u>...........................................</u>
														</td>
													</tr>
													<tr> 
														<td align="center">('.$adc['emp_t503t_ptext'].')</td> 
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
                        </table>
                    </td>
                </tr>
				<tr><td>&nbsp;</td></tr>
                <tr>
                    <td>
						1. Form Pengajuan Pemindahan Arsip wajib dilampirkan Daftar Arsip In Aktif yang diajukan dipindahkan.<br/>
						2. Proses pemindahan arsip dapat dilanjutkan apabila telah dianggap valid oleh Dinas Document Mgt setelah melalui proses pengecekan.<br/>
					</td>
                </tr>
            </table> 
		';
	//===================================================pertelaan======================================================//
	$dtwo	= mysql_fetch_array(mysql_query("SELECT * FROM t_workorder_m_pemindahan WHERE kodeworkorder='".base64_decode($_REQUEST['idp'])."'"));
	
	$page2 ='
		<table border="1" style="align:center; text-align:left; width:100%;">
			<tr>
				<td>
					<table border="0" text-align:center; class="isinya">
						<tr>
							<td rowspan="2" align="center">
								&nbsp;<br/>
								<img src="../../../images/logo2.png" style="width:100px"> 
							</td>
							<td align="center"><br/><br/>
								<b>DAFTAR PERTELAAN</b> 
							</td> 
						</tr>
						<tr> 
							<td align="center">
								<b>ARSIP '.$inaktif.'</b>   
							</td>
						</tr>
					</table>
				</td>
				<td>
					<br/><br/>
					<table border="0" style="text-align:left; align:left;" class="isinya">
						<tr>
							<td style="width:28%;">
								Cost Centre
							</td>
							<td style="width:5%;">:</td>
							<td align="left" style="width:65%;"> '.$dtwo['empkostl'].' </td> 
						</tr>
						<tr>
							<td>Unit Pengolah</td>
							<td style="width:5%;">:</td>
							<td align="left"> '.$dtwo['emp_cskt_ltext'].' </td> 
						</tr> 
					</table>
				</td>
			</tr>
		</table>
		<table border="1" width="100%">
			<tr align="center">
				<th style="width:5%;"><b>No</b></th> 
				<th style="width:45%;"><b>Kode Pertelaan</b></th> 
				<th style="width:10%;"><b>No.Box</b></th>  
				<th style="width:40%;"><b>Jenis Arsip</b></th>
			</tr>'; 
				$d_pertelaan = "SELECT
									t_workorder_d_pemindahan.*,
									t_m_pertelaan.*
								FROM
									t_m_pertelaan
									INNER JOIN t_workorder_d_pemindahan ON t_m_pertelaan.kodepertelaan = t_workorder_d_pemindahan.kodepertelaan 
								where t_workorder_d_pemindahan.kodeworkorder='".base64_decode($_REQUEST['idp'])."'
								order by t_m_pertelaan.kodepertelaan";
				$qd_pertelaan=mysql_query($d_pertelaan);
				$kd=0;                       
				while($isi_d = mysql_fetch_array($qd_pertelaan)){ 
					$kd++;                            
					$page2.='<tr>
								<td rowspan="2" align="center"><b>'.$kd.'</b></td> 
								<td> '.$isi_d['kodepertelaan'].'</td> 
								<td> '.$isi_d['nobox'].'</td> 
								<td> '.$isi_d['jenisarsip'].'</td> 
							</tr>
							<tr>
								<td colspan="3">
									<table>
										<tr class="isinya2">
											 <td style="width:5%;" align="center">No</td>
											 <td style="width:11%;" >No.Dokumen</td>
											 <td style="width:36%;" >Uraian Masalah</td>
											 <td style="width:7%;" >Jumlah</td>
											 <td style="width:8%;" >Satuan</td>
											 <td style="width:11%;" >Tgl Dokumen</td>
											 <td style="width:8%;" >Aktif s/d</td>
											 <td style="width:10%;" >In-Aktif s/d</td>
											 <td style="width:4%;">Asli</td>
										</tr> '; 
										$det2=mysql_query("SELECT * FROM t_d_pertelaan WHERE kodepertelaan='".$isi_d['kodepertelaan']."' and status='0' ORDER BY kodepertelaan");
										$a=1;
										while($det_isi=mysql_fetch_array($det2)){
											$satuan=mysql_fetch_array(mysql_query("SELECT * FROM m_satuan WHERE kodesatuan='".$det_isi['kodesatuan']."'"));
											$satuan=$satuan['satuan'];
											$page2.='<tr class="tesborder">
														<td align="center">'.$a.'</td>
														<td>'.$det_isi['nodokumen'].'</td>
														<td>'.$det_isi['uraianmasalah'].'</td>
														<td align="center">'.$det_isi['jumlah'].'</td>
														<td>'.$satuan.'</td>
														<td align="center">'.date('d-m-Y',strtotime($det_isi['tanggaldokumen'])).'</td>
														<td align="center">'.date('Y',strtotime($det_isi['aktifsampaidengan'])).'</td>
														<td align="center">'.date('Y',strtotime($det_isi['inaktifsampaidengan'])).'</td>                
														<td align="center"><img src="../../../images/ceklis2.png" border="0" title="Asli"></td>
												   </tr>';
												$a++;
										   }  
					$page2.='
								</table>
								</td>
							</tr>
					';
				}
				@mysql_free_result($qd_pertelaan);
	$page2.='</table>';
	
	
	
	class MYPDF extends TCPDF {
		//Page header
		public function Header() { }
		// Page footer
		public function Footer() { }
	}
	// create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set font
	$pdf->SetFont('tahoma', '', 9);

	// halaman pertama
	$pdf->AddPage('L', 'A4');
	$pdf->writeHTML($isi, true, false, true, false, '');

	// halaman kedua
	$pdf->AddPage('P', 'A4');
	$pdf->writeHTML($page2, true, false, true, false, '');

	// rOutput PDF
	$pdf->lastPage();
	$pdf->Output('Form Pemindahan Arsip.pdf', 'I');
?>