<?php
	include_once("../../../config.php");
	$conn = mysql_connect($hostname,$username,$password);
	@mysql_select_db($dbname,$conn);
    include_once('../../../includes/functions.php');
	
    require_once('../../../includes/tcpdf/tcpdf_include.php');
    require_once('../../../includes/tcpdf/config/lang/eng.php');
    require_once('../../../includes/tcpdf/tcpdf.php');
	
   class MYPDF extends TCPDF {
		//Page header
		public function Header() {
			
		}
		// Page footer
		public function Footer() {
			
		}
	}
    // create new PDF document
    // $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	$pdf = new MYPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false);
    // set document information
    $pdf->SetCreator(PDF_CREATOR); 
    $pdf->SetTitle('Memo Dinas'); 
    $pdf->SetKeywords('TCPDF, PDF, Report, Arsip');
    // set margins  
    // set auto page breaks 
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    	require_once(dirname(__FILE__).'/lang/eng.php');
    	$pdf->setLanguageArray($l);
    }
    // set font
    $pdf->SetFont('dejavusans', '', 9);
	$pdf->AddPage('P', 'A4');
	
	$idp = mysql_real_escape_string(base64_decode($_REQUEST['idp']));
	$dtm = mysql_fetch_array(mysql_query("SELECT * FROM t_surat_deployment where kodesuratbawah='".$idp."'"));
	$dtm = mysql_fetch_array(mysql_query("SELECT * FROM t_surat_deployment where kodesuratbawah='".$idp."'"));
	if($dtm['kodesuratutama']!=""){
		$depan = substr($dtm['kodesuratutama'],0,2);		
	}else{
		$dtm = mysql_fetch_array(mysql_query("SELECT * FROM t_surat_deployment where kodesuratatas='".$idp."'"));
		$depan = substr($dtm['kodesuratutama'],0,2);
	}
	// echo $depan;
	if($depan=='SK'){
		$surat = mysql_fetch_array(mysql_query("SELECT * FROM t_suratkeluar_m where kodesuratkeluar='".$dtm['kodesuratutama']."'"));
		$suratnya = $surat['kodesuratkeluar'];
		$sql = "SELECT COUNT(kodesuratatas) as a, kodesuratatas FROM `t_surat_deployment` where kodesuratutama='".$dtm['kodesuratutama']."' and kodesuratatas!='".$dtm['kodesuratutama']."' group by kodesuratatas order by kodesuratatas asc";
		$asal = "[".$surat['empnik_asal']."] ".$surat['empname_asal']."";
	}else{
		$surat = mysql_fetch_array(mysql_query("SELECT * FROM t_suratmasuk_m where kodesuratmasuk='".$dtm['kodesuratutama']."'"));
		$suratnya = $surat['kodesuratmasuk'];
		$sql = "SELECT COUNT(kodesuratatas) as a, kodesuratatas FROM `t_surat_deployment` where kodesuratutama='".$dtm['kodesuratutama']."' group by kodesuratatas order by kodesuratatas asc";
		$asal = "".$surat['empnamesurat']." ".$surat['emp_cskt_ltext_surat']."";
	}
	
	$tbl2 ='
	<style>
		body {
			font-family: Tahoma, Verdana, Segoe, sans-serif;
			font-size: 10px;
		}
		.judul{
			font-family: Tahoma, Verdana, Segoe, sans-serif;
			font-size: 14px;
		}
	</style>
	<body>
	<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
		<tr>
			<td class="judul">
				<img src="../../../images/hitam.jpg" width="150" height="30" />
				<br/>
				<b>LEMBAR DISPOSISI</b>
			</td>
		</tr>
	</table>
	<br/>
	<br/>
	
	<table width="100%" cellpadding="3" cellspacing="0" border="1">
		<tr align="center">
			<td width="25%" colspan="2"> INDEX </td>
			<td width="30%"> KODE KLASIFIKASI </td>
			<td width="25%"> TANGGAl/ NO SURAT </td>
			<td width="20%"> SIFAT SURAT </td>
		</tr>
		<tr align="center">
			<td colspan="2"> '.$suratnya.' </td>
			<td> '.$surat['kodeklasifikasi'].' </td>
			<td> '.$surat['tanggalsurat'].'/ '.$surat['nosuratkeluar'].' </td>
			<td> '.ss($surat['sifatsurat']).' </td>
		</tr>
		<tr>
			<td width="23%"> ASAL SURAT </td>
			<td width="2%" align="center"> : </td>
			<td colspan="3"> '.$asal.' </td>
		</tr>
		<tr>
			<td> PPERIHAL </td>
			<td align="center"> : </td>
			<td colspan="3">'.$surat['isiringkasan'].' </td>
		</tr>';
			$no=1;
			$rs = mysql_query($sql);
			while($lev=mysql_fetch_array($rs)){
				$surm 	= mysql_fetch_array(mysql_query("SELECT * FROM t_suratmasuk_m where kodesuratmasuk='".$lev['kodesuratatas']."'"));
				$hic 	= mysql_fetch_array(mysql_query("SELECT ".$dbname2.".structdisp.* FROM ".$dbname2.".structdisp where empnik='".$surm['empniktujuan']."' and no='1'"));
				$tbl2 .='
					<tr bgcolor="#d0cece">
						<td> DISPOSISI KE '.$no.' </td>
						<td align="center"> : </td>
						<td colspan="3"> ['.$surm['empniktujuan'].'] '.$surm['empnametujuan'].' ('.$hic['emppostx'].')</td>
					</tr>
					<tr>
						<td> TARGET PENYELESAIAN </td>
						<td align="center"> : </td>
						<td colspan="3"> '.tanggalok($surm['tanggalpenyelesaian']).' </td>
					</tr>
					<tr>
						<td> CATATAN </td>
						<td align="center"> : </td>
						<td colspan="3"> '.$surm['catatan'].' </td>
					</tr>
					<tr>
						<td> KEPADA </td>
						<td align="center"> : </td>
						<td align="center" bgcolor="#d0cece"> [ NIK ] NAMA</td>
						<td align="center" bgcolor="#d0cece"> JABATAN </td>
						<td align="center" bgcolor="#d0cece"> PESAN </td>
					</tr>
				';
				$nom=1;
				$sql2 = mysql_query("SELECT * FROM t_surat_deployment where kodesuratatas='".$lev['kodesuratatas']."'");
				while($lev2 = mysql_fetch_array($sql2)){
					$surm2 	= mysql_fetch_array(mysql_query("SELECT * FROM t_suratmasuk_m where kodesuratmasuk='".$lev2['kodesuratbawah']."'"));
					$hic2 	= mysql_fetch_array(mysql_query("SELECT ".$dbname2.".structdisp.* FROM ".$dbname2.".structdisp where empnik='".$surm2['empniktujuan']."' and no='1'"));
					$psn 	= mysql_fetch_array(mysql_query("select * from m_pesan  where kodepesan='".$surm2['catatanatasan']."'"));
					$tbl2 .='
						<tr>
							<td></td>
							<td align="center"> '.$nom.' </td>
							<td align="center"> ['.$surm2['empniktujuan'].'] '.$surm2['empnametujuan'].' </td>
							<td align="center"> '.$hic2['emppostx'].' </td>
							<td align="center"> '.$psn['pesan'].' </td>
						</tr>
					';
					$nom++;
				}
				$no++;
			}
	$tbl2 .='</table></body>';
	
	
	// $pdf->SetMargins(2, 2, 2);
	$pdf->writeHTML($tbl2, true, false, true, false, '');
	$pdf->lastPage();
	$pdf->Output('Report_disposisi.pdf', 'I');
?>