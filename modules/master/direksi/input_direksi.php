<?
out("title", "MASTER > DIREKSI > INPUT DIREKSI");
$u_agent = $_SERVER['HTTP_USER_AGENT'];
if(preg_match('/Chrome/i',$u_agent)){
	//$tg = "780px;";
}else{
	//$tg = "740px;";
}
//untuk memanggil fungsi group ID user atau admin
$grup_id = $_SESSION['group_id'];

$act	= base64_decode($_REQUEST['act']);
if ($act=='edit'){
	$idp=base64_decode($_REQUEST['idp']);
	$dt	= mysql_fetch_array(mysql_query("select ".$dbname2.".structdireksi.* from ".$dbname2.".structdireksi where ".$dbname2.".structdireksi.empnik='".$idp."'"));
	$pos = "readonly";
}else{
	$act='new';
	$pos = "required";
}


?>
<script type="text/javascript" src="javascript/jquery.form.js"></script>	
<script type="text/javascript">
	$(document).ready( function() {
		$(".popup").colorbox({ 		iframe:true		,width:"80%"		,height:"70%"	});
		
			var datePicker = $('.date-picker');
			datePicker.datepicker({
				showOn: "button",
				buttonImage: "templates/blue/images/calendar.gif",
				buttonImageOnly: true
			});
					

			var options = { 
				beforeSubmit:  showRequest,  // pre-submit callback 
				success:       showResponse,  // post-submit callback 
				url:'modules/master/direksi/ajax/save_direksi.php',
				iframe:true
			}; 	
			
			$('#myfm').submit(function(event) { 
				event.preventDefault();
				$(this).ajaxSubmit(options); 
				event.stopImmediatePropagation();
				return false; 
			});
		
	});

	function showRequest(formData, jqForm, options){ 
		var form = jqForm[0]; 
		return true; 
	} 
	 
	function showResponse(responseText, statusText, xhr, $form){
		var mydata = responseText.split(" ");
		if (mydata[0]=="OK"){
			alert("Data sudah disimpan!");
			window.location.href = "?mod="+mydata[1]+"&op="+mydata[2];
		}else{
			alert(responseText); 
		}
	}
</script>
<form method="POST" action="modules/master/direksi/ajax/save_direksi.php" enctype="multipart/form-data" name="myfm" id="myfm" >
	<div style="padding:3px; width:99%;  border:2px #E6F0F3 solid;">
		<table width="99%" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td width="10%">NIK Direksi</td>
				<td width="3%"><strong>:</strong></td>
				<td width="77%">
					<input type="text" name="empnik" id="empnik" value="<?=$dt['empnik']?>" size="10" <?=$pos?> />
				</td>
			</tr>
			<tr>
				<td>Nama Direksi</td>
				<td><strong>:</strong></td>
				<td>
					<input type="text" name="empname" id="empname" value="<?=$dt['empname']?>" size="35" required />
				</td>
			</tr>
			<tr>
				<td>EMPPOSID</td>
				<td><strong>:</strong></td>
				<td>
					<input type="text" name="empposid" id="empposid" value="<?=$dt['empposid']?>" size="20" required />
				</td>
			</tr>
			<tr>
				<td>emp_hrp1000_s_short</td>
				<td><strong>:</strong></td>
				<td>
					<input type="text" name="emp_hrp1000_s_short" id="emp_hrp1000_s_short" value="<?=$dt['emp_hrp1000_s_short']?>" size="20" required />
				</td>
			</tr>
			<tr>
				<td>emppostx</td>
				<td><strong>:</strong></td>
				<td>
					<input type="text" name="emppostx" id="emppostx" value="<?=$dt['emppostx']?>" size="50" required />
				</td>
			</tr>
			<tr>
				<td>emporid</td>
				<td><strong>:</strong></td>
				<td>
					<input type="text" name="emporid" id="emporid" value="<?=$dt['emporid']?>" size="20" required />
				</td>
			</tr>
			<tr>
				<td>emportx</td>
				<td><strong>:</strong></td>
				<td>
					<input type="text" name="emportx" id="emportx" value="<?=$dt['emportx']?>" size="20" required />
				</td>
			</tr>
			<tr>
				<td>emp_hrp1000_o_short</td>
				<td><strong>:</strong></td>
				<td>
					<input type="text" name="emp_hrp1000_o_short" id="emp_hrp1000_o_short" value="<?=$dt['emp_hrp1000_o_short']?>" size="30" required />
				</td>
			</tr>
			<tr>
				<td>empjobid</td>
				<td><strong>:</strong></td>
				<td>
					<input type="text" name="empjobid" id="empjobid" value="<?=$dt['empjobid']?>" size="30" required />
				</td>
			</tr>
			<tr>
				<td>empjobstext</td>
				<td><strong>:</strong></td>
				<td>
					<input type="text" name="empjobstext" id="empjobstext" value="<?=$dt['empjobstext']?>" size="30" required />
				</td>
			</tr>
			<tr>
				<td>emppersk</td>
				<td><strong>:</strong></td>
				<td>
					<input type="text" name="emppersk" id="emppersk" value="<?=$dt['emppersk']?>" size="30" required />
				</td>
			</tr>
			<tr>
				<td>emp_t503t_ptext</td>
				<td><strong>:</strong></td>
				<td>
					<input type="text" name="emp_t503t_ptext" id="emp_t503t_ptext" value="<?=$dt['emp_t503t_ptext']?>" size="30" required />
				</td>
			</tr>
			<tr>
				<td>empkostl</td>
				<td><strong>:</strong></td>
				<td>
					<input type="text" name="empkostl" id="empkostl" value="<?=$dt['empkostl']?>" size="15" readonly />
				</td>
			</tr>
			<tr>
				<td>emp_cskt_ltext</td>
				<td><strong>:</strong></td>
				<td>
					<input type="text" name="emp_cskt_ltext" id="emp_cskt_ltext" value="<?=$dt['emp_cskt_ltext']?>" size="45" readonly />
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<input name="act" type="hidden" value="<?=$act?>" />
					<input name="group_id" id="group_id" type="hidden" value="<?=$_SESSION['group_id']?>" />
					<input class="btn btn-blue" style="width:80px;" type="submit" name="submit" value="Simpan" />
					<?
						echo'
							<input class="btn btn-blue" style="width:80px;" type="button" name="Cancel" value="Cancel" onclick="window.location=\'?mod='.base64_encode('master/direksi').'&op='.base64_encode('list_direksi').' \'" />
						';
					?>
				</td>
				<td>&nbsp;</td>
			</tr>
		</table>
	</div>
</form>
