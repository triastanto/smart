<?
out("title", "MASTER > DIREKSI");
?>

<table border="0" cellpadding="1" cellspacing="1" width="100%">
	<tr bgcolor="#EEEEEE">
		<td >
			<form action="" name="frmaction" method="post">						
				<span>Cari : 
					<input type="text" name="txtval" id="txtval" size="40" />
					<input type="hidden" name="user" id="user" size="30" value="<?=$_SESSION['user']?>" />
					<input type="hidden" name="group_id" id="group_id" size="30" value="<?=$_SESSION['group_id']?>" />
				</span>
				<span class="level">
					<a class="btn btn-blue" id="cari"><i class="ion-android-search">&nbsp;</i><b>Cari</b></a>
            	</span>
			</form>
		</td>
		<td width="20%" align="right">
			<?
				echo'
					<input class="btn btn-blue" style="width:150px;" type="button" name="tambah " value="Tambah Direksi" onclick="window.location=\'?mod='.base64_encode('master/direksi').'&op='.base64_encode('input_direksi').' \'" />
				';
			?>
		</td>
	</tr>
</table>

<div id="view">
	<table width="100%" border="0">
		<tr>
			<th align="center" width="5%">NO</th>
			<th align="center" width="35%">NAMA DIREKSI</th>
			<th align="center" width="20%">ABBR DIREKSI</th>
			<th align="center" width="30%">POSISI DIREKSI</th>
			<th align="center" width="10%">OPTION</th>
		</tr>
	<?
		$rsql = "SELECT * FROM ".$dbname2.".structdireksi order by ".$dbname2.".structdireksi.empkostl ASC limit 0,15";
		// echo $rsql;
		$rs = mysql_query($rsql);	
		$i=0;
		while($lev=mysql_fetch_array($rs)){
			echo'
			<tr bgcolor="'.rowClass(++$i).'" class="bariswarna">
				<td align="center">'.$i.'</td>
				<td>['.$lev['empnik'].'] '.$lev['empname'].'</td>
				<td>'.$lev['empposid'].'</td>
				<td>'.$lev['emppostx'].'</td>
				<td align="center">
					<a href="?mod='.base64_encode('master/direksi').'&op='.base64_encode('input_direksi').'&act='.base64_encode('edit').'&idp='.base64_encode($lev['empnik']).'"><img src="images/edit.png" width="15" height="15" border="0"></a> 
				</td>
			</tr>';
		}
		@mysql_free_result($rs);	
	?>
	</table>
</div>
<div id="naf">
	<?
		$rsql = "SELECT * FROM ".$dbname2.".structdireksi order by ".$dbname2.".structdireksi.empkostl ASC";
		$hd		= "";
		$hal	= 0;
		$order 	= "";
		$pager 	= new ajaxpagination($conn, $rsql, $hd, $hal, 15, 15, $order);
		$rs = $pager->paginate();	
		include_once('includes/nav.php');
	?>
</div>
<script type="text/javascript" >
	$(document).ready( function() {
		$('#naf a').click(function() {
			var user = $("#user").val();
			var group_id = $("#group_id").val();
			$("#view").load("modules/master/direksi/list_direksi_detil.php", {'hal': $(this).attr("title"), 'user' : user, 'group_id' : group_id});
			$('#naf').fadeOut('slow').load("modules/master/direksi/list_direksi_page.php", {'hal':$(this).attr("title"), 'user' : user, 'group_id' : group_id}).fadeIn("slow");
		});
		
		$('#cari').click(function() {
			var a = $("#txtval").val();
			var user = $("#user").val();
			var group_id = $("#group_id").val();
			if (a==''){
				alert("Masukkan nilai yang akan dicari!");
			}else{
				$("#view").load("modules/master/direksi/list_direksi_detil.php", {'nm': a, 'user' : user, 'group_id' : group_id});
				$('#naf').fadeOut('slow').load("modules/master/direksi/list_direksi_page.php", {'hal':$(this).attr("title"), 'nm':a, 'user' : user, 'group_id' : group_id}).fadeIn("slow");
			} 
		});
	});
</script>