<?
    session_start();
    include_once('../../../../config.php');
    include_once('../../../../includes/functions.php');
    include_once('../../../../includes/koneksi.php');
    include_once('../../../../includes/ajaxpagination.php');
?>
    <script type="text/javascript" src="../../../../javascript/jquery-1.8.3.min.js"></script> 
    <link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/reset.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/grid.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/layout.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/nav.css" media="screen" />
    <style>
    	*{font-family:Trebuchet MS,"Arial", sans-serif;color:black; font-size:11px;}
    </style> 
<title>Search Material</title>
<table width="100%" cellpadding="2" cellspacing="2" border="0" style="border: 1px solid #DEDEDE;font-size: 10pt;" class="head_sr">
	<tr height="30">
		<td width="15%">CARI :</td>
		<td width="40%">
			<select name="pilihcari" id="pilihcari">
                <option value="">-Pilih Pencarian-</option>
                <option value="empnik">NIK</option>
                <option value="empname">Nama</option>
                <option value="empkostl">Kode CC</option>
               <option value="emp_cskt_ltext">Cost Center</option>
            </select>&nbsp;
			<input type="text" name="txtval" id="txtval" size="40" value="<?=$_REQUEST['fv']?>" />			
			<input type="hidden" name="user" id="user" value="<?=$_REQUEST['user']?>" />			
			<input type="hidden" name="group_id" id="group_id" value="<?=$_REQUEST['group_id']?>" />			
		</td>	
        <td width="38%" ><input type="button" name="button" id="cari" value="Cari" class='btn btn-blue' >&nbsp; 
        </td>
	</tr>
</table>
<div id="view">
	<table width="100%" border="0">
		<tr class="head_tr">
			<th align="center" width="3%">NO</th>
			<th align="center" width="10%">NIK</th>
			<th align="center" width="20%">NAMA</th>
			<th align="center" width="10%">ABBREVIASI</th>
			<th align="center" width="10%">KODE CC</th>
			<th align="center" width="20%">COST CENTER</th>
			<th align="center" width="20%">JABATAN</th>
			<th align="center" width="7%">OPTION</th>
		</tr>
    	<?	
			// echo $_REQUEST['group_id'];
			if($_REQUEST['group_id']==6 or $_REQUEST['group_id']==7){
				$rsql = "SELECT
						".$dbname2.".structdireksi.*,
						".$dbname.".sys_groupuser.*,
						".$dbname.".sys_user.*
					FROM
						".$dbname2.".structdireksi
						INNER JOIN ".$dbname.".sys_groupuser ON ".$dbname.".sys_groupuser.pos_abbr = ".$dbname2.".structdireksi.emp_hrp1000_s_short
						INNER JOIN ".$dbname.".sys_user ON ".$dbname.".sys_groupuser.userid = ".$dbname.".sys_user.userid
					WHERE
						".$dbname2.".structdireksi.`no` = '1' AND ".$dbname.".sys_groupuser.userid='".$_REQUEST['user']."'
					group by ".$dbname2.".structdisp.empnik
					ORDER BY ".$dbname2.".structdireksi.emp_hrp1000_s_short ASC limit 0,15";
			}else{
				$rsql = "SELECT
							".$dbname2.".structdisp.*,
							".$dbname.".sys_groupuser.*,
							".$dbname.".sys_user.*
						FROM
							".$dbname2.".structdisp
							INNER JOIN ".$dbname.".sys_groupuser ON ".$dbname.".sys_groupuser.pos_abbr = ".$dbname2.".structdisp.emp_hrp1000_s_short
							INNER JOIN ".$dbname.".sys_user ON ".$dbname.".sys_groupuser.userid = ".$dbname.".sys_user.userid
						WHERE
							".$dbname2.".structdisp.`no` = '1' AND ".$dbname.".sys_groupuser.userid='".$_REQUEST['user']."' and ".$dbname2.".structdisp.emppersk like 'A%' or
							".$dbname2.".structdisp.`no` = '1' AND ".$dbname.".sys_groupuser.userid='".$_REQUEST['user']."' and ".$dbname2.".structdisp.emppersk like 'B%' or
							".$dbname2.".structdisp.`no` = '1' AND ".$dbname.".sys_groupuser.userid='".$_REQUEST['user']."' and ".$dbname2.".structdisp.emppersk like 'C%' 
						group by ".$dbname2.".structdisp.empnik
						ORDER BY ".$dbname2.".structdisp.emp_hrp1000_s_short ASC limit 0,15";
			}
			
			// echo $rsql;
			$rs = mysql_query($rsql);	
			$i=1;
			while($lev=mysql_fetch_array($rs)){
				$i%2 == 0 ? $cl='#caefff' : $cl='#e4f3f9';
				$cek = mysql_num_rows(mysql_query("select * from m_singkatan where empkostl='".$lev['empkostl']."'"));
				$abbr = substr($lev['emp_hrp1000_o_short'],0,5);
				$zhr12 = mysql_fetch_array(mysql_query("SELECT ".$dbname2.".zhrom0012.* from ".$dbname2.".zhrom0012 where noorg='".$abbr."' and gol='".$lev['emppersk']."' GROUP BY noorg ORDER BY noorg asc"));
				echo'
				<tr bgcolor="'.$cl.'" onClick="ada(\''.$lev['empkostl'].'\' , \''.$lev['emp_cskt_ltext'].'\' , \''.$zhr12['noorg'].'\' , \''.$zhr12['unitkerja'].'\')">
					<td align="center">'.$i.'</td>
					<td>'.$lev['empnik'].'</td>
					<td>'.$lev['empname'].'</td>
					<td>'.$lev['emp_hrp1000_s_short'].'</td>
					<td>'.$lev['empkostl'].'</td>
					<td>'.$zhr12['unitkerja'].'</td> 
					<td>'.$zhr12['namajabatan'].'</td> 
					<td align="center">
				';
						if($cek>0){
							echo'<img src="../../../../images/chek.png" width="20" height="20" border="0">';
						}else{
							echo'<img src="../../../../images/pending.png" width="20" height="20" border="0">';
						}
				echo'
					</td> 
				</tr>';
				$i++;
			}
			@mysql_free_result($rs);

    	?>
	</table>
</div>
<div id="naf">
	<?
		if($_REQUEST['group_id']==6 or $_REQUEST['group_id']==7){
			$rsql = "SELECT
					".$dbname2.".structdireksi.*,
					".$dbname.".sys_groupuser.*,
					".$dbname.".sys_user.*
				FROM
					".$dbname2.".structdireksi
					INNER JOIN ".$dbname.".sys_groupuser ON ".$dbname.".sys_groupuser.pos_abbr = ".$dbname2.".structdireksi.emp_hrp1000_s_short
					INNER JOIN ".$dbname.".sys_user ON ".$dbname.".sys_groupuser.userid = ".$dbname.".sys_user.userid
				WHERE
					".$dbname2.".structdireksi.`no` = '1' AND
					".$dbname.".sys_groupuser.userid='".$_REQUEST['user']."'
				group by ".$dbname2.".structdisp.empnik
				ORDER BY ".$dbname2.".structdireksi.emp_hrp1000_s_short ASC";
		}else{
			$rsql = "SELECT
						".$dbname2.".structdisp.*,
						".$dbname.".sys_groupuser.*,
						".$dbname.".sys_user.*
					FROM
						".$dbname2.".structdisp
						INNER JOIN ".$dbname.".sys_groupuser ON ".$dbname.".sys_groupuser.pos_abbr = ".$dbname2.".structdisp.emp_hrp1000_s_short
						INNER JOIN ".$dbname.".sys_user ON ".$dbname.".sys_groupuser.userid = ".$dbname.".sys_user.userid
					WHERE
						".$dbname2.".structdisp.`no` = '1' AND
						".$dbname.".sys_groupuser.userid='".$_REQUEST['user']."'
					group by ".$dbname2.".structdisp.empnik
					ORDER BY ".$dbname2.".structdisp.emp_hrp1000_s_short ASC";
		}
		$hd		= "";
		$hal	= 0;
		$order 	= "";
		$pager 	= new ajaxpagination($conn, $rsql, $hd, $hal, 15, 15, $order);
		$rs = $pager->paginate();	
		include_once('../../../../includes/nav.php');
	?>
</div>
<script type="text/javascript" >
    $(document).ready( function() {
    	$('#naf a').click(function() {
    		$("#view").load("../../../../modules/master/singkatan/ajax/popfindcc_detil.php", {'hal': $(this).attr("title"), 'mod' : $("#txtmod").val(), 'user': $("#user").val(), 'group_id': $("#group_id").val() });
    		$('#naf').fadeOut('slow').load("../../../../modules/master/singkatan/ajax/popfindcc_page.php", {'hal':$(this).attr("title"), 'user': $("#user").val(), 'group_id': $("#group_id").val() }).fadeIn("slow");
    	}); 
    	$('#cari').click(function() {
    		var a = $("#txtval").val();
    		var pilih = $("#pilihcari").val();
    		if (a=='' || pilih==''){
    			alert("Masukkan nilai yang akan dicari!");
    		}else{
    			$("#view").load("../../../../modules/master/singkatan/ajax/popfindcc_detil.php", {'pilih': pilih, 'nm': a, 'user': $("#user").val(), 'group_id': $("#group_id").val() });
    			$('#naf').fadeOut('slow').load("../../../../modules/master/singkatan/ajax/popfindcc_page.php", {'hal':$(this).attr("title"), 'nm':a, 'user': $("#user").val(), 'group_id': $("#group_id").val() }).fadeIn("slow");
    		}
    		e.stopImmediatePropagation();
    	}); 
    }); 
    function ada(empkostl, emp_cskt_ltext, abbr_unit, namaunit){ 
    	parent.$("#empkostl").val(empkostl);
    	parent.$("#emp_cskt_ltext").val(emp_cskt_ltext);
    	parent.$("#abbr_unit").val(abbr_unit);
    	parent.$("#namaunit").val(namaunit);
    	parent.$.colorbox.close(); 
    }
</script>