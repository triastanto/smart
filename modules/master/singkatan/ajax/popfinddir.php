<?
    session_start();
    include_once('../../../../config.php');
    include_once('../../../../includes/functions.php');
    include_once('../../../../includes/koneksi.php');
    include_once('../../../../includes/ajaxpagination.php');
?>
    <script type="text/javascript" src="../../../../javascript/jquery-1.8.3.min.js"></script> 
    <link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/reset.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/grid.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/layout.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="../../../../templates/blue/css/nav.css" media="screen" />
    <style>
    	*{font-family:Trebuchet MS,"Arial", sans-serif;color:black; font-size:11px;}
    </style> 
<title>Search Material</title>
<table width="100%" cellpadding="2" cellspacing="2" border="0" style="border: 1px solid #DEDEDE;font-size: 10pt;" class="head_sr">
	<tr height="30">
		<td width="15%">CARI :</td>
		<td width="40%">
            <select name="pilihcaridir" id="pilihcaridir">
                <option value="">-Pilih Pencarian-</option>
                <option value="empnik">NIK</option>
                <option value="empname">Nama</option>
                <option value="emp_hrp1000_s_short">Abbreviasi</option>
                <option value="empkostl">Kode CC</option>
               <option value="emp_cskt_ltext">Cost Center</option>
            </select>&nbsp;
			<input type="text" name="txtvaldir" id="txtvaldir" size="40" value="<?=$_REQUEST['fv']?>" />			
			<input type="hidden" name="userdir" id="userdir"  value="<?=$_REQUEST['user']?>" />			
		</td>	
        <td width="38%" ><input type="button" name="button" id="caridir" value="Caridir" class='btn btn-blue' >&nbsp; 
        </td>
	</tr>
</table>
<div id="viewdir">
	<table width="100%" border="0">
		<tr class="head_tr">
			<th align="center" width="3%">NO</th>
			<th align="center" width="10%">NIK</th>
			<th align="center" width="20%">NAMA</th>
			<th align="center" width="10%">ABBREVIASI</th>
			<th align="center" width="10%">KODE CC</th>
			<th align="center" width="27%">COST CENTER</th>
			<th align="center" width="20%">JABATAN</th>
		</tr>
    	<?	
				$rsql = "SELECT ".$dbname2.".structdireksi.* FROM ".$dbname2.".structdireksi ORDER BY ".$dbname2.".structdireksi.emp_hrp1000_s_short ASC limit 0,15";
				// echo $rsql;
				$rs = mysql_query($rsql);	
				$i=1;
				while($lev=mysql_fetch_array($rs)){
					$i%2 == 0 ? $cl='#caefff' : $cl='#e4f3f9';
					$nama = str_replace("'", "`" , $lev['empname']);
					echo'
					<tr bgcolor="'.$cl.'" onClick="ada(\''.$lev['empkostl'].'\' , \''.$lev['emp_cskt_ltext'].'\' , \''.$lev['emp_hrp1000_o_short'].'\' , \''.$lev['emportx'].'\')">
						<td align="center">'.$i.'</td>
						<td align="left">'.$lev['empnik'].'</td>
						<td align="left">'.$nama.'</td>
						<td align="left">'.$lev['emp_hrp1000_s_short'].'</td>
						<td align="left">'.$lev['empkostl'].'</td>
						<td align="left">'.$lev['emportx'].'</td> 
						<td align="left">'.$lev['emppostx'].'</td> 
					</tr>';
					$i++;
				}
				@mysql_free_result($rs);
			
		?>
	</table>
</div>
<div id="nafdir">
	<?
		$rsql 	= "SELECT ".$dbname2.".structdireksi.* FROM ".$dbname2.".structdireksi ORDER BY ".$dbname2.".structdireksi.emp_hrp1000_s_short ASC";
		$hd		= "";
		$hal	= 0;
		$order 	= "";
		$pager 	= new ajaxpagination($conn, $rsql, $hd, $hal, 15, 15, $order);
		$rs = $pager->paginate();	
		include_once('../../../../includes/nav3.php');
	?>
</div>
<script type="text/javascript" >
    $(document).ready( function() {
    	$('#nafdir a').click(function() {
    		var a = $("#txtvaldir").val();
    		var pilih = $("#pilihcaridir").val();
    		var user = $("#userdir").val();
    		$("#view").load("../../../../modules/surat/suratkeluar/ajax/popfinddir_detil.php", {'hal': $(this).attr("title"), 'pilih': pilih, 'nm': a, 'user': user });
    		$('#nafdir').fadeOut('slow').load("../../../../modules/surat/suratkeluar/ajax/popfinddir_page.php", {'hal':$(this).attr("title"), 'pilih': pilih, 'nm': a, 'user': user }).fadeIn("slow");
    		// alert('a');
    	}); 
    	$('#caridir').click(function() {
    		var a = $("#txtvaldir").val();
    		var pilih = $("#pilihcaridir").val();
    		var user = $("#userdir").val();
    		if (a=='' || pilih==''){
    			alert("Masukkan nilai yang akan dicari!");
    		}else{
    			// alert("tes");
    			$("#viewdir").load("../../../../modules/surat/suratkeluar/ajax/popfinddir_detil.php", {'pilih': pilih, 'nm': a, 'user': user });
    			$('#nafdir').fadeOut('slow').load("../../../../modules/surat/suratkeluar/ajax/popfinddir_page.php", {'hal':$(this).attr("title"), 'pilih': pilih, 'nm': a, 'user': user}).fadeIn("slow");
    		}
    		e.stopImmediatePropagation();
    		// alert('b');
    	}); 
    }); 
    function ada(empkostl, emp_cskt_ltext, abbr_unit, namaunit){ 
    	parent.$("#empkostl").val(empkostl);
    	parent.$("#emp_cskt_ltext").val(emp_cskt_ltext);
    	parent.$("#abbr_unit").val(abbr_unit);
    	parent.$("#namaunit").val(namaunit);
    	parent.$.colorbox.close(); 
    }
</script>