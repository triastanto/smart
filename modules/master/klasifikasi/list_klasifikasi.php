<?
out("title", "DATA KLASIFIKASI");
?>

<table border="0" cellpadding="1" cellspacing="1" width="100%">
	<tr bgcolor="#EEEEEE">
		<td >
			<form action="" name="frmaction" method="post">						
				<span>Cari : <input type="text" name="txtval" id="txtval" size="40"  /></span>
				<span class="level">
					<a class="btn btn-blue" id="cari"><i class="ion-android-search">&nbsp;</i><b>Cari</b></a>
			        &nbsp;<input type="button" id="refresh" name="refresh" class="btn btn-blue" style="width:80px;" value="Refresh" />
            	</span>
			</form>
		</td>
		<td width="20%" align="right">
			<?
				echo'
					<input class="btn btn-blue" style="width:150px;" type="button" name="tambah " value="Tambah Klasifikasi" onclick="window.location=\'?mod='.base64_encode('master/klasifikasi').'&op='.base64_encode('input_klasifikasi').' \'" />
				';
			?>
		</td>
	</tr>
</table>

<div id="view" style="border:1px solid white;width:100%;overflow-y:hidden;overflow-x:scroll;">
	<table width="100%" border="0">
		<tr>
			<th align="center" width="5%">NO</th>
			<th align="center" width="20%">Kode Klasifikasi</th>
			<th align="center" width="70%">Klasifikasi</th>
			<th align="center" width="5%" colspan="2">Option</th>
		</tr>
	<?
		$rsql = "SELECT * from m_klasifikasi order by kodeklasifikasi limit 0,10 ";
		$rs = mysql_query($rsql);	
		$i=0;
		while($lev=mysql_fetch_array($rs)){
			echo'
			<tr bgcolor="'.rowClass(++$i).'" class="bariswarna">
				<td align="center">'.$i.'</td>
				<td align="left">'.$lev['kodeklasifikasi'].'</td>
				<td align="left">'.$lev['klasifikasi'].'</td>
				<td align="center">
					<a href="?mod='.base64_encode('master/klasifikasi').'&op='.base64_encode('input_klasifikasi').'&act='.base64_encode('edit').'&idp='.base64_encode($lev['kodeklasifikasi']).'"><img src="images/edit.png" width="15" height="15" border="0"></a>
				</td> 
				<td align="center">
    				<a href="?mod='.base64_encode('master/klasifikasi').'&op='.base64_encode('delete_klasifikasi').'&act='.base64_encode('delete').'&idp='.base64_encode($lev['kodeklasifikasi']).'" onclick="if(confirm(\'Apakah Data akan dihapus ?\')){return true;}{return false;}" ><img src="images/del.gif" title="Delete" width="15" height="15" border="0"></a>
    			</td>
			</tr>';
		}
		@mysql_free_result($rs);	
	?>
	</table>
</div>
<div id="naf">
	<?
		$rsql 	= "SELECT * from m_klasifikasi ";
		$hd		= "";
		$hal	= 0;
		$order 	= " order by kodeklasifikasi";
		$pager 	= new ajaxpagination($conn, $rsql, $hd, $hal, 10, 10, $order);
		$rs = $pager->paginate();	
		include_once('includes/nav.php');
	?>
</div>
<script type="text/javascript" >
	$(document).ready( function() {
		$('#naf a').click(function() {
			//alert('a');
			$("#view").load("modules/master/klasifikasi/list_klasifikasi_detil.php", {'hal': $(this).attr("title")});
			$('#naf').fadeOut('slow').load("modules/master/klasifikasi/list_klasifikasi_page.php", {'hal':$(this).attr("title")}).fadeIn("slow");
		});
		
		$('#cari').click(function() {
			var a = $("#txtval").val();
			if (a==''){
				alert("Masukkan nilai yang akan dicari!");
			}else{
				$("#view").load("modules/master/klasifikasi/list_klasifikasi_detil.php", {'nm': a });
				$('#naf').fadeOut('slow').load("modules/master/klasifikasi/list_klasifikasi_page.php", {'hal':$(this).attr("title"), 'nm':a}).fadeIn("slow");
			} 
		});
      	$('#refresh').click(function()
        {
    	   var a = ''; 
    	   $("#view").load("modules/master/klasifikasi/list_klasifikasi_detil.php", {'hal': $(this).attr("title"),'nm': a }); 
    	   $('#naf').fadeOut('slow').load("modules/master/klasifikasi/list_klasifikasi_page.php", {'hal':$(this).attr("title"),'nm': a}).fadeIn("slow");
    	});
	});
</script>