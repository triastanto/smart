
$(document).ready(function() {
	
	// alert('Maaf SMART Mobile Sedang Dalam Perbaikan');
	// return false;
	
	
    cek_versi();
    var userstorage =localStorage.getItem("namauser");
    get_page_index(userstorage);
    
    
    });
function geturl()
{
   var url ="http://10.10.9.59:212/api_smart/";
   //var url ="https://sso.krakatausteel.com/api_smart/"; 
//var url ="http://dev.krakatausteel.com/api_smart/"; 
return url;
}
function read_karyawan(){
     var userstorage =localStorage.getItem("namauser");
     $('#daftar-karyawan').DataTable({
            "processing": true,
            "serverSide": true,
            "responsive": true,
            "searching": true,
            "bDestroy": true,
            "order": [[ 1, "desc" ]],
            "ajax": {
                "url":geturl()+"mobile/readkaryawan",
                "type": "post"
            },
            "columns": [{
            "className": 'options',
            "data": "empnik",
            "render": function(data, type, full, meta){
                return '<p onclick="pilihkaryawan();">'+data+'</p>';
                }
            },{
            "className": 'options',
            "data": "empname",
            "render": function(data, type, full, meta){
                return '<p onclick="pilihkaryawan();">'+data+'</p>';
                }
            },{
            "className": 'options',
            "data": "emp_cskt_ltext",
            "render": function(data, type, full, meta){
                return '<p onclick="pilihkaryawan();">'+data+'</p>';
                }
            },{
            "className": 'options',
            "data": "emppostx",
            "render": function(data, type, full, meta){
                return '<p onclick="pilihkaryawan();">'+data+'</p>';
                }
            }
            /*{"data": "empnik"},{"data": "empname"},{"data": "emp_cskt_ltext"},{"data": "emppostx"}
            ,{
            "className": 'options',
            "data": "empnik",
            "render": function(data, type, full, meta){
                return '<a class="btn btn-primary btn-icon btn-circle btn-lg"><i class="fa fa-check" onclick="pilihkaryawan();"></i></a>';
                }
        }*/]
        });
}
function read_direksi(){
     var userstorage =localStorage.getItem("namauser");
     $('#daftar-karyawan').DataTable({
            "processing": true,
            "serverSide": true,
            "responsive": true,
            "searching": true,
            "bDestroy": true,
            "order": [[ 1, "desc" ]],
            "ajax": {
                "url":geturl()+"mobile/readdireksi",
                "type": "post"
            },
            "columns": [{
            "className": 'options',
            "data": "empnik",
            "render": function(data, type, full, meta){
                return '<p onclick="pilihkaryawan();">'+data+'</p>';
                }
            },{
            "className": 'options',
            "data": "empname",
            "render": function(data, type, full, meta){
                return '<p onclick="pilihkaryawan();">'+data+'</p>';
                }
            },{
            "className": 'options',
            "data": "emp_cskt_ltext",
            "render": function(data, type, full, meta){
                return '<p onclick="pilihkaryawan();">'+data+'</p>';
                }
            },{
            "className": 'options',
            "data": "emppostx",
            "render": function(data, type, full, meta){
                return '<p onclick="pilihkaryawan();">'+data+'</p>';
                }
            }
            /*{"data": "empnik"},{"data": "empname"},{"data": "emp_cskt_ltext"},{"data": "emppostx"}
            ,{
            "className": 'options',
            "data": "empnik",
            "render": function(data, type, full, meta){
                return '<a class="btn btn-primary btn-icon btn-circle btn-lg"><i class="fa fa-check" onclick="pilihkaryawan();"></i></a>';
                }
        }*/]
        });
}
function pilihkaryawan(){
    var userstorage =localStorage.getItem("namauser");
    var table = $('#daftar-karyawan').DataTable();
        $('#daftar-karyawan tbody').on('click', 'tr', function() {
            var data_kar = table.row(this).data();
            //alert(data_kar.empnik);
            var html='';
    
            $('.viewdisposisi').show();
            $(".carikaryawan").hide();
            if($('#'+data_kar.empnik+'').val()==''||$('#'+data_kar.empnik+'').val()==null){
                $('#tempdisposisi tbody').append(`
                      <tr id="input_${ data_kar.empnik }">
                      <td align="left">${ data_kar.empnik }<input type="hidden" value="${ data_kar.empnik }" name="empnik[]" id="${ data_kar.empnik }" class="form-control" readonly></td>
                      <td align="left">${ data_kar.empname }<input type="hidden" value="${ data_kar.empname }" name="empname[]" class="form-control" readonly></td>
                      <td align="left" class="combopesan"></td>
                      <td align="left"><input type="hidden" value="${ data_kar.emp_cskt_ltext }" name="emp_cskt_ltext[]" class="form-control" readonly><input type="hidden" value="${ data_kar.empposid }" name="empposid[]" class="form-control" readonly><input type="hidden" value="${ data_kar.emp_hrp1000_s_short }" name="emp_hrp1000_s_short[]" class="form-control" readonly><input type="hidden" value="${ data_kar.empkostl }" name="empkostl[]" class="form-control" readonly><input type="hidden"  value="${ data_kar.emppersk }" name="emppersk[]" class="form-control" readonly>
                      <a href="javascript:;" class="btn btn-warning btn-block" onclick="hapus_temp_disposisi(${ data_kar.empnik });"><i class="fa fa-trash"></a></td>
                    </tr>
                  `);
            }else{
                alert('Nik '+data_kar.empnik+' atas nama '+data_kar.empname+' sudah ada di list !');
            }
            get_combo_pesan();
           
            });
   
    
}
function get_combo_pesan(){
    $.ajax({
            url:geturl()+'mobile/pesan_combo2',
            method: 'POST',
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                $(".combopesan").empty();
                $(".combopesan").html(data);

           }
        });
    
}
function hapus_temp_disposisi(e){
    if(confirm("Apakah anda yakin ?")){
        $("#input_"+e).remove();
    }
    else{
        return false;
    }
}
function hapus_disposisi(kode,nik,kodeatas){var userstorage =localStorage.getItem("namauser");
       var r = confirm("Apakah anda yakin ?");
    if (r == true) {
        $.ajax({
            url:geturl()+"mobile/hapus_disposisi/"+kode+"/"+nik,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                if(data==1){
                    $(".loading").hide();
                    //alert('Disposisi sukses dihapus !');
                    var tempdisposisi = $('#tempdisposisi tbody').html();
                    localStorage.setItem("tempdispos", tempdisposisi);
                    //alert(localStorage.getItem("tempdispos"));
                    tambah_disposisi2(kodeatas);
                    //$('#tempdisposisi tbody').html(localStorage.getItem("tempdispos"));
                    console.log(localStorage.getItem("tempdispos"));
                    //alert(localStorage.getItem("tempdispos"));
                }else{
                    $(".loading").hide();
                    alert('Disposisi gagal dihapus !');
                }

           }
        });
                    
    }
}
function simpan_disposisi(kode){
    //var file_data = $('#file').prop('files')[0];
    //var form_data = new FormData();
    //form_data.append('file', file_data);
    //console.log(file_data);
    var data_val = $('#form_disposisi,#form_list_dispos,#form_dis_pesan').serializeArray();
    
    var r = confirm("Apakah data anda sudah benar ?");
    if (r == true) {
        $.ajax({
            url:geturl()+'mobile/simpan_disposisi/'+kode,
            method: 'POST',
            data: data_val,
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                if(data==1){
                    alert('Data sukses disimpan !');
                    $('.email-content').show();
                    $(".loading").hide();
                    $(".table-responsive").show();
                    $(".viewdisposisi").hide();
                    $(".viewdisposisi").empty();
                    
                    var table = $('#surat-masuk').DataTable();
                    table.ajax.reload(null, false);
                }else{
                    $(".loading").hide();
                    alert('Data gagal disimpan !');
                }

           }
        });
    } 
}
function struk_karyawan(){
    read_karyawan();
}
function struk_direksi(){
    read_direksi();
}
function cari_karyawan(){
    
    //var html='';
    //html+='<table class="table table-email" id="daftar-karyawan"><thead><tr><th data-priority="persist">Nik</th><th data-priority="1">Nama</th><th data-priority="2">Cos Center</th><th data-priority="3">Jabatan</th></tr></thead></table>';
    //html+='<a href="javascript:;" class="btn btn-danger btn-block" onclick="close_cari_karyawan();"><i class="fa fa-times pull-right"></i> Close</a>';
    $.ajax({
            url:geturl()+'mobile/carikaryawan',
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                $('.viewdisposisi').hide();
                $(".loading").hide();
                $(".table-responsive").hide();
                $(".carikaryawan").show();
                $(".carikaryawan").empty();
                $(".carikaryawan").html(data);
                //read_karyawan();
                //read_direksi();

           }
        });
    
    /*$.ajax({
            url:geturl()+'mobile/cari_karyawan',
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                $('.viewdisposisi').hide();
                $(".loading").hide();
                $(".table-responsive").hide();
                $(".carikaryawan").show();
                $(".carikaryawan").empty();
                html='';
                var html+='<table class="table table-email" id="daftar-karyawan"><thead><tr><th data-priority="persist">Nik</th><th data-priority="1">Nama</th><th data-priority="2">Cos Center</th></tr></thead></table>';
                
                $(".carikaryawan").html(html);

           }
        });*/
}
function close_cari_karyawan(){
    $(".viewdisposisi").show();
    $(".carikaryawan").hide();
}
function view_disposisi(kode){
    $.ajax({
            url:geturl()+'mobile/get_disposisi/'+kode,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                $('.viewmasuk').hide();
                $(".loading").hide();
                $(".table-responsive").hide();
                $(".viewdisposisi").show();
                $(".viewdisposisi").empty();
                $(".viewdisposisi").html(data);

           }
        });
}
function close_disposisi(){
        $(".viewmasuk").show();
        $(".viewdisposisi").hide();
    }

function reload_index(){
    delete_session();
    var userstorage =localStorage.getItem("namauser");
    get_page_index(userstorage);
}
function view_pedoman(e){
    var url="http://sso.krakatausteel.com/arsip/home/"+e;
            window.open(url);
}
function pedoman_kearsipan(){
    var grup ='1';
    $.ajax({
            url:geturl()+'mobile/get_pedoman_kearsipan/'+grup,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                $(".loading").hide();
                $("#listpedoman").hide();
                $("#detailpedoman").show();
                $("#detailpedoman").empty();
                $("#detailpedoman").html(data);

           }
        });
   
}
function petunjuk_penggunaan(){
    var grup ='2';
    $.ajax({
            url:geturl()+'mobile/get_pedoman_kearsipan/'+grup,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                $(".loading").hide();
                $("#listpedoman").hide();
                $("#detailpedoman").show();
                $("#detailpedoman").empty();
                $("#detailpedoman").html(data);

           }
        });
    
}
function informasi_kearsipan(){
    var grup ='3';
    $.ajax({
            url:geturl()+'mobile/get_pedoman_kearsipan/'+grup,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                $(".loading").hide();
                $("#listpedoman").hide();
                $("#detailpedoman").show();
                $("#detailpedoman").empty();
                $("#detailpedoman").html(data);

           }
        });
    
}
function close_pedoman(){
    $("#listpedoman").show();
    $("#detailpedoman").hide();
}

function get_pedoman(){
    $.post(geturl()+'mobile/get_pedoman', function(data, textStatus, xhr) {
            $("#listpedoman").empty();
            $("#listpedoman").html(data);
            
        });
}
function get_page_index(userstorage){
    get_loading();
    //var userstorage =localStorage.getItem("namauser");
    $.post(geturl()+'mobile/get_page_index/'+userstorage, function(data, textStatus, xhr) {
            $("#dashboard").empty();
            $("#dashboard").html(data);
            
        });
}
function get_loading(){
    $(".loading").html('<div  class="progress progress-striped active" ><div class="progress-bar" style="width: 100%">Sedang Memproses...</div></div>');
}
function previewsuratkeluar(id){
    $.mobile.changePage( "memosuratkeluar.html", { transition: "pop"} );
}
function close_viewmasuk(){
    $('.email-content').show();
    $('.viewmasuk').hide();
}
function close_view(){
    $('.email-content').show();
    $('.viewsurat').hide();
}
function view_masuk(kode){
    var userstorage =localStorage.getItem("namauser");
    $.ajax({
            url:geturl()+'mobile/get_view_masuk/'+kode+'/'+userstorage,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                $('.email-content').hide();
                $('.viewmasuk').show();
                
                $(".viewmasuk").html(data);
                var table = $('#surat-masuk').DataTable();
                table.ajax.reload(null, false);
                get_page_index(userstorage);
                $(".loading").hide();
                

           }
        });
}
/*function view_masuk(kode,sifat,jenis,grupid,status){
    
    var userstorage =localStorage.getItem("namauser");
    $.ajax({
            url:geturl()+'mobile/get_base_encode/'+kode+'/'+userstorage,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                console.log(data);
                //var url="http://sso.krakatausteel.com/arsip/modules/report/label/memodinasmasuk_view.php?idp="+data;//"http://dev.krakatausteel.com/arsip/modules/report/label/memodinasmasuk.php?idp="+data;
                //window.open(url);
                $('.email-content').hide();
                $('.viewmasuk').show();
                var html ='';
                if(jenis=='2'){
                    html +='<object type="text/html" style="float:left;width:100%;height:500px;background-color:white;" data="http://sso.krakatausteel.com/arsip/modules/surat/suratmasuk/download.php?idp='+data+'"></object><br>';
                
                }else{
                    html +='<object type="text/html" style="float:left;width:100%;height:500px;background-color:white;" data="http://sso.krakatausteel.com/arsip/modules/report/label/memodinasmasuk_view.php?idp='+data+'"></object><br>';
                
                }
                //var url="http://sso.krakatausteel.com/arsip/modules/surat/suratmasuk/download.php?idp="+data;
                html +='<a href="javascript:;" class="btn btn-danger btn-block" onclick="close_viewmasuk();"><i class="fa fa-times pull-right"></i> Close</a>';
                if(grupid=='4'&&status!='3'){
                    html +='<a href="javascript:;" class="btn btn-success btn-block" onclick="tambah_disposisi(\'' +kode+ '\');"><i class="fa fa-plus pull-right"></i> Tambah Disposisi</a>';
                
                }else{
                    html+='';
                }
                
                //html +='<a href="javascript:;" class="btn btn-primary btn-block" onclick="view_disposisi(\'' +kode+ '\');"><i class="fa fa-search pull-right"></i> View Disposisi</a>';
                html +='<a href="javascript:;" class="btn btn-warning btn-block" onclick="cetak_disposisi(\'' +kode+ '\');"><i class="fa fa-search pull-right"></i> View Disposisi</a>';
                
                $(".viewmasuk").html(html);
                var table = $('#surat-masuk').DataTable();
                table.ajax.reload(null, false);
                get_page_index(userstorage);
                $(".loading").hide();

           }
        });
    
}*/
function tambahdisp(){
    $(".viewdisposisi").empty();
    var html='';
    html+='<div role="main" class="ui-content jqm-content"><div><form class="form-horizontal" id="fm-disposisi" data-parsley-validate="true">';
    html+='<label for="tanggalpenyelesaian">Tanggal Penyelesaian :</label><input style="width:100%;" type="date" name="tanggalpenyelesaian" id="tanggalpenyelesaian" placeholder="Tanggal Penyelesaian">';
    html+='<label for="catatan">Catatan :</label><textarea style="width:100%;" name="catatan" id="catatan"></textarea>';
    html+='</form></div></div>';
    $(".viewdisposisi").html(html);
}
function tambah_disposisi(kode){
    localStorage.removeItem("tempdispos");
    var userstorage =localStorage.getItem("namauser");
    $.ajax({
            url:geturl()+'mobile/get_disposisi_by/'+kode+'/'+userstorage,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                $('.viewmasuk').hide();
                $(".loading").hide();
                $(".table-responsive").hide();
                $(".viewdisposisi").show();
                $(".viewdisposisi").empty();
                var html='';
                html+=data;
                
                $(".viewdisposisi").html(html);
                $('#tempdisposisi tbody').html(localStorage.getItem("tempdispos"));                

           }
        });
}
function tambah_disposisi2(kode){
    var userstorage =localStorage.getItem("namauser");
    $.ajax({
            url:geturl()+'mobile/get_disposisi_by/'+kode+'/'+userstorage,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                $('.viewmasuk').hide();
                $(".loading").hide();
                $(".table-responsive").hide();
                $(".viewdisposisi").show();
                $(".viewdisposisi").empty();
                var html='';
                html+=data;
                $(".viewdisposisi").html(html);
                $('#tempdisposisi tbody').html(localStorage.getItem("tempdispos"));                

           }
        });
}
function view_edaran(kode,validasiparaf){
    //alert(validasiparaf);
    console.log(validasiparaf);
    var userstorage =localStorage.getItem("namauser");
    $.ajax({
            url:geturl()+'mobile/get_base_encode_2/'+kode,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                $('.email-content').hide();
                $('.viewsurat').show();
                var html ='';
                html +='<object type="text/html" style="float:left;width:100%;height:500px;background-color:white;" data="http://sso.krakatausteel.com/arsip/modules/report/label/viewedaran.php?idp='+data+'"></object><br>';
                html +='<a href="javascript:;" class="btn btn-danger btn-block" onclick="close_view();"><i class="fa fa-times pull-right"></i> Close</a>';
                $(".viewsurat").html(html);
                var table = $('#surat-keluar').DataTable();
                table.ajax.reload(null, false);
                get_page_index(userstorage);
                $(".loading").hide();
                

           }
        });
   
}
function view(kode){
    var userstorage =localStorage.getItem("namauser");
    $.ajax({
            url:geturl()+'mobile/get_view_suratkeluar/'+kode+'/'+userstorage,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                $('.email-content').hide();
                $('.viewsurat').show();
                
                $(".viewsurat").html(data);
                var table = $('#surat-keluar').DataTable();
                table.ajax.reload(null, false);
                get_page_index(userstorage);
                $(".loading").hide();
                

           }
        });
}
/*function view(kode,validasiparaf,jenis,grupid,sifat){
    //alert(jenis);
    console.log(jenis+'-'+grupid+'-'+sifat);
    var userstorage =localStorage.getItem("namauser");
    $.ajax({
            url:geturl()+'mobile/get_base_encode_2/'+kode,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                //var url="http://sso.krakatausteel.com/arsip/modules/report/label/memodinaskeluar_view.php?idp="+data;
                //window.open(url);
                $('.email-content').hide();
                $('.viewsurat').show();
                var html ='';
                if(grupid=='1'||grupid=='2'||grupid=='4'||grupid=='5'||grupid=='7'){
                    if(jenis=='1'){
                        html +='<object type="text/html" style="float:left;width:100%;height:500px;background-color:white;" data="http://sso.krakatausteel.com/arsip/modules/report/label/memodinaskeluar_view.php?idp='+data+'"></object><br>';
                
                    }else if(jenis=='2'){
                        html +='<object type="text/html" style="float:left;width:100%;height:500px;background-color:white;" data="http://sso.krakatausteel.com/arsip/modules/report/label/memodinaskeluar_view.php?idp='+data+'"></object><br>';
                
                    }else{
                        html +='<object type="text/html" style="float:left;width:100%;height:500px;background-color:white;" data="http://sso.krakatausteel.com/arsip/modules/report/label/memodinaskeluaratas_view.php?idp='+data+'"></object><br>';
                
                    }
                }else{
                    if(sifat=='3'){
                        html +='<object type="text/html" style="float:left;width:100%;height:500px;background-color:white;" data="http://sso.krakatausteel.com/arsip/modules/report/label/memodinaskeluar_view.php?idp='+data+'"></object><br>';
                
                    }else{
                        if(jenis=='1'){
                            html +='<object type="text/html" style="float:left;width:100%;height:500px;background-color:white;" data="http://sso.krakatausteel.com/arsip/modules/report/label/memodinaskeluar_view.php?idp='+data+'"></object><br>';
                
                        }else if(jenis=='2'){
                        html +='<object type="text/html" style="float:left;width:100%;height:500px;background-color:white;" data="http://sso.krakatausteel.com/arsip/modules/report/label/memodinaskeluar_view.php?idp='+data+'"></object><br>';
                
                        }else{
                            html +='<object type="text/html" style="float:left;width:100%;height:500px;background-color:white;" data="http://sso.krakatausteel.com/arsip/modules/report/label/memodinaskeluaratas_view.php?idp='+data+'"></object><br>';
                    
                        }
                    }
                }
                html +='<a href="javascript:;" class="btn btn-danger btn-block" onclick="close_view();"><i class="fa fa-times pull-right"></i> Close</a>';
                html +='<a href="javascript:;" class="btn btn-primary btn-block" onclick="riwayat(\'' +kode+ '\');"><i class="fa fa-search pull-right"></i> Riwayat Perubahan</a>';
                if(validasiparaf==1){
                 html +='<a href="javascript:;" class="btn btn-success btn-block" ><i class="fa fa-check-square-o pull-right"></i> Sudah Divalidasi</a>';
                   
                }
                if(validasiparaf==2){
                 html +='<a href="javascript:;" class="btn btn-danger btn-block" ><i class="fa fa-times pull-right"></i> Menolak Divalidasi</a>';
                   
                }if(validasiparaf==0){
                 html +='<a href="javascript:;" class="btn btn-warning btn-block" ><i class="fa fa-gear pull-right"></i> Menunggu Divalidasi</a>';
                    
                }
                $(".viewsurat").html(html);
                var table = $('#surat-keluar').DataTable();
                table.ajax.reload(null, false);
                get_page_index(userstorage);
                $(".loading").hide();
                

           }
        });
   
}*/
function view_paraf(kode){
    var userstorage =localStorage.getItem("namauser");
    $.ajax({
            url:geturl()+'mobile/get_view_paraf/'+kode+'/'+userstorage,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                $('.email-content').hide();
                $('.viewsurat').show();
                
                $(".viewsurat").html(data);
                var table = $('#validasi-paraf').DataTable();
                table.ajax.reload(null, false);
                get_page_index(userstorage);
                $(".loading").hide();
                

           }
        });
}
/*function view_paraf(kode){
    //alert(validasiparaf);
    console.log(validasiparaf);
    var userstorage =localStorage.getItem("namauser");
    $.ajax({
            url:geturl()+'mobile/get_base_encode_2/'+kode,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                //var url="http://sso.krakatausteel.com/arsip/modules/report/label/memodinaskeluar_view.php?idp="+data;
                //window.open(url);
                $('.email-content').hide();
                $('.viewsurat').show();
                var html ='';
                html +='<object type="text/html" style="float:left;width:100%;height:500px;background-color:white;" data="http://sso.krakatausteel.com/arsip/modules/report/label/memodinaskeluar_view.php?idp='+data+'"></object><br>';
                html +='<a href="javascript:;" class="btn btn-danger btn-block" onclick="close_view();"><i class="fa fa-times pull-right"></i> Close</a>';
                html +='<a href="javascript:;" class="btn btn-primary btn-block" onclick="riwayat(\'' +kode+ '\');"><i class="fa fa-search pull-right"></i> Riwayat Perubahan</a>';
                if(validasiparaf==1){
                 html +='';
                   
                }
                if(validasiparaf==2){
                 html +='';
                   
                }if(validasiparaf==0){
                 html +='<a href="javascript:paraf(\'' +kode+ '\');" class="btn btn-warning btn-block" ><i class="fa fa-pencil-square pull-right"></i> Edit Format</a>';
                    
                }
                if(validasiparaf==1){
                 html +='<a href="javascript:;" class="btn btn-success btn-block" ><i class="fa fa-check-square-o pull-right"></i> Paraf sudah disetujui</a>';
                   
                }
                if(validasiparaf==2){
                 html +='';
                   
                }if(validasiparaf==0){
                 html +='<a href="javascript:setuju_paraf(\'' +kode+ '\');" class="btn btn-success btn-block" ><i class="fa fa-check-square-o pull-right"></i> Setuju Paraf</a>';
                    
                }
                if(validasiparaf==1){
                 html +='';
                   
                }
                if(validasiparaf==2){
                 html +='<a href="javascript:;" class="btn btn-warning btn-block" ><i class="fa fa-minus pull-right"></i> Paraf tidak disetujui</a>';
                   
                }if(validasiparaf==0){
                 html +='<a href="javascript:tolak_paraf(\'' +kode+ '\');" class="btn btn-warning btn-block" ><i class="fa fa-minus pull-right"></i> Tolak Paraf</a>';
                    
                }
                $(".viewsurat").html(html);
                var table = $('#validasi-paraf').DataTable();
                table.ajax.reload(null, false);
                get_page_index(userstorage);
                $(".loading").hide();
                

           }
        });
   
}*/
function view_validasi(kode){
    var userstorage =localStorage.getItem("namauser");
    $.ajax({
            url:geturl()+'mobile/get_view_validasi/'+kode+'/'+userstorage,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                $('.email-content').hide();
                $('.viewsurat').show();
                
                $(".viewsurat").html(data);
                var table = $('#validasi-paraf').DataTable();
                table.ajax.reload(null, false);
                get_page_index(userstorage);
                $(".loading").hide();
                

           }
        });
}
/*function view_validasi(kode,validasiparaf,jmlsudahparaf){
    //alert(validasiparaf);
    console.log(validasiparaf);
    var userstorage =localStorage.getItem("namauser");
    $.ajax({
            url:geturl()+'mobile/get_base_encode_2/'+kode,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                //var url="http://sso.krakatausteel.com/arsip/modules/report/label/memodinaskeluar_view.php?idp="+data;
                //window.open(url);
                $('.email-content').hide();
                $('.viewsurat').show();
                var html ='';
                html +='<object type="text/html" style="float:left;width:100%;height:500px;background-color:white;" data="http://sso.krakatausteel.com/arsip/modules/report/label/memodinaskeluar_view.php?idp='+data+'"></object><br>';
                html +='<a href="javascript:;" class="btn btn-danger btn-block" onclick="close_view();"><i class="fa fa-times pull-right"></i> Close</a>';
                html +='<a href="javascript:;" class="btn btn-primary btn-block" onclick="riwayat(\'' +kode+ '\');"><i class="fa fa-search pull-right"></i> Riwayat Perubahan</a>';
                if(validasiparaf==1){
                    html +='';
                 
                }else{
                    html +='<a href="javascript:paraf(\'' +kode+ '\');" class="btn btn-warning btn-block" ><i class="fa fa-pencil-square pull-right"></i> Edit Format</a>';
                  
                }
                if(validasiparaf==1){
                    html +='<a href="javascript:;" class="btn btn-success btn-block" ><i class="fa fa-check-square-o pull-right"></i> Sudah Divalidasi</a>';
                 
                }else if(jmlsudahparaf==1){
                    html +='<a href="javascript:validasi(\'' +kode+ '\');" class="btn btn-warning btn-block" ><i class="fa fa-gears (alias) pull-right"></i> Validasi</a>';
                  
                }
                $(".viewsurat").html(html);
                var table = $('#validasi-paraf').DataTable();
                table.ajax.reload(null, false);
                get_page_index(userstorage);
                $(".loading").hide();
                

           }
        });
   
}*/
function cetak_disposisi(kode){
    $.ajax({
            url:geturl()+'mobile/cetak_disposisi/'+kode,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                $('.viewmasuk').hide();
                $(".loading").hide();
                $(".table-responsive").hide();
                $(".viewdisposisi").show();
                $(".viewdisposisi").empty();
                $(".viewdisposisi").html(data);

           }
        });
}
/*function cetak_disposisi(kode){
        $.ajax({
            url:geturl()+'mobile/get_base_encode_2/'+kode,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                $('.viewmasuk').hide();
                $(".loading").hide();
                $(".table-responsive").hide();
                $(".viewdisposisi").show();
                $(".viewdisposisi").empty();
                var html ='';
                html +='<object type="text/html" style="float:left;width:100%;height:500px;background-color:white;" data="http://sso.krakatausteel.com/arsip/modules/surat/suratmasuk/ajax/viewdisposisi.php?idp='+data+'"></object><br>';
                html +='<a href="javascript:;" class="btn btn-danger btn-block" onclick="close_disposisi();"><i class="fa fa-times pull-right"></i> Close</a>';
                $(".viewdisposisi").html(html);

           }
        });
        
    
}*/
function get_notif(){
    
    $(".notif").empty();
    $('#suratmasuk').html(7);
    $('#suratkeluar').html(3);
    $('#paraf').html(5);
    $('#validasi').html(9);
    
    
}
function submit_contact(){
    url =geturl()+"mobile/contactus";
     var data_val = $('#form-contact').serializeArray();
     $.post(url, data_val, function(data, textStatus, xhr) {
      if(data){
            alert(data);
      }
      else{
            var url = '';
            alert(data);
      }

    });

}
function read_paraf(){
     var userstorage =localStorage.getItem("namauser");
     $('#validasi-paraf').DataTable({
            "processing": true,
            "serverSide": true,
            "responsive": true,
            "searching": false,
            "order": [[ 1, "desc" ]],
            "ajax": {
                "url":geturl()+"mobile/readparaf/"+userstorage,
                "type": "post"
            },
            "columns": [{
                "className": 'options',
                "data": "kodesuratkeluar",
                "render": function(data, type, full, meta){
                    
                       //return '<font color="black" onclick="view_paraf(\'' +full.kodesuratkeluar+ '\',\'' +full.validasiparaf+ '\');">'+full.empnik_asal+'</font>';
                       return '<font color="black" onclick="view_paraf(\'' +full.kodesuratkeluar+ '\');">'+full.empnik_asal+'</font>';
                 
                }
            },{
                "className": 'options',
                "data": "kodesuratkeluar",
                "render": function(data, type, full, meta){
                    
                       //return '<font color="black" onclick="view_paraf(\'' +full.kodesuratkeluar+ '\',\'' +full.validasiparaf+ '\');">'+full.isiringkasan+'</font>';
                       return '<font color="black" onclick="view_paraf(\'' +full.kodesuratkeluar+ '\');">'+full.isiringkasan+'</font>';
                 
                }
            },{
                "className": 'options',
                "data": "size",
                "render": function(data, type, full, meta){
                    
                       if(data==0 || data==''){
                        return '-';
                       }else{
                        return '<a href="javascript:;" class="btn btn-warning btn-block" onclick="download_lampiran(\'' +full.kodesuratkeluar+ '\');"><i class="fa fa-download"></i></a>';
                       }
                }
            }]
        });
    }
function read_validasi(){
     var userstorage =localStorage.getItem("namauser");
     $('#validasisurat').DataTable({
            "processing": true,
            "serverSide": true,
            "responsive": true,
            "searching": false,
            "order": [[ 1, "desc" ]],
            "ajax": {
                "url":geturl()+"mobile/readvalidasi/"+userstorage,
                "type": "post"
            },
            "columns": [{
                "className": 'options',
                "data": "kodesuratkeluar",
                "render": function(data, type, full, meta){
                    
                       //return '<font color="black" onclick="view_validasi(\'' +full.kodesuratkeluar+ '\',\'' +full.validasiparaf+ '\',\'' +full.jmlsudahparaf+ '\');">'+full.empnik_asal+'</font>';
                       return '<font color="black" onclick="view_validasi(\'' +full.kodesuratkeluar+ '\');">'+full.empnik_asal+'</font>';
                 
                }
            },{
                "className": 'options',
                "data": "kodesuratkeluar",
                "render": function(data, type, full, meta){
                    
                       //return '<font color="black" onclick="view_validasi(\'' +full.kodesuratkeluar+ '\',\'' +full.validasiparaf+ '\',\'' +full.jmlsudahparaf+ '\');">'+full.isiringkasan+'</font>';
                       return '<font color="black" onclick="view_validasi(\'' +full.kodesuratkeluar+ '\');">'+full.isiringkasan+'</font>';
                 
                }
            },{
                "className": 'options',
                "data": "size",
                "render": function(data, type, full, meta){
                    
                       if(data==0 || data==''){
                        return '-';
                       }else{
                        return '<a href="javascript:;" class="btn btn-warning btn-block" onclick="download_lampiran(\'' +full.kodesuratkeluar+ '\');"><i class="fa fa-download"></i></a>';
                       }
                }
            }]
        });
    }
function read_suratkeluar(){
     var userstorage =localStorage.getItem("namauser");
     $('#surat-keluar').DataTable({
            "processing": true,
            "serverSide": true,
            "responsive": true,
            "searching": false,
            "order": [[ 1, "desc" ]],
            "ajax": {
                "url":geturl()+"mobile/readsuratkeluar/"+userstorage,
                "type": "post"
            },
            "columns": [{
                "className": 'options',
                "data": "kodesuratkeluar",
                "render": function(data, type, full, meta){
                    
                       //return '<font color="black" onclick="view(\'' +full.kodesuratkeluar+ '\',\'' +full.validasiparaf+ '\',\'' +full.jenissurat+ '\',\'' +full.group_id+ '\',\'' +full.sifatsurat+ '\');">'+full.empnik_asal+'</font>';
                        return '<font color="black" onclick="view(\'' +full.kodesuratkeluar+ '\');">'+full.empnik_asal+'</font>';
                
                }
            },{
                "className": 'options',
                "data": "kodesuratkeluar",
                "render": function(data, type, full, meta){
                    
                       //return '<font color="black" onclick="view(\'' +full.kodesuratkeluar+ '\',\'' +full.validasiparaf+ '\',\'' +full.jenissurat+ '\',\'' +full.group_id+ '\',\'' +full.sifatsurat+ '\');">'+full.isiringkasan+'</font>';
                        return '<font color="black" onclick="view(\'' +full.kodesuratkeluar+ '\');">'+full.isiringkasan+'</font>';
                
                }
            }
            ,{
                "className": 'options',
                "data": "size",
                "render": function(data, type, full, meta){
                    
                       if(data==0 || data==''){
                        return '-';
                       }else{
                        return '<a href="javascript:;" class="btn btn-warning btn-block" onclick="download_lampiran(\'' +full.kodesuratkeluar+ '\');"><i class="fa fa-download"></i></a>';
                       }
                }
            }]
        });
    }
function download_lampiran(kode){
    var userstorage =localStorage.getItem("namauser");
    $.ajax({
            url:geturl()+'mobile/get_base_encode_2/'+kode,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                var url="http://sso.krakatausteel.com/arsip/modules/surat/suratkeluar/download.php?idp="+data;
                window.open(url);
                $(".loading").hide();
                

           }
        });
}
function download_lampiran_masuk(kode){
    var userstorage =localStorage.getItem("namauser");
    $.ajax({
            url:geturl()+'mobile/get_base_encode/'+kode+'/'+userstorage,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                var url="http://sso.krakatausteel.com/arsip/modules/surat/suratmasuk/download.php?idp="+data;
                window.open(url);
                $(".loading").hide();
                

           }
        });
}

function paraf(kode){
    var userstorage =localStorage.getItem("namauser");
    $.ajax({
            url:geturl()+'mobile/cek_session_paraf/'+kode,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                if(data==1){
                    alert('Pemaraf lain sedang melakukan revisi surat ini !');
                    $(".loading").hide();
            }else{
                $.ajax({
                    url:geturl()+'mobile/get_paraf/'+kode+"/"+userstorage,
                    method: 'POST',
                    beforeSend: function() {
                    $(".loading").show();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status+" : "+thrownError);
                        $(".loading").hide();
                      },
                    success: function(data) {
                        
                        $('#kodesuratkeluar').val(kode);
                        $('#nosuratkeluarx').val(data[0].nosuratkeluar);
                        $('#tanggalsurat').val(data[0].tanggalsurat);
                        $('#isiringkasan').val(data[0].isiringkasan);
                        $('#memo').jqteVal(data[0].memo);
                        
                       
        
                   }
                });
                 $.mobile.changePage( "paraf.html", { transition: "slide"} );
                 url =geturl()+'mobile/simpan_paraf/'+kode+'/'+userstorage;
                        $(".loading").hide();
            }

           }
        });
      
        
        }
function simpan_paraf(){
    
    var data_val = $('#form-paraf').serializeArray();
    $.ajax({
            url:url,
            method: 'POST',
            data :data_val,
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                if(data=='0'){
                    alert('Keterangan Paraf Harus Diisi !');
                    $(".loading").hide();
                }else{
                    alert(data);
                    $.mobile.changePage( "validasiparaf.html", { transition: "slide"} );
                    var url = '';
                    var userstorage =localStorage.getItem("namauser");
                    get_page_index(userstorage);
                    $(".loading").hide();
                }

           }
        });
  
}

function read_suratmasuk(){
     var userstorage =localStorage.getItem("namauser");
     $('#surat-masuk').DataTable({
            "processing": true,
            "serverSide": true,
            "searching": false,
            "ajax": {
                "url":geturl()+"mobile/readsuratmasuk/"+userstorage,
                "type": "post"
            },
            "columns": [
            {
                "className": 'options',
                "data": "statusview",
                "render": function(data, type, full, meta){
                    if(data==0){
                        
                        //return'<font color="black" onclick="view_masuk(\'' +full.kodesuratmasuk+ '\',\'' +full.kodesifatsurat+ '\',\'' +full.jenissurat+ '\',\'' +full.group_id+ '\',\'' +full.status+ '\');"><b>'+full.empnik_asal+'</b></font>';
                        return'<font color="black" onclick="view_masuk(\'' +full.kodesuratmasuk+ '\');"><b>'+full.empnik_asal+'</b></font>';
                
                    }else{
                        
                        //return '<font color="black" onclick="view_masuk(\'' +full.kodesuratmasuk+ '\',\'' +full.kodesifatsurat+ '\',\'' +full.jenissurat+ '\',\'' +full.group_id+ '\',\'' +full.status+ '\');">'+full.empnik_asal+'</font>';
                        return '<font color="black" onclick="view_masuk(\'' +full.kodesuratmasuk+ '\');">'+full.empnik_asal+'</font>';
                
                    }
                    }
            },{
                "className": 'options',
                "data": "statusview",
                "render": function(data, type, full, meta){
                    if(data==0){
                        
                        //return'<font color="black" onclick="view_masuk(\'' +full.kodesuratmasuk+ '\',\'' +full.kodesifatsurat+ '\',\'' +full.jenissurat+ '\',\'' +full.group_id+ '\',\'' +full.status+ '\');"><b>'+full.isiringkasan+'</b></font>';
                        return'<font color="black" onclick="view_masuk(\'' +full.kodesuratmasuk+ '\');"><b>'+full.isiringkasan+'</b></font>';
                
                    }else{
                        
                        //return '<font color="black" onclick="view_masuk(\'' +full.kodesuratmasuk+ '\',\'' +full.kodesifatsurat+ '\',\'' +full.jenissurat+ '\',\'' +full.group_id+ '\',\'' +full.status+ '\');">'+full.isiringkasan+'</font>';
                        return '<font color="black" onclick="view_masuk(\'' +full.kodesuratmasuk+ '\');">'+full.isiringkasan+'</font>';
                
                    }
                    }
            },{
                "className": 'options',
                "data": "size",
                "render": function(data, type, full, meta){
                    
                       if(data==0 || data==''){
                        return '-';
                       }else{
                        return '<a href="javascript:;" class="btn btn-warning btn-block" onclick="download_lampiran_masuk(\'' +full.kodesuratmasuk+ '\');"><i class="fa fa-download"></i></a>';
                       }
                }
            }]
        });
    }
function read_suratedaran(){
     var userstorage =localStorage.getItem("namauser");
     $('#surat-edaran').DataTable({
            "processing": true,
            "serverSide": true,
            "responsive": true,
            "searching": false,
            "order": [[ 1, "desc" ]],
            "ajax": {
                "url":geturl()+"mobile/readedaran/"+userstorage,
                "type": "post"
            },
            "columns": [{
                "className": 'options',
                "data": "kodesuratkeluar",
                "render": function(data, type, full, meta){
                    
                       return '<font color="black" onclick="view_edaran(\'' +full.kodesuratkeluar+ '\',\'' +full.validasiparaf+ '\');">'+full.isiringkasan+'</font>';
                
                }
            },{
                "className": 'options',
                "data": "kodesuratkeluar",
                "render": function(data, type, full, meta){
                    
                       return '<font color="black" onclick="view_edaran(\'' +full.kodesuratkeluar+ '\',\'' +full.validasiparaf+ '\');">'+full.tanggalsuratnice+'</font>';
                
                }
            },{
                "className": 'options',
                "data": "size",
                "render": function(data, type, full, meta){
                    
                       if(data==0 || data==''){
                        return '-';
                       }else{
                        return '<a href="javascript:;" class="btn btn-warning btn-block" onclick="download_lampiran(\'' +full.kodesuratkeluar+ '\');"><i class="fa fa-download"></i></a>';
                       }
                }
            }]
        });
    }
function validasi(kode){
    var r = confirm("Validasi surat, anda yakin ?");
    if (r == true) {
        $.ajax({
            url:geturl()+"mobile/validasi/"+kode,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                if(data==1){
                    $(".loading").hide();
                    alert('Validasi Sukses !');
                    var table = $('#validasisurat').DataTable();
                    table.ajax.reload(null, false);
                    var userstorage =localStorage.getItem("namauser");
                    get_page_index(userstorage);
                    $('.email-content').show();
                    $('.viewsurat').hide();
                }else{
                    $(".loading").hide();
                    alert('Validasi Gagal !');
                }

           }
        });
    } 
    //var encodedData = window.btoa(kode);
    //alert(encodedData);
    
}

function get_localstorage(){
        if (typeof(Storage) !== "undefined" && localStorage.getItem("namauser")!==null ) {
            var userstorage =localStorage.getItem("namauser");
            
            $("#login").hide();
            $("#dashboard").show();
            $("#menu").show();
            $("#navbar").show();
            $("#headerloginnya").hide();
            $("#headerberandanya").show();
            avatar();
            get_page_index(userstorage);
        }else{
            $("#menu").hide();
            $("#dashboard").hide();
            $("#login").show();
            $("#navbar").hide();
            $("#headerberandanya").hide();
            $("#headerloginnya").show();
        }
    }

    function login(){
        var username    = $('#username').val();
        var pwd         = $('#password').val();
        //alert(username);
        //data = $('#fm-login').serializeArray();
        if(username=='' || pwd==''){
            alert('Username dan password harus diisi !');
        }else{
            $.ajax({
            url:geturl()+"mobile/validate_login/"+username+"/"+pwd,
            //url:geturl()+"home/home/validate_login/",
            method: 'POST',
            //data: data,
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                if(data!=0){
                    //alert(data[0].fullname);
                    //getIds(data[0].userid);
                    $(".loading").hide();
                    alert('Selamat Datang '+data[0].fullname+', login anda sukses !');
                    localStorage.setItem("namauser", data[0].userid);
                    localStorage.setItem("fullname", data[0].fullname);
                    localStorage.setItem("nik", data[0].userid);
                    $("#login").hide();
                    $("#headerloginnya").hide();
                    $("#headerberandanya").show();
                    $("#dashboard").show();
                    $("#menu").show();
                    $("#navbar").show();
                    avatar();
                    get_page_index(data[0].userid);
                }else{
                    $(".loading").hide();
                    alert('Login Gagal !');
                }

           }
        });
        }
        
        
        
    }
    /*function getIds(users){
        var tokenID="afafafadfafs";
        
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "http://localhost/api_smart/mobile/get_token/"+tokenID+"/"+users , false);
        xhr.send();
    }*/
    function get_panel(){
        $('.fanelkiri').empty();
        $('.fanelkiri').append('<li data-filtertext="beranda" ><a onclick ="reload_index();"href="#index" class="ui-btn ui-shadow ui-corner-all ui-btn-icon-right ui-icon-home" data-transition="flow">Beranda</a></li><li data-filtertext="edaran" ><a href="edaran.html" class="ui-btn ui-shadow ui-corner-all ui-btn-icon-right ui-icon-tag" data-transition="slide" onclick="delete_session();">Edaran</a></li><li data-filtertext="surat masuk" ><a href="suratmasuk.html" class="ui-btn ui-shadow ui-corner-all ui-btn-icon-right ui-icon-mail" data-transition="slide" onclick="delete_session();">Surat Masuk</a></li><li data-filtertext="notifikasi progress" ><a href="notifikasiprogress.html" class="ui-btn ui-shadow ui-corner-all ui-btn-icon-right ui-icon-bars" data-transition="slide" onclick="delete_session();">Notifikasi progress</a></li><li data-filtertext="surat keluar" ><a href="suratkeluar.html" class="ui-btn ui-shadow ui-corner-all ui-btn-icon-right ui-icon-mail" data-transition="slide" onclick="delete_session();">Surat Keluar</a></li><li data-filtertext="paraf"><a href="validasiparaf.html" class="ui-btn ui-shadow ui-corner-all ui-btn-icon-right ui-icon-edit" data-transition="slide" onclick="delete_session();">Paraf</a></li><li data-filtertext="validasi"><a href="validasi.html" class="ui-btn ui-shadow ui-corner-all ui-btn-icon-right ui-icon-check" data-transition="slide" onclick="delete_session();">Validasi</a></li><li data-filtertext="pedoman"><a href="pedoman.html" data-transition="slide" class="ui-btn ui-shadow ui-corner-all ui-btn-icon-right ui-icon-info" onclick="delete_session();">Pedoman</a></li><li data-filtertext="contact us"><a href="#" data-transition="slide" class="ui-btn ui-shadow ui-corner-all ui-btn-icon-right ui-icon-phone" >Contact Us</a></li><li data-filtertext="validasi"><a href="#" onclick="logout()" class="ui-btn ui-shadow ui-corner-all ui-btn-icon-right ui-icon-power">Log Out</a></li>');
    }
    function get_navbar(){
        avatar();
        $('.navbar').empty();
        $('.navbar').append('<h1 class="ui-title" role="heading" aria-level="1">Surat Keluar</h1><a href="#leftpanel3" data-icon="bars" data-iconpos="notext" class="ui-link ui-btn-left ui-btn ui-icon-bars ui-btn-icon-notext ui-shadow ui-corner-all" data-role="button" role="button">Menu</a><a href="#index" data-rel="back" data-icon="carat-l" data-iconpos="notext" class="ui-link ui-btn-right ui-btn ui-icon-carat-l ui-btn-icon-notext ui-shadow ui-corner-all" data-role="button" role="button">Back</a>');
    }
    
    function avatar(){
        var fullname =localStorage.getItem("fullname");
        var nik =localStorage.getItem("nik");
        var hsl = $(".avatar").html("<i align=center><font color=white><strong>Hai, "+fullname+"...<br><small><font color=white>( "+nik+" )</font></small></strong></font></i>");
        return hsl;
    }
    function riwayat(kode){
        $.ajax({
            url:geturl()+'mobile/get_riwayat_perubahan/'+kode,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                $('.viewsurat').hide();
                $(".loading").hide();
                $(".email-content").hide();
                $(".positionSelector").show();
                $(".positionSelector").empty();
                $(".positionSelector").html(data);

           }
        });
       
    }
    function close_riwayat(){
        //$(".email-content").show(); 
        $(".viewsurat").show();
        $(".positionSelector").hide();
    }
    function setuju_paraf(kode){
        var userstorage =localStorage.getItem("namauser");
       var r = confirm("Menyetujui paraf, anda yakin ?");
    if (r == true) {
        $.ajax({
            url:geturl()+"mobile/setuju_paraf/"+kode+"/"+userstorage,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                if(data==1){
                    $(".loading").hide();
                    alert('Data Sukses Disetujui !');
                    var table = $('#validasi-paraf').DataTable();
                    table.ajax.reload(null, false);
                    var userstorage =localStorage.getItem("namauser");
                    get_page_index(userstorage);
                    $('.email-content').show();
                    $('.viewsurat').hide();
                }else{
                    $(".loading").hide();
                    alert('validasi paraf gagal !');
                }

           }
        });
    }
    }
    function tolak_paraf(kode){
         var userstorage =localStorage.getItem("namauser");
         var r = prompt("Silahkan tulis alasan anda menolak paraf", "");//confirm("Menolak paraf, anda yakin ?");
         //var person = prompt("Please enter your name", "Harry Potter");
    if (r != null) {
        //alert(r);
        $.ajax({
            url:geturl()+"mobile/tolak_paraf/"+kode+"/"+userstorage+"/"+r,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                if(data==1){
                    $(".loading").hide();
                    alert('Data Sukses Ditolak !');
                    var table = $('#validasi-paraf').DataTable();
                    table.ajax.reload(null, false);
                    var userstorage =localStorage.getItem("namauser");
                    get_page_index(userstorage);
                    $('.email-content').show();
                    $('.viewsurat').hide();
                }else{
                    $(".loading").hide();
                    alert('validasi paraf gagal !');
                }

           }
        });
    }
    }
    function delete_session(){
        var kode =$('#kodesuratkeluar').val();
        var userstorage =localStorage.getItem("namauser");
        $.post(geturl()+'mobile/delete_session_paraf/'+kode+'/'+userstorage,function(data, textStatus, xhr){console.log(data);});
    }
    function logout()
    {
    
    localStorage.removeItem("namauser"); 
    navigator.app.exitApp();
    
    }
    function read_notifprogress(){
     var userstorage =localStorage.getItem("namauser");
     $('#notif-progress').DataTable({
            "processing": true,
            "serverSide": true,
            "searching": false,
            "ajax": {
                "url":geturl()+"mobile/readnotifprogress/"+userstorage,
                "type": "post"
            },
            "columns": [
            {
                "className": 'options',
                "data": "statusview",
                "render": function(data, type, full, meta){
                    if(data==0){
                        
                        return'<font color="black" onclick="view_progress(\'' +full.kodesuratmasuk+ '\');"><b>'+full.empniktujuan+'</b></font>';
                        //return'<font color="black"><b>'+full.empniktujuan+'</b></font>';
                
                    }else{
                        
                        return'<font color="black" onclick="view_progress(\'' +full.kodesuratmasuk+ '\');">'+full.empniktujuan+'</font>';
                        //return '<font color="black">'+full.empniktujuan+'</font>';
                
                    }
                    }
            },{
                "className": 'options',
                "data": "statusview",
                "render": function(data, type, full, meta){
                    if(data==0){
                        //return'<font color="black"><b>'+full.progressurat+'</b></font>';
                        return'<font color="black" onclick="view_progress(\'' +full.kodesuratmasuk+ '\');"><b>'+full.progressurat+'</b></font>';
                        
                    }else{
                        
                        return'<font color="black" onclick="view_progress(\'' +full.kodesuratmasuk+ '\');">'+full.progressurat+'</font>';
                        //return '<font color="black">'+full.progressurat+'</font>';
                
                    }
                    }
            }
            ]
        });
    }
    function view_progress(kode){
    var userstorage =localStorage.getItem("namauser");
    $.ajax({
            url:geturl()+'mobile/get_view_progress/'+kode+'/'+userstorage,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                $('.email-content').hide();
                $('.viewmasuk').show();
                
                $(".viewmasuk").html(data);
                var table = $('#notif-progress').DataTable();
                table.ajax.reload(null, false);
                get_page_index(userstorage);
                $(".loading").hide();
                

           }
        });
}
    function tutup_progress(kode,nomor){
    var r = confirm("Apakah anda ingin menutup progres No Surat "+nomor+" ?");
    if (r == true) {
        $.ajax({
            url:geturl()+"mobile/tutup_progress/"+kode,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                if(data==1){
                    $(".loading").hide();
                    alert('Progress berhasil ditutup !');
                    var table = $('#notif-progress').DataTable();
                    table.ajax.reload(null, false);
                    var userstorage =localStorage.getItem("namauser");
                    get_page_index(userstorage);
                    $('.email-content').show();
                    $('.viewmasuk').hide();
                    $(".viewdisposisi").hide();
                }else{
                    $(".loading").hide();
                    alert('Tutup progress gagal !');
                }

           }
        });
        
        }
    }
    function input_progress(kode){
        var userstorage =localStorage.getItem("namauser");
        $.ajax({
            url:geturl()+'mobile/input_progress/'+kode+'/'+userstorage,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                $('.viewmasuk').hide();
                $(".loading").hide();
                $(".table-responsive").hide();
                $(".viewdisposisi").show();
                $(".viewdisposisi").empty();
                var html='';
                html+=data;
                
                $(".viewdisposisi").html(html);
                            

           }
        });
    }
    function simpan_progress(kode){
        var data_val = $('#fminput_progress').serializeArray();
        var r = confirm("Apakah data anda sudah benar ?");
        if (r == true) {
        $.ajax({
            url:geturl()+'mobile/simpan_progress/'+kode,
            method: 'POST',
            data: data_val,
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                if(data==1){
                    alert('Data sukses disimpan !');
                    $('.email-content').show();
                    $(".loading").hide();
                    $(".table-responsive").show();
                    $(".viewdisposisi").hide();
                    $(".viewdisposisi").empty();
                    
                    var table = $('#surat-masuk').DataTable();
                    table.ajax.reload(null, false);
                }else{
                    $(".loading").hide();
                    alert('Data gagal disimpan !');
                }

           }
        });
        }
        
    }
    function list_progress(kode){
        var userstorage =localStorage.getItem("namauser");
    $.ajax({
            url:geturl()+'mobile/list_progress/'+kode+'/'+userstorage,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                $('.viewmasuk').hide();
                $(".loading").hide();
                $(".table-responsive").hide();
                $(".viewdisposisi").show();
                $(".viewdisposisi").empty();
                var html='';
                html+=data;
                
                $(".viewdisposisi").html(html);
                            

           }
        });
    }
    function nilaiprogress(kode){
        var userstorage =localStorage.getItem("namauser");
    $.ajax({
            url:geturl()+'mobile/nilai_progress/'+kode+'/'+userstorage,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                $('.viewmasuk').hide();
                $(".loading").hide();
                $(".table-responsive").hide();
                $(".viewdisposisi").show();
                $(".viewdisposisi").empty();
                var html='';
                html+=data;
                
                $(".viewdisposisi").html(html);
                            

           }
        });
    }
    function simpannilai_progress(kode){
         var data_val = $('#fm_close_progress').serializeArray();
        var r = confirm("Apakah data anda sudah benar ?");
        if (r == true) {
        $.ajax({
            url:geturl()+'mobile/simpannilai_progress/'+kode,
            method: 'POST',
            data: data_val,
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                if(data==1){
                    alert('Data sukses disimpan !');
                    $('.email-content').show();
                    $(".loading").hide();
                    $(".table-responsive").show();
                    $(".viewdisposisi").hide();
                    $(".viewdisposisi").empty();
                    
                    var table = $('#surat-masuk').DataTable();
                    table.ajax.reload(null, false);
                }else{
                    $(".loading").hide();
                    alert('Data gagal disimpan !');
                }

           }
        });
        }
        
    }
    