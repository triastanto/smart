
$(document).ready(function() {
    cek_versi();
    var userstorage =localStorage.getItem("namauser");
    get_page_index(userstorage);
    
    });
function geturl()
{
    var url ="http://localhost/api_smart/";
//var url ="https://sso.krakatausteel.com/api_smart/"; 
//var url ="http://dev.krakatausteel.com/api_smart/"; 
return url;
}
function view_disposisi(kode){
    $.ajax({
            url:geturl()+'mobile/get_disposisi/'+kode,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                $(".loading").hide();
                $(".table-responsive").hide();
                $(".viewdisposisi").show();
                $(".viewdisposisi").empty();
                $(".viewdisposisi").html(data);

           }
        });
}
function close_disposisi(){
        $(".table-responsive").show();
        $(".viewdisposisi").hide();
    }
function reload_index(){
    delete_session();
    var userstorage =localStorage.getItem("namauser");
    get_page_index(userstorage);
}
function view_pedoman(e){
    var url="http://sso.krakatausteel.com/arsip/home/"+e;
            window.open(url);
}
function pedoman_kearsipan(){
    var grup ='1';
    $.ajax({
            url:geturl()+'mobile/get_pedoman_kearsipan/'+grup,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                $(".loading").hide();
                $("#listpedoman").hide();
                $("#detailpedoman").show();
                $("#detailpedoman").empty();
                $("#detailpedoman").html(data);

           }
        });
    /*$.post(geturl()+'mobile/get_pedoman_kearsipan/'+grup, function(data, textStatus, xhr) {
            $("#listpedoman").hide();
            $("#detailpedoman").show();
            $("#detailpedoman").empty();
            $("#detailpedoman").html(data);
            
        });*/
}
function petunjuk_penggunaan(){
    var grup ='2';
    $.ajax({
            url:geturl()+'mobile/get_pedoman_kearsipan/'+grup,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                $(".loading").hide();
                $("#listpedoman").hide();
                $("#detailpedoman").show();
                $("#detailpedoman").empty();
                $("#detailpedoman").html(data);

           }
        });
    /*$.post(geturl()+'mobile/get_pedoman_kearsipan/'+grup, function(data, textStatus, xhr) {
            $("#listpedoman").hide();
            $("#detailpedoman").show();
            $("#detailpedoman").empty();
            $("#detailpedoman").html(data);
            
        });*/
}
function informasi_kearsipan(){
    var grup ='3';
    $.ajax({
            url:geturl()+'mobile/get_pedoman_kearsipan/'+grup,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                $(".loading").hide();
                $("#listpedoman").hide();
                $("#detailpedoman").show();
                $("#detailpedoman").empty();
                $("#detailpedoman").html(data);

           }
        });
    /*$.post(geturl()+'mobile/get_pedoman_kearsipan/'+grup, function(data, textStatus, xhr) {
            $("#listpedoman").hide();
            $("#detailpedoman").show();
            $("#detailpedoman").empty();
            $("#detailpedoman").html(data);
            
        });*/
}
function close_pedoman(){
    $("#listpedoman").show();
    $("#detailpedoman").hide();
}
function cek_versi(){
    var e = $("#versi").html();
    var v ="1.0.1";
    if(e!=v){
        alert("Smart anda versi "+e+" download versi "+v+" terbaru di https://sso.krakatausteel.com/api_smart/master/smart.apk!");
        window.location="https://sso.krakatausteel.com/api_smart/master/smart.apk";
        //navigator.app.exitApp();
    }
}
function get_pedoman(){
    $.post(geturl()+'mobile/get_pedoman', function(data, textStatus, xhr) {
            $("#listpedoman").empty();
            $("#listpedoman").html(data);
            
        });
}
function get_page_index(userstorage){
    get_loading();
    //var userstorage =localStorage.getItem("namauser");
    $.post(geturl()+'mobile/get_page_index/'+userstorage, function(data, textStatus, xhr) {
            $("#dashboard").empty();
            $("#dashboard").html(data);
            
        });
}
function get_loading(){
    $(".loading").html('<div  class="progress progress-striped active" ><div class="progress-bar" style="width: 100%">Sedang Memproses...</div></div>');
}
function previewsuratkeluar(id){
    $.mobile.changePage( "memosuratkeluar.html", { transition: "pop"} );
}
function view_masuk(kode){
    
    var userstorage =localStorage.getItem("namauser");
    $.ajax({
            url:geturl()+'mobile/get_base_encode/'+kode+'/'+userstorage,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                var url="http://sso.krakatausteel.com/arsip/modules/report/label/memodinasmasuk.php?idp="+data;//"http://dev.krakatausteel.com/arsip/modules/report/label/memodinasmasuk.php?idp="+data;
                window.open(url);
                var table = $('#surat-masuk').DataTable();
                table.ajax.reload(null, false);
                get_page_index(userstorage);
                $(".loading").hide();

           }
        });
    /*$.post(geturl()+'mobile/get_base_encode/'+kode+'/'+userstorage, function(data, textStatus, xhr) {
            var url="http://dev.krakatausteel.com/arsip/modules/report/label/memodinasmasuk.php?idp="+data;//"http://dev.krakatausteel.com/arsip/modules/report/label/memodinasmasuk.php?idp="+data;
            window.open(url);
            var table = $('#surat-masuk').DataTable();
            table.ajax.reload(null, false);
            get_page_index(userstorage);
        });*/
}
function view(kode){
    $.ajax({
            url:geturl()+'mobile/get_base_encode_2/'+kode,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                var url="http://sso.krakatausteel.com/arsip/modules/report/label/memodinaskeluar.php?idp="+data;
                window.open(url);
                $(".loading").hide();

           }
        });
    /*$.post(geturl()+'mobile/get_base_encode_2/'+kode, function(data, textStatus, xhr) {
            //alert(data);
            
            var url="http://sso.krakatausteel.com/arsip/modules/report/label/memodinaskeluar.php?idp="+data;
            window.open(url);
        });*/
}
function cetak_disposisi(kode){
    $.ajax({
            url:geturl()+'mobile/get_base_encode_2/'+kode,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                var url="http://sso.krakatausteel.com/arsip/modules/report/label/disposisi.php?idp="+data;//"http://dev.krakatausteel.com/arsip/modules/report/label/memodinasmasuk.php?idp="+data;
            window.open(url);
                $(".loading").hide();

           }
        });
    /*$.post(geturl()+'mobile/get_base_encode_2/'+kode, function(data, textStatus, xhr) {
            var url="http://sso.krakatausteel.com/arsip/modules/report/label/disposisi.php?idp="+data;//"http://dev.krakatausteel.com/arsip/modules/report/label/memodinasmasuk.php?idp="+data;
            window.open(url);
        });*/
}
function get_notif(){
    
    $(".notif").empty();
    $('#suratmasuk').html(7);
    $('#suratkeluar').html(3);
    $('#paraf').html(5);
    $('#validasi').html(9);
    
    
}
function submit_contact(){
    url =geturl()+"mobile/contactus";
     var data_val = $('#form-contact').serializeArray();
     $.post(url, data_val, function(data, textStatus, xhr) {
      if(data){
            alert(data);
      }
      else{
            var url = '';
            alert(data);
      }

    });

}
function read_paraf(){
     var userstorage =localStorage.getItem("namauser");
     $('#validasi-paraf').DataTable({
            "processing": true,
            "serverSide": true,
            "responsive": true,
            "searching": false,
            "order": [[ 1, "desc" ]],
            "ajax": {
                "url":geturl()+"mobile/readparaf/"+userstorage,
                "type": "post"
            },
            "columns": [/*{
              "data": "empnik",
              "render": function(data, type, full, meta){
                return meta.row+1;
                 
              }
            },*/
            {"data": "nosuratkeluar"},{"data": "tanggalsurat"},{"data": "empnik_asal"},{"data": "isiringkasan"},
            {
                "className": 'options',
                "data": "kodesuratkeluar",
                "render": function(data, type, full, meta){
                    
                     //return '<a href="#" class="fa fa-2x fa-search text-success" data-rel="popup" onclick="javascript:riwayat(\'' +full.kodesuratkeluar+ '\');"></a>';
                    return '<a href="javascript:riwayat(\'' +full.kodesuratkeluar+ '\');" class="btn btn-primary btn-xs"><i class="fa fa-search"></i>&nbsp;Riwayat Perubahan</a>';
                
                }
            },{
                "className": 'options',
                "data": "validasi",
                "render": function(data, type, full, meta){
                    
                    //return '<a href="#" class="fa fa-2x fa-pencil-square text-warning" onclick="javascript:paraf(\'' +full.kodesuratkeluar+ '\');"></a>';
                    if(full.validasiparaf==1){
                        //return '<a href="javascript:;" class="btn btn-success btn-xs"><i class="fa fa-check-square-o"></i>&nbsp;Sudah Divalidasi</a>';
                         return '<a href="javascript:;" class="btn btn-success btn-xs"><i class="fa fa-check-square-o"></i>&nbsp;Sudah Divalidasi</a>';
                
                  
                    }if(full.validasiparaf==2){
                        //return '<a href="javascript:;" class="btn btn-success btn-xs"><i class="fa fa-check-square-o"></i>&nbsp;Sudah Divalidasi</a>';
                         //return '<a href="javascript:;" class="btn btn-danger btn-xs"><i class="fa fa-times"></i>&nbsp;Tidak Setuju</a>';
                         return '';
                
                  
                    }else{
                        //return '<a href="#" class="fa fa-2x fa-pencil-square text-warning" onclick="javascript:paraf(\'' +full.kodesuratkeluar+ '\');"></a>';
                        return '<a href="javascript:paraf(\'' +full.kodesuratkeluar+ '\');" class="btn btn-warning btn-xs"><i class="fa fa-pencil-square"></i>&nbsp;Edit Format</a>';
                
                    }
                  
                }
            },{
                "className": 'options',
                "data": "validasi",
                "render": function(data, type, full, meta){
                    //return '<a href="#"><i title="Sudah Divalidasi" class="fa fa-2x fa-check-square-o"></i></a>';
               if(full.validasiparaf==1){
                    //return '<a href="#"><i title="Sudah Disetujui Paraf" class="fa fa-2x fa-check text-primary"></i></a>';
                    return '<a href="javascript:;" class="btn btn-success btn-xs"><i class="fa fa-check-square-o"></i>&nbsp;Setuju</a>';
                    
                  
                }
                if(full.validasiparaf==2){
                   // return '<a href="#" class="disabled"><i class="fa fa-2x fa-times text-warning"></i></a>';
                   // return '<a href="javascript:;" class="btn btn-danger btn-xs"><i class="fa fa-times"></i>&nbsp;Tidak Setuju</a>';
                    return '';
                  
                }else{
                    //return '<a href="#"><i title="Menunggu Disetujui Paraf" class="fa fa-2x fa-spinner" onclick="javascript:setuju_paraf(\'' +full.kodesuratkeluar+ '\')"></i></a>';
                  return '<a href="javascript:setuju_paraf(\'' +full.kodesuratkeluar+ '\');" class="btn btn-warning btn-xs"><i class="fa fa-gear"></i>&nbsp;Setuju Paraf</a>';
                
                }
                  
                }
            },{
                "className": 'options',
                "data": "validasi",
                "render": function(data, type, full, meta){
                    //return '<a href="#"><i title="Sudah Divalidasi" class="fa fa-2x fa-check-square-o"></i></a>';
               if(full.validasiparaf==2){
                   // return '<a href="#"><i title="Paraf Sudah Ditolak" class="fa fa-2x fa-check text-primary"></i></a>';
                   return '<a href="javascript:;" class="btn btn-danger btn-xs"><i class="fa fa-times"></i>&nbsp;Tidak Setuju</a>';
                
                  
                }
                if(full.validasiparaf==1){
                    //return '<a href="#" class="disabled"><i title="Paraf Sudah Disetujui" class="fa fa-2x fa-times text-warning"></i></a>';
                    //return '<a href="javascript:;" class="btn btn-success btn-xs"><i class="fa fa-check-square-o"></i>&nbsp;Setuju</a>';
                    return '';
                  
                }else{
                    //return '<a href="#"><i title="Menunggu Paraf Ditolak" class="fa fa-2x fa-spinner" onclick="javascript:tolak_paraf(\'' +full.kodesuratkeluar+ '\')"></i></a>';
                  return '<a href="javascript:tolak_paraf(\'' +full.kodesuratkeluar+ '\');" class="btn btn-warning btn-xs"><i class="fa fa-gear"></i>&nbsp;Tolak Paraf</a>';
                
                }
                  
                }
            },
            {
                "className": 'options',
                "data": "kodesuratkeluar",
                "render": function(data, type, full, meta){
                    
                      // return '<a href="#" class="fa fa-2x fa-file-o text-success" onclick="javascript:view(\'' +full.kodesuratkeluar+ '\');"></a>';
                 return '<a href="javascript:view(\'' +full.kodesuratkeluar+ '\');" class="btn btn-inverse btn-xs"><i class="fa fa-file-pdf-o"></i>&nbsp;Preview</a>';
                
                }
            }]
        });
    }
function read_validasi(){
     var userstorage =localStorage.getItem("namauser");
     $('#validasisurat').DataTable({
            "processing": true,
            "serverSide": true,
            "responsive": true,
            "searching": false,
            "order": [[ 1, "desc" ]],
            "ajax": {
                "url":geturl()+"mobile/readvalidasi/"+userstorage,
                "type": "post"
            },
            "columns": [/*{
              "data": "empnik",
              "render": function(data, type, full, meta){
                return meta.row+1;
                 
              }
            },*/
            {"data": "nosuratkeluar"},{"data": "tanggalsuratnice"},{"data": "empnik_asal"},{"data": "isiringkasan"},
            {
                "className": 'options',
                "data": "kodesuratkeluar",
                "render": function(data, type, full, meta){
                    
                      // return '<a href="#" class="fa fa-2x fa-search text-success" data-rel="popup" onclick="javascript:riwayat(\'' +full.kodesuratkeluar+ '\');"></a>';
                    return '<a href="javascript:riwayat(\'' +full.kodesuratkeluar+ '\');" class="btn btn-primary btn-xs"><i class="fa fa-search"></i>&nbsp;Riwayat Perubahan</a>';
                
                }
            },{
                "className": 'options',
                "data": "validasi",
                "render": function(data, type, full, meta){
                    
                    if(full.validasiparaf==1){
                        //return '<a href="javascript:;" class="btn btn-success btn-xs"><i class="fa fa-check-square-o"></i>&nbsp;Sudah Divalidasi</a>';
                         return '<a href="javascript:;" class="btn btn-success btn-xs"><i class="fa fa-check-square-o"></i>&nbsp;Sudah Divalidasi</a>';
                
                  
                    }else{
                        //return '<a href="#" class="fa fa-2x fa-pencil-square text-warning" onclick="javascript:paraf(\'' +full.kodesuratkeluar+ '\');"></a>';
                        return '<a href="javascript:paraf(\'' +full.kodesuratkeluar+ '\');" class="btn btn-warning btn-xs"><i class="fa fa-pencil-square"></i>&nbsp;Edit Format</a>';
                
                    }
                  
                }
            },{
                "className": 'options',
                "data": "validasi",
                "render": function(data, type, full, meta){
                    //return '<a href="#"><i title="Sudah Divalidasi" class="fa fa-2x fa-check-square-o"></i></a>';
               if(full.validasiparaf==1){
                    return '<a href="javascript:;" class="btn btn-success btn-xs"><i class="fa fa-check-square-o"></i>&nbsp;Sudah Divalidasi</a>';
                
                  
                }
                else{
                    //return '<a href="#"><i title="Menunggu Validasi" class="fa fa-2x fa-spinner" onclick="javascript:validasi(\'' +full.kodesuratkeluar+ '\')"></i></a>';
                  return '<a href="javascript:validasi(\'' +full.kodesuratkeluar+ '\');" class="btn btn-warning btn-xs"><i class="fa fa-gears (alias)"></i>&nbsp;Validasi</a>';
                
                }
                  
                }
            },
            {
                "className": 'options',
                "data": "kodesuratkeluar",
                "render": function(data, type, full, meta){
                    
                      //return '<a href="http://dev.krakatausteel.com/arsip/modules/report/label/memodinaskeluar.php?idp='+full.kodesuratkeluar+'" target="_blank"><i class="fa fa-2x fa-file"></i></a>';
                       return '<a href="javascript:view(\'' +full.kodesuratkeluar+ '\');" class="btn btn-inverse btn-xs"><i class="fa fa-file-pdf-o"></i>&nbsp;Preview</a>';
                
                }
            }]
        });
    }
function read_suratkeluar(){
     var userstorage =localStorage.getItem("namauser");
     $('#surat-keluar').DataTable({
            "processing": true,
            "serverSide": true,
            "responsive": true,
            "searching": false,
            "order": [[ 1, "desc" ]],
            "ajax": {
                "url":geturl()+"mobile/readsuratkeluar/"+userstorage,
                "type": "post"
            },
            "columns": [/*{
              "data": "empnik",
              "render": function(data, type, full, meta){
                return meta.row+1;
                 
              }
            },*/
            {"data": "nosuratkeluar"},{"data": "tanggalsuratnice"},{"data": "empnik_asal"},{"data": "isiringkasan"},
            {
                "className": 'options',
                "data": "kodesuratkeluar",
                "render": function(data, type, full, meta){
                    
                      //return '<a href="#" class="fa text-success" data-rel="popup" onclick="javascript:riwayat(\'' +full.kodesuratkeluar+ '\');">Riwayat</a>';
                    return '<a href="javascript:riwayat(\'' +full.kodesuratkeluar+ '\');" class="btn btn-primary btn-xs"><i class="fa fa-search"></i>&nbsp;Riwayat Perubahan</a>';
                }
            },{
                "className": 'options',
                "data": "validasi",
                "render": function(data, type, full, meta){
                    //return '<a href="#"><i title="Sudah Divalidasi" class="fa fa-2x fa-check-square-o"></i></a>';
               if(full.validasiparaf==1){
                    //return '<a href="#"><i title="Sudah Divalidasi" class="fa fa-2x fa-check text-primary"></i></a>';
                    return '<a href="javascript:;" class="btn btn-success btn-xs"><i class="fa fa-check-square-o"></i>&nbsp;Sudah Divalidasi</a>';
                
                  
                }
                if(full.validasiparaf==2){
                    //return '<a href="#" class="disabled"><i class="fa fa-2x fa-times text-warning"></i></a>';
                   return '<a href="javascript:;" class="btn btn-danger btn-xs"><i class="fa fa-times"></i>&nbsp;Menolak Divalidasi</a>';
                
                  
                }else{
                    //return '<a href="#"><i title="Menunggu Validasi" class="fa fa-2x fa-spinner"></i></a>';
                    return '<a href="javascript:;" class="btn btn-warning btn-xs"><i class="fa fa-gear"></i>&nbsp;Menunggu Divalidasi</a>';
                
                  
                }
                  
                }
            },
            {
                "className": 'options',
                "data": "kodesuratkeluar",
                "render": function(data, type, full, meta){
                    
                       //return '<a href="#" class="fa fa-2x fa-file-o text-success" onclick="javascript:view(\'' +full.kodesuratkeluar+ '\');"></a>';
                        return '<a href="javascript:view(\'' +full.kodesuratkeluar+ '\');" class="btn btn-inverse btn-xs"><i class="fa fa-file-pdf-o"></i>&nbsp;Preview</a>';
                
                }
            }]
        });
    }

function paraf(kode){
    var userstorage =localStorage.getItem("namauser");
    $.ajax({
            url:geturl()+'mobile/cek_session_paraf/'+kode,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                if(data==1){
                    alert('Pemaraf lain sedang melakukan revisi surat ini !');
                    $(".loading").hide();
            }else{
                $.ajax({
                    url:geturl()+'mobile/get_paraf/'+kode+"/"+userstorage,
                    method: 'POST',
                    beforeSend: function() {
                    $(".loading").show();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status+" : "+thrownError);
                        $(".loading").hide();
                      },
                    success: function(data) {
                        
                        $('#kodesuratkeluar').val(kode);
                        $('#nosuratkeluarx').val(data[0].nosuratkeluar);
                        $('#tanggalsurat').val(data[0].tanggalsurat);
                        $('#isiringkasan').val(data[0].isiringkasan);
                        $('#memo').jqteVal(data[0].memo);
                        
                       
        
                   }
                });
                 $.mobile.changePage( "paraf.html", { transition: "slide"} );
                 url =geturl()+'mobile/simpan_paraf/'+kode+'/'+userstorage;
                        $(".loading").hide();
            }

           }
        });
        /*$.post(geturl()+'mobile/cek_session_paraf/'+kode,function(data,textStatus,xhr){
            var userstorage =localStorage.getItem("namauser");
        if(data==1){
            alert('Pengguna lain sedang mengubah data ini !');
        }else{
            $.post(geturl()+'mobile/get_paraf/'+kode+"/"+userstorage, function(data, textStatus, xhr) {
            $('#kodesuratkeluar').val(kode);
            $('#nosuratkeluarx').val(data[0].nosuratkeluar);
            $('#tanggalsurat').val(data[0].tanggalsurat);
            $('#isiringkasan').val(data[0].isiringkasan);
            $('#memo').jqteVal(data[0].memo);
            
            });
            $.mobile.changePage( "paraf.html", { transition: "slide"} );
            
            url =geturl()+'mobile/simpan_paraf/'+kode+'/'+userstorage;
            }
        
            });*/
        
        }
function simpan_paraf(){
    
    var data_val = $('#form-paraf').serializeArray();
    $.ajax({
            url:url,
            method: 'POST',
            data :data_val,
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                if(data=='0'){
                    alert('Keterangan Paraf Harus Diisi !');
                    $(".loading").hide();
                }else{
                    alert(data);
                    $.mobile.changePage( "validasiparaf.html", { transition: "slide"} );
                    var url = '';
                    var userstorage =localStorage.getItem("namauser");
                    get_page_index(userstorage);
                    $(".loading").hide();
                }

           }
        });
   /*  $.post(url, data_val,function(data, textStatus, xhr) {
      if(data){
            if(data=='0'){
               alert('Keterangan Paraf Harus Diisi !');
            }else{
            
            alert(data);
            $.mobile.changePage( "validasiparaf.html", { transition: "slide"} );
            var url = '';
            var userstorage =localStorage.getItem("namauser");
            get_page_index(userstorage);
      }
            //alert(data);
            //$.mobile.changePage( "validasiparaf.html", { transition: "slide"} );
      }
      

    });*/
}
function read_suratmasuk(){
     var userstorage =localStorage.getItem("namauser");
     $('#surat-masuk').DataTable({
            "processing": true,
            "serverSide": true,
            "searching": false,
            "ajax": {
                "url":geturl()+"mobile/readsuratmasuk/"+userstorage,
                "type": "post"
            },
            "columns": [
            {
                "className": 'options',
                "data": "statusview",
                "render": function(data, type, full, meta){
                    
                    if(data==0){
                        return '<font color="black"><b>'+full.nosuratmasuk+'</b></font>';
                    }else{
                        return full.nosuratmasuk;
                    }
                    }
            },{
                "className": 'options',
                "data": "statusview",
                "render": function(data, type, full, meta){
                    if(data==0){
                        return '<font color="black"><b>'+full.isiringkasan+'</b></font>';
                    }else{
                        return full.isiringkasan;
                    }
                    }
            }/*,
            {
                "className": 'options',
                "data": "statusview",
                "render": function(data, type, full, meta){
                    if(data==0){
                        return '<font color="black"><b>'+full.tanggalsuratnice+'</b></font>';
                    }else{
                        return full.tanggalsuratnice;
                    }
                    }
            },
            {
                "className": 'options',
                "data": "statusview",
                "render": function(data, type, full, meta){
                    if(data==0){
                        return '<font color="black"><b>'+full.empnik_asal+'</b></font>';
                    }else{
                        return full.empnik_asal;
                    }
                    }
            },
            {
                "className": 'options',
                "data": "statusview",
                "render": function(data, type, full, meta){
                    if(data==0){
                        return '<font color="black"><b>'+full.isiringkasan+'</b></font>';
                    }else{
                        return full.isiringkasan;
                    }
                    }
            },
            {
                "className": 'options',
                "data": "statusview",
                "render": function(data, type, full, meta){
                    if(data==0){
                        return '<font color="black"><b>'+full.empniktujuan+'</b></font>';
                    }else{
                        return full.empniktujuan;
                    }
                    }
            },
            {
                "className": 'options',
                "data": "kodesuratkeluar",
                "render": function(data, type, full, meta){
                    
                      return '<a href="javascript:view_masuk(\'' +full.kodesuratmasuk+ '\');" class="btn btn-inverse btn-xs"><i class="fa fa-file-pdf-o"></i>&nbsp;Preview</a>';
                
                }
            },
            {
                "className": 'options',
                "data": "kodesuratkeluar",
                "render": function(data, type, full, meta){
                    
                       return '<a href="javascript:view_disposisi(\'' +full.kodesuratmasuk+ '\');" class="btn btn-primary btn-xs"><i class="fa fa-search"></i>&nbsp;View Disposisi</a>|<a href="javascript:cetak_disposisi(\'' +full.kodesuratmasuk+ '\');" class="btn btn-success btn-xs"><i class="fa fa-print"></i>&nbsp;Cetak Disposisi</a>';
                
                }
            }*/]
        });
    }
function validasi(kode){
    var r = confirm("Validasi surat, anda yakin ?");
    if (r == true) {
        $.ajax({
            url:geturl()+"mobile/validasi/"+kode,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                if(data==1){
                    $(".loading").hide();
                    alert('Validasi Sukses !');
                    var table = $('#validasisurat').DataTable();
                    table.ajax.reload(null, false);
                    var userstorage =localStorage.getItem("namauser");
                    get_page_index(userstorage);
                }else{
                    $(".loading").hide();
                    alert('Validasi Gagal !');
                }

           }
        });
    } 
    //var encodedData = window.btoa(kode);
    //alert(encodedData);
    
}

function get_localstorage(){
        if (typeof(Storage) !== "undefined" && localStorage.getItem("namauser")!==null ) {
            var userstorage =localStorage.getItem("namauser");
            
            $("#login").hide();
            $("#dashboard").show();
            $("#menu").show();
            $("#navbar").show();
            $("#headerloginnya").hide();
            $("#headerberandanya").show();
            avatar();
            get_page_index(userstorage);
        }else{
            $("#menu").hide();
            $("#dashboard").hide();
            $("#login").show();
            $("#navbar").hide();
            $("#headerberandanya").hide();
            $("#headerloginnya").show();
        }
    }

    function login(){
        var username    = $('#username').val();
        var pwd         = $('#password').val();
        //alert(username);
        //data = $('#fm-login').serializeArray();
        $.ajax({
            url:geturl()+"mobile/validate_login/"+username+"/"+pwd,
            //url:geturl()+"home/home/validate_login/",
            method: 'POST',
            //data: data,
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                if(data!=0){
                    //alert(data[0].fullname);
                    //getIds(username);
                    $(".loading").hide();
                    alert('Selamat Datang '+data[0].fullname+', login anda sukses !');
                    localStorage.setItem("namauser", username);
                    localStorage.setItem("fullname", data[0].fullname);
                    localStorage.setItem("nik", data[0].nik);
                    $("#login").hide();
                    $("#headerloginnya").hide();
                    $("#headerberandanya").show();
                    $("#dashboard").show();
                    $("#menu").show();
                    $("#navbar").show();
                    avatar();
                    get_page_index(username);
                }else{
                    $(".loading").hide();
                    alert('Login Gagal !');
                }

           }
        });
        
        
    }
    /*function getIds(users){
        var tokenID="afafafadfafs";
        
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "http://localhost/api_smart/mobile/get_token/"+tokenID+"/"+users , false);
        xhr.send();
    }*/
    function get_panel(){
        $('.fanelkiri').empty();
        $('.fanelkiri').append('<li data-filtertext="beranda" ><a onclick ="reload_index();"href="#index" class="ui-btn ui-shadow ui-corner-all ui-btn-icon-right ui-icon-home" data-transition="flow">Beranda</a></li><li data-filtertext="surat masuk" ><a href="suratmasuk.html" class="ui-btn ui-shadow ui-corner-all ui-btn-icon-right ui-icon-mail" data-transition="slide" onclick="delete_session();">Surat Masuk</a></li><li data-filtertext="surat keluar" ><a href="suratkeluar.html" class="ui-btn ui-shadow ui-corner-all ui-btn-icon-right ui-icon-mail" data-transition="slide" onclick="delete_session();">Surat Keluar</a></li><li data-filtertext="paraf"><a href="validasiparaf.html" class="ui-btn ui-shadow ui-corner-all ui-btn-icon-right ui-icon-edit" data-transition="slide" onclick="delete_session();">Paraf</a></li><li data-filtertext="validasi"><a href="validasi.html" class="ui-btn ui-shadow ui-corner-all ui-btn-icon-right ui-icon-check" data-transition="slide" onclick="delete_session();">Validasi</a></li><li data-filtertext="pedoman"><a href="pedoman.html" data-transition="slide" class="ui-btn ui-shadow ui-corner-all ui-btn-icon-right ui-icon-info" onclick="delete_session();">Pedoman</a></li><li data-filtertext="contact us"><a href="#" data-transition="slide" class="ui-btn ui-shadow ui-corner-all ui-btn-icon-right ui-icon-phone" >Contact Us</a></li><li data-filtertext="validasi"><a href="#" onclick="logout()" class="ui-btn ui-shadow ui-corner-all ui-btn-icon-right ui-icon-power">Log Out</a></li>');
    }
    function get_navbar(){
        avatar();
        $('.navbar').empty();
        $('.navbar').append('<h1 class="ui-title" role="heading" aria-level="1">Surat Keluar</h1><a href="#leftpanel3" data-icon="bars" data-iconpos="notext" class="ui-link ui-btn-left ui-btn ui-icon-bars ui-btn-icon-notext ui-shadow ui-corner-all" data-role="button" role="button">Menu</a><a href="#index" data-rel="back" data-icon="carat-l" data-iconpos="notext" class="ui-link ui-btn-right ui-btn ui-icon-carat-l ui-btn-icon-notext ui-shadow ui-corner-all" data-role="button" role="button">Back</a>');
    }
    
    function avatar(){
        var fullname =localStorage.getItem("fullname");
        var nik =localStorage.getItem("nik");
        var hsl = $(".avatar").html("<i align=center><font color=white><strong>Hai, "+fullname+"...<br><small><font color=white>( "+nik+" )</font></small></strong></font></i>");
        return hsl;
    }
    function riwayat(kode){
        $.ajax({
            url:geturl()+'mobile/get_riwayat_perubahan/'+kode,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                $(".loading").hide();
                $(".table-responsive").hide();
                $(".positionSelector").show();
                $(".positionSelector").empty();
                $(".positionSelector").html(data);

           }
        });
        /*$.post(geturl()+'mobile/get_riwayat_perubahan/'+kode, function(data, textStatus, xhr) {
            $(".table-responsive").hide();
            $(".positionSelector").show();
            $(".positionSelector").empty();
            $(".positionSelector").html(data);
            
        });*/
    }
    function close_riwayat(){
        $(".table-responsive").show();
        $(".positionSelector").hide();
    }
    function setuju_paraf(kode){
        var userstorage =localStorage.getItem("namauser");
       var r = confirm("Menyetujui paraf, anda yakin ?");
    if (r == true) {
        $.ajax({
            url:geturl()+"mobile/setuju_paraf/"+kode+"/"+userstorage,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                if(data==1){
                    $(".loading").hide();
                    alert('Data Sukses Disetujui !');
                    var table = $('#validasi-paraf').DataTable();
                    table.ajax.reload(null, false);
                    var userstorage =localStorage.getItem("namauser");
                    get_page_index(userstorage);
                }else{
                    $(".loading").hide();
                    alert('validasi paraf gagal !');
                }

           }
        });
    }
    }
    function tolak_paraf(kode){
         var userstorage =localStorage.getItem("namauser");
         var r = confirm("Menolak paraf, anda yakin ?");
    if (r == true) {
        $.ajax({
            url:geturl()+"mobile/tolak_paraf/"+kode+"/"+userstorage,
            method: 'POST',
            beforeSend: function() {
            $(".loading").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+" : "+thrownError);
                $(".loading").hide();
              },
            success: function(data) {
                if(data==1){
                    $(".loading").hide();
                    alert('Data Sukses Ditolak !');
                    var table = $('#validasi-paraf').DataTable();
                    table.ajax.reload(null, false);
                    var userstorage =localStorage.getItem("namauser");
                    get_page_index(userstorage);
                }else{
                    $(".loading").hide();
                    alert('validasi paraf gagal !');
                }

           }
        });
    }
    }
    function delete_session(){
        var kode =$('#kodesuratkeluar').val();
        var userstorage =localStorage.getItem("namauser");
        $.post(geturl()+'mobile/delete_session_paraf/'+kode+'/'+userstorage,function(data, textStatus, xhr){console.log(data);});
    }
    function logout()
    {
    
    localStorage.removeItem("namauser"); 
    navigator.app.exitApp();
    
    }