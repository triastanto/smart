function read_menu(){
     $('#tblmenu').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "<?php echo base_url('administrasi/menu/read'); ?>",
                "type": "post"
            },
            "columns": [{
                "data": "displaytext"
            }, {
                "data": "linkaddress"
            }, {
                "data": "icon"
            }, {
                "data": "stat"
            }, {
                "className": 'options',
                "data": "node",
                "render": function(data, type, full, meta){
                    //return "<button type='button' id='edit' title='Edit' class='btn btn-circle btn-outline green-jungle' value='primary' onclick='javascript:edit(\""+data+"\");'><i class='fa fa-edit'></i></button>";
                      return "<a class='btn btn-warning btn-icon btn-circle btn-xs' onclick='javascript:ubah();'><i class='fa fa-edit'></i></a><a class='btn btn-danger  btn-icon btn-circle btn-xs' onclick='javascript:hapus();'><i class='fa fa-times'></i></a>";
                }
            }]
        });
}