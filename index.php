<?php
$tgl=getdate();
$timezone="Asia/Jakarta";
date_default_timezone_set($timezone);
date('Y-m-d H:i:s', time());

session_start();
include_once('config.php');
include_once('includes/functions.php');
include_once('includes/db.php');
//include_once('includes/ps_pagination.php');
include_once('includes/ajaxpagination.php');
// if(date('Y-m-d')<='2019-09-01'){
	// echo'<script language="JavaScript">alert("Maaf aplikasi SMART dapat di input mulai tanggal 02 Februari 2019"); document.location="https://sso.krakatausteel.com/"</script>';
// }else{
	ini_set('register_globals','off' );
	ob_start("ui_output_callback");
	out('site_title',$appName);
	global $database, $auth;
	$conn = new db($hostname, $username , $password, $dbname);
	
	// $_SESSION['user']=10112;
	// $_SESSION['group_id'] = 4;
	// $_SESSION['empposid'] = 50121903;
	// $_SESSION['nik'] = 10112;
	
	out('template', $template);
	if (!isset($_SESSION['user'])) {
		$tahunsekarang = date('Y');
		$bulansekarang = date('M');
		$mulai = $tahunsekarang - 5;
		$start = $tahunsekarang + 5;
		$thn .='
		<select name="txtbln">
			<option value="01">Review Interim</option>
			<option value="02">Akhir Tahun</option>
		</select>
		<select name="ttahun">';
		for($i=$mulai; $i<=$start; $i++){
			$i==$tahunsekarang ? $sel = ' selected ' : $sel = ' ';
			$thn .='<option value="'.$i.'" '.$sel.'>'.$i.'</option>';
		}
		$thn .='</select>';
		
		if (PHP_OS == "WINNT" || PHP_OS == "WIN32") {
			$ui_template = dirname(__FILE__) . "\\templates\\" . $template . "\login.html";
		} else {
			$ui_template = dirname(__FILE__) . "/templates/" . $template . "/login.html";
		}
		out('thn', $thn);
	}else{
		if (PHP_OS == "WINNT" || PHP_OS == "WIN32") {
			$ui_template = dirname(__FILE__) . "\\templates\\" . $template . "\index.html";
		} else {
			$ui_template = dirname(__FILE__) . "/templates/" . $template . "/index.html";
		}
	}

	if (isset($_SESSION['user'])) {
		$log='	<ul class="inline-ul floatleft">
					<li>&nbsp;Hello <i class="ion-android-contact"></i> <b>'.strtoupper($_SESSION['user']).'</b></li>
					<li>&nbsp;<a href="logout.php"><font color="#ffffff"><i class="ion-android-lock"></i>&nbsp;<b>Logout</b></a></font></li>
				</ul>';
		out('login', $log);
		
		include_once('menus.php');
	}
	if (isset($_GET['inv'])){
		$pesan ='
			<div class="msg warning">
				<span>'.base64_decode($_GET['inv']).'</span>
				<a href="#" class="close">x</a>
			</div>';
		out('pesan', $pesan);
		
	 echo $pesan;
	}

	if ($_REQUEST['mod']) {
		if (file_exists('modules/' . base64_decode($_REQUEST['mod']) . '/' . base64_decode($_REQUEST['op']) . '.php')) {
			include_once('modules/' . base64_decode($_REQUEST['mod']) . '/' . base64_decode($_REQUEST['op']) . '.php');
		} else {
			echo '<div class="warning" align="center"> <h2>Error !!</h2> <img src="images/error.gif" /> '._MODULE_NOT_EXIST.'</div>';
		}
	} else {
		redirect('index.php?mod=' . base64_encode('default').'&op='.base64_encode('default'));
		
	}
// }

?>
<script>
window.lastActivity = Date.now();
document.addEventListener('click', function(){
    window.lastActivity = Date.now();
})
var TIMEOUT = 15000000 //10 mins
var activityChecker = setInterval(check, 3000);

function check() {
    var currentTime = Date.now();
    if (currentTime - window.lastActivity > TIMEOUT) {
        window.location="https://sso.krakatausteel.com";
		
    }
}
</script>
