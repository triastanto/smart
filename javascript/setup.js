﻿function setSidebarHeight(){
	setTimeout(function(){
var height = $(document).height();
    $('.grid_12').each(function () {
        height -= $(this).outerHeight();
    });
    height -= $('#site_info').outerHeight();
	//height -= 1;
	//salert(height);
    $('.sidemenu').css('height', height);
	$('#isidata').css('height', height-40);
	}
	,100);
}

//setup DatePicker
function setDatePicker(containerElement) {
    var datePicker = $('#' + containerElement);
    datePicker.datepicker({
        showOn: "button",
        buttonImage: "img/calendar.gif",
        buttonImageOnly: true
    });
}

function setupAccordion(containerElement) {
    $("#" + containerElement).accordion();
}

//setup left menu

function setupLeftMenu() {
    $("#section-menu").accordion({
		//"header": "a.menuitem"
		header: 'a.menuitem',										// h3 header tags
		navigation: true,
		active: '.selected',
		autoHeight: false,
		clearStyle: true,
		collapsibe: true,									// allow to close completely
		alwaysOpen: true,
		animated: 'slide',
		change: function(event,ui) {							// function on change
			var hid = ui.newHeader.attr('id');		// get id of the open menu
			//alert(hid);
			if (hid === undefined) {							// if the menu was closed, then hid = undefined
				$.cookie('menustate', null);					// delete cookie
			} else {
				$.cookie('menustate', hid, { expires: 2 });		// set cookie = id
			}
		}
	});
		/*
        .bind("accordionchangestart", function (e, data) {
            data.newHeader.next().andSelf().addClass("current");
            data.oldHeader.next().andSelf().removeClass("current");
        })
        .find("a.menuitem:first").addClass("current")
        .next().addClass("current");
		*/
		
	//alert($.cookie('menustate'));
	
	if($.cookie('menustate')) {
		//$('#section-menu').accordion('option', 'animated', false);							// turn off animation
		$('#section-menu').accordion('activate', $('#' + $.cookie('menustate')))//$('#' + $.cookie('menustate')).parent('a.menuitem'));		// open menu from cookie
		//$('#section-menu').accordion('option', 'animated', 'slide');							// turn animation back on
	}
	else {
		$('#section-menu').accordion('activate', 1);		// open default
	}
	
	$('#section-menu .submenu').css('height','auto');
}
