<?php
include_once('../config.php');
include_once('includes/koneksi.php');
function login(){
	session_start();
	if (!session_is_registered('user')) {
        if (!eregi('login.php', $_SERVER['SCRIPT_NAME'])) {
        	echo '<script language="Javascript">window.location="login.php"</script>';
        }
    }
     return true;
}


function ss($index = 1){
    $ss = array(1 => 'Biasa', 2 => 'Penting', 3 => 'Rahasia');
    return $ss[$index];
}

function sifat($index = 1){
    $ss = array(1 => 'Asli', 2 => 'Tembusan', 3 => 'Struktural', 4 => 'Functional');
    return $ss[$index];
}

function js($index = 1){
    $ss = array(1 => 'Internal Selevel/ Lebih Rendah', 2 => 'Eksternal', 3 => 'Internal Ke Pejabat Lebih Tinggi');
    return $ss[$index];
}


function _convertDate($date){
    if (empty($date))
        return null;

    $date = explode("-", $date);
    return
    $date[2] . '-' . $date[1] . '-' . $date[0];
}

function tanggalok($date){
    if (empty($date))
        return null;

    $date = explode("-", $date);
    return
    $date[2] . ' ' . namabulan($date[1]) . ' ' . $date[0];
}

function converttanggal($date)
{
    if (empty($date))
        return null;

    $date = explode("-", $date);
    return
//    $date[1] . '/' . $date[2] . '/' . $date[0];
    $date[1] . '-' . $date[2] . '-' . $date[0];
}

function using_ie(){
    $u_agent = $_SERVER['HTTP_USER_AGENT'];
    $ub = False;
    if(preg_match('/MSIE/i',$u_agent))
    {
        $ub = True;
    }
   
    return $ub;
}

function _convertTglSAP($tgl){
		$tg=explode('-',$tgl);
		return $tg[2].'.'.$tg[1].'.'.$tg[0];
}

function _convertTgl($tgl){
		$tg=explode('-',$tgl);
		return $tg[2].'/'.$tg[1].'/'.$tg[0];
}

function rowClass($i = 0){
    $i % 2 == 1 ? $clash = "#EEEEEE" : $clash = "#FFFFFF";

    return $clash;
}

function bulan($index = 1)
{
    $bulan = array(1 => 'Januari', 2 => 'Februari', 3 => 'Maret', 4 => 'April', 5 => 'Mei', 6 => 'Juni', 7 => 'Juli', 8 => 'Agustus', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Desember');
    return $bulan[$index];
}

function namabulan($index = 1){
    $bulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
    return $bulan[$index];
}


function tanggal($tgl){
	$tg=explode('-',$tgl);
	return $tg[2].' '.namabulan($tg[1]).' '.$tg[0];
}


function romawi($index = 1){
    $bulan = array('01' => 'I', '02' => 'II', '03' => 'III', '04' => 'IV', '05' => 'V', '06' => 'VI', '07' => 'VII', '08' => 'VIII', '09' => 'IX', '10' => 'X', '11' => 'XI', '12' => 'XII');
    return $bulan[$index];
}

function hari($index = 1)
{
    $hari = array(0 => 'Minggu', 1 => 'Senin', 2 => 'Selasa', 3 => 'Rabu', 4 => 'Kamis', 5 => 'Jum\'at', 6 => 'Sabtu', 7 => 'Minggu');
    return $hari[$index];
}

function fget_contents($file)
{
    $fd = fopen($file, "r");
    $content = "";
    while (!feof($fd)) {
        $content .= fgets($fd, 4096);
    }
    fclose($fd);
    return $content;
}

function ui_output_callback($buffer)
{
    global $ui_template, $ui_vars, $auth, $public, $amp;

    session_cache_limiter('private');
    session_cache_expire(30);

    session_start();
    $script_name = explode('/', $_SERVER["SCRIPT_NAME"]);

    $template = fget_contents($ui_template);
    $ui_vars["content"] .= ui_build($buffer, $ui_vars);
    $html = ui_build($template, $ui_vars);
    return $html;
}

/**
 * buat ngeganti {} dengan ui_var
 *
 * @param string $template template yang pengen dirubah-rubah ...
 * @param array $vars variable global ui_vars
 * @return string template yang deh diganti {} nya ... :p
 * @author HQM
 */
function ui_build($template, $vars)
{
    if (preg_match_all('/\{([\w]+)\}/', $template, $matches) > 0) {
        $patterns = array();
        $replacements = array();
        for ($i = 0; $i < count($matches[0]); $i++) {
            $var = $matches[1][$i];
            $patterns[] = '/\{' . $var . '\}/';
            $replacements[] = $vars[$var];
        }
        return preg_replace($patterns, $replacements, $template);
    }
    return $template;
}

/**
 * simple, buat ngasih nilah ke ui_vars .. itu aja bener deh gak boohong .. suweerr sampe doweerr
 *
 * @param string $var key di $ui_var
 * @param string $content nilai yang mo dikasih getho
 * @author HQM
 */
function out($var, $content)
{
    global $ui_vars;
    $ui_vars[$var] .= $content;
}

/**
 * redirect ke halaman lain, tapi ob na dibersiin dulu biar memori na juga terbebas dari segala prasangka buruk .. :D
 *
 * @param string $page halaman yang mo dituju
 * @author HQM
 */
function redirect($page){
    ob_end_clean();
    header("Location: $page");
    exit;
}

function ubah_huruf_awal($pemisah, $paragrap) {
	//pisahkan $paragraf berdasarkan $pemisah dengan fungsi explode
	$pisahkalimat=explode($pemisah, $paragrap);
	$kalimatbaru = array();

	//looping dalam array
	foreach ($pisahkalimat as $kalimat) {
		//jadikan awal huruf masing2 array menjadi huruf besar dengan fungsi ucwords
		$kalimatawalhurufbesar=ucwords(strtolower($kalimat));
		$kalimatbaru[] = $kalimatawalhurufbesar;
	}

	//kalo udah gabungin lagi dengan fungsi implode
	$textgood = implode($pemisah, $kalimatbaru);
	return $textgood;
}


function romaw($index = 1){
	$romaw = array('01' => 'I', '02' => 'II', '03' => 'III', '04' => 'IV', '05' => 'V', '06' => 'VI', '07' => 'VII', '08' => 'VIII', '09' => 'IX', '10' => 'X', '11' => 'XI', '12' => 'XII');
	return $romaw[$index];
}
function log_activity($log_time,$log_user,$log_aksi,$log_url,$log_item,$kode,$log_ip,$log_device){
	$insert ="insert into log_transaksi (log_time,log_user,log_aksi,log_url,log_item,kode,log_ip,log_device) values('$log_time','$log_user','$log_aksi','$log_url','$log_item','$kode','$log_ip','$log_device')";
	mysql_query($insert);
	
}
function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

?>