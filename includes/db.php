<?php

/**
 * db.php
 *
 * @version 0.1
 * @copyright 2005
 * @author HQM
 */

/**
 * DB
 */
class db {
    public $dbConnection;

    /**
     * hasil query dikasi kemari $q = mysql_query();
     */
    public $dbQuery;
    public $rowResult;
    public $hostname;
    public $username;
    public $password;
    public $dbname;

    /**
     *
     * @access public
     * @return query result
     */
    public function doQuery($sql = '')
    {
        if ($_REQUEST['debug'] == 1) {
            echo $sql . '<br />';
        }

        if ($this->dbQuery = @mysql_query($sql)) {
            if (strtoupper(substr($sql, 0, 6)) != 'SELECT') {
                $sql = mysql_escape_string($sql);
                $query = mysql_query('INSERT INTO logs (username, d, query, machineaddress) VALUES("' . base64_decode($_SESSION['userID']) . '",NOW(), \'' . $sql . '\', "' . $_SERVER['REMOTE_ADDR'] . '")');
            }
            return $this->dbQuery;
        } else {
            echo '<i>' . $sql . '</i><br />';
            echo mysql_error();
            return 0;
        }
    }

    /**
     * Constructor
     *
     * @access protected
     */
    function db($hostname = '', $username = '', $password = '', $dbname = '')
    {
        $this->connection($hostname, $username, $password, $dbname);
    }

    private function connection($hostname = '', $username = '', $password = '', $dbname = '')
    {
        if ($this->dbConnection = mysql_connect($hostname, $username, $password)) {
            if ($selectDB = @mysql_select_db($dbname, $this->dbConnection)) {
                $this->hostname = $hostname;
                $this->username = $username;
                $this->password = $password;
                $this->dbname = $dbname;
                return 1;
            } else {
                return 0;
            }
        } else {
            echo mysql_error();
            return 0;
        }
    }

    public function __sleep()
    {
        mysql_close($this->dbConnection);
    }

    public function __wakeup()
    {
        $this->connection($this->hostname, $this->username, $this->password, $this->dbname);
    }
}

?>